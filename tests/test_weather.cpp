#include <stdint.h>
#include <gtest/gtest.h>
#include "../src/internet_weather/internet_weather.h"
#include "mock/log.h"
#include "mock/settings.h"
#include "mock/ledmatrix_font.h"
#include "mock/esp8266_core/ESP8266HTTPClient.h"

uint32_t device_uptime = 0;
bool HTTP_CLI_MOCK_begin_return = true;
String HTTP_CLI_MOCK_getString_return = "";
int HTTP_CLI_MOCK_http_response_code = HTTP_CODE_OK;
String HTTP_CLI_MOCK_called_url = "";
uint8_t SETT_MOCK_read_byte_s_return = 0u;
uint8_t* SETT_MOCK_read_block_return = NULL;
uint8_t SETT_MOCK_get_setting_length_return = 0u;

float convert_celsius_to_fahrenheit(float input) {
	return input * 1.8f + 32.0f;
}

Logger fLog;
Settings fSettings;
JsonParser fJsonParser(fLog);
Webweather fWebWeather(fLog, fSettings, fJsonParser);

class TestWeatherSubscriber : public WeatherSubscriber {
public:
    TestWeatherSubscriber() {}
    void weather_change_callback(const int16_t temperature, const uint8_t state_icon, bool validity) {
        this->temperature = temperature;
        this->state_icon = state_icon;
        this->validity = validity;
    }

    int16_t temperature = 0;
    uint8_t state_icon = 0;
    bool validity = false;
};

void configure_response_json(int tempertature, String state_text, String icon) {
    String json_response_template = R"=====({"coord":{"lon":19.0399,"lat":47.498},"weather":[{"id":800,"main":"%MAIN%","description":"clear sky","icon":"%ICON%"}],"base":"stations","main":{"temp":%TEMPERATURE%,"feels_like":295.75,"temp_min":283.00,"temp_max":293.0,"pressure":1013,"humidity":36},"visibility":10000,"wind":{"speed":2.24,"deg":45,"gust":2.24},"clouds":{"all":0},"dt":1623680446,"sys":{"type":2,"id":2009313,"country":"HU","sunrise":1623638768,"sunset":1623696135},"timezone":7200,"id":3054643,"name":"Budapest","cod":200})=====";
    json_response_template.replace("%TEMPERATURE%", (String)(tempertature+273));
    json_response_template.replace("%MAIN%", state_text);
    json_response_template.replace("%ICON%", icon);
    HTTP_CLI_MOCK_getString_return = json_response_template;
}

TEST(WeatherTest, TestSync) {
    /* Check initial state */
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), false);
    ASSERT_EQ(fWebWeather.get_enabled(), false);

    /* Configure the JSON and do a sync */
    configure_response_json(42, "Meteor showers", "03");
    fWebWeather.enable();
    ASSERT_EQ(fWebWeather.get_enabled(), true);
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    /* Sync with different data */
    configure_response_json(-100, "Ice rain", "04");
    fWebWeather.disable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_enabled(), false);
    ASSERT_EQ(fWebWeather.get_data_available(), false);
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), -100);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Ice rain");

    /* Check all data from JSON */
    ASSERT_EQ(fWebWeather.get_temp_feels_like(), 22);
    ASSERT_EQ(fWebWeather.get_state(), "04");
    ASSERT_EQ(fWebWeather.get_wind_speed(), 2);
    ASSERT_EQ(fWebWeather.get_pressure(), 1013);
    ASSERT_EQ(fWebWeather.get_humidity(), 36);
}

TEST(WeatherTest, TestSyncFailed) {
    configure_response_json(42, "Meteor showers", "03");
    fWebWeather.disable();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    configure_response_json(-100, "Ice rain", "04");
    HTTP_CLI_MOCK_http_response_code = 400;
    device_uptime += WEATHER_SYNC_FREQUENCY_MINUTES*60;
    fWebWeather.task();
    fWebWeather.task();
    /* As the sync fails, data should be the same as in the previous sync */
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");
}

TEST(WeatherTest, TestDataInvalidation) {
    HTTP_CLI_MOCK_http_response_code = HTTP_CODE_OK;
    configure_response_json(42, "Meteor showers", "03");
    fWebWeather.disable();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    /* Connection fails */
    HTTP_CLI_MOCK_begin_return = false;
    device_uptime += 60;
    fWebWeather.task();
    fWebWeather.task();
    HTTP_CLI_MOCK_begin_return = true;

    /* GET fails */
    HTTP_CLI_MOCK_http_response_code = -1;
    device_uptime += 60;
    fWebWeather.task();
    fWebWeather.task();

    /* HTTP response code is not 200 */
    HTTP_CLI_MOCK_http_response_code = 400;
    for(int i = 0; i < 3; i++) {
        device_uptime += 60;
        fWebWeather.task();
        fWebWeather.task();
    }
    /* After 5 unsuccessful syncs the data should be marked as not available */
    ASSERT_EQ(fWebWeather.get_data_available(), false);
    HTTP_CLI_MOCK_http_response_code = HTTP_CODE_OK;
}

TEST(WeatherTest, TestIncorrectResponse) {
    configure_response_json(42, "Meteor showers", "03");
    fWebWeather.disable();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    /* Empty response */
    HTTP_CLI_MOCK_getString_return = "";
    device_uptime += WEATHER_SYNC_FREQUENCY_MINUTES*60;
    fWebWeather.task();
    fWebWeather.task();
    /* As the sync fails, data should be the same as in the previous sync */
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    /* Incomplete response */
    HTTP_CLI_MOCK_getString_return = R"=====(
        {"temp":10,"feels_like":295.75,"temp_min":283.00,"temp_max":293.0,"pressure":1013,"humidity":36}
    )=====";
    device_uptime += WEATHER_SYNC_FREQUENCY_MINUTES*60;
    fWebWeather.task();
    fWebWeather.task();
    /* As the sync fails, data should be the same as in the previous sync */
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    /* Incomplete response 2: return of the unsuccessful sync */
    /* 'country' key is malformed */
    HTTP_CLI_MOCK_getString_return = R"=====(
        {"coord":{"lon":19.0399,"lat":47.498},"weather":[{"id":800,"main":"lol","description":"clear sky","icon":"42r"}],"base":"stations","main":{"temp":420,"feels_like":295.75,"temp_min":283.00,"temp_max":293.0,"pressure":1013,"humidity":36},"visibility":10000,"wind":{"speed":2.24,"deg":45,"gust":2.24},"clouds":{"all":0},"dt":1623680446,"sys":{"type":2,"id":2009313,"country_bad":"HU","sunrise":1623638768,"sunset":1623696135},"timezone":7200,"id":3054643,"name":"Budapest","cod":200}"
    )=====";
    device_uptime += WEATHER_SYNC_FREQUENCY_MINUTES*60;
    fWebWeather.task();
    fWebWeather.task();
    /* As the sync fails, data should be the same as in the previous sync */
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");
}

TEST(WeatherTest, TestSubscription) {
    TestWeatherSubscriber test_sub;
    TestWeatherSubscriber* sub_ptr = &test_sub;
    fWebWeather.subscribe(sub_ptr);

    HTTP_CLI_MOCK_http_response_code = HTTP_CODE_OK;
    configure_response_json(42, "Meteor showers", "03");
    fWebWeather.disable();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");

    ASSERT_EQ(test_sub.temperature, 42);
    ASSERT_EQ(test_sub.state_icon, fWebWeather.get_weather_icon());
    ASSERT_EQ(test_sub.validity, true);
    fWebWeather.unsubscribe(sub_ptr);
}

TEST(WeatherTest, TestSettings) {
    fWebWeather.disable();
    const char nvm_return[] = "NVM_VALID_DATA";
    SETT_MOCK_read_byte_s_return = true;
    SETT_MOCK_read_block_return = (uint8_t*)nvm_return;
    SETT_MOCK_get_setting_length_return = sizeof(nvm_return);
    fWebWeather.read_settings();
    fWebWeather.set_temperature_unit(TU_CELSIUS);

    ASSERT_EQ(fWebWeather.get_enabled(), true);
    configure_response_json(-100, "Ice rain", "04");
    HTTP_CLI_MOCK_http_response_code = HTTP_CODE_OK;
    fWebWeather.task();
    ASSERT_EQ(HTTP_CLI_MOCK_called_url, "http://api.openweathermap.org/data/2.5/weather?q=NVM_VALID_DATA&appid=NVM_VALID_DATA");
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), -100);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Ice rain");
}

TEST(WeatherTest, TestGetIcon) {
    HTTP_CLI_MOCK_http_response_code = HTTP_CODE_OK;
    configure_response_json(42, "Meteor showers", "03");
    fWebWeather.disable();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_data_available(), true);
    ASSERT_EQ(fWebWeather.get_temp(), 42);
    ASSERT_EQ(fWebWeather.get_state_as_text(), "Meteor showers");
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_LIGHT_CLOUD);

    configure_response_json(-100, "Sulphur Clouds", "04");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_HEAVY_CLOUD);

    configure_response_json(-100, "Thunder!", "11");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_THUNDERSTORM);

    configure_response_json(-100, "Mist", "50");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_MIST);

    configure_response_json(-100, "Clear", "01d");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_CLEAR);

    configure_response_json(-100, "Clear", "01n");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_CLEAR_NIGHT);

    configure_response_json(-100, "Showers", "09");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_SHOWERS);

    configure_response_json(-100, "Light rain", "10");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_LIGHT_RAIN);

    configure_response_json(-100, "Snow, Hey-Oh", "13");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_SNOW);

    configure_response_json(-100, "Not valid :(", "asd123");
    fWebWeather.disable();
    fWebWeather.task();
    fWebWeather.enable();
    fWebWeather.task();
    ASSERT_EQ(fWebWeather.get_weather_icon(), LEDMATRIX_ICON_EMPTY);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}
