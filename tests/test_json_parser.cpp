#include <gtest/gtest.h>
#include "mock/esp8266_core/WString.h"
#include "mock/log.h"
#include "../src/json_parser/json_parser.h"

Logger fLog;
JsonParser fJsonParser(fLog);

TEST(JsonParserTest, TestParseInvalidJson) {
    String json_data = "not JSON data";
    String value = fJsonParser.get_value_for_key(json_data, "main/temp");
    ASSERT_EQ(value, "");
}

TEST(JsonParserTest, TestParseSimpleValidJson) {
    String json_data = R"=====({"data": "very nice"})=====";
    String value = fJsonParser.get_value_for_key(json_data, "data");
    ASSERT_TRUE(value == "very nice");
}

TEST(JsonParserTest, TestParseSimpleValidJsonWithWhitespace) {
    String json_data = R"=====(
        {
            "data": "very nice"
        }
        )=====";
    String value = fJsonParser.get_value_for_key(json_data, "data");
    ASSERT_TRUE(value == "very nice");
}

TEST(JsonParserTest, TestParseSimpleValidJsonValue) {
    String json_data = R"=====(
        {
            "data": 420
        }
        )=====";
    String value = fJsonParser.get_value_for_key(json_data, "data");
    ASSERT_TRUE(value == "420");
}

TEST(JsonParserTest, TestParseBadKey) {
    String json_data = R"=====(
        {
            "data": 420
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "");
    ASSERT_TRUE(value == "");
    value = fJsonParser.get_value_for_key(json_data, "/");
    ASSERT_TRUE(value == "");
}

TEST(JsonParserTest, TestParseSubtree) {
    String json_data = R"=====(
        {
            "data": 420,
            "subtree": {
                "not_data": "text",
                "data": 69420
            }
        }
        )=====";
    String value = fJsonParser.get_value_for_key(json_data, "subtree/data");
    ASSERT_TRUE(value == "69420");
}

TEST(JsonParserTest, TestParseSubtreeInsideSubtree) {
    String json_data = R"=====(
        {
            "data": 420,
            "subtree": {
                "not_data": "text",
                "data": 69420,
                "subtree2": {
                    "data": 500
                }
            }
        }
        )=====";
    String value = fJsonParser.get_value_for_key(json_data, "subtree/subtree2/data");
    ASSERT_TRUE(value == "500");
}

TEST(JsonParserTest, TestParseSubtreeInsideSubtreeInsideSubtree) {
    String json_data = R"=====(
        {
            "data": 420,
            "subtree": {
                "not_data": "text",
                "data": 69420,
                "subtree2": {
                    "data": 500,
                    "subtree3": {
                        "text": "test",
                        "data": 42
                    }
                }
            }
        }
        )=====";
    String value = fJsonParser.get_value_for_key(json_data, "subtree/subtree2/subtree3/data");
    ASSERT_TRUE(value == "42");
}

TEST(JsonParserTest, TestParseMultiSubtreeInsideSubtreeInsideSubtree) {
    String json_data = R"=====(
        {
            "data": 420,
            "other_subtree": {
                "not_data": "text",
                "data": 69420,
                "subtree2": {
                    "data": 500,
                    "subtree3": {
                        "text": "test",
                        "data": 42
                    }
                }
            }
            "subtree": {
                "not_data": "text",
                "data": 69420,
                "subtree2": {
                    "data": 500,
                    "subtree3": {
                        "text": "test",
                        "data": 91
                    }
                }
            }
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "subtree/subtree2/subtree3/data");
    ASSERT_TRUE(value == "91");

    value = fJsonParser.get_value_for_key(json_data, "subtree/subtree2/subtree3/data2");
    ASSERT_TRUE(value == "");
}

TEST(JsonParserTest, TestParseArray) {
    String json_data = R"=====(
        {
            "data": 420,
            "array": [
                {
                    "not_data": "text",
                    "data": 69420
                }
            ]
        }
        )=====";
    String value = fJsonParser.get_value_for_key(json_data, "array/0/data");
    ASSERT_TRUE(value == "69420");
}

TEST(JsonParserTest, TestParseArrayWithMultipleElements) {
    String json_data = R"=====(
        {
            "data": 420,
            "array": [
                {
                    "not_data": "text",
                    "data": 69420
                },
                {
                    "not_data": "text",
                    "data": 42069
                }
            ]
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "array/1/data");
    ASSERT_TRUE(value == "42069");

    value = fJsonParser.get_value_for_key(json_data, "array/2/data");
    ASSERT_TRUE(value == "");
}

TEST(JsonParserTest, TestParseArrayInsideArray) {
    String json_data = R"=====(
        {
            "data": 420,
            "array": [
                {
                    "not_data": "text",
                    "data": 5555,
                },
                {
                    "not_data": "text",
                    "array2": [
                        {
                            "good_number": 69420,
                            "bad_number": 12345
                        }
                    ]
                }
            ]
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/0/good_number");
    ASSERT_TRUE(value == "69420");
}

TEST(JsonParserTest, TestParseArrayWithSingleValues) {
    String json_data = R"=====(
        {
            "data": 420,
            "array": [
                "high_data": 323,
                {
                    "not_data": "text",
                    "data": 69420
                },
                "medium_data": 520,
                {
                    "not_data": "text",
                    "data": 42069
                },
                "low_data": 999
            ]
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "array/0");
    ASSERT_TRUE(value == "323");

    value = fJsonParser.get_value_for_key(json_data, "array/2");
    ASSERT_TRUE(value == "520");

    value = fJsonParser.get_value_for_key(json_data, "array/4");
    ASSERT_TRUE(value == "999");
}

TEST(JsonParserTest, TestBadFomatting) {
    String json_data = R"=====(
        {
       "data":                                     420,
            "array":                        [
         "high_data":     323,
                                                                                    {
        "not_data":                                        "text",
                    "data":69420
},
                "medium_data":520,
                {"not_data":"text","data":42069},                        "low_data":             999
                                       ],            "data2": 
                                       "99999"
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "array/50");
    ASSERT_TRUE(value == "");

    value = fJsonParser.get_value_for_key(json_data, "array/0");
    ASSERT_TRUE(value == "323");

    value = fJsonParser.get_value_for_key(json_data, "array/1/not_data");
    ASSERT_TRUE(value == "text");

    value = fJsonParser.get_value_for_key(json_data, "array/1/data");
    ASSERT_TRUE(value == "69420");

    value = fJsonParser.get_value_for_key(json_data, "array/3/not_data");
    ASSERT_TRUE(value == "text");

    value = fJsonParser.get_value_for_key(json_data, "array/3/data");
    ASSERT_TRUE(value == "42069");

    value = fJsonParser.get_value_for_key(json_data, "array/2");
    ASSERT_TRUE(value == "520");

    value = fJsonParser.get_value_for_key(json_data, "array/4");
    ASSERT_TRUE(value == "999");

    value = fJsonParser.get_value_for_key(json_data, "data2");
    ASSERT_TRUE(value == "99999");
}

TEST(JsonParserTest, TestParseComplicatedJson) {
    String json_data = R"=====(
        {
            "data": 420,
            "array": [
                {
                    "not_data": "text",
                    "data": 5555,
                },
                {
                    "not_data": "text",
                    "array2": [
                        {
                            "good_number": 69420,
                            "bad_number": 12345
                        },
                        "something": 730,
                        {
                            "deep_number": 91,
                            "deep_element": {
                                "james_cameron": 888,
                                "deep_array": [
                                    {
                                        "so_deep": 9876
                                    },
                                    "such_deep": 9988,
                                    "mega_deep": "9900"
                                ],
                                "deeper_element": {
                                    "so_complex": "indeed",
                                    "very_complicate": "such JSON"
                                }
                            }
                        }
                    ]
                }
            ],
            "data2": "good data yo"
        }
        )=====";
    String value;
    value = fJsonParser.get_value_for_key(json_data, "data2");
    ASSERT_TRUE(value == "good data yo");

    value = fJsonParser.get_value_for_key(json_data, "array/0/data");
    ASSERT_TRUE(value == "5555");

    value = fJsonParser.get_value_for_key(json_data, "array/200");
    ASSERT_TRUE(value == "");

    value = fJsonParser.get_value_for_key(json_data, "array/nope");
    ASSERT_TRUE(value == "");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/0/good_number");
    ASSERT_TRUE(value == "69420");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/0/bad_number");
    ASSERT_TRUE(value == "12345");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/1");
    ASSERT_TRUE(value == "730");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_number");
    ASSERT_TRUE(value == "91");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_element/james_cameron");
    ASSERT_TRUE(value == "888");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_element/deep_array/0/so_deep");
    ASSERT_TRUE(value == "9876");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_element/deep_array/1");
    ASSERT_TRUE(value == "9988");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_element/deep_array/2");
    ASSERT_TRUE(value == "9900");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_element/deeper_element/so_complex");
    ASSERT_TRUE(value == "indeed");

    value = fJsonParser.get_value_for_key(json_data, "array/1/array2/2/deep_element/deeper_element/very_complicate");
    ASSERT_TRUE(value == "such JSON");
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
