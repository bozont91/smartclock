#ifndef SETTINGS_MOCK_H
#define SETTINGS_MOCK_H

#include <stdint.h>

extern uint8_t SETT_MOCK_read_byte_s_return;
extern uint8_t* SETT_MOCK_read_block_return;
extern uint8_t SETT_MOCK_get_setting_length_return;

class Settings {

public:

    Settings() {}

    void init() {}
    void initialize_with_default_values() {}
    bool get_valid() { return true; }
    void write(uint16_t settings_index, const uint8_t* data, bool immediate_write = false, uint16_t length = 0) {}
    void read_block(uint16_t settings_index, uint8_t* data) { memcpy(data, SETT_MOCK_read_block_return, SETT_MOCK_get_setting_length_return); }
    uint8_t read_byte_s(uint16_t settings_index) { return SETT_MOCK_read_byte_s_return; }
    uint16_t get_setting_offset(uint8_t settings_index) { return 0; }
    uint16_t get_setting_length(uint8_t settings_index) { return SETT_MOCK_get_setting_length_return; }
    void do_factory_reset() {}
    void task() {}

private:

};

enum settings_indexes {
    SETT_VALIDITY,
    SETT_SSID,
    SETT_WIFI_PASS,
    SETT_HOURLY_BEEP,
    SETT_CLOCK_STYLE,
    SETT_BRIGHTNESS,
    SETT_ALARM_ENABLED,
    SETT_ALARM_HOUR,
    SETT_ALARM_MINUTE,
    SETT_SCREENONOFF_ENABLED,
    SETT_SCREENON_HOUR,
    SETT_SCREENON_MINUTE,
    SETT_SCREENOFF_HOUR,
    SETT_SCREENOFF_MINUTE,
    SETT_TIMEZONE,
    SETT_DST_ENABLED,
    SETT_SCREENOFF_ONLY_ON_WEEKDAYS,
    SETT_MQTT_CLIENT_ENABLED,
    SETT_MQTT_BROKER_ADDRESS,
    SETT_MQTT_BROKER_PORT,
    SETT_TEMPERATURE_SENSOR_CALIBRATION,
    SETT_ADAFRUITIO_ENABLED,
    SETT_ADAFRUITIO_USERNAME,
    SETT_ADAFRUITIO_API_KEY,
    SETT_TEMPERATURE_UNIT,
    SETT_WEBWEATHER_ENABLED,
    SETT_OPENWEATHERMAP_API_KEY,
    SETT_OPENWEATHERMAP_LOCATION,
    SETT_ALARM_ONLY_ON_WEEKDAYS,
    SETT_MUTE,
    SETT_ALARM_SNOOZE_TIME,
    SETT_SNAKE_HISCORE,
    SETT_TETRIS_HISCORE,
    SETT_ALARM_SOUND,
    SETT_MQTT_BROKER_USERNAME,
    SETT_MQTT_BROKER_PASSWORD,
    SETT_OTA_ENABLED,
    SETT_OTA_USE_PASSWORD,
    SETT_OTA_PASSWORD,
    SETT_MAX
};

#endif /* SETTINGS_MOCK_H */
