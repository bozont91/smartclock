#ifndef LOGGER_MOCK_H
#define LOGGER_MOCK_H

#include <cstdio>
#include <cstdarg>
#include <stdint.h>

#define __FlashStringHelper char
#define F(string_literal) #string_literal

enum log_dest_e {
  LOG_TCPREMOTE,
  LOG_WEATHER,
  LOG_JSON
};

enum log_level_e {
    L_ERROR,
    L_INFO,
    L_DEBUG,
    L_VERBOSE
};

class Logger {

public:
    Logger() { ; }
    void log(uint8_t log_dest, uint8_t log_level, const __FlashStringHelper *fmt, ... ) {
        char message[4096];
        va_list args;
        va_start(args, fmt);
        vsnprintf(message, 4096u, fmt, args);
        va_end(args);
        printf("Log output: (%c) %s\n", get_loglevel_char(log_level), message);
    }

    char get_loglevel_char(uint8_t log_level) {
        if(log_level == L_INFO) return 'i';
        if(log_level == L_ERROR) return 'e';
        if(log_level == L_DEBUG) return 'd';
        if(log_level == L_VERBOSE) return 'v';
        return ' ';
    }

};

#endif /* LOGGER_MOCK_H */
