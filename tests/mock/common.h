#ifndef COMMON_MOCK_H
#define COMMON_MOCK_H

#include <stdint.h>

#define WEATHER_SYNC_FREQUENCY_MINUTES 1u
extern uint32_t device_uptime;

/* Temperature */
enum temperature_units {
    TU_CELSIUS,
    TU_FAHRENHEIT,
    TU_MAX
};

float convert_celsius_to_fahrenheit(float input);

#endif /* COMMON_MOCK_H */
