#ifndef ESP8266HTTPCLIENT_MOCK_H
#define ESP8266HTTPCLIENT_MOCK_H

#include "WString.h"
#define HTTP_CODE_OK 200

class WiFiClient {

};

extern bool HTTP_CLI_MOCK_begin_return;
extern String HTTP_CLI_MOCK_getString_return;
extern int HTTP_CLI_MOCK_http_response_code;
extern String HTTP_CLI_MOCK_called_url;

class HTTPClient {
public:
    bool begin(WiFiClient& client, String url) { HTTP_CLI_MOCK_called_url = url; return HTTP_CLI_MOCK_begin_return; }
    void end() {}
    int GET() { return HTTP_CLI_MOCK_http_response_code; }
    String errorToString(int errorCode) { return "error "+errorCode; }
    String getString() { return HTTP_CLI_MOCK_getString_return; }
private:

};


#endif /* ESP8266HTTPCLIENT_MOCK_H */
