#ifndef ARDUINO_MOCK_H
#define ARDUINO_MOCK_H

#include <stdint.h>

#define IRAM_ATTR

void yield();

#endif /* ARDUINO_MOCK_H */
