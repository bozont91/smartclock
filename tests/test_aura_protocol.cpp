/* g++ protocol_test.cpp ../../src/tcp_remote/aura_protocol.cpp -o prot -g -DPROTOCOL_TEST */
/* valgrind --leak-check=full ./prot */

#include <cstdio>
#include <cassert>
#include "../mock/log.h"
#include "../../src/tcp_remote/aura_protocol.h"

Logger fLog;

void print_repr_message(AuraProtocol::message *msg);
void print_raw_message(uint8_t *msg, uint8_t size);

int main() {
    AuraProtocol ap(fLog);

    AuraProtocol::message msg;
    msg.msg_id = AuraProtocol::DISPLAY_BRIGHTNESS;
    msg.msg_type = AuraProtocol::REQUEST;
    msg.payload_size = 1u;
    uint8_t payload = 99u;
    msg.payload = (uint8_t*)&payload;

    AuraProtocol::message *msg_rcv = NULL;
    uint8_t *current_raw_message;
    current_raw_message = ap.build(&msg);


    /* Normal case */
    printf("\n--- NORMAL CASE ---\n");
    print_raw_message(current_raw_message, msg.size);
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);

    assert(msg_rcv != NULL);
    assert(msg_rcv->header == msg.header);
    assert(msg_rcv->size == msg.size);
    assert(msg_rcv->msg_id == msg.msg_id);
    assert(msg_rcv->msg_type == msg.msg_type);
    assert(msg_rcv->payload_size == msg.payload_size);
    assert(msg_rcv->crc32 == msg.crc32);
    assert(*msg_rcv->payload == *msg.payload);

    ap.discard_msg(msg_rcv);
    free(current_raw_message);

    /* CRC error */
    printf("\n--- CRC ERROR CASE ---\n");
    current_raw_message = ap.build(&msg);
    current_raw_message[8] = 0xFF;
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);
    assert(msg_rcv == NULL);
    ap.discard_msg(msg_rcv);
    free(current_raw_message);

    /* Header error */
    printf("\n--- HEADER ERROR CASE ---\n");
    current_raw_message = ap.build(&msg);
    current_raw_message[0] = 0xAA;
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);
    assert(msg_rcv == NULL);
    ap.discard_msg(msg_rcv);
    free(current_raw_message);

    /* Message ID error */
    printf("\n--- MESSAGE ID ERROR CASE ---\n");
    msg.msg_id = AuraProtocol::MSG_ID_MAX;
    current_raw_message = ap.build(&msg);
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);
    assert(msg_rcv == NULL);
    ap.discard_msg(msg_rcv);
    free(current_raw_message);
    msg.msg_id = AuraProtocol::DISPLAY_BRIGHTNESS;

    /* Message type error */
    printf("\n--- MESSAGE TYPE ERROR CASE ---\n");
    msg.msg_type = AuraProtocol::MSG_TYPE_MIN;
    current_raw_message = ap.build(&msg);
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);
    assert(msg_rcv == NULL);
    ap.discard_msg(msg_rcv);
    free(current_raw_message);
    msg.msg_type = AuraProtocol::REQUEST;

    /* Empty payload case */
    printf("\n--- EMPTY PAYLOAD CASE ---\n");
    msg.payload = NULL;
    msg.payload_size = 0;
    current_raw_message = ap.build(&msg);
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);

    assert(msg_rcv != NULL);
    assert(msg_rcv->header == msg.header);
    assert(msg_rcv->size == msg.size);
    assert(msg_rcv->msg_id == msg.msg_id);
    assert(msg_rcv->msg_type == msg.msg_type);
    assert(msg_rcv->payload_size == msg.payload_size);
    assert(msg_rcv->crc32 == msg.crc32);
    assert(msg_rcv->payload == NULL);

    ap.discard_msg(msg_rcv);
    free(current_raw_message);

    /* Text in payload */
    printf("\n--- TEXT IN PAYLOAD CASE ---\n");
    const char text[] = "Well, this is a good test!";
    msg.payload = (uint8_t*)&text;
    msg.payload_size = strlen(text) + 1;
    current_raw_message = ap.build(&msg);
    print_raw_message(current_raw_message, msg.size);
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);
    printf("Payload as text: '%s'\n", msg_rcv->payload);

    assert(msg_rcv != NULL);
    assert(msg_rcv->header == msg.header);
    assert(msg_rcv->size == msg.size);
    assert(msg_rcv->msg_id == msg.msg_id);
    assert(msg_rcv->msg_type == msg.msg_type);
    assert(msg_rcv->payload_size == msg.payload_size);
    assert(msg_rcv->crc32 == msg.crc32);
    assert(strcmp((char*)msg_rcv->payload, (char*)msg.payload) == 0);

    ap.discard_msg(msg_rcv);
    free(current_raw_message);

    /* Long text in payload */
    printf("\n--- LONG TEXT IN PAYLOAD CASE ---\n");
    const char text2[] = "Well, this is a good test and this payload is going to be a bit large. Stellar nucleosynthesis is the creation (nucleosynthesis) of chemical elements by nuclear fusion reactions within stars.";
    msg.payload = (uint8_t*)&text2;
    msg.payload_size = strlen(text2) + 1;
    current_raw_message = ap.build(&msg);
    print_raw_message(current_raw_message, msg.size);
    msg_rcv = ap.parse(current_raw_message);
    print_repr_message(msg_rcv);
    printf("Payload as text: '%s'\n", msg_rcv->payload);

    assert(msg_rcv != NULL);
    assert(msg_rcv->header == msg.header);
    assert(msg_rcv->size == msg.size);
    assert(msg_rcv->msg_id == msg.msg_id);
    assert(msg_rcv->msg_type == msg.msg_type);
    assert(msg_rcv->payload_size == msg.payload_size);
    assert(msg_rcv->crc32 == msg.crc32);
    assert(strcmp((char*)msg_rcv->payload, (char*)msg.payload) == 0);

    ap.discard_msg(msg_rcv);
    free(current_raw_message);

    printf("\nAll passed! Everything seems to be okay :)\n");

    return 0;
}

void print_raw_message(uint8_t *msg, uint8_t size) {
    printf("Raw message:\n");
    for(int i = 0 ; i < size ; i++) {
        printf("0x%02x ", msg[i]);
    }
    printf("\n");
}

void print_repr_message(AuraProtocol::message *msg) {
    if(!msg) {
        printf("NULL message!\n");
    } else {
        printf("Message size: 0x%02x (%d)\n", msg->size, msg->size);
        printf("Message ID: 0x%02x\n", msg->msg_id);
        printf("Message type: 0x%02x\n", msg->msg_type);
        printf("Payload size: 0x%02x (%d)\n", msg->payload_size, msg->payload_size);

        printf("Payload: ");
        for(int i = 0 ; i < msg->payload_size ; i++) {
            printf("0x%02x ", msg->payload[i]);
        }
        printf("\n");

        printf("CRC32: 0x%02x\n", msg->crc32);

    }
}
