#include <gtest/gtest.h>
#include "../src/deviceclock/deviceclock.h"

DeviceClock fDeviceClock;


TEST(DeviceClockTest, TestChange) {
    fDeviceClock.change(4200u);
    ASSERT_EQ(fDeviceClock.get(), 4200u);
}

TEST(DeviceClockTest, TestIncrement) {
    uint32_t current_value = fDeviceClock.get();
    fDeviceClock.increment();
    ASSERT_EQ(fDeviceClock.get(), current_value + 1u);
}

TEST(DeviceClockTest, TestAdd) {
    uint32_t current_value = fDeviceClock.get();
    fDeviceClock.add(69u);
    ASSERT_EQ(fDeviceClock.get(), current_value + 69u);
}

TEST(DeviceClockTest, TestSubtract) {
    fDeviceClock.set(5u);
    fDeviceClock.subtract(1u);
    ASSERT_EQ(fDeviceClock.get(), 4u);
}

TEST(DeviceClockTest, TestSet) {
    fDeviceClock.set(1200u);
    ASSERT_EQ(fDeviceClock.get(), 1200u);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
