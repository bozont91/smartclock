aura SmartClock
===============

*aura* is an ESP8266 based WiFi smart clock

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    Hardware <hardware>
    Building the software <softwarebuild>
    First time setup <firsttimesetup>
    Troubleshooting <troubleshooting>
    FAQ <faq>
