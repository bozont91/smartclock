Hardware Setup
--------------

For the official hardware schematics see the *pcb_aura* folder in the project tree.

The aura SmartClock contains the following main hardware parts:
 - ESP8266 D1 Mini WiFi microcontroller board
 - MAX7219 LED matrix display
 - LM75A temperature sensor
 - EC11 rotary encoder with button
 - Piezo buzzer

A feature full breadboard version can be assembled like this:

.. image:: img/hw_sch.png
    :align: center
    :width: 600px
