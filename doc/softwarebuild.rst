Building the software
**********************

After you obtained a copy of the software you have two choices on building it.
It can be built and flashed using the *Arduino IDE* or using the provided *makefiles*.

Obtaining the source code
=========================

The git repository can be found at the following URL:

.. list-table::
   :widths: auto
   :align: center

   * - **https://gitlab.com/bozont91/smartclock**

Grab a copy with your favourite git client.

Building with the Arduino IDE
=============================

Prerequisties
-------------
 - Arduino IDE installed
 - ESP8266 Arduino Core installed
    - https://github.com/esp8266/Arduino#installing-with-boards-manager
 - The following Arduino libraries installed:
    - PubSubClient (https://github.com/knolleary/pubsubclient)
    - TickerScheduler (https://github.com/bozont/TickerScheduler)

Configuration
-------------
Select your board from the *Tools > Board* menu in the Arduino IDE.
It's typicall the *LOLIN(WEMOS) D1 R2 & mini* or the *LOLIN(WEMOS) D1 mini (clone)* but the software is capable of running on any ESP8266 board with at least 1MB of flash.
Set the *CPU Frequency* to *160 MHz*. The other options can be left at their defaults.

Building and flashing
---------------------

Connect your device to your computer and set the port in *Tools > Port*. Press the *upload* arrow. The software will be compiled and uploaded to your hardware.


Building with makefiles
=======================

soon
