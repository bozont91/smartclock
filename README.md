# aura SmartClock

https://aura-smartclock.readthedocs.io/

## Features:
 - 32x8 LED display with animations
 - NTP clock synchronization
 - 12/24 hour clock display
 - Date display
 - Automatic DST handling
 - Timezone handling
 - Alarm
 - Countdown timer
 - Weather forecast (OpenWeatherMap)
 - Temperature sensor
 - Automatic display on/off
 - Display brightness control
 - Persistent settings via NVM
 - Custom text display
 - Snake 🐍
 - Tetris 🧱
 - Android client
 - Webserver
 - Menu - controllable by a rotary encoder
 - MQTT client
 - AdafruitIO intregration (IFTTT, Google Home, Alexa)
 - Welcome Wizard for WiFi setup on first use
 - Musical tone playback via piezo buzzer
 - Debug logging via the serial line
 - Debug logging via TCP
 - Detailed debug info via the Android client

## Planned features:
 - See current issues

## Hardware:
 - ESP8266 D1 Mini
 - MAX7219 LED display
 - LM75A temperature sensor
 - EC11 rotary encoder
 - Piezo buzzer
 - Custom PCB design available

## Required libraries
The project depends on the following libraries:
 - Arduino ESP8266 Core (https://github.com/esp8266/Arduino)
 - TickerScheduler (https://github.com/bozont/TickerScheduler)
 - PubSubClient (https://github.com/knolleary/pubsubclient)

## Building the software:
To build the software the following are needed:
 - [arduino-cli](https://github.com/arduino/arduino-cli) - with all the libraries listed above installed
 - make

Then type (to flash the board after build if connected):

    make all

 or (for compilation only):

    make build


The project can also be built via the [Arduino IDE](https://www.arduino.cc/en/Main/software) by opening the .ino file.
