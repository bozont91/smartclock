#ifndef ESP_HAL_H
#define ESP_HAL_H

#include <Arduino.h>
#include "../log/log.h"

class EspHal {

public:
    EspHal(Logger &log);
    void system_reboot();
    uint32_t get_true_random_number();

private:
    Logger &fLog;

};


#endif /* ESP_HAL_H */
