#include "esp_hal.h"

EspHal::EspHal(Logger &log) :
fLog(log)
{
    ;
}

void EspHal::system_reboot() {
    fLog.log(LOG_SYSTEM, L_INFO, F("System reboot"));
    ESP.restart();
}

uint32_t EspHal::get_true_random_number() {
    return *(volatile uint32_t *)0x3FF20E44;
}
