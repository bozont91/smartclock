#ifndef LEDMATRIX_GFX_H
#define LEDMATRIX_GFX_H

#include <Arduino.h>
#include "../ledmatrix_hal/ledmatrix_hal.h"

class LedmatrixGFX {

public:

    enum ledmatrix_transition_modes {
        LEDMATRIX_DO_TRANSITION,
        LEDMATRIX_NO_TRANSITION
    };

    enum ledmatrix_transition_types {
        TR_LEFT_TO_RIGHT,
        TR_RIGHT_TO_LEFT,
        TR_TOP_TO_BOTTOM,
        TR_BOTTOM_TO_TOP,
    };

    LedmatrixGFX(LedmatrixHal &ledmatrixhal);
    void clear_framebuffer();
    void clear_framebuffer(uint8_t x_offset, uint8_t y_offset);
    void setxy_framebuffer(uint8_t x, uint8_t y, bool val);
    void setxy_framebuffer(uint8_t x, uint8_t y, uint8_t val);
    uint8_t getxy_framebuffer(uint8_t x, uint8_t y);
    void fill_column_framebuffer(uint8_t x);
    void clear_column_framebuffer(uint8_t x);
    void render_framebuffer_to_screen();
    void do_transition(uint8_t start_offset = 0u, uint8_t req_transition_type = TR_LEFT_TO_RIGHT);
    bool is_transitioning();
    void end_transition();
    bool render_framebuffer_transition_to_screen();
    void draw_num(uint8_t x_offset, uint8_t y_offset, uint8_t number);
    void draw_single_line_of_num(uint8_t x_offset, uint8_t y_offset, uint8_t number, uint8_t line);
    void draw_small_num(uint8_t x_offset, uint8_t y_offset, uint8_t number);
    void setup_text_scroll(String text);
    void render_text_to_framebuffer(String text, uint8_t x_offset, uint8_t y_offset);
    bool scroll_text();
    void show_text(String text);
    void draw_shutdown();

    const uint8_t LEDMATRIX_WIDTH = MAX7219_LEDMATRIX_COLS;
    const uint8_t LEDMATRIX_HEIGHT = MAX7219_LEDMATRIX_ROWS;


private:

    uint8_t ledmatrix_framebuffer[MAX7219_LEDMATRIX_COLS][MAX7219_LEDMATRIX_ROWS];

    #define GFX_TEXTTOSHOW_BUFFER_SIZE 256u
    const uint8_t* text_to_show_data[GFX_TEXTTOSHOW_BUFFER_SIZE];

    void inline render_pixel_to_frambuffer(uint8_t x, uint8_t y, bool val);
    void shift_frambuffer_left();
    uint8_t get_character_width(char c);
    uint8_t draw_letter(uint8_t x_offset, uint8_t y_offset, const uint8_t* character_data);
    void print_framebuffer();
    bool ledmatrix_is_transitioning = false;

    int32_t txtscroll_fall_off_character_position = LEDMATRIX_WIDTH+1;
    int32_t txtscroll_last_character_end_position = LEDMATRIX_WIDTH+1;
    uint32_t txtscroll_string_iter = 0u;

    uint8_t transition_current_step = 0u;
    uint8_t transition_type = TR_LEFT_TO_RIGHT;
    inline bool render_transition_left_to_right();
    inline bool render_transition_right_to_left();
    inline bool render_transition_top_to_bottom();
    inline bool render_transition_bottom_to_top();

    LedmatrixHal &fLedmatrixHal;

};

#endif /* LEDMATRIX_GFX_H */
