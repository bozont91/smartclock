#include "ledmatrix_gfx.h"
#include "../ledmatrix_font/ledmatrix_font.h"

LedmatrixGFX::LedmatrixGFX(LedmatrixHal &ledmatrixhal) :
fLedmatrixHal(ledmatrixhal)
{
    clear_framebuffer();
    memset(text_to_show_data, 0u, sizeof(text_to_show_data));
}

void inline LedmatrixGFX::render_pixel_to_frambuffer(uint8_t x, uint8_t y, bool val) {
    if(x >= LEDMATRIX_WIDTH || y >= LEDMATRIX_HEIGHT) return;
    ledmatrix_framebuffer[x][y] = val;
}

void LedmatrixGFX::clear_framebuffer() {
    memset(ledmatrix_framebuffer, false, LEDMATRIX_WIDTH * LEDMATRIX_HEIGHT);
}

void LedmatrixGFX::clear_framebuffer(uint8_t x_offset, uint8_t y_offset) {
    for(uint8_t x = x_offset ; x < LEDMATRIX_WIDTH ; x++) {
        for(uint8_t y = y_offset ; y < LEDMATRIX_HEIGHT ; y++) {
            ledmatrix_framebuffer[x][y] = false;
        }
    }
}

void LedmatrixGFX::setxy_framebuffer(uint8_t x, uint8_t y, bool val) {
    if(x >= LEDMATRIX_WIDTH || y >= LEDMATRIX_HEIGHT) return;
    ledmatrix_framebuffer[x][y] = val;
}

void LedmatrixGFX::setxy_framebuffer(uint8_t x, uint8_t y, uint8_t val) {
    if(x >= LEDMATRIX_WIDTH || y >= LEDMATRIX_HEIGHT) return;
    ledmatrix_framebuffer[x][y] = val;
}

uint8_t LedmatrixGFX::getxy_framebuffer(uint8_t x, uint8_t y) {
    if(x >= LEDMATRIX_WIDTH || y >= LEDMATRIX_HEIGHT) return 0;
    return ledmatrix_framebuffer[x][y];
}

void LedmatrixGFX::fill_column_framebuffer(uint8_t x) {
    if(x >= LEDMATRIX_WIDTH) return;
    for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
        setxy_framebuffer(x, y, true);
    }
}

void LedmatrixGFX::clear_column_framebuffer(uint8_t x) {
    if(x >= LEDMATRIX_WIDTH) return;
    for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
        setxy_framebuffer(x, y, false);
    }
}

void LedmatrixGFX::render_framebuffer_to_screen() {
    for(uint8_t x = 0 ; x < LEDMATRIX_WIDTH ; x++) {
        for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
            fLedmatrixHal.set_pixel(x, y, ledmatrix_framebuffer[x][y]);
        }
    }
}

void LedmatrixGFX::do_transition(uint8_t start_offset, uint8_t req_transition_type) {
    ledmatrix_is_transitioning = true;
    transition_current_step = start_offset;
    transition_type = req_transition_type;
}

bool LedmatrixGFX::is_transitioning() {
    return ledmatrix_is_transitioning;
}

void LedmatrixGFX::end_transition() {
    ledmatrix_is_transitioning = false;
}

bool LedmatrixGFX::render_framebuffer_transition_to_screen() {

    switch(transition_type) {
        case TR_LEFT_TO_RIGHT:
            return render_transition_left_to_right();

        case TR_RIGHT_TO_LEFT:
            return render_transition_right_to_left();

        case TR_TOP_TO_BOTTOM:
            return render_transition_top_to_bottom();

        case TR_BOTTOM_TO_TOP:
            return render_transition_bottom_to_top();

        default:
            return render_transition_left_to_right();
    };
}

inline bool LedmatrixGFX::render_transition_left_to_right() {
    static const bool leading_bar = true;

    for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
        fLedmatrixHal.set_pixel(transition_current_step, y, ledmatrix_framebuffer[transition_current_step][y]);
        if(leading_bar) fLedmatrixHal.set_pixel(transition_current_step+1, y, false);
    }

    transition_current_step++;
    if(transition_current_step >= LEDMATRIX_WIDTH)
    {
        transition_current_step = 0;
        return true;
    }

    return false;
}

inline bool LedmatrixGFX::render_transition_right_to_left() {
    static const bool leading_bar = true;

    for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
        uint8_t current_x = LEDMATRIX_WIDTH-1-transition_current_step;
        fLedmatrixHal.set_pixel(current_x, y, ledmatrix_framebuffer[current_x][y]);
        if(leading_bar) fLedmatrixHal.set_pixel(current_x-1, y, false);
    }

    transition_current_step++;
    if(transition_current_step >= LEDMATRIX_WIDTH)
    {
        transition_current_step = 0;
        return true;
    }

    return false;
}

inline bool LedmatrixGFX::render_transition_top_to_bottom() {
    static const bool leading_bar = true;

    for(uint8_t x = 0 ; x < LEDMATRIX_WIDTH ; x++) {
        fLedmatrixHal.set_pixel(x, transition_current_step, ledmatrix_framebuffer[x][transition_current_step]);
        if(leading_bar) fLedmatrixHal.set_pixel(x, transition_current_step+1, false);
    }

    transition_current_step++;
    if(transition_current_step >= LEDMATRIX_HEIGHT)
    {
        transition_current_step = 0;
        return true;
    }

    return false;
}

inline bool LedmatrixGFX::render_transition_bottom_to_top() {
    static const bool leading_bar = true;

    for(uint8_t x = 0 ; x < LEDMATRIX_WIDTH ; x++) {
        uint8_t current_y = LEDMATRIX_HEIGHT-1-transition_current_step;
        fLedmatrixHal.set_pixel(x, current_y, ledmatrix_framebuffer[x][current_y]);
        if(leading_bar) fLedmatrixHal.set_pixel(x, current_y-1, false);
    }

    transition_current_step++;
    if(transition_current_step >= LEDMATRIX_HEIGHT)
    {
        transition_current_step = 0;
        return true;
    }

    return false;
}

void LedmatrixGFX::draw_num(uint8_t x_offset, uint8_t y_offset, uint8_t number) {

    if(number >= ledmatrix_number_font_len) return;

    for(uint8_t cols = 0u; cols < LEDMATRIX_CHAR_Y_LEN; cols++) {
        for(uint8_t rows = 0u; rows < LEDMATRIX_CHAR_X_LEN; rows++) {
            /* If the bit at the current position is set*/
            if(ledmatrix_number_font[number][cols] & (1 << (LEDMATRIX_CHAR_X_LEN-1-rows))) {
                setxy_framebuffer(x_offset+rows, y_offset+cols, true);
            }
        }
    }

}

void LedmatrixGFX::draw_small_num(uint8_t x_offset, uint8_t y_offset, uint8_t number) {

    if(number >= ledmatrix_small_number_font_len) return;

    for(uint8_t cols = 0u; cols < LEDMATRIX_SMALL_CHAR_Y_LEN; cols++) {
        for(uint8_t rows = 0u; rows < LEDMATRIX_SMALL_CHAR_X_LEN; rows++) {
            /* If the bit at the current position is set*/
            if(ledmatrix_small_number_font[number][cols] & (1 << (LEDMATRIX_SMALL_CHAR_X_LEN-1-rows))) {
                setxy_framebuffer(x_offset+rows, y_offset+cols, true);
            }
        }
    }

}

void LedmatrixGFX::draw_single_line_of_num(uint8_t x_offset, uint8_t y_offset, uint8_t number, uint8_t line) {

    for(uint8_t rows = 0u; rows < LEDMATRIX_CHAR_X_LEN; rows++) {

        /* If the bit at the current position is set*/
        if(ledmatrix_number_font[number][line] & (1 << (LEDMATRIX_CHAR_X_LEN-1-rows))) {
            setxy_framebuffer(x_offset+rows, y_offset+line, true);
        } else {
            setxy_framebuffer(x_offset+rows, y_offset+line, false);
        }

        /* Blank the next line */
        setxy_framebuffer(x_offset+rows, y_offset+line+1, false);
    }

}

uint8_t LedmatrixGFX::draw_letter(uint8_t x_offset, uint8_t y_offset, const uint8_t* character_data) {

    /* If there's nothing to draw, just return with the received offset */
    if(character_data == NULL) return x_offset;

    uint8_t charachter_width;
    memcpy_P(&charachter_width, &character_data[0], 1u);
    charachter_width++;

    uint8_t cols;
    uint8_t rows;
    uint8_t character_row;

    for(rows = 0u; rows < LEDMATRIX_CHAR_Y_LEN; rows++) {
        for(cols = 0u; cols < charachter_width; cols++) {

            /* If the it at the current position is set*/
            /* Offset cols by one, because length is stored at 0*/
            memcpy_P(&character_row, &character_data[rows+1], 1u);
            if(character_row & (1 << (7-cols))) {
                render_pixel_to_frambuffer(x_offset+cols, y_offset+rows, true);
            }
        }
    }

    /* Return the x position where the next character can safely be drawn */
    return x_offset+charachter_width;

}

void LedmatrixGFX::shift_frambuffer_left() {
    /* Shift every column to the left */
    for(uint8_t x = 0 ; x < LEDMATRIX_WIDTH-1 ; x++) {
        for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
            ledmatrix_framebuffer[x][y] = ledmatrix_framebuffer[x+1][y];
        }
    }

    /* Fill the last column with false values */
    for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
        ledmatrix_framebuffer[LEDMATRIX_WIDTH-1][y] = false;
    }
}

uint8_t LedmatrixGFX::get_character_width(char c) {
    return ledmatrix_ascii_table[(uint8_t)c][0];
}

void LedmatrixGFX::render_text_to_framebuffer(String text, uint8_t x_offset, uint8_t y_offset) {
    uint8_t character_offset = 0u;
    for(uint8_t i = 0 ; text[i] != '\0' ; i++) {

        /* Workaround - fix to have UTF8 here as well */
        const uint8_t* char_to_draw = ledmatrix_ascii_table[(uint8_t)text[i]];

        character_offset = draw_letter(x_offset+character_offset, y_offset, char_to_draw);
        x_offset = 0;
        if(character_offset >= LEDMATRIX_WIDTH) {
            break;
        }
    }
}

void LedmatrixGFX::setup_text_scroll(String text) {

    /* Initialize the fall off position off the screen, so the scrolling starts from off the screen on the right */
    /* Also add a ten pixel divide to the screen width to have some spacer from anything thats on screen */
    txtscroll_fall_off_character_position = LEDMATRIX_WIDTH+10;
    txtscroll_last_character_end_position = txtscroll_fall_off_character_position;
    txtscroll_string_iter = 0u;

    convert_utf8_string_to_character_data(text_to_show_data, text);
}

bool LedmatrixGFX::scroll_text() {

    /* If we do not clear the framebuffer on start, it makes for a cool transition */
    /*
    if(string_iter == 0)  {
    ledmatrix_clear_framebuffer();
    }
    */

    /* Render what's visible on screen */
    /* Render the next character where it has fallen off screen */
    int32_t x_render_offset = txtscroll_fall_off_character_position;
    int32_t character_offset;

    for( ; text_to_show_data[txtscroll_string_iter] != NULL ; txtscroll_string_iter++) {

        /* Draw the current character */
        character_offset = draw_letter(x_render_offset, 0, text_to_show_data[txtscroll_string_iter]);
        txtscroll_last_character_end_position = character_offset;

        /*If the rendered character falls down from the screen on the right end */
        if(character_offset >= LEDMATRIX_WIDTH) {
            /* Remember the starting position of the character that fell off */
            txtscroll_fall_off_character_position = x_render_offset;
            /* Do no render anything else */
            break;
        } else {
            /* The character fitted on the screen, set the render offset to its end */
            x_render_offset = character_offset;
        }

    }

    /* Check if we have finished displaying the text */
    /* If we have ran out of characters to render and we shifted all the text to the left end */
    if(text_to_show_data[txtscroll_string_iter] == NULL && txtscroll_last_character_end_position < 1) {
        txtscroll_fall_off_character_position = LEDMATRIX_WIDTH+1;
        txtscroll_string_iter = 0u;
        return false;
    }

    render_framebuffer_to_screen();

    /* Move the display to the left */
    shift_frambuffer_left();
    /* Move the falling off character to the left by one */
    /* The next iteration will render it one position to the left */
    txtscroll_fall_off_character_position--;
    txtscroll_last_character_end_position--;

    return true;

}

void LedmatrixGFX::show_text(String text) {
    clear_framebuffer();
    render_text_to_framebuffer(text, 0, 0);
    do_transition();
}

void LedmatrixGFX::print_framebuffer() {
    for(uint8_t y = 0 ; y < LEDMATRIX_HEIGHT ; y++) {
        for(uint8_t x = 0 ; x < LEDMATRIX_WIDTH ; x++) {
            Serial.print(ledmatrix_framebuffer[x][y]);
            Serial.print(" ");
        }
        Serial.println("");
    }
}

/*
void ledmatrix_printf(int x, int y, char *fmt, ... ){
    char tmp[128]; // resulting string limited to 128 chars
    va_list args;
    va_start (args, fmt );
    vsnprintf(tmp, 128, fmt, args);
    va_end (args);
}
*/

void LedmatrixGFX::draw_shutdown() {
    clear_framebuffer();
    do_transition(0, TR_RIGHT_TO_LEFT);
}
