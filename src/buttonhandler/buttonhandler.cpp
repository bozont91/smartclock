#include "buttonhandler.h"

ButtonHandler::ButtonHandler(
    Logger &log,
    Beep &beep,
    Settings &settings,
    AlarmManager &alarmmanager,
    LedmatrixHal &ledmatrixhal,
    LedmatrixStateManager &ledmatrixmain,
    LedmatrixScreens &ledmatrixscreens,
    LedmatrixCountdownTimer &ledmatrixcountdowntimer,
    LedmatrixMenu &ledmatrixmenu,
    LedmatrixTetris &tetris,
    TemperatureManager &temperaturemanager,
    Webweather &webweather,
    ClockManager &clockmanager
) :
fLog(log),
fBeep(beep),
fSettings(settings),
fAlarmManager(alarmmanager),
fLedmatrixHal(ledmatrixhal),
fLedmatrixMain(ledmatrixmain),
fLedmatrixScreens(ledmatrixscreens),
fLedmatrixCountdownTimer(ledmatrixcountdowntimer),
fLedmatrixMenu(ledmatrixmenu),
fTetris(tetris),
fTemperatureManager(temperaturemanager),
fWebweather(webweather),
fClockManager(clockmanager)
{
    ;
}

void ButtonHandler::read_settings() {
    set_shortpress_action(fSettings.read_byte_s(SETT_BUTTON_SHORTPRESS_ACTION));
    set_longpress_action(fSettings.read_byte_s(SETT_BUTTON_LONGPRESS_ACTION));
}

void ButtonHandler::set_shortpress_action(uint8_t action) {
    if(action >= BTN_ACT_MAX) {
        shortpress_action = BTN_ACT_MUSIC;
        return;
    }
    shortpress_action = action;
    fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Setting shortpress action to %d"), shortpress_action);
}

uint8_t ButtonHandler::get_shortpress_action() {
    return shortpress_action;
}

void ButtonHandler::set_longpress_action(uint8_t action) {
    if(action >= BTN_ACT_MAX) {
        longpress_action = BTN_ACT_SCREEN_ON_OFF;
        return;
    }
    longpress_action = action;
    fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Setting longpress action to %d"), longpress_action);
}

uint8_t ButtonHandler::get_longpress_action() {
    return longpress_action;
}

void ButtonHandler::task() {

    if(!button_pressed) return;

    if(digitalRead(PIN_BUTTON) == LOW) {
        if(immediate_action_performed) return;

        longpress_cnt++;
        if(longpress_cnt == BTN_LONGPRESS_TIME) fBeep.play_tone(MUSICAL_A4, DURATION_SIXTEENTH);
        if(longpress_cnt == BTN_REALLYLONGPRESS_TIME) fBeep.play_tone(MUSICAL_G4, DURATION_SIXTEENTH);
        if(longpress_cnt == BTN_FACTORYRESET_TIME) fBeep.play_tone(MUSICAL_A4, DURATION_EIGHTH);

        /* Process immediate actions (done without releasing the button) */
        if(handle_immediate_actions()) immediate_action_performed = true;

        return;
    }

    button_pressed = false;

    if(immediate_action_performed) {
        immediate_action_performed = false;
        longpress_cnt = 0u;
        return;
    }

    if(longpress_cnt >= BTN_FACTORYRESET_TIME) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Factory reset"));
        fSettings.do_factory_reset();
        return;
    }

    /* Only the factory reset is allowed until the device starts up */
    if(!button_enabled) {
        longpress_cnt = 0u;
        return;
    }

    if(longpress_cnt >= BTN_REALLYLONGPRESS_TIME) {
        handle_really_long_press();
    } else if(longpress_cnt >= BTN_LONGPRESS_TIME) {
        handle_long_press();
    } else {
        handle_short_press();
    }

    longpress_cnt = 0u;

}

void ButtonHandler::set_enabled(bool val) {
    button_enabled = val;
}

bool ButtonHandler::handle_immediate_actions() {
    /* Switch off the alarm when it's ringing */
    if(fAlarmManager.get_is_alarm_in_progress() && longpress_cnt >= BTN_LONGPRESS_TIME) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Switching off alarm"));
        fAlarmManager.dismiss_alarm();
        fBeep.play_negative_acknowledge();
        fLedmatrixScreens.clock_force_update();
        return true;
    }

    return false;
}

void ButtonHandler::handle_really_long_press() {
    /* Save game and exit Tetris */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Quit tetris"));
        fTetris.save_game();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        return;
    }

    /* Exit the menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_MENU) {
        fLedmatrixMenu.deactivate();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Exit menu"));
    }
}

void ButtonHandler::handle_long_press() {

    /* Disable the alarm if we're currently in the alarm setting menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SETALARM) {
        fAlarmManager.disable();
        uint8_t alarm_enabled = false;

        if(fSettings.read_byte_s(SETT_ALARM_ENABLED) == true) {
            fSettings.write(SETT_ALARM_ENABLED, &alarm_enabled);
        }

        fLedmatrixScreens.clock_force_update();
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Disabling alarm"));
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        return;
    }

    /* Do a quickfall in Tetris */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Tetris quickfall"));
        fTetris.do_quickfall();
        return;
    }

    /* Handle long presses in the menu */
    if(fLedmatrixMenu.get_active()) {
        switch(fLedmatrixMenu.get_current_menu()) {

            /* Print detailed weather info */
            case fLedmatrixMenu.MENU_TEMPERATURE:
                {
                    if(!fWebweather.get_enabled()) return;
                    show_detailed_weather();
                    fLedmatrixMenu.deactivate();
                    return;
                }
                break;

            /* Print detailed date */
            case fLedmatrixMenu.MENU_DATE:
                {
                    show_detailed_date();
                    fLedmatrixMenu.deactivate();
                }
                break;
        }
        return;
    }

    execute_configured_action(BTN_PRESS_LONG);

}

void ButtonHandler::handle_short_press() {

    /* Start snoozing when the alarm is ringing */
    if(fAlarmManager.get_is_alarm_in_progress()) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Starting snooze"));
        fAlarmManager.snooze_start();
        return;
    }

    /* Dismiss the snooze timer if it's running */
    if(fAlarmManager.is_snoozing()) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Dismissing snooze timer"));
        fAlarmManager.snooze_cancel();
        fBeep.play_negative_acknowledge();
        fLedmatrixScreens.clock_force_update();
        return;
    }

    /* Handle when an item is selected in the menu */
    if(fLedmatrixMenu.get_active()) {
        fBeep.beep_short();
        switch(fLedmatrixMenu.get_current_menu()) {
            case fLedmatrixMenu.MENU_TEMPERATURE:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Temperature"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_TEMPERATURE);
                break;

            case fLedmatrixMenu.MENU_DATE:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Date"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_YEAR);
                break;

            case fLedmatrixMenu.MENU_SNAKE:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Snake"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_SNAKE);
                break;

            case fLedmatrixMenu.MENU_TETRIS:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Tetris"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_TETRIS);
                break;

            case fLedmatrixMenu.MENU_EXIT:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Exit"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
                break;

            case fLedmatrixMenu.MENU_INFO:
                {
                    fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Info"));
                    char* info = (char*)malloc(200);
                    snprintf_P(info, 200, PSTR("aura SmartClock | SW: %s %s (%s) | Uptime: %d s | IP: %s"), SOFTWARE_VERSION, SOFTWARE_RELEASE_TYPE, SOFTWARE_COMMIT_ID, device_uptime, WiFi.localIP().toString().c_str());
                    fLedmatrixMain.show_text(info);
                    free(info);
                }
                break;

            case fLedmatrixMenu.MENU_BRIGHTNESS:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Brightness setting"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_BRIGHTNESSADJUST);
                break;

            case fLedmatrixMenu.MENU_ALARM:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Alarm"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_SETALARM);
                break;

            case fLedmatrixMenu.MENU_MUTE:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Mute setting"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_SETMUTE);
                break;

            case fLedmatrixMenu.MENU_CLOCK_STYLE:
                fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Menu selection: Clock type and style"));
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCKTYPE_SELECT);
                break;

            default:
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
                break;
        }

        fLedmatrixMenu.deactivate();

        return;
    }

    /* Cancel the countdown timer */
    if(fLedmatrixCountdownTimer.state == fLedmatrixCountdownTimer.COUNTDOWNTIMER_RUN) {
        fLedmatrixCountdownTimer.cancel();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        fBeep.beep_short();
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Cancelling countdown while running"));
        return;
    }

    /* Switch off the countdown timer alarm when it's finished */
    if(fLedmatrixCountdownTimer.state == fLedmatrixCountdownTimer.COUNTDOWNTIMER_FINISHED) {
        fLedmatrixCountdownTimer.cancel();
        fBeep.disable_alarm();
        fBeep.stop_music();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Switching off countdown finished alarm"));
        return;
    }

    /* Turn on the screen if it's off */
    if(!fLedmatrixMain.get_display_on()) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Screen on"));
        fLedmatrixMain.display_on();
        fBeep.beep_short();
        return;
    }

    /* Save brightness adjusted from the menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_BRIGHTNESSADJUST) {
        fLedmatrixScreens.handle_brightness_setting_selection();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        return;
    }

    /* Handle alarm setting adjusting from the menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SETALARM) {
        if(fLedmatrixScreens.handle_alarm_setting_selection()) {
            fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        }
        return;
    }

    /* Handle mute setting adjusting from the menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SETMUTE) {
        fLedmatrixScreens.handle_mute_setting_selection();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        return;
    }

    /* Handle the AM/PM selection from the menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_CLOCKTYPE_SELECT) {
        fLedmatrixScreens.handle_clocktype_selection();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCKSTYLE_SELECT);
        return;
    }

    /* Handle the clock style selection from the menu */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_CLOCKSTYLE_SELECT) {
        fLedmatrixScreens.handle_clockstyle_selection();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_CLOCK);
        return;
    }

    /* Rotate block in Tetris */
    if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Tetris rotate"));
        fTetris.request_game_input(fTetris.TETRIS_ROTATE);
        return;
    }

    execute_configured_action(BTN_PRESS_SHORT);

}

void ButtonHandler::execute_configured_action(uint8_t btn_press_type) {
    uint8_t action_to_perform = BTN_ACT_NOTHING;
    bool beep_needed = false;

    if(btn_press_type == BTN_PRESS_SHORT) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Custom shortpress action"));
        action_to_perform = shortpress_action;
        if(action_to_perform != BTN_ACT_MUSIC) beep_needed = true;
    } else if(btn_press_type == BTN_PRESS_LONG) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Custom longpress action"));
        action_to_perform = longpress_action;
        beep_needed = false;
    }

    switch(action_to_perform) {
        case BTN_ACT_NOTHING:
            return;

        case BTN_ACT_MUSIC:
            play_music();
            break;

        case BTN_ACT_WEATHER:
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_TEMPERATURE);
            break;

        case BTN_ACT_DETAILED_WEATHER:
            if(!fWebweather.get_enabled()) return;
            show_detailed_weather();
            break;

        case BTN_ACT_DATE:
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_YEAR);
            break;

        case BTN_ACT_DETAILED_DATE:
            show_detailed_date();
            break;

        case BTN_ACT_SCREEN_ON_OFF:
            display_off();
            break;

        case BTN_ACT_ALARM_CONFIGURE:
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_SETALARM);
            break;

        case BTN_ACT_SNAKE:
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_SNAKE);
            break;

        case BTN_ACT_TETRIS:
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_TETRIS);
            break;

        default:
            break;

    }

    if(beep_needed) fBeep.beep_short();
}

void ButtonHandler::display_off() {
    if(fLedmatrixMain.get_display_on()) {
        fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Screen off"));
        fLedmatrixMenu.deactivate();
        fLedmatrixMain.display_off();
    }
}

void ButtonHandler::show_detailed_date() {
    fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Show detailed date"));
    char* detailed_date = (char*)malloc(100);
    fLedmatrixScreens.get_detailed_date_string(detailed_date, 100);
    fLedmatrixMain.show_text(detailed_date);
    free(detailed_date);
}

void ButtonHandler::show_detailed_weather() {
    fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Detailed weather"));

    if(!fWebweather.get_data_available()) {
        fLedmatrixMain.show_text("Online weather data currently not available");
        return;
    }

    char* detailed_weather = (char*)malloc(200);
    fLedmatrixScreens.get_detailed_weather_string(detailed_weather, 200);
    fLedmatrixMain.show_text(detailed_weather);
    free(detailed_weather);
}

void ButtonHandler::play_music() {
    fLog.log(LOG_BUTTONHANDLER, L_INFO, F("Play music"));

    static uint8_t music_selector = 0u;

    switch(music_selector) {
        case 0:
            fBeep.start_music(furelise, MUSIC_BLOCK_FUR_ELISE_SIZE);
            music_selector++;
            break;

        case 1:
            fBeep.start_music(jesus_of_suburbia, MUSIC_BLOCK_JESUS_OF_SUBURBIA_SIZE);
            music_selector++;
            break;

        case 2:
            fBeep.start_music(stacys_mom, MUSIC_BLOCK_STACYS_MOM_SIZE);
            music_selector++;
            break;

        case 3:
            fBeep.start_music(ill_fly, MUSIC_BLOCK_ILL_FLY_SIZE);
            music_selector++;
            break;

        case 4:
            fBeep.start_music(everyday, MUSIC_BLOCK_EVERYDAY_SIZE);
            music_selector = 0u;
            break;
    }
}
