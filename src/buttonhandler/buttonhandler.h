#ifndef BUTTONHANDLER_h
#define BUTTONHANDLER_h

#include <Arduino.h>
#include "../common.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../settings/settings.h"
#include "../alarm/alarm.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../ledmatrix_screens/ledmatrix_screens.h"
#include "../ledmatrix_countdown_timer/ledmatrix_countdown_timer.h"
#include "../ledmatrix_menu/ledmatrix_menu.h"
#include "../ledmatrix_tetris/ledmatrix_tetris.h"
#include "../temperature/temperature.h"
#include "../internet_weather/internet_weather.h"
#include "../clockmanager/clockmanager.h"


class ButtonHandler {

public:
    ButtonHandler(
        Logger &log,
        Beep &beep,
        Settings &settings,
        AlarmManager &alarmmanager,
        LedmatrixHal &ledmatrixhal,
        LedmatrixStateManager &ledmatrixmain,
        LedmatrixScreens &ledmatrixscreens,
        LedmatrixCountdownTimer &ledmatrixcountdowntimer,
        LedmatrixMenu &ledmatrixmenu,
        LedmatrixTetris &tetris,
        TemperatureManager &temperaturemanager,
        Webweather &webweather,
        ClockManager &clockmanager
    );

    enum button_actions_e {
        BTN_ACT_NOTHING,
        BTN_ACT_MUSIC,
        BTN_ACT_WEATHER,
        BTN_ACT_DETAILED_WEATHER,
        BTN_ACT_DATE,
        BTN_ACT_DETAILED_DATE,
        BTN_ACT_SCREEN_ON_OFF,
        BTN_ACT_ALARM_CONFIGURE,
        BTN_ACT_SNAKE,
        BTN_ACT_TETRIS,
        BTN_ACT_MAX
    };

    enum button_press_type_e {
        BTN_PRESS_SHORT,
        BTN_PRESS_LONG
    };

    void task();
    void set_enabled(bool val);
    void read_settings();
    void set_shortpress_action(uint8_t action);
    uint8_t get_shortpress_action();
    void set_longpress_action(uint8_t action);
    uint8_t get_longpress_action();
    volatile bool button_pressed = false;

private:
    bool button_enabled = false;
    uint32_t longpress_cnt = 0u;
    bool immediate_action_performed = false;

    uint8_t shortpress_action = BTN_ACT_MUSIC;
    uint8_t longpress_action = BTN_ACT_SCREEN_ON_OFF;

    bool handle_immediate_actions();
    void handle_short_press();
    void handle_long_press();
    void handle_really_long_press();

    void play_music();
    void display_off();
    void show_detailed_date();
    void show_detailed_weather();
    void execute_configured_action(uint8_t btn_press_type);

    /* This will result in a ~500ms long press */
    static const uint8_t BTN_LONGPRESS_TIME = 5u;

    /* This will result in a ~1500ms really long press */
    static const uint8_t BTN_REALLYLONGPRESS_TIME = 15u;

    /* About 15 seconds to perform a factory reset */
    static const uint8_t BTN_FACTORYRESET_TIME = 150u;

    Logger &fLog;
    Beep &fBeep;
    Settings &fSettings;
    AlarmManager &fAlarmManager;
    LedmatrixHal &fLedmatrixHal;
    LedmatrixStateManager &fLedmatrixMain;
    LedmatrixScreens &fLedmatrixScreens;
    LedmatrixCountdownTimer &fLedmatrixCountdownTimer;
    LedmatrixMenu &fLedmatrixMenu;
    LedmatrixTetris &fTetris;
    TemperatureManager &fTemperatureManager;
    Webweather &fWebweather;
    ClockManager &fClockManager;

};

#endif /* BUTTONHANDLER_h */
