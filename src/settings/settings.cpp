#include "settings.h"
#include "../alarm/alarm.h"
#include "../ledmatrix_screens/ledmatrix_screens.h"
#include "../temperature/temperature.h"
#include "../beep/beep.h"
#include "../buttonhandler/buttonhandler.h"

const Settings::settings_element Settings::settings_map[SETT_MAX] = {
    /* offs     len     initialize  default byte                            sensitive   */
    {0u,        1u,     true,       SETTINGS_VALID_BYTE,                    false},     /* SETT_VALIDITY */
    {1u,        65u,    false,      0u,                                     true},      /* SETT_SSID */
    {66u,       65u,    false,      0u,                                     true},      /* SETT_WIFI_PASS */
    {131u,      1u,     true,       1u,                                     false},     /* SETT_HOURLY_BEEP */
    {132u,      1u,     true,       LedmatrixScreens::CLOCK_STYLE_BIG,      false},     /* SETT_CLOCK_STYLE */
    {133u,      1u,     true,       1u,                                     false},     /* SETT_BRIGHTNESS */
    {134u,      1u,     true,       0u,                                     false},     /* SETT_ALARM_ENABLED */
    {135u,      1u,     true,       0u,                                     false},     /* SETT_ALARM_HOUR */
    {136u,      1u,     true,       0u,                                     false},     /* SETT_ALARM_MINUTE */
    {137u,      1u,     true,       0u,                                     false},     /* SETT_SCREENONOFF_ENABLED */
    {138u,      1u,     true,       8u,                                     false},     /* SETT_SCREENON_HOUR */
    {139u,      1u,     true,       0u,                                     false},     /* SETT_SCREENON_MINUTE */
    {140u,      1u,     true,       1u,                                     false},     /* SETT_SCREENOFF_HOUR */
    {141u,      1u,     true,       0u,                                     false},     /* SETT_SCREENOFF_MINUTE */
    {142u,      1u,     true,       0u,                                     false},     /* SETT_TIMEZONE */
    {143u,      1u,     true,       1u,                                     false},     /* SETT_DST_ENABLED */
    {144u,      1u,     true,       0u,                                     false},     /* SETT_SCREENOFF_ONLY_ON_WEEKDAYS */
    {145u,      1u,     true,       0u,                                     false},     /* SETT_MQTT_CLIENT_ENABLED */
    {146u,      65u,    false,      0u,                                     true},      /* SETT_MQTT_BROKER_ADDRESS */
    {211u,      2u,     false,      0u,                                     true},      /* SETT_MQTT_BROKER_PORT */
    {213u,      1u,     true,       0u,                                     false},     /* SETT_TEMPERATURE_SENSOR_CALIBRATION */
    {214u,      1u,     true,       0u,                                     false},     /* SETT_ADAFRUITIO_ENABLED */
    {215u,      32u,    false,      0u,                                     true},      /* SETT_ADAFRUITIO_USERNAME */
    {247u,      65u,    false,      0u,                                     true},      /* SETT_ADAFRUITIO_API_KEY */
    {312u,      1u,     true,       TU_CELSIUS,                             false},     /* SETT_TEMPERATURE_UNIT */
    {313u,      1u,     true,       0u,                                     false},     /* SETT_WEBWEATHER_ENABLED */
    {314u,      33u,    false,      0u,                                     true},      /* SETT_OPENWEATHERMAP_API_KEY */
    {347u,      33u,    false,      0u,                                     true},      /* SETT_OPENWEATHERMAP_LOCATION */
    {380u,      1u,     true,       0u,                                     false},     /* SETT_ALARM_ONLY_ON_WEEKDAYS */
    {381u,      1u,     true,       0u,                                     false},     /* SETT_MUTE */
    {382u,      1u,     true,       AlarmManager::SNOOZE_TIME_MIN_DEFAULT,  false},     /* SETT_ALARM_SNOOZE_TIME */
    {383u,      1u,     true,       0u,                                     false},     /* SETT_SNAKE_HISCORE */
    {384u,      2u,     true,       0u,                                     false},     /* SETT_TETRIS_HISCORE */
    {386u,      1u,     true,       Beep::ALARMSOUND_SUBURBIA,              false},     /* SETT_ALARM_SOUND */
    {387u,      32u,    true,       '\0',                                   true},      /* SETT_MQTT_BROKER_USERNAME */
    {419u,      32u,    true,       '\0',                                   true},      /* SETT_MQTT_BROKER_PASSWORD */
    {451u,      1u,     true,       1u,                                     false},     /* SETT_OTA_ENABLED */
    {452u,      1u,     true,       0u,                                     false},     /* SETT_OTA_USE_PASSWORD */
    {453u,      32u,    false,      0u,                                     true},      /* SETT_OTA_PASSWORD */
    {485u,      1u,     true,       0u,                                     false},     /* SETT_MAGIC_NUMBERS_ENABLED */
    {486u,      1u,     true,       0u,                                     false},     /* SETT_MUTE_WHEN_SCREEN_IS_OFF */
    {487u,      1u,     true,       LedmatrixScreens::CLOCK_TYPE_24H,       false},     /* SETT_CLOCK_TYPE */
    {488u,      1u,     true,       ButtonHandler::BTN_ACT_MUSIC,           false},     /* SETT_BUTTON_SHORTPRESS_ACTION */
    {489u,      1u,     true,       ButtonHandler::BTN_ACT_SCREEN_ON_OFF,   false},     /* SETT_BUTTON_LONGPRESS_ACTION */
    {490u,      1u,     true,       0u,                                     false},     /* SETT_MUTE_SCHEDULE_ENABLED */
    {491u,      1u,     true,       22u,                                    false},     /* SETT_MUTE_ON_HOUR */
    {492u,      1u,     true,       0u,                                     false},     /* SETT_MUTE_ON_MINUTE */
    {493u,      1u,     true,       8u,                                     false},     /* SETT_MUTE_OFF_HOUR */
    {494u,      1u,     true,       0u,                                     false},     /* SETT_MUTE_OFF_MINUTE */
};


Settings::Settings(
    EspHal &esphal,
    Logger &log
):
fEspHal(esphal),
fLog(log)
{
    ;
}

void inline Settings::eeprom_commit() {
    fLog.log(LOG_SETTINGS, L_INFO, F("NVM commit"));
    EEPROM.commit();
}

void Settings::init() {

    EEPROM.begin(512);

    /* Invalidate EEPROM */
    /*
    uint8_t validity_byte = 69u;
    write(SETTINGS_VALIDITY_OFFSET, SETTINGS_VALIDITY_LENGTH, &validity_byte, true);
    */

    uint8_t settings_valid_value = EEPROM.read(settings_map[SETT_VALIDITY].nvm_offset);

    if(settings_valid_value == SETTINGS_VALID_BYTE) {
        settings_valid = true;
        fLog.log(LOG_SETTINGS, L_INFO, F("NVM contains valid settings data"));
    } else {
        settings_valid = false;
    }

}

bool Settings::get_valid() {
    return settings_valid;
}

void Settings::initialize_with_default_values() {
    for(uint8_t i = 0 ; i < SETT_MAX ; i++) {
        if(settings_map[i].initialize) {
            uint8_t initializer_value[settings_map[i].length];
            memset(initializer_value, settings_map[i].default_byte, settings_map[i].length);
            write(i, initializer_value, false, settings_map[i].length);
        }
    }

    eeprom_write_timer = 0u;
    eeprom_commit();
}

void Settings::write(uint16_t settings_index, const uint8_t* data, bool immediate_write, uint16_t length) {
    if(!check_settings_index(settings_index)) return;
    uint16_t offset = settings_map[settings_index].nvm_offset;
    if(length == 0) length = settings_map[settings_index].length;

    /* Copy the data locally so we don't change what's originally passed */
    uint8_t* data_copy = (uint8_t*)malloc(length * sizeof(uint8_t));
    memcpy(data_copy, data, length);

    if(settings_map[settings_index].is_sensitive_data) {
        crypt(data_copy, length);
    }

    fLog.log(LOG_SETTINGS, L_INFO, F("NVM write: %d bytes at offset %d"), length, offset);
    for(uint16_t i = 0 ; i < length ; i++) {
        EEPROM.write(offset+i, *(data_copy+i));
    }

    if(immediate_write) {
        eeprom_commit();
    } else {
        eeprom_write_timer = EEPROM_WRITE_DELAY_SECONDS;
    }

    free(data_copy);
}

uint16_t Settings::get_setting_offset(uint8_t settings_index) {
    if(!check_settings_index(settings_index)) return 0;
    return settings_map[settings_index].nvm_offset;
}

uint16_t Settings::get_setting_length(uint8_t settings_index) {
    if(!check_settings_index(settings_index)) return 0;
    return settings_map[settings_index].length;
}

bool Settings::check_settings_index(uint8_t settings_index) {
    if(settings_index >= SETT_MAX) {
        fLog.log(LOG_SETTINGS, L_ERROR, F("NVM read: unmapped settings address (%d)"), settings_index);
        return false;
    }
    return true;
}

uint8_t Settings::read_byte_s(uint16_t settings_index) {
    if(!check_settings_index(settings_index)) return 0;

    uint8_t data = EEPROM.read(get_setting_offset(settings_index));
    if(settings_map[settings_index].is_sensitive_data) {
        crypt(&data, 1u);
    }

    return data;
}

void Settings::read_block(uint16_t settings_index, uint8_t* data) {
    if(!check_settings_index(settings_index)) return;

    uint16_t offset = settings_map[settings_index].nvm_offset;
    uint8_t length = settings_map[settings_index].length;

    fLog.log(LOG_SETTINGS, L_DEBUG, F("NVM read: %d bytes at offset %d"), length, offset);
    for(uint16_t i = 0 ; i < length ; i++) {
        *(data+i) = EEPROM.read(offset+i);
    }

    if(settings_map[settings_index].is_sensitive_data) {
        crypt(data, length);
    }
}

uint8_t Settings::read_byte_raw(uint16_t offset) {
    return EEPROM.read(offset);
}

void Settings::crypt(uint8_t *data, uint16_t length) {
    uint8_t key_step = 0;
    uint8_t key[6];
    WiFi.macAddress(key);

    for(uint16_t i = 0 ; i < length ; i++) {
        data[i] = data[i] ^ key[key_step];
        key_step++;
        if(key_step == 6u) key_step = 0u;
    }
}

void Settings::do_factory_reset() {
    /* Invalidate the NVM by changing the validity byte, then reboot */
    fLog.log(LOG_SETTINGS, L_INFO, F("Performing factory reset..."));

    fLog.log(LOG_SETTINGS, L_INFO, F("Invalidating NVM"));
    uint8_t validity_byte = 0xff;
    write(SETT_VALIDITY, &validity_byte, false);

    fLog.log(LOG_SETTINGS, L_INFO, F("Erasing sensitive data"));
    for(uint8_t i = 0 ; i < SETT_MAX ; i++) {
        if(settings_map[i].is_sensitive_data) {
            uint16_t length = settings_map[i].length;
            uint8_t offset = settings_map[i].nvm_offset;
            for(uint16_t j = 0 ; j < length ; j++) {
                EEPROM.write(offset + j, 0xff);
            }
        }
    }
    eeprom_commit();

    fLog.log(LOG_SETTINGS, L_INFO, F("Reset completed. Goodbye!"));
    fEspHal.system_reboot();
}

void Settings::task() {

    if(eeprom_write_timer > 0) {
        eeprom_write_timer--;
        if(eeprom_write_timer == 0) {
            eeprom_commit();
        }
    }

}
