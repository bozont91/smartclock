#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>
#include <EEPROM.h>
#include "../esp_hal/esp_hal.h"
#include "../log/log.h"


enum settings_indexes {
    SETT_VALIDITY,
    SETT_SSID,
    SETT_WIFI_PASS,
    SETT_HOURLY_BEEP,
    SETT_CLOCK_STYLE,
    SETT_BRIGHTNESS,
    SETT_ALARM_ENABLED,
    SETT_ALARM_HOUR,
    SETT_ALARM_MINUTE,
    SETT_SCREENONOFF_ENABLED,
    SETT_SCREENON_HOUR,
    SETT_SCREENON_MINUTE,
    SETT_SCREENOFF_HOUR,
    SETT_SCREENOFF_MINUTE,
    SETT_TIMEZONE,
    SETT_DST_ENABLED,
    SETT_SCREENOFF_ONLY_ON_WEEKDAYS,
    SETT_MQTT_CLIENT_ENABLED,
    SETT_MQTT_BROKER_ADDRESS,
    SETT_MQTT_BROKER_PORT,
    SETT_TEMPERATURE_SENSOR_CALIBRATION,
    SETT_ADAFRUITIO_ENABLED,
    SETT_ADAFRUITIO_USERNAME,
    SETT_ADAFRUITIO_API_KEY,
    SETT_TEMPERATURE_UNIT,
    SETT_WEBWEATHER_ENABLED,
    SETT_OPENWEATHERMAP_API_KEY,
    SETT_OPENWEATHERMAP_LOCATION,
    SETT_ALARM_ONLY_ON_WEEKDAYS,
    SETT_MUTE,
    SETT_ALARM_SNOOZE_TIME,
    SETT_SNAKE_HISCORE,
    SETT_TETRIS_HISCORE,
    SETT_ALARM_SOUND,
    SETT_MQTT_BROKER_USERNAME,
    SETT_MQTT_BROKER_PASSWORD,
    SETT_OTA_ENABLED,
    SETT_OTA_USE_PASSWORD,
    SETT_OTA_PASSWORD,
    SETT_MAGIC_NUMBERS_ENABLED,
    SETT_MUTE_WHEN_SCREEN_IS_OFF,
    SETT_CLOCK_TYPE,
    SETT_BUTTON_SHORTPRESS_ACTION,
    SETT_BUTTON_LONGPRESS_ACTION,
    SETT_MUTE_SCHEDULE_ENABLED,
    SETT_MUTE_ON_HOUR,
    SETT_MUTE_ON_MINUTE,
    SETT_MUTE_OFF_HOUR,
    SETT_MUTE_OFF_MINUTE,
    SETT_MAX
};


class Settings {

public:

    Settings(
        EspHal &esphal,
        Logger &log
    );

    void init();
    void initialize_with_default_values();
    bool get_valid();
    void write(uint16_t settings_index, const uint8_t* data, bool immediate_write = false, uint16_t length = 0);
    void read_block(uint16_t settings_index, uint8_t* data);
    uint8_t read_byte_s(uint16_t settings_index);
    uint16_t get_setting_offset(uint8_t settings_index);
    uint16_t get_setting_length(uint8_t settings_index);
    void do_factory_reset();
    void task();

private:

    bool settings_valid = false;
    const uint32_t EEPROM_WRITE_DELAY_SECONDS = 5u;
    uint32_t eeprom_write_timer = 0u;

    void inline eeprom_commit();
    uint8_t read_byte_raw(uint16_t offset);
    bool check_settings_index(uint8_t settings_index);
    void crypt(uint8_t *data, uint16_t length);

    EspHal &fEspHal;
    Logger &fLog;

    #define SETTINGS_VALID_BYTE 42u

    typedef struct {
        uint16_t nvm_offset;
        uint8_t length;
        bool initialize;
        uint8_t default_byte;
        bool is_sensitive_data;
    } settings_element;

    static const settings_element settings_map[SETT_MAX];

};


#endif /* SETTINGS_H */
