#include "encodermanager.h"


EncoderManager::EncoderManager(
    RotaryencoderHAL& rotaryencoderhal,
    LedmatrixStateManager& ledmatrixmain,
    LedmatrixMenu& ledmatrixmenu,
    LedmatrixSnake& snake,
    LedmatrixTetris& tetris,
    LedmatrixScreens& ledmatrixscreens
) :
fRotaryencoderHAL(rotaryencoderhal),
fLedmatrixMain(ledmatrixmain),
fLedmatrixMenu(ledmatrixmenu),
fSnake(snake),
fTetris(tetris),
fLedmatrixScreens(ledmatrixscreens)
{
    fRotaryencoderHAL.subscribe(this);
}

void EncoderManager::encoder_input_callback(const uint8_t enc_direction) {

    uint8_t current_screen = fLedmatrixMain.get_current_screen();

    switch(current_screen) {

        case LedmatrixStateManager::SCREEN_MENU:
            /* Control the menu */
            fLedmatrixMenu.process_encoder_input(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_SNAKE:
            /* Control snake */
            fSnake.process_rotary_encoder_input(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_TETRIS:
            /* Control tetris */
            fTetris.process_rotary_encoder_input(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_BRIGHTNESSADJUST:
            /* Control the brightness adjustment */
            fLedmatrixScreens.calculate_brightness_setting_from_encoder(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_SETALARM:
            /* Control the alarm setting */
            fLedmatrixScreens.calculate_alarm_setting_from_encoder(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_SETMUTE:
            /* Control the mute setting screen */
            fLedmatrixScreens.calculate_mute_setting_from_encoder(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_CLOCKTYPE_SELECT:
            /* Control the Clock Type (AM/PM) selection screen */
            fLedmatrixScreens.calculate_clocktype_setting_from_encoder(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_CLOCKSTYLE_SELECT:
            /* Control the clock style selection screen */
            fLedmatrixScreens.calculate_clockstyle_setting_from_encoder(enc_direction);
            break;

        case LedmatrixStateManager::SCREEN_LEDMATRIX_SHUTDOWN:
            /* Do nothing when the screen is off */
            break;

        default:
            /* Enter the menu */
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_MENU);
            break;
    }

}
