#ifndef ROTARYENCODER_H
#define ROTARYENCODER_H

#include <Arduino.h>
#include <list>
#include "../common.h"
#include "../log/log.h"


class EncoderSubscriber {
public:
    virtual void encoder_input_callback(const uint8_t enc_direction) = 0;
};


class EncoderPublisher {
public:
    virtual void subscribe(EncoderSubscriber *sub) = 0;
    virtual void unsubscribe(EncoderSubscriber *sub) = 0;
    virtual void notify_subscribers(const uint8_t enc_direction) = 0;
};


class RotaryencoderHAL : public EncoderPublisher {

public:

    enum rotaryencoder_directions_e {
        RE_IDLE,
        RE_CLOCKWISE,
        RE_COUNTERCLOCKWISE
    };

    RotaryencoderHAL(callback_function clk_isr_function, Logger &logger);
    void task();
    void IRAM_ATTR handle_clk_interrupt();
    void process_clk_interrupt();
    void set_enabled(bool state);
    void subscribe(EncoderSubscriber *sub) override;
    void unsubscribe(EncoderSubscriber *sub) override;

    volatile bool clk_interrupt_triggered = false;

private:
    bool enabled = false;
    volatile bool clkVal = false;
    volatile bool dtVal = false;
    static const uint8_t ENC_READ_INTERVAL = 100u;
    std::list<EncoderSubscriber*> subscriber_list;

    void notify_subscribers(const uint8_t direction) override;

    Logger &fLog;

};

#endif /* ROTARYENCODER_H */
