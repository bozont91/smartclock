#include "rotaryencoder.h"


void IRAM_ATTR RotaryencoderHAL::handle_clk_interrupt() {
    static uint32_t last_read_time = 0u;

    if(last_read_time + ENC_READ_INTERVAL > general_timer) return;
    last_read_time = general_timer;
    //fLog.log(LOG_ROTARYENCODER, "Generaltimer: %d", general_timer);

    clk_interrupt_triggered = true;
    clkVal = digitalRead(PIN_ROTARYENCODER_CLK);
    dtVal = digitalRead(PIN_ROTARYENCODER_DT);

}

void RotaryencoderHAL::process_clk_interrupt() {

    clk_interrupt_triggered = false;

    if(clkVal != dtVal) {
        fLog.log(LOG_ROTARYENCODER, L_INFO, F("Counterclockwise turn"));
        notify_subscribers(RE_COUNTERCLOCKWISE);
    } else {
        fLog.log(LOG_ROTARYENCODER, L_INFO, F("Clockwise turn"));
        notify_subscribers(RE_CLOCKWISE);
    }
}

void RotaryencoderHAL::set_enabled(bool state) {
    enabled = state;
}

void RotaryencoderHAL::subscribe(EncoderSubscriber *sub) {
    subscriber_list.push_back(sub);
}

void RotaryencoderHAL::unsubscribe(EncoderSubscriber *sub) {
    subscriber_list.remove(sub);
}

void RotaryencoderHAL::notify_subscribers(const uint8_t enc_direction) {
    fLog.log(LOG_ROTARYENCODER, L_DEBUG, F("Notifying subscribers; direction='%d'"), enc_direction);
    for(auto &i : subscriber_list) {
        i->encoder_input_callback(enc_direction);
    }
}

void RotaryencoderHAL::task() {
    if(!enabled) return;

    if(clk_interrupt_triggered) {
        process_clk_interrupt();
    }
}

RotaryencoderHAL::RotaryencoderHAL(callback_function clk_isr_function, Logger &logger) :
fLog(logger)
{
    pinMode(PIN_ROTARYENCODER_DT, INPUT_PULLUP);
    pinMode(PIN_ROTARYENCODER_CLK, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(PIN_ROTARYENCODER_CLK), clk_isr_function, CHANGE);
}
