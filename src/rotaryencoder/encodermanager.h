#ifndef ENCODERMANAGER_H
#define ENCODERMANAGER_H

#include <Arduino.h>
#include "../common.h"
#include "rotaryencoder.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../ledmatrix_menu/ledmatrix_menu.h"
#include "../ledmatrix_snake/ledmatrix_snake.h"
#include "../ledmatrix_tetris/ledmatrix_tetris.h"
#include "../ledmatrix_screens/ledmatrix_screens.h"

class EncoderManager : public EncoderSubscriber {

public:
    EncoderManager(
        RotaryencoderHAL& rotaryencoderhal,
        LedmatrixStateManager& ledmatrixmain,
        LedmatrixMenu& ledmatrixmenu,
        LedmatrixSnake& snake,
        LedmatrixTetris& tetris,
        LedmatrixScreens& ledmatrixscreens
    );
    void encoder_input_callback(const uint8_t enc_direction) override;

private:
    RotaryencoderHAL& fRotaryencoderHAL;
    LedmatrixStateManager& fLedmatrixMain;
    LedmatrixMenu& fLedmatrixMenu;
    LedmatrixSnake& fSnake;
    LedmatrixTetris& fTetris;
    LedmatrixScreens& fLedmatrixScreens;

};

#endif /* ENCODERMANAGER_H */
