#ifndef JSON_PARSER_H
#define JSON_PARSER_H
/* A lightweight simple JSON parser */

#ifndef AURA_UNITTEST
    #include <Arduino.h>
    #include "../log/log.h"
#else
    #include "../../tests/mock/esp8266_core/WString.h"
    #include "../../tests/mock/log.h"
#endif


class JsonParser {

public:
    JsonParser(Logger &log);
    String get_value_for_key(String json, String key);

private:
    enum subelement_type_e {
        SE_SUBTREE,
        SE_ARRAY,
        SE_VALUE
    };

    subelement_type_e identify_next_subelement(String json, int from_index = 0);
    int find_end_index_of_next_subelement(subelement_type_e subelement_type, String json, int from_index = 0);
    String get_and_remove_leftmost_key(String& key);
    int find_next_non_whitespace_char_index(String data, int from_index = 0);
    int find_next_non_whitespace_char_index_reverse(String data);
    void remove_excess_whitespace_front_and_back(String& data);
    int number_of_char_occurences(String data, char c);
    void remove_all_newline_chars(String& data);
    bool is_number(String data);

    Logger& fLog;
};


#endif /* JSON_PARSER_H */
