#include "json_parser.h"

JsonParser::JsonParser(Logger& log)
:
fLog(log)
{
    ;
}

bool JsonParser::is_number(String data) {
    for(auto c : data) {
        if(!isdigit(c)) return false;
    }
    return true;
}

int JsonParser::number_of_char_occurences(String data, char char_to_count) {
    int occurences = 0;
    for(auto c : data) {
        if(char_to_count == c) occurences++;
    }
    return occurences;
}

void JsonParser::remove_excess_whitespace_front_and_back(String& data) {
    data = data.substring(find_next_non_whitespace_char_index(data), find_next_non_whitespace_char_index_reverse(data)+1);
}

void JsonParser::remove_all_newline_chars(String& data) {
    int index = 0;
    for(auto c : data) {
        if(c == '\r' || c == '\n') data.remove(index, 1);
        index++;
    }
}

int JsonParser::find_next_non_whitespace_char_index(String data, int from_index) {
    /* Find the first non-space character */
    while(data[from_index] == ' ' || data[from_index] == '\n' || data[from_index] == '\r') from_index++;
    return from_index;
}

int JsonParser::find_next_non_whitespace_char_index_reverse(String data) {
    /* Find the first non-space character from the back */
    int pos = data.length() - 1;
    while(data[pos] == ' ' || data[pos] == '\n' || data[pos] == '\r') pos--;
    return pos;
}

JsonParser::subelement_type_e JsonParser::identify_next_subelement(String json, int from_index) {
    char subelement_opening_char = json[from_index];
    if(subelement_opening_char == '{') {
        fLog.log(LOG_JSON, L_VERBOSE, F("Type: subtree"));
        return SE_SUBTREE;
    } else if(subelement_opening_char == '[') {
        fLog.log(LOG_JSON, L_VERBOSE, F("Type: array"));
        return SE_ARRAY;
    } else {
        fLog.log(LOG_JSON, L_VERBOSE, F("Type: value"));
        return SE_VALUE;
    }
}

String JsonParser::get_and_remove_leftmost_key(String& key) {
    String leftmost_key;
    int leftmost_key_end_index = key.indexOf('/');
    if(leftmost_key_end_index == -1) {
        leftmost_key = key;
    } else {
        leftmost_key = key.substring(0, leftmost_key_end_index);
        key = key.substring(leftmost_key_end_index+1);
    }

    return leftmost_key;
}

int JsonParser::find_end_index_of_next_subelement(subelement_type_e subelement_type, String json, int from_index) {
    char subelement_start_char;
    char subelement_end_char;
    if(subelement_type == SE_SUBTREE) {
        subelement_start_char = '{';
        subelement_end_char = '}';
    } else if(subelement_type == SE_ARRAY) {
        subelement_start_char = '[';
        subelement_end_char = ']';
    } else {
        int end_of_value_index = json.indexOf(',');
        if(end_of_value_index == -1) return json.length();
        return end_of_value_index;
    }

    String subtree = json.substring(from_index);
    //fLog.log(LOG_JSON, L_VERBOSE, F("Current subtree: %s"), subtree.c_str());
    int closing_brace_index;
    int closing_brace_index_offset = 0;
    while(true) {
        closing_brace_index = subtree.indexOf(subelement_end_char, closing_brace_index_offset);
        String cut_subtree = subtree.substring(0, closing_brace_index + 1);
        //fLog.log(LOG_JSON, L_VERBOSE, F("Cut subtree: %s"), cut_subtree.c_str());
        if(number_of_char_occurences(cut_subtree, subelement_start_char) == number_of_char_occurences(cut_subtree, subelement_end_char)) {
            subtree = subtree.substring(0, closing_brace_index + 1);
            fLog.log(LOG_JSON, L_VERBOSE, F("%s"), subtree.c_str());
            break;
        }
        closing_brace_index_offset = closing_brace_index + 1;
    }

    return (from_index + closing_brace_index);
}

String JsonParser::get_value_for_key(String json, String key) {
    fLog.log(LOG_JSON, L_VERBOSE, F("JSON parser called"));
    fLog.log(LOG_JSON, L_VERBOSE, F("JSON: %s"), json.c_str());
    fLog.log(LOG_JSON, L_VERBOSE, F("Key: %s"), key.c_str());

    /* Check the data for validity */
    int first_valid_char_pos = find_next_non_whitespace_char_index(json);
    int last_valid_char_pos = find_next_non_whitespace_char_index_reverse(json);
    if(json[first_valid_char_pos] != '{' || json[last_valid_char_pos] != '}') {
        fLog.log(LOG_JSON, L_ERROR, F("Invalid JSON data"));
        return "";
    }

    /* Check for empty key */
    if(key.length() == 0) {
        fLog.log(LOG_JSON, L_VERBOSE, F("Empty key"));
        return "";
    }

    /* Get the first (leftmost) key from the path and remove it from the path */
    String leftmost_key = get_and_remove_leftmost_key(key);
    fLog.log(LOG_JSON, L_VERBOSE, F("Leftmost key: %s"), leftmost_key.c_str());

    /* Drop the first and last curly brace */
    json = json.substring(first_valid_char_pos+1, last_valid_char_pos);
    /* Remove all the excess whitespace from the beginning and the end */
    remove_excess_whitespace_front_and_back(json);
    remove_all_newline_chars(json);
    fLog.log(LOG_JSON, L_VERBOSE, F("%s"), json.c_str());

    /* Iterate through the JSON */
    while(true) {

        /* If the JSON starts with a comma, remove it */
        if(json[0] == ',') json = json.substring(1);

        fLog.log(LOG_JSON, L_VERBOSE, F("---------------"));
        //fLog.log(LOG_JSON, L_VERBOSE, F("JSON: %s"), json.c_str());

        int current_key_start_index = json.indexOf('\"');
        int current_key_end_index = json.indexOf("\"", current_key_start_index + 1);

        if(current_key_start_index == -1 || current_key_end_index == -1) {
            fLog.log(LOG_JSON, L_VERBOSE, F("End of JSON data"));
            return "";
        }

        String current_key = json.substring(current_key_start_index + 1, current_key_end_index);
        fLog.log(LOG_JSON, L_VERBOSE, F("Current key: %s"), current_key.c_str());

        /* Check if we're dealing with a subtree, array or a concrete value */
        int subtree_opening_char_index = find_next_non_whitespace_char_index(json, current_key_end_index + 2);
        subelement_type_e subelement = identify_next_subelement(json, subtree_opening_char_index);

        if(subelement == SE_SUBTREE) {
            int closing_brace_index = find_end_index_of_next_subelement(SE_SUBTREE, json, subtree_opening_char_index);
            String subtree = json.substring(subtree_opening_char_index, closing_brace_index + 1);

            /* Call ourselves recursively with the subtree if the leftmost key matches */
            if(leftmost_key == current_key) {
                fLog.log(LOG_JSON, L_VERBOSE, F("Recursive call with subtree"));
                return get_value_for_key(subtree, key);
            }

            /* Remove the current subtree from the original */
            json = json.substring(closing_brace_index+1);

        } else if(subelement == SE_ARRAY) {
            int closing_brace_index = find_end_index_of_next_subelement(SE_ARRAY, json, subtree_opening_char_index);
            String subtree = json.substring(subtree_opening_char_index, closing_brace_index + 1);

            /* Check if the key matches */
            if(leftmost_key == current_key) {
                String array_index_s = get_and_remove_leftmost_key(key);
                if(!is_number(array_index_s)) {
                    fLog.log(LOG_JSON, L_VERBOSE, F("Array index is not a number"));
                    return "";
                }

                int requested_array_index = array_index_s.toInt();
                fLog.log(LOG_JSON, L_VERBOSE, F("Requested array index: %d"), requested_array_index);

                /* Remove the array braces */
                subtree = subtree.substring(1, subtree.length()-1);

                /* Iterate through the indexes till the requested one */
                int curr_index = 0;
                int array_subelement_end_index;
                String requested_subelement;
                subelement_type_e subelement_type;
                do {
                    remove_excess_whitespace_front_and_back(subtree);
                    subelement_type = identify_next_subelement(subtree);
                    array_subelement_end_index = find_end_index_of_next_subelement(subelement_type, subtree);
                    requested_subelement = subtree.substring(0, array_subelement_end_index+1);
                    subtree = subtree.substring(array_subelement_end_index+2);
                    if(subtree.length() == 0 && requested_subelement.length() == 0) {
                        fLog.log(LOG_JSON, L_VERBOSE, F("Array has no more indexes"));
                        return "";
                    }
                    curr_index++;
                } while(curr_index <= requested_array_index);

                fLog.log(LOG_JSON, L_VERBOSE, F("Requested array subelement at index %d: %s"), curr_index-1, requested_subelement.c_str());

                /* Extract value */
                if(subelement_type == SE_VALUE) {
                    int start_of_value_index = requested_subelement.indexOf(':')+1;
                    int end_of_value_index = requested_subelement.indexOf(',');
                    if(end_of_value_index == -1) end_of_value_index = requested_subelement.length();
                    String value = requested_subelement.substring(start_of_value_index, end_of_value_index);
                    remove_excess_whitespace_front_and_back(value);

                    /* Remove the quotes from the value */
                    if(value.indexOf('"') >= 0) {
                        value = value.substring(1);
                        value.remove(value.indexOf('\"'));
                    }

                    fLog.log(LOG_JSON, L_VERBOSE, F("Extracted value from array: %s"), value.c_str());
                    return value;

                } else {
                    /* If it's anything else besides a value do a recursive call */
                    fLog.log(LOG_JSON, L_VERBOSE, F("Recursive call with array member"));
                    return get_value_for_key(requested_subelement, key);
                }
            }

            /* Remove the current subtree from the original */
            json = json.substring(closing_brace_index+1);

        } else if(subelement == SE_VALUE) {
            int end_of_value_index = json.indexOf(',');
            if(end_of_value_index == -1) end_of_value_index = json.length();

            /* Check if we're at the end of our key and the key equals the current key */
            if(key.indexOf('/') == -1 && leftmost_key == current_key) {
                String value = json.substring(subtree_opening_char_index, end_of_value_index);

                /* Remove the quotes from the value */
                if(value.indexOf('"') >= 0) {
                    value = value.substring(1);
                    value.remove(value.indexOf('\"'));
                }

                fLog.log(LOG_JSON, L_VERBOSE, F("Value: %s"), value.c_str());
                fLog.log(LOG_JSON, L_VERBOSE, F("------------------------"));
                return value;
            }

            /* Remove the current subtree from the original */
            json = json.substring(end_of_value_index+1);
        }

    }
}
