#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <Arduino.h>
#include "../log/log.h"
#include "../beep/beep.h"
#include "../ntp/ntp.h"
#include "../settings/settings.h"
#include "../temperature/temperature.h"
#include "../deviceclock/deviceclock.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../ledmatrix_main/ledmatrix_main.h"


extern const char html_ok_response_template[];
extern const char html_not_ok_response_template[] PROGMEM;
extern const char html_404_template[] PROGMEM;


class Webserver {

public:
    Webserver(
        Logger &log,
        Beep &beep,
        NtpManager &ntp,
        Settings &settings,
        TemperatureManager &temperaturemanager,
        DeviceClock &deviceclock,
        LedmatrixHal &ledmatrixhal,
        LedmatrixStateManager &ledmatrixmain
    );
    static String url_decode(String str);
    void begin();
    void task();

private:
    void prepareHtmlPage(String& html_template);
    String build_http_ok_response(String response_data);
    static unsigned char hex2int(char c);

    String client_ip_str;

    Logger &fLog;
    Beep &fBeep;
    NtpManager &fNtp;
    Settings &fSettings;
    TemperatureManager &fTemperatureManager;
    DeviceClock &fDeviceClock;
    LedmatrixHal &fLedmatrixHal;
    LedmatrixStateManager &fLedmatrixMain;

};


#endif /* WEBSERVER_H */
