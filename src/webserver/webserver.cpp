#include "webserver.h"


WiFiServer webserver(80);
WiFiClient webserver_client;


const char html_ok_response_template[] = R"=====(
HTTP/1.1 200 OK
Content-Type: text/html
Connection: close

<!DOCTYPE HTML>
)=====";


const char html_not_ok_response_template[] PROGMEM = R"=====(
HTTP/1.1 418 I'm a teapot
Content-Type: text/html
Connection: close

<!DOCTYPE HTML>
)=====";


const char html_404_template[] PROGMEM = R"=====(
<span style='font-size:40px;'>4&#128125;4</span>  <b>The aliens abducted this page!</b> | aura SmartClock
)=====";


static const char html_template[] PROGMEM = R"=====(
HTTP/1.1 200 OK
Content-Type: text/html
Connection: close

<!DOCTYPE HTML>

<html>
<head>
<title>aura SmartClock</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body {
height: 100%;
background-repeat: no-repeat;
background-attachment: fixed;
background-image: linear-gradient(to right top, #1f6ce1, #00a1ff, #00cae4, #00e88c, #aff80d);
}
.btn {
  background-color: DodgerBlue;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
}
.btn:hover {
  background-color: RoyalBlue;
}
</style>
<script>
var x = setInterval(function() {loadData("refreshtime",function(){document.getElementById("time").innerHTML = this.responseText;} )}, 1000);
var y = setInterval(function() {loadData("refreshtemp",function(){document.getElementById("temp").innerHTML = this.responseText;} )}, 10000);
var z = setInterval(function() {loadData("refreshuptime",function(){document.getElementById("uptime").innerHTML = this.responseText;} )}, 10000);
var a = setInterval(function() {loadData("refreshdate",function(){document.getElementById("date").innerHTML = this.responseText;} )}, 10000);
var b = setInterval(function() {loadData("refreshlastsync",function(){document.getElementById("lastsync").innerHTML = this.responseText;} )}, 30000);
var c = setInterval(function() {loadData("refreshnextsync",function(){document.getElementById("nextsync").innerHTML = this.responseText;} )}, 30000);
function loadData(url, callback) {
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if(this.readyState == 4 && this.status == 200) {
  callback.apply(xhttp);
}
};
xhttp.open("GET", url, true);
xhttp.send();
}
function send_get_request(handle) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", handle, true);
  xhttp.send();
}
</script>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body>
<h2><b>aura SmartClock</b></h2>
<p><font id="date">%DATE%</font> | <font size="5pt"><b id="time"> %TIME% </b></font> | <i class="fa fa-thermometer-half"> <font id="temp">%TEMPERATURE%</font></i></p>
<br><br>
<div align="center">
<button class="btn" onclick="send_get_request('beep')"><i class="fa fa-bullhorn"></i> Beep</button>
<br><br>
<button class="btn" onclick="send_get_request('showdate')"><i class="fa fa-calendar"></i> Show date</button>
<br><br>
<button class="btn" onclick="send_get_request('showtemp')"><i class="fa fa-thermometer-half"></i> Show temperature</button>
<br><br>
<button class="btn" onclick="send_get_request('brightnessup')"><i class="fa fa-arrow-circle-up"></i> Brightness up</button>
<br><br>
<button class="btn" onclick="send_get_request('brightnessdown')"><i class="fa fa-arrow-circle-down"></i> Brightness down</button>
<br><br>
<button class="btn" onclick="send_get_request('displayonoff')"><i class="fa fa-power-off"></i> Display on/off</button>
<br><br>
<input type="text" name="texttoshow" id="texttoshow" value="hello!" maxlength="200"><br>
<button class="btn" onclick="send_get_request('showtext?texttoshow=' + document.getElementById('texttoshow').value)"><i class="fa fa-comment"></i> Show text</button>
<br><br>
<i class="fa fa-info-circle"><b> Device info:</b></i>
<p>Uptime: <font id="uptime">%UPTIME%</font> seconds</p>
<p>Last NTP sync: <font id="lastsync">%LASTSYNC%</font></p>
<p>Next NTP sync: <font id="nextsync">%NEXTSYNC%</font></p>
<p>CPU speed: %CPUSPEED% MHz</p>
<p>WiFi network: %WIFINETWORK%</p>
<p>IPv4 address: %IPADDRESS%</p>
<p>Software version: %SWVERSION%</p>
</div>
</body>
</html>

)=====";


Webserver::Webserver(
    Logger &log,
    Beep &beep,
    NtpManager &ntp,
    Settings &settings,
    TemperatureManager &temperaturemanager,
    DeviceClock &deviceclock,
    LedmatrixHal &ledmatrixhal,
    LedmatrixStateManager &ledmatrixmain
) :
fLog(log),
fBeep(beep),
fNtp(ntp),
fSettings(settings),
fTemperatureManager(temperaturemanager),
fDeviceClock(deviceclock),
fLedmatrixHal(ledmatrixhal),
fLedmatrixMain(ledmatrixmain)
{
    ;
}

void Webserver::begin() {
    webserver.begin();
}

String Webserver::build_http_ok_response(String response_data) {
    String response = (String)html_ok_response_template;
    return response + response_data + "\n";
}

void Webserver::prepareHtmlPage(String& html_template) {

    time_t now = fDeviceClock.get();
    struct tm ts = *localtime(&now);
    char timebuf[80];

    strftime(timebuf, sizeof(timebuf), "%a, %Y. %m. %d.", &ts);
    html_template.replace(F("%DATE%"), timebuf);

    strftime(timebuf, sizeof(timebuf), "%H:%M", &ts);
    html_template.replace(F("%TIME%"), timebuf);

    String temperature = "--";
    if(fTemperatureManager.get_sensor_present()) {
        temperature = (String)(int32_t)fTemperatureManager.get_temperature() + "<span>&#176;</span>" + fTemperatureManager.get_temperature_postfix_as_char();
    }
    html_template.replace(F("%TEMPERATURE%"), temperature);

    html_template.replace(F("%UPTIME%"), String(device_uptime));

    now = fNtp.get_last_sync_unix_timestamp();
    ts = *localtime(&now);
    strftime(timebuf, sizeof(timebuf), "%H:%M  ", &ts);
    html_template.replace(F("%LASTSYNC%"), timebuf);

    now = fNtp.get_next_sync_unix_timestamp();
    ts = *localtime(&now);
    strftime(timebuf, sizeof(timebuf), "%H:%M  ", &ts);
    html_template.replace(F("%NEXTSYNC%"), timebuf);

    html_template.replace(F("%CPUSPEED%"), String(ESP.getCpuFreqMHz()));
    html_template.replace(F("%WIFINETWORK%"), WiFi.SSID().c_str());
    html_template.replace(F("%IPADDRESS%"), WiFi.localIP().toString());

    sprintf_P(timebuf, PSTR("%s %s (%s) - %s"), SOFTWARE_VERSION, SOFTWARE_RELEASE_TYPE, SOFTWARE_COMMIT_ID, SOFTWARE_BUILD_DATE);
    html_template.replace(F("%SWVERSION%"), timebuf);

}


void Webserver::task() {

    webserver_client = webserver.available();
    if(!webserver_client) return;

    client_ip_str = webserver_client.remoteIP().toString();
    fLog.log(LOG_WEBSERVER, L_DEBUG, F("Client connected; address='%s'"), client_ip_str.c_str());

    while(webserver_client.connected()) {

        if(!webserver_client.available()) continue;

        String line = webserver_client.readStringUntil('\r');
        //Serial.println(line);

        if((line.indexOf(F("GET")) < 0)) break;
        String called_handle = line;
        called_handle.remove(called_handle.indexOf(" HTTP"));
        called_handle = called_handle.substring(called_handle.indexOf("GET ") + 4);
        fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called handle: %s"), called_handle.c_str());

        if(called_handle == "/beep") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called beep; client='%s'"), client_ip_str.c_str());
            fBeep.beep_short();
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle == "/showdate") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called showdate; client='%s'"), client_ip_str.c_str());
            fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_YEAR);
            fBeep.beep_short();
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle == "/showtemp") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called showtemp; client='%s'"), client_ip_str.c_str());
            fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_TEMPERATURE);
            fBeep.beep_short();
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle == "/brightnessup") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called brightnessup; client='%s'"), client_ip_str.c_str());
            fLedmatrixHal.increase_brightness();
            uint8_t brightnessval = fLedmatrixHal.get_brightness();
            fSettings.write(SETT_BRIGHTNESS, &brightnessval);
            fBeep.beep_short();
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle == "/brightnessdown") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called brightnessdown; client='%s'"), client_ip_str.c_str());
            fLedmatrixHal.decrease_brightness();
            uint8_t brightnessval = fLedmatrixHal.get_brightness();
            fSettings.write(SETT_BRIGHTNESS, &brightnessval);
            fBeep.beep_short();
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle == "/displayonoff") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called displayonoff; client='%s'"), client_ip_str.c_str());
            if(fLedmatrixMain.get_display_on()) {
                fLedmatrixMain.display_off();
                fBeep.play_negative_acknowledge();
            } else {
                fLedmatrixMain.display_on();
                fBeep.play_acknowledge();
            }
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle.indexOf("/showtext") >= 0) {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Called showtext; client='%s'"), client_ip_str.c_str());
            line = url_decode(line);

            String payload = line.substring(line.indexOf("texttoshow=")+11);
            payload.remove(payload.indexOf("HTTP/"));

            fLedmatrixMain.show_text(payload);
            fBeep.play_acknowledge();
            webserver_client.println(html_ok_response_template);
            break;
        }

        if(called_handle == "/refreshtime") {
            fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called refreshtime; client='%s'"), client_ip_str.c_str());
            time_t now = fDeviceClock.get();
            struct tm ts = *localtime(&now);
            char timebuf[10];
            strftime(timebuf, sizeof(timebuf), "%H:%M", &ts);

            String response = build_http_ok_response(timebuf);
            webserver_client.println(response);
            break;
        }

        if(called_handle == "/refreshdate") {
            fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called refreshdate; client='%s'"), client_ip_str.c_str());
            time_t now = fDeviceClock.get();
            struct tm ts = *localtime(&now);
            char timebuf[20];
            strftime(timebuf, sizeof(timebuf), "%a, %Y. %m. %d.", &ts);

            String response = build_http_ok_response(timebuf);
            webserver_client.println(response);
            break;
        }

        if(called_handle == "/refreshtemp") {
            fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called refreshtemp; client='%s'"), client_ip_str.c_str());

            String temperature = "--";
            if(fTemperatureManager.get_sensor_present()) {
                temperature = (String)(int32_t)fTemperatureManager.get_temperature() + "°" + fTemperatureManager.get_temperature_postfix_as_char();
            }

            String response = build_http_ok_response(temperature);
            webserver_client.print(response);
            break;
        }

        if(called_handle == "/refreshuptime") {
            fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called refreshuptime; client='%s'"), client_ip_str.c_str());
            String response = build_http_ok_response((String)device_uptime);
            webserver_client.print(response);
            break;
        }

        if(called_handle == "/refreshlastsync") {
            fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called refreshlastsync; client='%s'"), client_ip_str.c_str());
            time_t now = fNtp.get_last_sync_unix_timestamp();
            struct tm ts = *localtime(&now);
            char timebuf[10];
            strftime(timebuf, sizeof(timebuf), "%H:%M  ", &ts);

            String response = build_http_ok_response(timebuf);
            webserver_client.print(response);
            break;
        }

        if(called_handle == "/refreshnextsync") {
            fLog.log(LOG_WEBSERVER, L_DEBUG, F("Called refreshnextsync; client='%s'"), client_ip_str.c_str());
            time_t now = fNtp.get_next_sync_unix_timestamp();
            struct tm ts = *localtime(&now);
            char timebuf[10];
            strftime(timebuf, sizeof(timebuf), "%H:%M  ", &ts);

            String response = build_http_ok_response(timebuf);
            webserver_client.print(response);
            break;
        }

        if(called_handle == "/") {
            fLog.log(LOG_WEBSERVER, L_INFO, F("Serving webpage root; client='%s'"), client_ip_str.c_str());
            String html = String(html_template);
            prepareHtmlPage(html);
            webserver_client.println(html);
            break;
        }

        fLog.log(LOG_WEBSERVER, L_INFO, F("Serving not found page; client='%s'"), client_ip_str.c_str());
        String response = build_http_ok_response(FPSTR(html_404_template));
        webserver_client.println(response);
        break;
    }
}

/* Source: https://circuits4you.com/2019/03/21/esp8266-url-encode-decode-example/ */
unsigned char Webserver::hex2int(char c) {
    if(c >= '0' && c <='9') {
        return((unsigned char)c - '0');
    }
    if(c >= 'a' && c <='f') {
        return((unsigned char)c - 'a' + 10);
    }
    if(c >= 'A' && c <='F') {
        return((unsigned char)c - 'A' + 10);
    }
    return 0;
}

String Webserver::url_decode(String str) {
    String encodedString = "";
    char c;
    char code0;
    char code1;
    for(uint16_t i = 0 ; i < str.length() ; i++) {
        c = str.charAt(i);
        if(c == '+') {
            encodedString+=' ';
        } else if(c == '%') {
            i++;
            code0 = str.charAt(i);
            i++;
            code1 = str.charAt(i);
            c = (hex2int(code0) << 4) | hex2int(code1);
            encodedString+=c;
        } else {
            encodedString+=c;
        }

        yield();
    }

    return encodedString;
}
