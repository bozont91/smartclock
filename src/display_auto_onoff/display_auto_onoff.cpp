#include "display_auto_onoff.h"


DisplayAutoOnOff::DisplayAutoOnOff(
    Logger &log,
    Beep &beep,
    LedmatrixStateManager &ledmatrixmain,
    DeviceClock &deviceclock,
    ClockManager &clockmanager,
    Settings &settings
):
fLog(log),
fBeep(beep),
fLedmatrixMain(ledmatrixmain),
fDeviceClock(deviceclock),
fClockManager(clockmanager),
fSettings(settings)
{
    ;
}

void DisplayAutoOnOff::read_settings() {
    set_enabled(fSettings.read_byte_s(SETT_SCREENONOFF_ENABLED));
    set_off_time(fSettings.read_byte_s(SETT_SCREENOFF_HOUR), fSettings.read_byte_s(SETT_SCREENOFF_MINUTE));
    set_on_time(fSettings.read_byte_s(SETT_SCREENON_HOUR), fSettings.read_byte_s(SETT_SCREENON_MINUTE));

    bool on_off_only_on_weekdays = (bool)fSettings.read_byte_s(SETT_ALARM_ONLY_ON_WEEKDAYS);
    if(on_off_only_on_weekdays) set_option(ONLY_ON_WEEKDAYS);
}

bool DisplayAutoOnOff::get_enabled() {
    return display_onoff_enabled;
}

void DisplayAutoOnOff::set_enabled(bool state) {
    display_onoff_enabled = state;
    if(state) {
        fLog.log(LOG_DISPLAYAUTOONOFF, L_INFO, F("Automatic display on/off enabled"));
    } else {
        fLog.log(LOG_DISPLAYAUTOONOFF, L_INFO, F("Automatic display on/off disabled"));
    }
}

void DisplayAutoOnOff::set_option(uint8_t option) {
    if(option == ONLY_ON_WEEKDAYS) {
        only_on_weekdays_option = true;
    } else if(option == EVERYDAY) {
        only_on_weekdays_option = false;
    }
}

bool DisplayAutoOnOff::get_only_on_weekdays_option() {
    return only_on_weekdays_option;
}

uint8_t DisplayAutoOnOff::get_off_hour() {
    return display_off_hour;
}

uint8_t DisplayAutoOnOff::get_off_minute() {
    return display_off_minute;
}

void DisplayAutoOnOff::set_off_time(uint8_t hour, uint8_t minute) {
    if(hour > 23 || minute > 59) return;
    display_off_hour = hour;
    display_off_minute = minute;
    fLog.log(LOG_DISPLAYAUTOONOFF, L_INFO, F("Setting off time: %02d:%02d"), hour, minute);
}


uint8_t DisplayAutoOnOff::get_on_hour() {
    return display_on_hour;
}

uint8_t DisplayAutoOnOff::get_on_minute() {
    return display_on_minute;
}

void DisplayAutoOnOff::set_on_time(uint8_t hour, uint8_t minute) {
    if(hour > 23 || minute > 59) return;
    display_on_hour = hour;
    display_on_minute = minute;
    fLog.log(LOG_DISPLAYAUTOONOFF, L_INFO, F("Setting on time: %02d:%02d"), hour, minute);
}

bool DisplayAutoOnOff::check_on_off_day() {
    if(only_on_weekdays_option == false) return true;

    if(fClockManager.isWeekend()) {
        return false;
    }

    return true;
}

void DisplayAutoOnOff::task() {
    static uint32_t displayonoff_cooldown_timer = 0u;
    if(display_onoff_enabled == false || !fClockManager.isClockSynchronizedToNtp() || displayonoff_cooldown_timer > fDeviceClock.get()) return;

    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    if(hour == display_off_hour && minute == display_off_minute && check_on_off_day()) {
        if(fLedmatrixMain.get_display_on()) {
            displayonoff_cooldown_timer = fDeviceClock.get()+60;
            fLog.log(LOG_DISPLAYAUTOONOFF, L_INFO, F("Turning display off"));
            fLedmatrixMain.display_off();
            fBeep.beep_short();
        }
    }

    if(hour == display_on_hour && minute == display_on_minute && check_on_off_day()) {
        if(!fLedmatrixMain.get_display_on()) {
            displayonoff_cooldown_timer = fDeviceClock.get()+60;
            fLog.log(LOG_DISPLAYAUTOONOFF, L_INFO, F("Turning display on"));
            fLedmatrixMain.display_on();
            fBeep.beep_short();
        }
    }

}
