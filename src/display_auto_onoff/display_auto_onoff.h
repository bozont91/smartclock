#ifndef DISPLAY_AUTO_ONOFF_H
#define DISPLAY_AUTO_ONOFF_H

#include <Arduino.h>
#include "../log/log.h"
#include "../beep/beep.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../deviceclock/deviceclock.h"
#include "../clockmanager/clockmanager.h"
#include "../settings/settings.h"


class DisplayAutoOnOff {

public:

    DisplayAutoOnOff(
        Logger &log,
        Beep &beep,
        LedmatrixStateManager &ledmatrixmain,
        DeviceClock &deviceclock,
        ClockManager &clockmanager,
        Settings &settings
    );

    enum autoonoff_options {
        EVERYDAY,
        ONLY_ON_WEEKDAYS
    };

    void read_settings();
    bool get_enabled();
    void set_enabled(bool state);
    void set_option(uint8_t option);
    bool get_only_on_weekdays_option();
    uint8_t get_off_hour();
    uint8_t get_off_minute();
    void set_off_time(uint8_t hour, uint8_t minute);
    uint8_t get_on_hour();
    uint8_t get_on_minute();
    void set_on_time(uint8_t hour, uint8_t minute);
    void task();

private:

    bool check_on_off_day();
    bool display_onoff_enabled = false;
    bool only_on_weekdays_option = false;
    uint8_t display_off_hour = 0u;
    uint8_t display_off_minute = 0u;
    uint8_t display_on_hour = 0u;
    uint8_t display_on_minute = 0u;

    Logger &fLog;
    Beep &fBeep;
    LedmatrixStateManager &fLedmatrixMain;
    DeviceClock &fDeviceClock;
    ClockManager &fClockManager;
    Settings &fSettings;

};


#endif /* DISPLAY_AUTO_ONOFF_H */
