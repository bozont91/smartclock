#include "ledmatrix_menu.h"


LedmatrixMenu::LedmatrixMenu(
    RotaryencoderHAL &rotaryencoderhal,
    Logger &log,
    Beep &beep,
    LedmatrixGFX &ledmatrixgfx,
    LedmatrixScreens &ledmatrixscreens
) :
fRotaryencoderHAL(rotaryencoderhal),
fLog(log),
fBeep(beep),
fLedmatrixGFX(ledmatrixgfx),
fLedmatrixScreens(ledmatrixscreens)
{
    ;
}

bool LedmatrixMenu::get_active() {
    return is_active;
}

uint8_t LedmatrixMenu::get_current_menu() {
    return menuitems[current_menu_position].item;
}

void LedmatrixMenu::deactivate() {
    is_active = false;
    current_menu_offset = MENU_OFFSET_DEFAULT;
    current_menu_position = 0u;
    requested_menu_position = 0u;
}

void LedmatrixMenu::process_encoder_input(uint8_t encoder_direction) {

    if(encoder_direction == fRotaryencoderHAL.RE_CLOCKWISE) {
        if(requested_menu_position != MENUITEM_MAX-1) {
            requested_menu_position++;
        } else {
            fBeep.beep_short();
        }
        fLog.log(LOG_LEDMATRIX, L_INFO, F("Menu++"));
    }

    if(encoder_direction == fRotaryencoderHAL.RE_COUNTERCLOCKWISE) {
        if(requested_menu_position != 0u) {
            requested_menu_position--;
        } else {
            fBeep.beep_short();
        }
        fLog.log(LOG_LEDMATRIX, L_INFO, F("Menu--"));
    }
}

void LedmatrixMenu::task() {

    if(menu_transition_in_progress) do_menu_scroll();

    if(requested_menu_position != current_menu_position) {
        menu_transition_in_progress = true;
        do_menu_scroll();
    }

}

void LedmatrixMenu::do_menu_scroll() {
    static uint8_t scroll_cnt = MENU_SCROLL_AMOUNT;

    /* Scroll right */
    if(requested_menu_position > current_menu_position) {
        current_menu_offset--;
        render_menu(current_menu_offset);
        scroll_cnt--;
    }

    /* Scroll left */
    if(requested_menu_position < current_menu_position) {
        current_menu_offset++;
        render_menu(current_menu_offset);
        scroll_cnt--;
    }

    fLedmatrixGFX.render_framebuffer_to_screen();

    if(scroll_cnt == 0) {
        scroll_cnt = MENU_SCROLL_AMOUNT;
        current_menu_position = requested_menu_position;
        menu_transition_in_progress = false;
    }

}

void LedmatrixMenu::render_menu(int32_t x_offset) {

    fLedmatrixGFX.clear_framebuffer();

    for(uint8_t i = 0 ; i < MENUITEM_MAX ; i++) {
        /* Only draw the icon when the offset is near enough to zero to appear on screen */
        if(x_offset > (int32_t)0-MENU_SCROLL_AMOUNT) {
            fLedmatrixGFX.draw_num(x_offset, 0, menuitems[i].icon);
        }

        x_offset += MENU_SCROLL_AMOUNT;

        /* Break if the starting offset of the next icon would not fit on screen */
        if(x_offset >= (int8_t)MAX7219_LEDMATRIX_COLS) break;
    }

    /* Draw the selection columns */
    fLedmatrixGFX.fill_column_framebuffer(11);
    fLedmatrixGFX.fill_column_framebuffer(20);
}

void LedmatrixMenu::draw_menu() {
    is_active = true;
    render_menu(current_menu_offset);
    fLedmatrixGFX.do_transition(0u, fLedmatrixGFX.TR_RIGHT_TO_LEFT);
}
