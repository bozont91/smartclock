#ifndef LEDMATRIX_MENU_H
#define LEDMATRIX_MENU_H

#include <Arduino.h>
#include "../common.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../rotaryencoder/rotaryencoder.h"
#include "../ledmatrix_font/ledmatrix_font.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"
#include "../ledmatrix_screens/ledmatrix_screens.h"


class LedmatrixMenu {

public:

    LedmatrixMenu(
        RotaryencoderHAL &rotaryencoderhal,
        Logger &log,
        Beep &beep,
        LedmatrixGFX &ledmatrixgfx,
        LedmatrixScreens &ledmatrixscreens
    );

    enum ledmatrix_menu_items {
        MENU_TEMPERATURE,
        MENU_DATE,
        MENU_INFO,
        MENU_SNAKE,
        MENU_BRIGHTNESS,
        MENU_ALARM,
        MENU_CLOCK_STYLE,
        MENU_TETRIS,
        MENU_MUTE,
        MENU_EXIT,
        MENUITEM_MAX
    };

    bool get_active();
    void task();
    void deactivate();
    uint8_t get_current_menu();
    void process_encoder_input(uint8_t encoder_direction);
    void draw_menu();

private:

    typedef struct menuitem_s {
        uint8_t item;
        uint8_t icon;
    } menuitem;

    menuitem m1 = {MENU_TEMPERATURE, LEDMATRIX_ICON_WEATHER};
    menuitem m2 = {MENU_DATE, LEDMATRIX_ICON_DATE};
    menuitem m3 = {MENU_ALARM, LEDMATRIX_ICON_ALARM};
    menuitem m4 = {MENU_BRIGHTNESS, LEDMATRIX_ICON_BRIGHTNESS};
    menuitem m5 = {MENU_MUTE, LEDMATRIX_ICON_SOUND};
    menuitem m6 = {MENU_CLOCK_STYLE, LEDMATRIX_ICON_CLOCK};
    menuitem m7 = {MENU_SNAKE, LEDMATRIX_ICON_SNAKE};
    menuitem m8 = {MENU_TETRIS, LEDMATRIX_ICON_TETRIS};
    menuitem m9 = {MENU_INFO, LEDMATRIX_ICON_INFO};
    menuitem mA = {MENU_EXIT, LEDMATRIX_ICON_BACK};

    menuitem menuitems[MENUITEM_MAX] = {m1, m2, m3, m4, m5, m6, m7, m8, m9, mA};


    static const uint8_t MENU_OFFSET_DEFAULT = 12u;
    static const uint8_t MENU_SCROLL_AMOUNT = 10u;

    uint8_t current_menu_position = 0u;
    uint8_t requested_menu_position = 0u;

    int32_t current_menu_offset = MENU_OFFSET_DEFAULT;
    bool menu_transition_in_progress = false;

    void do_menu_scroll();
    void render_menu(int32_t x_offset);

    bool is_active = false;

    RotaryencoderHAL &fRotaryencoderHAL;
    Logger &fLog;
    Beep &fBeep;
    LedmatrixGFX &fLedmatrixGFX;
    LedmatrixScreens &fLedmatrixScreens;

};


#endif /* LEDMATRIX_MENU_H */
