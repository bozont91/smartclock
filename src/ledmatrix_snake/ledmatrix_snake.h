#ifndef LEDMATRIX_SNAKE_H
#define LEDMATRIX_SNAKE_H

#include <Arduino.h>
#include "../common.h"
#include "../esp_hal/esp_hal.h"
#include "../beep/beep.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"
#include "../rotaryencoder/rotaryencoder.h"
#include "../log/log.h"
#include "../settings/settings.h"

class LedmatrixSnake {

public:

    enum snake_directions {
        SNAKE_EMPTY,
        SNAKE_RIGHT,
        SNAKE_LEFT,
        SNAKE_UP,
        SNAKE_DOWN,
        SNAKE_FOOD
    };

    void new_game();
    void set_direction(uint8_t dir);
    uint8_t get_direction();
    void process_rotary_encoder_input(uint8_t encoder_direction);
    uint8_t get_score();
    uint8_t get_hiscore();
    void read_settings();
    bool task();
    LedmatrixSnake(EspHal &esphal, Beep &beep, LedmatrixGFX &ledmatrixgfx, Logger &log, Settings &settings);

private:

    uint8_t snake_direction = SNAKE_RIGHT;
    uint8_t snake_head_x = 0;
    uint8_t snake_head_y = 0;
    uint8_t snake_tail_x = 0;
    uint8_t snake_tail_y = 0;

    uint8_t LEDMATRIX_WIDTH;
    uint8_t LEDMATRIX_HEIGHT;

    static const uint8_t SNAKE_WAIT_CYCLES_DEFAULT = 200u;
    uint8_t snake_wait_cycles_current = SNAKE_WAIT_CYCLES_DEFAULT;

    inline void snake_setxy_framebuffer(uint8_t x, uint8_t y, uint8_t val);
    inline uint8_t snake_getxy_framebuffer(uint8_t x, uint8_t y);
    void spawn_food();

    uint8_t score = 0u;
    uint8_t hiscore = 0u;

    EspHal &fEspHal;
    Beep &fBeep;
    LedmatrixGFX &fLedmatrixGFX;
    Logger &fLog;
    Settings &fSettings;

};

#endif /* LEDMATRIX_SNAKE_H */
