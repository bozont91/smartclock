#include "ledmatrix_snake.h"

LedmatrixSnake::LedmatrixSnake(EspHal &esphal, Beep &beep, LedmatrixGFX &ledmatrixgfx, Logger &log, Settings &settings) :
fEspHal(esphal),
fBeep(beep),
fLedmatrixGFX(ledmatrixgfx),
fLog(log),
fSettings(settings)
{
    LEDMATRIX_WIDTH = MAX7219_LEDMATRIX_COLS;
    LEDMATRIX_HEIGHT = MAX7219_LEDMATRIX_ROWS;
}

void LedmatrixSnake::read_settings() {
    hiscore = fSettings.read_byte_s(SETT_SNAKE_HISCORE);
}

inline void LedmatrixSnake::snake_setxy_framebuffer(uint8_t x, uint8_t y, uint8_t val) {
    fLedmatrixGFX.setxy_framebuffer(x, y, val);
}

inline uint8_t LedmatrixSnake::snake_getxy_framebuffer(uint8_t x, uint8_t y) {
    return fLedmatrixGFX.getxy_framebuffer(x, y);
}

void LedmatrixSnake::spawn_food() {
    uint8_t food_x = 0u;
    uint8_t food_y = 0u;

    do {
        food_x = fEspHal.get_true_random_number()%LEDMATRIX_WIDTH;
        food_y = fEspHal.get_true_random_number()%LEDMATRIX_HEIGHT;
    } while(snake_getxy_framebuffer(food_x, food_y) != SNAKE_EMPTY);

    snake_setxy_framebuffer(food_x, food_y, SNAKE_FOOD);
}

void LedmatrixSnake::new_game() {
    fLog.log(LOG_SNAKE, L_INFO, F("New Game"));

    fLedmatrixGFX.clear_framebuffer();

    snake_head_x = 5;
    snake_head_y = 3;
    snake_tail_x = 1;
    snake_tail_y = 3;

    snake_setxy_framebuffer(1,3, SNAKE_RIGHT);
    snake_setxy_framebuffer(2,3, SNAKE_RIGHT);
    snake_setxy_framebuffer(3,3, SNAKE_RIGHT);
    snake_setxy_framebuffer(4,3, SNAKE_RIGHT);
    snake_setxy_framebuffer(5,3, SNAKE_RIGHT);

    score = 0u;
    snake_wait_cycles_current = SNAKE_WAIT_CYCLES_DEFAULT;
    snake_direction = SNAKE_RIGHT;
    spawn_food();
    fLedmatrixGFX.do_transition();
}

void LedmatrixSnake::set_direction(uint8_t dir) {
    snake_direction = dir;
}

uint8_t LedmatrixSnake::get_direction() {
    return snake_direction;
}

bool LedmatrixSnake::task() {

    static uint8_t snake_wait_cycles = 0;

    if(snake_wait_cycles != 0) {
        snake_wait_cycles--;
        return true;
    }

    snake_wait_cycles = snake_wait_cycles_current;

    /* Move the snake */

    /* Prevent the snake from moving into itself */
    uint8_t current_direction = snake_getxy_framebuffer(snake_head_x, snake_head_y);
    if(current_direction == SNAKE_RIGHT && snake_direction == SNAKE_LEFT) snake_direction = SNAKE_RIGHT;
    else if(current_direction == SNAKE_LEFT && snake_direction == SNAKE_RIGHT) snake_direction = SNAKE_LEFT;
    else if(current_direction == SNAKE_UP && snake_direction == SNAKE_DOWN) snake_direction = SNAKE_UP;
    else if(current_direction == SNAKE_DOWN && snake_direction == SNAKE_UP) snake_direction = SNAKE_DOWN;

    /* Add head */
    snake_setxy_framebuffer(snake_head_x,snake_head_y, snake_direction);
    switch(snake_direction) {
        case SNAKE_RIGHT:
            snake_head_x++;
            break;

        case SNAKE_LEFT:
            snake_head_x--;
            break;

        case SNAKE_UP:
            snake_head_y--;
            break;

        case SNAKE_DOWN:
            snake_head_y++;
            break;
    }

    /* Check if we hit food or ourselves */
    if(snake_getxy_framebuffer(snake_head_x, snake_head_y) == SNAKE_FOOD) {
        snake_setxy_framebuffer(snake_head_x,snake_head_y, snake_direction);
        spawn_food();
        if(snake_wait_cycles_current > 0) snake_wait_cycles_current -= 10;
        score++;
        fLog.log(LOG_SNAKE, L_INFO, F("Score: %d"), score);
        if(score == hiscore + 1) {
            fBeep.start_music(stronger_powerup_chime, MUSIC_BLOCK_STRONGER_POWERUP_CHIME_SIZE);
        } else if(score%5 == 0) {
            fBeep.start_music(strong_powerup_chime, MUSIC_BLOCK_STRONG_POWERUP_CHIME_SIZE);
        } else {
            fBeep.start_music(powerup_chime, MUSIC_BLOCK_POWERUP_CHIME_SIZE);
        }
        goto snake_next_round;
    } else if(snake_getxy_framebuffer(snake_head_x, snake_head_y) != SNAKE_EMPTY) {
        goto snake_collision;
    } else {
        snake_setxy_framebuffer(snake_head_x,snake_head_y, snake_direction);
    }

    /* Remove tail */
    switch(snake_getxy_framebuffer(snake_tail_x, snake_tail_y)) {
        case SNAKE_RIGHT:
            snake_setxy_framebuffer(snake_tail_x,snake_tail_y, SNAKE_EMPTY);
            snake_tail_x++;
            break;

        case SNAKE_LEFT:
            snake_setxy_framebuffer(snake_tail_x,snake_tail_y, SNAKE_EMPTY);
            snake_tail_x--;
            break;

        case SNAKE_UP:
            snake_setxy_framebuffer(snake_tail_x,snake_tail_y, SNAKE_EMPTY);
            snake_tail_y--;
            break;

        case SNAKE_DOWN:
            snake_setxy_framebuffer(snake_tail_x,snake_tail_y, SNAKE_EMPTY);
            snake_tail_y++;
            break;
    }

    /* Detect collision with the wall */
    if(snake_head_x == LEDMATRIX_WIDTH || snake_head_y == LEDMATRIX_HEIGHT || snake_head_x == 255 || snake_head_y == 255) {
        goto snake_collision;
    } else {
        goto snake_next_round;
    }

snake_collision:
    if(score > hiscore) {
        hiscore = score;
        fSettings.write(SETT_SNAKE_HISCORE, &hiscore);
    }
    fLog.log(LOG_SNAKE, L_INFO, F("Game Over! Score: %d Best: %d"), score, hiscore);
    fBeep.start_music(snake_collision_chime, MUSIC_BLOCK_SNAKECOLLISION_CHIME_SIZE);
    return false;

snake_next_round:
    fLedmatrixGFX.render_framebuffer_to_screen();
    return true;

}

void LedmatrixSnake::process_rotary_encoder_input(uint8_t encoder_direction) {
    uint8_t snake_direction = get_direction();
    uint8_t snake_new_direction = SNAKE_RIGHT;

    if(     snake_direction == SNAKE_RIGHT && encoder_direction == RotaryencoderHAL::RE_CLOCKWISE) { snake_new_direction = SNAKE_DOWN; }
    else if(snake_direction == SNAKE_RIGHT && encoder_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) { snake_new_direction = SNAKE_UP; }
    else if(snake_direction == SNAKE_LEFT && encoder_direction == RotaryencoderHAL::RE_CLOCKWISE) { snake_new_direction = SNAKE_UP; }
    else if(snake_direction == SNAKE_LEFT && encoder_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) { snake_new_direction = SNAKE_DOWN; }
    else if(snake_direction == SNAKE_UP && encoder_direction == RotaryencoderHAL::RE_CLOCKWISE) { snake_new_direction = SNAKE_RIGHT; }
    else if(snake_direction == SNAKE_UP && encoder_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) { snake_new_direction = SNAKE_LEFT; }
    else if(snake_direction == SNAKE_DOWN && encoder_direction == RotaryencoderHAL::RE_CLOCKWISE) { snake_new_direction = SNAKE_LEFT; }
    else if(snake_direction == SNAKE_DOWN && encoder_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) { snake_new_direction = SNAKE_RIGHT; }

    set_direction(snake_new_direction);

}

uint8_t LedmatrixSnake::get_score() {
    return score;
}

uint8_t LedmatrixSnake::get_hiscore() {
    return hiscore;
}
