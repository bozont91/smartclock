#include "internet_weather.h"


HTTPClient webweather_http_client;
WiFiClient webweather_wifi_client;

Webweather::Webweather(Logger &log, Settings &settings, JsonParser& jsonparser) :
fLog(log),
fSettings(settings),
fJsonParser(jsonparser)
{
    ;
}

void Webweather::read_settings() {
    set_temperature_unit(fSettings.read_byte_s(SETT_TEMPERATURE_UNIT));
    bool enabled = (bool)fSettings.read_byte_s(SETT_WEBWEATHER_ENABLED);
    if(enabled) {
        char api_key[fSettings.get_setting_length(SETT_OPENWEATHERMAP_API_KEY)];
        fSettings.read_block(SETT_OPENWEATHERMAP_API_KEY, (uint8_t *)api_key);
        char location[fSettings.get_setting_length(SETT_OPENWEATHERMAP_LOCATION)];
        fSettings.read_block(SETT_OPENWEATHERMAP_LOCATION, (uint8_t *)location);
        set_api_key(api_key);
        set_location(location);
        enable();
    }
}

int16_t Webweather::get_temp() {
    return (temperature_unit == TU_CELSIUS) ? owm_temp : (int16_t)convert_celsius_to_fahrenheit(owm_temp);
}

int16_t Webweather::get_temp_feels_like() {
    return (temperature_unit == TU_CELSIUS) ? owm_temp_feels_like : (int16_t)convert_celsius_to_fahrenheit(owm_temp_feels_like);
}

String Webweather::get_state() {
    return owm_weather_state;
}

String Webweather::get_state_as_text() {
    return owm_weahter_state_text;
}

int16_t Webweather::get_wind_speed() {
    return owm_wind_speed;
}

uint16_t Webweather::get_pressure() {
    return owm_pressure;
}

uint8_t Webweather::get_humidity() {
    return owm_humidity;
}

uint16_t Webweather::get_rain() {
    return owm_rain;
}

String Webweather::get_location() {
    return owm_city + ", " + owm_country;
}

bool Webweather::get_data_available() {
    return weather_data_up_to_date;
}

bool Webweather::get_enabled() {
    return service_enabled;
}

void Webweather::enable() {
    service_enabled = true;
    weather_data_up_to_date = false;
    ww_state = WW_SYNC_IN_PROGRESS;
    fLog.log(LOG_WEATHER, L_INFO, F("Service enabled"));
}

void Webweather::disable() {
    service_enabled = false;
    weather_data_up_to_date = false;
    ww_state = WW_DISABLED;
    notify_subscribers();
    fLog.log(LOG_WEATHER, L_INFO, F("Service disabled"));
}

uint8_t Webweather::get_temperature_unit() {
    return temperature_unit;
}

void Webweather::set_temperature_unit(uint8_t tu_new) {
    if(tu_new == temperature_unit) return;
    temperature_unit = tu_new;
    notify_subscribers(get_temp(), get_weather_icon());
}

void Webweather::set_api_key(char *api_key) {
    openweathermap_api_key = String(api_key);
}

void Webweather::set_location(char *location) {
    openweathermap_location = String(location);
}

bool Webweather::parse_weather_response() {
    String ret = "";

    /* Temporary variables for the received data - we only assign them to the real ones if everything checks out */
    int16_t t_owm_temp = 0;
    int16_t t_owm_temp_feels_like = 0;
    int16_t t_owm_wind_speed = 0;
    uint16_t t_owm_pressure = 0u;
    uint8_t t_owm_humidity = 0u;
    uint8_t t_owm_rain = 0u;
    String t_owm_weather_state = "";
    String t_owm_weahter_state_text = "";
    String t_owm_city = "";
    String t_owm_country = "";

    /* https://openweathermap.org/current */
    /* The service returns the temperature in Kelvin, so we convert it to Celsius */
    ret = fJsonParser.get_value_for_key(http_response_payload, "main/temp");
    if(ret.length() > 0) t_owm_temp = ret.toInt() - 273;
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "main/feels_like");
    if(ret.length() > 0) t_owm_temp_feels_like = ret.toInt() - 273;
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "weather/0/icon");
    if(ret.length() > 0) t_owm_weather_state = ret;
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "weather/0/main");
    if(ret.length() > 0) t_owm_weahter_state_text = ret;
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "wind/speed");
    if(ret.length() > 0) t_owm_wind_speed = ret.toInt();
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "main/pressure");
    if(ret.length() > 0) t_owm_pressure = ret.toInt();
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "main/humidity");
    if(ret.length() > 0) t_owm_humidity = ret.toInt();
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "rain/1h");
    if(ret.length() > 0) t_owm_rain = ret.toInt();
    else t_owm_rain = 0u;

    ret = fJsonParser.get_value_for_key(http_response_payload, "name");
    if(ret.length() > 0) t_owm_city = ret;
    else return false;

    ret = fJsonParser.get_value_for_key(http_response_payload, "sys/country");
    if(ret.length() > 0) t_owm_country = ret;
    else return false;

    /* Copy the data from the temporary variables */
    owm_temp = t_owm_temp;
    owm_temp_feels_like = t_owm_temp_feels_like;
    owm_wind_speed = t_owm_wind_speed;
    owm_pressure = t_owm_pressure;
    owm_humidity = t_owm_humidity;
    owm_rain = t_owm_rain;
    owm_weather_state = t_owm_weather_state;
    owm_weahter_state_text = t_owm_weahter_state_text;
    owm_city = t_owm_city;
    owm_country = t_owm_country;

    fLog.log(LOG_WEATHER, L_INFO, F("Location: %s"), get_location().c_str());
    fLog.log(LOG_WEATHER, L_INFO, F("Temperature: %d"), get_temp());
    fLog.log(LOG_WEATHER, L_INFO, F("Weather: %s (%s)"), owm_weahter_state_text.c_str(), owm_weather_state.c_str());

    http_response_payload = "";
    notify_subscribers(get_temp(), get_weather_icon());
    return true;
}

uint8_t Webweather::get_weather_icon() {

    /* https://openweathermap.org/weather-conditions */

    if(owm_weather_state.indexOf("13") >= 0) {
        return LEDMATRIX_ICON_SNOW;
    } else if(owm_weather_state.indexOf("11") >= 0) {
        return LEDMATRIX_ICON_THUNDERSTORM;
    } else if(owm_weather_state.indexOf("10") >= 0) {
        return LEDMATRIX_ICON_LIGHT_RAIN;
    } else if(owm_weather_state.indexOf("09") >= 0) {
        return LEDMATRIX_ICON_SHOWERS;
    } else if(owm_weather_state.indexOf("04") >= 0) {
        return LEDMATRIX_ICON_HEAVY_CLOUD;
    } else if(owm_weather_state.indexOf("02") >= 0 || owm_weather_state.indexOf("03") >= 0) {
        return LEDMATRIX_ICON_LIGHT_CLOUD;
    } else if(owm_weather_state.indexOf("01d") >= 0) {
        return LEDMATRIX_ICON_CLEAR;
    } else if(owm_weather_state.indexOf("01n") >= 0) {
        return LEDMATRIX_ICON_CLEAR_NIGHT;
    } else if (owm_weather_state.indexOf("50") >= 0) {
        return LEDMATRIX_ICON_MIST;
    }

    /* Return space if nothing matches */
    return LEDMATRIX_ICON_EMPTY;
}

uint8_t Webweather::get_sync_state() {
    return ww_state;
}

void Webweather::request_resync() {
    if(ww_state == WW_DISABLED) return;
    weather_data_up_to_date = false;
    notify_subscribers();
    ww_state = WW_SYNC_IN_PROGRESS;
    fLog.log(LOG_WEATHER, L_INFO, F("Invalidating data and requesting sync"));
}

bool Webweather::start_sync() {

    String sync_url = openweathermap_api_url_base;
    sync_url.replace("%LOCATION%", openweathermap_location);
    sync_url.replace("%API_KEY%", openweathermap_api_key);

    fLog.log(LOG_WEATHER, L_INFO, F("Starting weather sync..."));

    if(!webweather_http_client.begin(webweather_wifi_client, sync_url)) {
        fLog.log(LOG_WEATHER, L_ERROR, F("Connection failed!"));
        return false;
    }

    int httpCode = webweather_http_client.GET();

    if(httpCode <= 0) {
        fLog.log(LOG_WEATHER, L_ERROR, F("HTTP GET failed, error: %s"), webweather_http_client.errorToString(httpCode).c_str());
        webweather_http_client.end();
        return false;
    }

    fLog.log(LOG_WEATHER, L_INFO, F("HTTP response code: %d"), httpCode);

    if(httpCode != HTTP_CODE_OK) {
        fLog.log(LOG_WEATHER, L_ERROR, F("Request failed!"));
        webweather_http_client.end();
        return false;
    }

    http_response_payload = webweather_http_client.getString();
    webweather_http_client.end();

    /* Trim the result to 600 chars */
    http_response_payload.remove(600);

    //fLog.log(LOG_WEATHER, L_INFO, F("Payload len: %d"), http_response_payload.length());
    //fLog.log(LOG_WEATHER, L_INFO, F("Payload: %s"), http_response_payload.c_str());
    //Serial.println(http_response_payload);

    if(!parse_weather_response()) {
        fLog.log(LOG_WEATHER, L_ERROR, F("Parsing response failed!"));
        return false;
    }

    return true;
}

void Webweather::subscribe(WeatherSubscriber *sub) {
    subscriber_list.push_back(sub);
}

void Webweather::unsubscribe(WeatherSubscriber *sub) {
    subscriber_list.remove(sub);
}

void Webweather::notify_subscribers(const int16_t temperature, const uint8_t state_icon, bool validity) {
    for(auto &i : subscriber_list) {
        i->weather_change_callback(temperature, state_icon, validity);
    }
}

void Webweather::notify_subscribers() {
    notify_subscribers(0, LEDMATRIX_ICON_EMPTY, false);
}

void Webweather::task() {

    switch(ww_state) {
        case WW_DISABLED:
            break;

        case WW_SYNC_IN_PROGRESS:
            last_sync_timestamp = device_uptime;
            if(start_sync()) {
                ww_state = WW_SLEEP;
                sync_failure_cnt = 0u;
                weather_data_up_to_date = true;
            } else {
                ww_state = WW_SYNC_FAILED;
                sync_failure_cnt++;
                if(sync_failure_cnt == WEATHER_DATA_OUTDATED_TRESHOLD) {
                    weather_data_up_to_date = false;
                    notify_subscribers();
                    fLog.log(LOG_WEATHER, L_INFO, F("Marking weather data as outdated"));
                }
                fLog.log(LOG_WEATHER, L_INFO, F("Retrying sync in %d seconds"), SYNC_FAILED_RETRY_TIME_SEC);
            }
            yield();
            break;

        case WW_SLEEP:
            if(last_sync_timestamp + (WEATHER_SYNC_FREQUENCY_MINUTES * 60) <= device_uptime) {
                ww_state = WW_SYNC_IN_PROGRESS;
            }
            break;

        case WW_SYNC_FAILED:
            if(last_sync_timestamp + SYNC_FAILED_RETRY_TIME_SEC <= device_uptime) {
                ww_state = WW_SYNC_IN_PROGRESS;
            }
            break;
    }

}
