#ifndef INTERNET_WEATHER_H
#define INTERNET_WEATHER_H

#ifndef AURA_UNITTEST
    #include <Arduino.h>
    #include <list>
    #include <ESP8266HTTPClient.h>
    #include "../log/log.h"
    #include "../ledmatrix_font/ledmatrix_font.h"
    #include "../settings/settings.h"
    #include "../json_parser/json_parser.h"
#else
    #include <Arduino.h>
    #include <list>
    #include "../json_parser/json_parser.h"
    #include "../../tests/mock/esp8266_core/WString.h"
    #include "../../tests/mock/esp8266_core/ESP8266HTTPClient.h"
    #include "../../tests/mock/common.h"
    #include "../../tests/mock/log.h"
    #include "../../tests/mock/settings.h"
    #include "../../tests/mock/ledmatrix_font.h"
#endif


class WeatherSubscriber {
public:
    virtual ~WeatherSubscriber(){};
    virtual void weather_change_callback(const int16_t temperature, const uint8_t state_icon, bool validity) = 0;
};


class WeatherPublisher {
public:
    virtual ~WeatherPublisher(){};
    virtual void subscribe(WeatherSubscriber *sub) = 0;
    virtual void unsubscribe(WeatherSubscriber *sub) = 0;
    virtual void notify_subscribers(const int16_t temperature, const uint8_t state_icon, bool validity) = 0;
};


class Webweather : public WeatherPublisher {

public:

    Webweather(Logger &log, Settings &settings, JsonParser& jsonparser);

    enum ww_state_e {
        WW_DISABLED,
        WW_SYNC_IN_PROGRESS,
        WW_SLEEP,
        WW_SYNC_FAILED
    };

    void read_settings();
    int16_t get_temp();
    int16_t get_temp_feels_like();
    String get_state();
    String get_state_as_text();
    int16_t get_wind_speed();
    uint16_t get_pressure();
    uint8_t get_humidity();
    uint16_t get_rain();
    String get_location();
    uint8_t get_weather_icon();
    bool get_data_available();
    bool get_enabled();
    uint8_t get_sync_state();
    void request_resync();
    void enable();
    void disable();
    void set_api_key(char *api_key);
    void set_location(char *location);
    void subscribe(WeatherSubscriber *sub) override;
    void unsubscribe(WeatherSubscriber *sub) override;
    uint8_t get_temperature_unit();
    void set_temperature_unit(uint8_t tu_new);
    void task();

private:

    bool service_enabled = false;

    const uint8_t SYNC_FAILED_RETRY_TIME_SEC = 60u;
    const uint8_t WEATHER_DATA_OUTDATED_TRESHOLD = 5u;

    uint8_t ww_state = WW_DISABLED;
    uint32_t last_sync_timestamp = 0u;

    String openweathermap_api_key = "";
    String openweathermap_location = "";

    const String openweathermap_api_url_base = "http://api.openweathermap.org/data/2.5/weather?q=%LOCATION%&appid=%API_KEY%";

    int16_t owm_temp = 0;
    int16_t owm_temp_feels_like = 0;
    int16_t owm_wind_speed = 0;
    uint16_t owm_pressure = 0u;
    uint8_t owm_humidity = 0u;
    uint16_t owm_rain = 0u;
    String owm_weather_state = "";
    String owm_weahter_state_text = "";
    String owm_city = "";
    String owm_country = "";

    String http_response_payload;
    uint32_t sync_failure_cnt = 0u;
    bool weather_data_up_to_date = false;
    uint8_t temperature_unit = TU_CELSIUS;

    bool start_sync();
    bool parse_weather_response();

    void notify_subscribers(const int16_t temperature, const uint8_t state_icon, bool validity = true) override;
    void notify_subscribers();
    std::list<WeatherSubscriber*> subscriber_list;

    Logger &fLog;
    Settings &fSettings;
    JsonParser& fJsonParser;

};


#endif /* INTERNET_WEATHER_H */
