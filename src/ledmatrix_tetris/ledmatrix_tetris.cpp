#include "ledmatrix_tetris.h"


LedmatrixTetris::LedmatrixTetris(
    LedmatrixGFX &ledmatrixgfx,
    Beep &beep,
    EspHal &esphal,
    Logger &log,
    Settings &settings
) :
fLedmatrixGFX(ledmatrixgfx),
fBeep(beep),
fEspHal(esphal),
fLog(log),
fSettings(settings)
{
    ;
}

void LedmatrixTetris::read_settings() {
    fSettings.read_block(SETT_TETRIS_HISCORE, (uint8_t*)&hiscore);
}

void LedmatrixTetris::new_game() {
    fLedmatrixGFX.clear_framebuffer();

    if(saved_game_framebuffer) {
        load_game();
    } else {
        score = 0u;
        is_block_falling = false;
        requested_move_direction = TETRIS_NONE;
        fLog.log(LOG_TETRIS, L_INFO, F("New Game"));
    }

    fLedmatrixGFX.do_transition();
}

void LedmatrixTetris::save_game() {
    if(saved_game_framebuffer) return;

    fLog.log(LOG_TETRIS, L_INFO, F("Saving game"), score);

    saved_game_framebuffer = (uint8_t*)malloc(MAX7219_LEDMATRIX_ROWS * MAX7219_LEDMATRIX_COLS * sizeof(uint8_t));

    for(uint8_t y = 0 ; y < MAX7219_LEDMATRIX_ROWS ; y++) {
        for(uint8_t x = 0 ; x < MAX7219_LEDMATRIX_COLS ; x++) {
            saved_game_framebuffer[(y * MAX7219_LEDMATRIX_COLS) + x] = fLedmatrixGFX.getxy_framebuffer(x, y);
        }
    }
}

void LedmatrixTetris::load_game() {

    fLog.log(LOG_TETRIS, L_INFO, F("Loading saved game"));

    for(uint8_t y = 0 ; y < MAX7219_LEDMATRIX_ROWS ; y++) {
        for(uint8_t x = 0 ; x < MAX7219_LEDMATRIX_COLS ; x++) {
            fLedmatrixGFX.setxy_framebuffer(x, y, saved_game_framebuffer[(y * MAX7219_LEDMATRIX_COLS) + x]);
        }
    }

    free(saved_game_framebuffer);
    saved_game_framebuffer = NULL;
}

inline void LedmatrixTetris::tetris_setxy_framebuffer(uint8_t x, uint8_t y, uint8_t val) {
    fLedmatrixGFX.setxy_framebuffer(x, y, val);
}

inline uint8_t LedmatrixTetris::tetris_getxy_framebuffer(uint8_t x, uint8_t y) {
    if(x >= MAX7219_LEDMATRIX_COLS || y >= MAX7219_LEDMATRIX_ROWS) return TETRIS_EMPTY;
    return fLedmatrixGFX.getxy_framebuffer(x, y);
}

void LedmatrixTetris::process_rotary_encoder_input(uint8_t encoder_direction) {
    if(encoder_direction == RotaryencoderHAL::RE_CLOCKWISE) {
        request_game_input(TETRIS_DOWN);
    } else if(encoder_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) {
        request_game_input(TETRIS_UP);
    }
}

void LedmatrixTetris::request_game_input(uint8_t move) {
    requested_move_direction = move;
}

inline void LedmatrixTetris::spawn_block() {
    const uint8_t *current_block_to_spawn;

    switch(falling_block_type) {
        case T_SQUARE:
            falling_block_matrix_size = 2;
            current_block_to_spawn = T_SQUARE_GFX;
            break;

        case T_LINE:
            falling_block_matrix_size = 4;
            current_block_to_spawn = T_LINE_GFX;
            break;

        case T_TEE:
            falling_block_matrix_size = 3;
            current_block_to_spawn = T_TEE_GFX;
            break;

        case T_L_PIECE:
            falling_block_matrix_size = 3;
            current_block_to_spawn = T_L_GFX;
            break;

        case T_J_PIECE:
            falling_block_matrix_size = 3;
            current_block_to_spawn = T_J_GFX;
            break;

        case T_S_PIECE:
            falling_block_matrix_size = 3;
            current_block_to_spawn = T_S_GFX;
            break;

        case T_Z_PIECE:
            falling_block_matrix_size = 3;
            current_block_to_spawn = T_Z_GFX;
            break;

        default:
            current_block_to_spawn = T_SQUARE_GFX;
            break;
    }

    for(uint8_t j = 0 ; j < falling_block_matrix_size ; j++) {
        for(uint8_t i = 0; i < falling_block_matrix_size ; i++) {
            if(current_block_to_spawn[(j * falling_block_matrix_size) + i] == 1) {
                tetris_setxy_framebuffer(falling_block_anchor_x + i, falling_block_anchor_y + j, TETRIS_FALLING);
            }
        }
    }

}

inline void LedmatrixTetris::render_block_move(uint8_t direction) {

    uint8_t falling_block_matrix_buffer[falling_block_matrix_size][falling_block_matrix_size];
    memset(falling_block_matrix_buffer, TETRIS_EMPTY, falling_block_matrix_size * falling_block_matrix_size);

    /* Save the current state of the enclosing matrix for the falling block - and at the same time delete it from the framebuffer */
    for(uint8_t y = 0 ; y < falling_block_matrix_size ; y++) {
        for(uint8_t x = 0 ; x < falling_block_matrix_size ; x++) {

            int8_t x_read_position = falling_block_anchor_x + x;
            int8_t y_read_position = falling_block_anchor_y + y;

            if(y_read_position < 0) {
                continue;
            } else if(tetris_getxy_framebuffer(x_read_position, y_read_position) == TETRIS_FALLING) {
                falling_block_matrix_buffer[x][y] = TETRIS_FALLING;
                tetris_setxy_framebuffer(x_read_position, y_read_position, TETRIS_EMPTY);
            }

        }
    }

    /* Determine the moving direction */
    switch(direction) {

        case TETRIS_UP:
            falling_block_anchor_y--;
            break;

        case TETRIS_DOWN:
            falling_block_anchor_y++;
            break;

        case TETRIS_RIGHT:
            falling_block_anchor_x++;
            break;
    }

    /* Draw the enclosing matrix back to the framebuffer */
    for(uint8_t y = 0 ; y < falling_block_matrix_size ; y++) {
        for(uint8_t x = 0 ; x < falling_block_matrix_size ; x++) {

            int8_t x_write_position = falling_block_anchor_x + x;
            int8_t y_write_position = falling_block_anchor_y + y;

            if(y_write_position < 0) {
                continue;
            } else if(falling_block_matrix_buffer[x][y] == TETRIS_FALLING) {
                tetris_setxy_framebuffer(x_write_position, y_write_position, TETRIS_FALLING);
            }

        }
    }


}

inline void LedmatrixTetris::render_block_rotation() {

    uint8_t falling_block_matrix_buffer[falling_block_matrix_size][falling_block_matrix_size];
    memset(falling_block_matrix_buffer, TETRIS_EMPTY, falling_block_matrix_size * falling_block_matrix_size);

    /* Save the current state of the enclosing matrix for the falling block */
    for(uint8_t y = 0 ; y < falling_block_matrix_size ; y++) {
        for(uint8_t x = 0 ; x < falling_block_matrix_size ; x++) {

            int8_t x_read_position = falling_block_anchor_x + x;
            int8_t y_read_position = falling_block_anchor_y + y;

            if(y_read_position < 0) {
                continue;
            } else if(tetris_getxy_framebuffer(x_read_position, y_read_position) == TETRIS_FALLING) {
                falling_block_matrix_buffer[x][y] = TETRIS_FALLING;
            }

        }
    }

    /* Rotate the matrix */
    for(uint8_t x = 0; x < falling_block_matrix_size / 2; x++) {
        for(uint8_t y = x; y < falling_block_matrix_size-x-1; y++) {
            /* Store current value */
            uint8_t temp = falling_block_matrix_buffer[x][y];

            /* Move values from right to top */
            falling_block_matrix_buffer[x][y] = falling_block_matrix_buffer[y][falling_block_matrix_size-1-x];

            /* Move values from bottom to right */
            falling_block_matrix_buffer[y][falling_block_matrix_size-1-x] = falling_block_matrix_buffer[falling_block_matrix_size-1-x][falling_block_matrix_size-1-y];

            /* Move values from left to bottom */
            falling_block_matrix_buffer[falling_block_matrix_size-1-x][falling_block_matrix_size-1-y] = falling_block_matrix_buffer[falling_block_matrix_size-1-y][x];

            /* Assign temp to left */
            falling_block_matrix_buffer[falling_block_matrix_size-1-y][x] = temp;
        }
    }

    /* Check if the block would fall off the screen after rotation */
    /* If we fall off, try shifting the block upwards - if it would cause trouble it will fail on the check below */
    int8_t drawback_anchor_y = falling_block_anchor_y;
    if(drawback_anchor_y + falling_block_matrix_size - 1 >= (int8_t)MAX7219_LEDMATRIX_ROWS) {
        while(drawback_anchor_y + falling_block_matrix_size - 1 >= (int8_t)MAX7219_LEDMATRIX_ROWS) drawback_anchor_y--;
    }

    /* If we fall off at the top, try shifting the block downwards - if it would cause trouble it will fail on the check below */
    if(drawback_anchor_y < 0) {
        while(drawback_anchor_y < 0) drawback_anchor_y++;
    }

    /* Check if we would overwrite something fixed after the rotation */
    for(uint8_t j = 0 ; j < falling_block_matrix_size ; j++) {
        for(uint8_t i = 0; i < falling_block_matrix_size ; i++) {
            if(falling_block_matrix_buffer[i][j] == TETRIS_FALLING && tetris_getxy_framebuffer(i + falling_block_anchor_x, j + drawback_anchor_y) == TETRIS_FIXED) {
                return;
            }
        }
    }

    /* Delete the current state from the framebuffer */
    for(int8_t i = falling_block_anchor_y ; i < falling_block_anchor_y + falling_block_matrix_size ; i++) {
        for(int8_t j = falling_block_anchor_x ; j < falling_block_anchor_x + falling_block_matrix_size ; j++) {
            if(i < 0) {
                continue;
            } else if(tetris_getxy_framebuffer(j, i) == TETRIS_FALLING) {
                tetris_setxy_framebuffer(j, i, TETRIS_EMPTY);
            }
        }
    }

    falling_block_anchor_y = drawback_anchor_y;

    /* Draw the rotated enclosing matrix back to the framebuffer */
    for(uint8_t y = 0 ; y < falling_block_matrix_size ; y++) {
        for(uint8_t x = 0 ; x < falling_block_matrix_size ; x++) {

            int8_t x_write_position = falling_block_anchor_x + x;
            int8_t y_write_position = falling_block_anchor_y + y;

            if(y_write_position < 0) {
                continue;
            } else if(falling_block_matrix_buffer[x][y] == TETRIS_FALLING) {
                tetris_setxy_framebuffer(x_write_position, y_write_position, TETRIS_FALLING);
            }

        }
    }


}

inline bool LedmatrixTetris::handle_touchdown() {

    uint8_t check_start = falling_block_anchor_x;
    uint8_t check_end = falling_block_anchor_x + 5;

    if(check_end >= (uint8_t)MAX7219_LEDMATRIX_COLS) check_end = MAX7219_LEDMATRIX_COLS;

    for(uint8_t i = check_start ; i < check_end ; i++) {
        for(uint8_t j = 0 ; j < MAX7219_LEDMATRIX_ROWS ; j++) {
            if
            (
                (tetris_getxy_framebuffer(i, j) == TETRIS_FALLING) &&
                ((tetris_getxy_framebuffer(i+1, j) == TETRIS_FIXED) || (i + 1 == MAX7219_LEDMATRIX_COLS))
            )
            {
                /* Change the block type in the framebuffer to fixed */
                for(int8_t k = falling_block_anchor_y ; k < falling_block_anchor_y + falling_block_matrix_size ; k++) {
                    for(int8_t l = falling_block_anchor_x ; l < falling_block_anchor_x + falling_block_matrix_size ; l++) {
                        if(k < 0) {
                            continue;
                        } else if(tetris_getxy_framebuffer(l, k) == TETRIS_FALLING) {
                            tetris_setxy_framebuffer(l, k, TETRIS_FIXED);
                        }
                    }
                }

                is_block_falling = false;
                fBeep.play_tone(MUSICAL_E5, DURATION_SIXTEENTH);
                //Serial.printf("Touchdown; xanchor: %d\r\n", falling_block_anchor_x);
                quickfall_in_progress = false;
                return true;
            }
        }
    }

    return false;

}

void LedmatrixTetris::move_falling_block(uint8_t direction) {

    if(!is_block_falling) return;

    bool not_on_edge = true;
    bool no_neighbour_block = true;
    if(direction == TETRIS_UP) {
        /* Check for edges */
        for(uint8_t i = 0 ; i < MAX7219_LEDMATRIX_COLS ; i++) {
            if(tetris_getxy_framebuffer(i, 0) == TETRIS_FALLING) {
                not_on_edge = false;
            }
        }

        /* Check for neighbouring fixed blocks */
        for(uint8_t i =  falling_block_anchor_x; i < MAX7219_LEDMATRIX_COLS ; i++) {
            if(
                tetris_getxy_framebuffer(i, falling_block_anchor_y-1) == TETRIS_FIXED &&
                tetris_getxy_framebuffer(i, falling_block_anchor_y) == TETRIS_FALLING
            ) {
                no_neighbour_block = false;
            }
        }

        if(not_on_edge && no_neighbour_block) {
            render_block_move(TETRIS_UP);
            fLedmatrixGFX.render_framebuffer_to_screen();
        }

        return;
    }

    not_on_edge = true;
    no_neighbour_block = true;
    if(direction == TETRIS_DOWN) {
        /* Check for edges */
        for(uint8_t i = 0 ; i < MAX7219_LEDMATRIX_COLS; i++) {
            if(tetris_getxy_framebuffer(i, 7u) == TETRIS_FALLING) {
                not_on_edge = false;
            }
        }

        /* Check for neighbouring fixed blocks */
        for(uint8_t i =  falling_block_anchor_x; i < MAX7219_LEDMATRIX_COLS ; i++) {
            if(
                tetris_getxy_framebuffer(i, falling_block_anchor_y+1) == TETRIS_FIXED &&
                tetris_getxy_framebuffer(i, falling_block_anchor_y) == TETRIS_FALLING
            ) {
                no_neighbour_block = false;
            }
        }

        if(not_on_edge && no_neighbour_block) {
            render_block_move(TETRIS_DOWN);
            fLedmatrixGFX.render_framebuffer_to_screen();
        }

        return;
    }

    if(direction == TETRIS_ROTATE) {
        render_block_rotation();
        fLedmatrixGFX.render_framebuffer_to_screen();
    }

}

void LedmatrixTetris::process_game_input() {
    /* Execute movement requested by outside controls */
    if(requested_move_direction == TETRIS_DOWN) {
        move_falling_block(TETRIS_DOWN);
        requested_move_direction = TETRIS_NONE;
    } else if(requested_move_direction == TETRIS_UP) {
        move_falling_block(TETRIS_UP);
        requested_move_direction = TETRIS_NONE;
    } else if(requested_move_direction == TETRIS_ROTATE) {
        move_falling_block(TETRIS_ROTATE);
        requested_move_direction = TETRIS_NONE;
    }
}

void LedmatrixTetris::do_quickfall() {
    quickfall_in_progress = true;
    tetris_wait_cycles = TETRIS_WAIT_CYCLES_QUICKFALL;
}

inline bool LedmatrixTetris::check_for_full_line() {
    for(uint8_t i = 0 ; i < MAX7219_LEDMATRIX_COLS; i++) {
        for(uint8_t j = 0 ; j < MAX7219_LEDMATRIX_ROWS ; j++) {
            if(tetris_getxy_framebuffer(i, j) != TETRIS_FIXED) break;
            if(j == MAX7219_LEDMATRIX_ROWS - 1) {
                full_line_x = i;
                //Serial.printf("Full line found; x: %d\r\n", full_line_x);
                return true;
            }
        }
    }
    return false;
}

inline void LedmatrixTetris::delete_full_line() {
    /* Delete the line which is full and move everything above it down by 1 */

    for(uint8_t i = 0 ; i < MAX7219_LEDMATRIX_ROWS ; i++) {
        tetris_setxy_framebuffer(full_line_x, i, TETRIS_EMPTY);
    }

    for(uint8_t i = full_line_x-1 ; i != 0 ; i--) {
        for(uint8_t j = 0 ; j < MAX7219_LEDMATRIX_ROWS ; j++) {
            /* Copy the current line to the line to the right */
            tetris_setxy_framebuffer(i + 1, j, tetris_getxy_framebuffer(i, j));
        }
    }

}

uint16_t LedmatrixTetris::get_score() {
    return score;
}

uint16_t LedmatrixTetris::get_hiscore() {
    return hiscore;
}


bool LedmatrixTetris::task() {

    if(tetris_wait_cycles != 0) {
        tetris_wait_cycles--;
        /* Allow an input at half of the wait cycle - game is more responsive and playable */
        if(tetris_wait_cycles == TETRIS_WAIT_CYCLES_DEFAULT / 2) process_game_input();
        return true;
    }


    if(!quickfall_in_progress) {
        tetris_wait_cycles = TETRIS_WAIT_CYCLES_DEFAULT;
    } else {
        tetris_wait_cycles = TETRIS_WAIT_CYCLES_QUICKFALL;
    }

    /* Check if we have a full line which needs to be deleted */
    if(check_for_full_lines && check_for_full_line()) {
        delete_full_line();
        score++;
        fLog.log(LOG_TETRIS, L_INFO, F("Score: %d"), score);

        if(score == hiscore + 1) {
            fBeep.start_music(stronger_powerup_chime, MUSIC_BLOCK_STRONGER_POWERUP_CHIME_SIZE);
        } else if(score%10 == 0) {
            fBeep.start_music(strong_powerup_chime, MUSIC_BLOCK_STRONG_POWERUP_CHIME_SIZE);
        } else {
            fBeep.start_music(powerup_chime, MUSIC_BLOCK_POWERUP_CHIME_SIZE);
        }

        tetris_wait_cycles = 0;
        return true;
    } else {
        check_for_full_lines = false;
    }

    /* Check if our falling block reached the end, or some fixed block */
    /* and if a touchdown happened, return */
    if(is_block_falling && handle_touchdown()) {
        check_for_full_lines = true;
        return true;
    }

    /* Check if we have reached the top */
    for(uint8_t i = 0u ; i < MAX7219_LEDMATRIX_ROWS ; i++) {
        if(tetris_getxy_framebuffer(0, i) == TETRIS_FIXED) {
            fBeep.start_music(snake_collision_chime, MUSIC_BLOCK_SNAKECOLLISION_CHIME_SIZE);
            if(score > hiscore) {
                hiscore = score;
                fSettings.write(SETT_TETRIS_HISCORE, (uint8_t*)&hiscore);
            }
            fLog.log(LOG_TETRIS, L_INFO, F("Game Over! Score: %d Best: %d"), score, hiscore);
            return false;
        }
    }

    /* Move if we have something falling */
    if(is_block_falling) {
        render_block_move(TETRIS_RIGHT);
        fLedmatrixGFX.render_framebuffer_to_screen();
        process_game_input();
    }

    /* Spawn a new block if nothing is falling */
    if(!is_block_falling) {
        falling_block_type = fEspHal.get_true_random_number() % T_MAX;
        falling_block_anchor_x = 0u;
        falling_block_anchor_y = 3u;
        spawn_block();
        is_block_falling = true;
        requested_move_direction = TETRIS_NONE;
        fLedmatrixGFX.render_framebuffer_to_screen();
        //Serial.printf("Spawn new block; type='%d'\r\n", falling_block_type);
    }

    return true;

}
