#ifndef LEDMATRIX_TETRIS_H
#define LEDMATRIX_TETRIS_H

#include <Arduino.h>
#include "../common.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"
#include "../beep/beep.h"
#include "../esp_hal/esp_hal.h"
#include "../log/log.h"
#include "../rotaryencoder/rotaryencoder.h"
#include "../settings/settings.h"

class LedmatrixTetris {

public:

    enum Tetris_driections {
        TETRIS_NONE,
        TETRIS_UP,
        TETRIS_DOWN,
        TETRIS_RIGHT,
        TETRIS_ROTATE
    };

    LedmatrixTetris(
        LedmatrixGFX &ledmatrixgfx,
        Beep &beep,
        EspHal &esphal,
        Logger &log,
        Settings &settings
    );

    void new_game();
    void save_game();
    void load_game();
    void process_rotary_encoder_input(uint8_t encoder_direction);
    void request_game_input(uint8_t move);
    void do_quickfall();
    uint16_t get_score();
    uint16_t get_hiscore();
    void read_settings();
    bool task();

private:
    enum Tetrominoes {
        T_SQUARE,
        T_LINE,
        T_TEE,
        T_L_PIECE,
        T_J_PIECE,
        T_S_PIECE,
        T_Z_PIECE,
        T_MAX
    };

    enum Tetris_pixel_types {
        TETRIS_EMPTY,
        TETRIS_FIXED,
        TETRIS_FALLING
    };

    inline void tetris_setxy_framebuffer(uint8_t x, uint8_t y, uint8_t val);
    inline uint8_t tetris_getxy_framebuffer(uint8_t x, uint8_t y);
    inline void spawn_block();
    inline void render_block_move(uint8_t direction);
    inline void render_block_rotation();
    inline bool handle_touchdown();
    inline bool check_for_full_line();
    inline void delete_full_line();
    void move_falling_block(uint8_t direction);
    void process_game_input();

    bool is_block_falling = false;
    bool quickfall_in_progress = false;
    bool check_for_full_lines = false;
    uint8_t falling_block_type = T_MAX;
    int8_t falling_block_anchor_x = 0u;
    int8_t falling_block_anchor_y = 0u;
    uint8_t falling_block_matrix_size = 0u;
    uint8_t full_line_x = 0u;
    uint8_t requested_move_direction = TETRIS_NONE;

    static const uint8_t TETRIS_WAIT_CYCLES_DEFAULT = 150u;
    static const uint8_t TETRIS_WAIT_CYCLES_QUICKFALL = 5u;
    uint8_t tetris_wait_cycles = 0u;

    uint16_t score = 0u;
    uint16_t hiscore = 0u;
    uint8_t *saved_game_framebuffer = NULL;

    LedmatrixGFX &fLedmatrixGFX;
    Beep &fBeep;
    EspHal &fEspHal;
    Logger &fLog;
    Settings &fSettings;

    const uint8_t T_SQUARE_GFX[4] = {
        1, 1,
        1, 1
    };

    const uint8_t T_LINE_GFX[16] = {
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    };

    const uint8_t T_TEE_GFX[9] = {
        1, 1, 1,
        0, 1, 0,
        0, 0, 0
    };

    const uint8_t T_L_GFX[9] = {
        0, 1, 0,
        0, 1, 0,
        0, 1, 1
    };

    const uint8_t T_J_GFX[9] = {
        0, 1, 0,
        0, 1, 0,
        1, 1, 0
    };

    const uint8_t T_S_GFX[9] = {
        0, 1, 1,
        1, 1, 0,
        0, 0, 0
    };

    const uint8_t T_Z_GFX[9] = {
        1, 1, 0,
        0, 1, 1,
        0, 0, 0
    };


};

#endif /* LEDMATRIX_TETRIS_H */
