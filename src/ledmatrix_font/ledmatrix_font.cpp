#include "ledmatrix_font.h"

const byte ledmatrix_number_font[][8] = {
{ // 0
  B01110000,
  B10001000,
  B10001000,
  B10001000,
  B10001000,
  B10001000,
  B10001000,
  B01110000
},
{ // 1
  B00100000,
  B01100000,
  B00100000,
  B00100000,
  B00100000,
  B00100000,
  B00100000,
  B01110000
},{ // 2
  B01110000,
  B10001000,
  B00001000,
  B00001000,
  B00010000,
  B00100000,
  B01000000,
  B11111000
},{ // 3
  B01110000,
  B10001000,
  B00001000,
  B00110000,
  B00001000,
  B00001000,
  B10001000,
  B01110000
},{ // 4
  B00001000,
  B00011000,
  B00101000,
  B01001000,
  B10001000,
  B11111000,
  B00001000,
  B00001000
},{ // 5
  B11111000,
  B10000000,
  B10000000,
  B11110000,
  B00001000,
  B00001000,
  B10001000,
  B01110000
},{ // 6
  B01110000,
  B10001000,
  B10000000,
  B11110000,
  B10001000,
  B10001000,
  B10001000,
  B01110000
},{ // 7
  B11111000,
  B00001000,
  B00001000,
  B00010000,
  B00100000,
  B01000000,
  B01000000,
  B01000000
},{ // 8
  B01110000,
  B10001000,
  B10001000,
  B01110000,
  B10001000,
  B10001000,
  B10001000,
  B01110000
},{ // 9
  B01110000,
  B10001000,
  B10001000,
  B10001000,
  B01111000,
  B00001000,
  B10001000,
  B01110000
},{ // empty
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},{ // colon
  B00000000,
  B00000000,
  B00000000,
  B01000000,
  B00000000,
  B00000000,
  B01000000,
  B00000000
},{ // dot
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B01000000
},{ // celsius
  B11100000,
  B10100000,
  B11100000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},{ // snow (14)
  B01111110,
  B10000001,
  B10000001,
  B01111110,
  B00000000,
  B01010101,
  B00100010,
  B01010101
},{ // sleet
  B01111110,
  B10000001,
  B10000001,
  B01111110,
  B00000000,
  B01010101,
  B00100101,
  B01010101
},{ // mist
  B01111110,
  B10000001,
  B10000001,
  B01111110,
  B00000000,
  B01110111,
  B00000000,
  B01110111
},{ // thunderstorms
  B01111110,
  B10000001,
  B10000001,
  B01111110,
  B00000100,
  B01010010,
  B01010100,
  B01010010
},{ // heavy rain
  B01111110,
  B10000001,
  B10000001,
  B01111110,
  B00000000,
  B01010100,
  B01010100,
  B01010100
},{ // light rain
  B01111110,
  B10000001,
  B10000001,
  B01111110,
  B00000000,
  B00101000,
  B00000000,
  B00101000
},{ // showers
  B01110000,
  B10001001,
  B10001010,
  B01110010,
  B00000001,
  B01010000,
  B00000000,
  B01010000
},{ // heavy cloud
  B01111000,
  B10000100,
  B10000110,
  B10000101,
  B01111001,
  B00001110,
  B00000000,
  B00000000
},{ // light cloud
  B01110000,
  B10001001,
  B10001010,
  B01110010,
  B00000001,
  B00000000,
  B00000000,
  B00000000
},{ // sunny
  B00000000,
  B00010000,
  B01000100,
  B00111000,
  B10101010,
  B00111000,
  B01000100,
  B00010000
},{ // Clear night
  B00000000,
  B01111000,
  B11100000,
  B11000000,
  B11000000,
  B11100000,
  B01111000,
  B00000000
},{ // AM
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11100000,
  B10100000,
  B11100000,
  B10100000
},{ // PM
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11100000,
  B10100000,
  B11100000,
  B10000000
},{ // WiFi icon
  B00000000,
  B01111110,
  B10000001,
  B00111100,
  B01000010,
  B00011000,
  B00000000,
  B00011000
},{ // Up icon
  B00010000,
  B00111000,
  B01111100,
  B11111110,
  B00111000,
  B00111000,
  B00111000,
  B00111000
},{ // Weather icon
  B00000000,
  B01100000,
  B01100110,
  B00001000,
  B00001000,
  B00001000,
  B00000110,
  B00000000
},{ // Date icon
  B01110110,
  B00000000,
  B01110010,
  B00010110,
  B00110010,
  B00010010,
  B01110010,
  B00000000
},{ // Back icon
  B00000000,
  B00000100,
  B00111110,
  B01000100,
  B01000000,
  B01000000,
  B00111100,
  B00000000
},{ // Snake icon
  B00000000,
  B01110010,
  B01010000,
  B01010010,
  B01010010,
  B01010010,
  B01011110,
  B00000000
},{ // Info icon
  B00000000,
  B00011000,
  B00000000,
  B00111000,
  B00011000,
  B00011000,
  B00111100,
  B00000000
},{ // Brightness icon
  B00000000,
  B01111110,
  B01000010,
  B01000010,
  B01111110,
  B00011000,
  B00111100,
  B00000000
},{ // Alarm icon
  B00000000,
  B00011000,
  B00100100,
  B00100100,
  B01000010,
  B01111110,
  B00011000,
  B00000000
},{ // Tetris icon
  B00000000,
  B00000000,
  B01111110,
  B01111110,
  B00011000,
  B00011000,
  B00000000,
  B00000000
},{ // Sound icon
  B00000000,
  B00010000,
  B00011000,
  B00010100,
  B00010100,
  B00110000,
  B00110000,
  B00000000
},{ // Yes icon
  B00000000,
  B00000010,
  B00000110,
  B00001100,
  B01011000,
  B01110000,
  B00100000,
  B00000000
},{ // No icon
  B00000000,
  B01100110,
  B00111100,
  B00011000,
  B00011000,
  B00111100,
  B01100110,
  B00000000
},{ // Dash icon
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11111000,
  B00000000,
  B00000000,
  B00000000
},{ // Menu clock icon
  B00000000,
  B00010000,
  B00010000,
  B00010000,
  B00010000,
  B00001000,
  B00000100,
  B00000000
},{ // Left arrow icon
  B00000000,
  B00010000,
  B00110000,
  B01100000,
  B11000000,
  B01100000,
  B00110000,
  B00010000
},{ // Right arrow icon
  B00000000,
  B00001000,
  B00001100,
  B00000110,
  B00000011,
  B00000110,
  B00001100,
  B00001000
}
};

const int ledmatrix_number_font_len = sizeof(ledmatrix_number_font)/8;


const byte ledmatrix_small_number_font[][5] = {
{ // 0
  B00000111,
  B00000101,
  B00000101,
  B00000101,
  B00000111
},{ // 1
  B00000001,
  B00000011,
  B00000001,
  B00000001,
  B00000001
},{ // 2
  B00000111,
  B00000001,
  B00000111,
  B00000100,
  B00000111
},{ // 3
  B00000111,
  B00000001,
  B00000011,
  B00000001,
  B00000111
},{ // 4
  B00000101,
  B00000101,
  B00000111,
  B00000001,
  B00000001
},{ // 5
  B00000111,
  B00000100,
  B00000111,
  B00000001,
  B00000111
},{ // 6
  B00000111,
  B00000100,
  B00000111,
  B00000101,
  B00000111
},{ // 7
  B00000111,
  B00000001,
  B00000001,
  B00000001,
  B00000001
},{ // 8
  B00000111,
  B00000101,
  B00000111,
  B00000101,
  B00000111
},{ // 9
  B00000111,
  B00000101,
  B00000111,
  B00000001,
  B00000111
},{ // colon
  B00000000,
  B00000010,
  B00000000,
  B00000010,
  B00000000
},{ // celsius
  B00000010,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},{ // dash
  B00000000,
  B00000000,
  B00000111,
  B00000000,
  B00000000
}
};

const int ledmatrix_small_number_font_len = sizeof(ledmatrix_small_number_font)/5;

/*
 Well, well - the size of the first dimension of this array requires some explanation.
 This may be a complier or linker bug.
 If I leave the size blank, it will mess up the webserver HTML template and cause an exception, which is not related at all.
 The only relation is that they are linked after each other in irom.text:

 .irom.text.ledmatrix_font.cpp.344.0
                0x0000000040252668      0x3d5 /tmp/arduino-sketch-46FE38A118D66C7BE720FD8E76F0FED5/sketch/src/ledmatrix_font/ledmatrix_font.cpp.o
 .irom.text.webserver.cpp.8.2
                0x0000000040252a3d      0x7d4 /tmp/arduino-sketch-46FE38A118D66C7BE720FD8E76F0FED5/sketch/src/webserver/webserver.cpp.o

 The exception I get is:

 Exception 3: LoadStoreError: Processor internal physical address or data error during load or store
 PC: 0x4000bf64
 EXCVADDR: 0x4025291d
 0x40209ccc: Webserver::task() at /tmp/arduino_build_407489/sketch/src/webserver/webserver.cpp line 205

 If I give it the exact size (eg. 109) it will still be messed up, however if I decrease the size by one (by deleting an initializer) to 108, it will work as blank.

 So, in order to work this around, I've given it a much larger size, than it really is. (it only takes up flash, which we have plenty of)
 Be careful when adding new elements, always make sure that the array is bigger than it's contents!

*/

const PROGMEM uint8_t ledmatrix_letter_font[150][9] = {
{ // uppercase letters
  2,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11111100,
  B11001100,
  B11001100,
  B11001100
},{
  6,
  B00000000,
  B11111000,
  B11001100,
  B11001100,
  B11111000,
  B11001100,
  B11001100,
  B11111000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11000000,
  B11000000,
  B11000000,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B11111000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B11111000
},{
  6,
  B00000000,
  B11111100,
  B11000000,
  B11000000,
  B11111000,
  B11000000,
  B11000000,
  B11111100
},{
  6,
  B00000000,
  B11111100,
  B11000000,
  B11000000,
  B11111000,
  B11000000,
  B11000000,
  B11000000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11000000,
  B11000000,
  B11011100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11111100,
  B11001100,
  B11001100,
  B11001100
},{
  4,
  B00000000,
  B11110000,
  B01100000,
  B01100000,
  B01100000,
  B01100000,
  B01100000,
  B11110000
},{
  6,
  B00000000,
  B00111100,
  B00011000,
  B00011000,
  B00011000,
  B11011000,
  B11011000,
  B01110000
},{
  6,
  B00000000,
  B11001100,
  B11011000,
  B11110000,
  B11100000,
  B11110000,
  B11011000,
  B11001100
},{
  6,
  B00000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11111100
},{
  7,
  B00000000,
  B11000110,
  B11101110,
  B11111110,
  B11010110,
  B11000110,
  B11000110,
  B11000110
},{
  7,
  B00000000,
  B11000110,
  B11100110,
  B11110110,
  B11011110,
  B11001110,
  B11000110,
  B11000110
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B11111000,
  B11001100,
  B11001100,
  B11001100,
  B11111000,
  B11000000,
  B11000000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11001100,
  B11011100,
  B01111000,
  B00001100
},{
  6,
  B00000000,
  B11111000,
  B11001100,
  B11001100,
  B11111000,
  B11110000,
  B11011000,
  B11001100
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11000000,
  B01111000,
  B00001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B11111100,
  B10110100,
  B00110000,
  B00110000,
  B00110000,
  B00110000,
  B00110000
},{
  6,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111100
},{
  6,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111000,
  B00110000
},{
  7,
  B00000000,
  B11000110,
  B11000110,
  B11000110,
  B11010110,
  B11111110,
  B11101110,
  B11000110
},{
  7,
  B00000000,
  B11000110,
  B11000110,
  B01101100,
  B00111000,
  B01101100,
  B11000110,
  B11000110
},{
  6,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B01111000,
  B00110000,
  B00110000,
  B00110000
},{
  6,
  B00000000,
  B11111100,
  B00001100,
  B00011000,
  B00110000,
  B01100000,
  B11000000,
  B11111100
},
/* Lowercase letters*/
{
  6,
  B00000000,
  B00000000,
  B00000000,
  B01111000,
  B00001100,
  B01111100,
  B11001100,
  B01111100
},{
  6,
  B00000000,
  B11000000,
  B11000000,
  B11000000,
  B11111000,
  B11001100,
  B11001100,
  B11111000
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B01111000,
  B11001100,
  B11000000,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B00001100,
  B00001100,
  B00001100,
  B01111100,
  B11001100,
  B11001100,
  B01111100
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B01111000,
  B11001100,
  B11111100,
  B11000000,
  B01111000
},{
  6,
  B00000000,
  B00111000,
  B01101100,
  B01100000,
  B01100000,
  B11111000,
  B01100000,
  B01100000
},{
  6,
  B00000000,
  B00000000,
  B01111100,
  B11001100,
  B11001100,
  B01111100,
  B00001100,
  B01111000
},{
  6,
  B00000000,
  B11000000,
  B11000000,
  B11000000,
  B11111000,
  B11001100,
  B11001100,
  B11001100
},{
  4,
  B00000000,
  B00000000,
  B01100000,
  B00000000,
  B01100000,
  B01100000,
  B01100000,
  B11110000
},{
  5,
  B00000000,
  B00011000,
  B00000000,
  B00011000,
  B00011000,
  B11011000,
  B11011000,
  B01110000
},{
  6,
  B00000000,
  B11000000,
  B11000000,
  B11001100,
  B11011000,
  B11110000,
  B11011000,
  B11001100
},{
  2,
  B00000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000
},{
  7,
  B00000000,
  B00000000,
  B00000000,
  B11000110,
  B11101110,
  B11111110,
  B11010110,
  B11010110
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B11111000,
  B11111100,
  B11001100,
  B11001100,
  B11001100
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B00000000,
  B11111000,
  B11001100,
  B11001100,
  B11111000,
  B11000000,
  B11000000
},{
  5,
  B00000000,
  B00000000,
  B01111000,
  B11011000,
  B11011000,
  B01111000,
  B00011000,
  B00011000
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B11111000,
  B11001100,
  B11001100,
  B11000000,
  B11000000
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B01111100,
  B10000000,
  B01111000,
  B00000100,
  B11111000
},{
  6,
  B00000000,
  B00000000,
  B00110000,
  B00110000,
  B11111100,
  B00110000,
  B00110000,
  B00110000
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111100
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11001100,
  B11001100,
  B01111000,
  B00110000
},{
  7,
  B00000000,
  B00000000,
  B00000000,
  B11000110,
  B11010110,
  B11010110,
  B11010110,
  B01111100
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B11001100,
  B01111000,
  B00110000,
  B01111000,
  B11001100
},{
  6,
  B00000000,
  B00000000,
  B00000000,
  B11001100,
  B11001100,
  B01111100,
  B00001100,
  B01111000
},{
  4,
  B00000000,
  B00000000,
  B00000000,
  B11110000,
  B00110000,
  B01100000,
  B11000000,
  B11110000
},
/* Numbers start here*/
{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11011100,
  B11101100,
  B11001100,
  B11001100,
  B01111000
},
{
  6,
  B00000000,
  B00110000,
  B00110000,
  B01110000,
  B00110000,
  B00110000,
  B00110000,
  B11111100
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B00001100,
  B00011000,
  B01100000,
  B11000000,
  B11111100
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B00001100,
  B00111000,
  B00001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B00011000,
  B00111000,
  B01011000,
  B10011000,
  B11111100,
  B00011000,
  B00011000
},{
  6,
  B00000000,
  B11111100,
  B11000000,
  B11111000,
  B00001100,
  B00001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11000000,
  B11111000,
  B11001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B11111100,
  B11001100,
  B00011000,
  B00011000,
  B00110000,
  B00110000,
  B00110000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B01111000,
  B11001100,
  B11001100,
  B01111000
},{
  6,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B01111100,
  B00001100,
  B11001100,
  B01111000
},
/* Symbols start here */
{ /* !  ASCII 33*/
  4,
  B00000000,
  B01100000,
  B11110000,
  B11110000,
  B01100000,
  B01100000,
  B00000000,
  B01100000
},{ /* " */
  5,
  B00000000,
  B11011000,
  B11011000,
  B01010000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},{ /* # */
  7,
  B00000000,
  B01101100,
  B01101100,
  B11111110,
  B01101100,
  B11111110,
  B01101100,
  B01101100
},{ /* $ */
  5,
  B00000000,
  B00100000,
  B01111000,
  B10000000,
  B01110000,
  B00001000,
  B11110000,
  B00100000
},{ /* % */
  6,
  B00000000,
  B11000000,
  B11001100,
  B00011000,
  B00110000,
  B01100000,
  B11001100,
  B00001100
},{ /* & */
  7,
  B00000000,
  B01111000,
  B11001100,
  B01111000,
  B01010000,
  B11001010,
  B11001100,
  B01111110
},{ /* '  */
  3,
  B00000000,
  B11000000,
  B11000000,
  B01100000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
},{ /* ( */
  4,
  B00000000,
  B00110000,
  B01100000,
  B11000000,
  B11000000,
  B11000000,
  B01100000,
  B00110000
},{ /* ) */
  4,
  B00000000,
  B11000000,
  B01100000,
  B00110000,
  B00110000,
  B00110000,
  B01100000,
  B11000000
},{ /* * */
  7,
  B00000000,
  B00000000,
  B01101100,
  B00111000,
  B11111110,
  B00111000,
  B01101100,
  B00000000
},{ /* + */
  5,
  B00000000,
  B00000000,
  B00100000,
  B00100000,
  B11111000,
  B00100000,
  B00100000,
  B00000000
},{ /* , */
  3,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B01100000,
  B01100000,
  B01100000,
  B11000000
},{ /* - */
  4,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11110000,
  B00000000,
  B00000000,
  B00000000
},{ /* . */
  2,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11000000,
  B11000000
},{ /* /  ASCII 47 */
  6,
  B00000000,
  B00000000,
  B00001100,
  B00011000,
  B00110000,
  B01100000,
  B11000000,
  B00000000
},{ /* :  ASCII 58 */
  2,
  B00000000,
  B00000000,
  B11000000,
  B11000000,
  B00000000,
  B11000000,
  B11000000,
  B00000000
},{ /* ; */
  3,
  B00000000,
  B00000000,
  B01100000,
  B01100000,
  B00000000,
  B01100000,
  B01100000,
  B11000000
},{ /* < */
  5,
  B00000000,
  B00011000,
  B00110000,
  B01100000,
  B11000000,
  B01100000,
  B00110000,
  B00011000
},{ /* = */
  4,
  B00000000,
  B00000000,
  B00000000,
  B11110000,
  B00000000,
  B11110000,
  B00000000,
  B00000000
},{ /* > */
  5,
  B00000000,
  B11000000,
  B01100000,
  B00110000,
  B00011000,
  B00110000,
  B01100000,
  B11000000
},{ /* ? */
  6,
  B00000000,
  B01111000,
  B11001100,
  B00001100,
  B00111000,
  B00110000,
  B00000000,
  B00110000
},{ /* @  ASCII 64*/
  6,
  B00000000,
  B01110000,
  B10001000,
  B10111000,
  B10110000,
  B10000100,
  B01111000,
  B00000000
},{ /* [  ASCII 91 */
  4,
  B00000000,
  B11110000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11110000
},{ /* \ */
  6,
  B00000000,
  B00000000,
  B11000000,
  B01100000,
  B00110000,
  B00011000,
  B00001100,
  B00000000
},{ /* ] */
  4,
  B00000000,
  B11110000,
  B00110000,
  B00110000,
  B00110000,
  B00110000,
  B00110000,
  B11110000
},{ /* ^ */
  7,
  B00000000,
  B00010000,
  B00101000,
  B01000100,
  B10000010,
  B00000000,
  B00000000,
  B00000000
},{ /* _ */
  4,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B11110000
},{ /* ` <3  ASCII 96 */
  7,
  B00000000,
  B01000100,
  B11101110,
  B11111110,
  B11111110,
  B01111100,
  B00111000,
  B00010000
},{ /* {  ASCII 123 */
  5,
  B00000000,
  B00111000,
  B01100000,
  B01100000,
  B11000000,
  B01100000,
  B01100000,
  B00111000
},{ /* | */
  2,
  B00000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000,
  B11000000
},{ /* } */
  5,
  B00000000,
  B11100000,
  B00110000,
  B00110000,
  B00011000,
  B00110000,
  B00110000,
  B11100000
},{ /* ~  ASCII 126 */
  6,
  B00000000,
  B00000000,
  B00000000,
  B01110100,
  B11011000,
  B00000000,
  B00000000,
  B00000000
},

{ /* á (133) */
  6,
  B00001000,
  B00010000,
  B00000000,
  B01111000,
  B00001100,
  B01111100,
  B11001100,
  B01111100
},{ /* é */
  6,
  B00001000,
  B00010000,
  B00000000,
  B01111000,
  B11001100,
  B11111100,
  B11000000,
  B01111000
},{ /* í */
  4,
  B00000000,
  B00100000,
  B01000000,
  B00000000,
  B01100000,
  B01100000,
  B01100000,
  B11110000
},{ /* ó */
  6,
  B00001000,
  B00010000,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11001100,
  B01111000
},{ /* ö */
  6,
  B00000000,
  B00101000,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11001100,
  B01111000
},{ /* ő */
  6,
  B00100100,
  B01001000,
  B00000000,
  B01111000,
  B11001100,
  B11001100,
  B11001100,
  B01111000
},{ /* ú */
  6,
  B00001000,
  B00010000,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111100
},{ /* ü */
  6,
  B00000000,
  B00101000,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111100
},{ /* ű */
  6,
  B00010100,
  B00101000,
  B00000000,
  B11001100,
  B11001100,
  B11001100,
  B11001100,
  B01111100
},{
  8, /* Face with tears of joy */
  B00000000,
  B01100110,
  B01100110,
  B10000001,
  B00000000,
  B01111110,
  B01000010,
  B00111100
},{ /* Crying face */
  8,
  B00000000,
  B01100110,
  B01100110,
  B10000000,
  B10000000,
  B00111100,
  B01000010,
  B00000000
},{ /* Smiling face with open mouth */
  8,
  B00000000,
  B01100110,
  B01100110,
  B00000000,
  B00000000,
  B01111110,
  B01000010,
  B00111100
},{ /* B button */
  8,
  B11111111,
  B10000011,
  B10011001,
  B10011001,
  B10000011,
  B10011001,
  B10011001,
  B10000011
},{ // degree
  3,
  B11100000,
  B10100000,
  B11100000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
}
};

const int ledmatrix_letter_font_size = sizeof(ledmatrix_letter_font)/9;

const uint8_t* ledmatrix_ascii_table[128];

void ledmatrix_ascii_table_init() {
  /* Fill all the places with space */
  for(uint8_t i = 0 ; i < 128 ; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[0];
  }

  /* Fill the capital letters (26) */
  #define ASCII_CAPTIAL_LETTERS_START_INDEX 65u
  #define ASCII_CAPTIAL_LETTERS_END_INDEX 91u
  uint8_t ledmatrix_character_offset_cnt = 1u;
  for(uint8_t i = ASCII_CAPTIAL_LETTERS_START_INDEX; i < ASCII_CAPTIAL_LETTERS_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

  /* Fill the lower case letters (26) */
  #define ASCII_LOWERCASE_LETTERS_START_INDEX 97u
  #define ASCII_LOWERCASE_LETTERS_END_INDEX 123u
  ledmatrix_character_offset_cnt = 27u;
  for(uint8_t i = ASCII_LOWERCASE_LETTERS_START_INDEX; i < ASCII_LOWERCASE_LETTERS_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

  /* Fill the numbers (10) */
  #define ASCII_NUMBERS_START_INDEX 48u
  #define ASCII_NUMBERS_END_INDEX 58u
  ledmatrix_character_offset_cnt = 53u;
  for(uint8_t i = ASCII_NUMBERS_START_INDEX; i < ASCII_NUMBERS_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

  /* Fill symbol group 1 (15) */
  #define ASCII_SYMBOL_GROUP_1_START_INDEX 33u
  #define ASCII_SYMBOL_GROUP_1_END_INDEX 48u
  ledmatrix_character_offset_cnt = 63u;
  for(uint8_t i = ASCII_SYMBOL_GROUP_1_START_INDEX; i < ASCII_SYMBOL_GROUP_1_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

  /* Fill symbol group 2 (7) */
  #define ASCII_SYMBOL_GROUP_2_START_INDEX 58u
  #define ASCII_SYMBOL_GROUP_2_END_INDEX 65u
  ledmatrix_character_offset_cnt = 78u;
  for(uint8_t i = ASCII_SYMBOL_GROUP_2_START_INDEX; i < ASCII_SYMBOL_GROUP_2_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

  /* Fill symbol group 3 (6) */
  #define ASCII_SYMBOL_GROUP_3_START_INDEX 91u
  #define ASCII_SYMBOL_GROUP_3_END_INDEX 97u
  ledmatrix_character_offset_cnt = 85u;
  for(uint8_t i = ASCII_SYMBOL_GROUP_3_START_INDEX; i < ASCII_SYMBOL_GROUP_3_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

  /* Fill symbol group 4 (4) */
  #define ASCII_SYMBOL_GROUP_4_START_INDEX 123u
  #define ASCII_SYMBOL_GROUP_4_END_INDEX 127u
  ledmatrix_character_offset_cnt = 91u;
  for(uint8_t i = ASCII_SYMBOL_GROUP_4_START_INDEX; i < ASCII_SYMBOL_GROUP_4_END_INDEX; i++) {
    ledmatrix_ascii_table[i] = ledmatrix_letter_font[ledmatrix_character_offset_cnt];
    ledmatrix_character_offset_cnt++;
  }

}


#define UTF8_LUT_SIZE 14u
/* UTF-8 Look Up Table */
                          /* UTF-8 code | Local array index */
static const uint32_t utf8_lut[UTF8_LUT_SIZE][2] = {
                            {0xc3a1,      95u},  /* á */
                            {0xc3a9,      96u},  /* é */
                            {0xc3ad,      97u},  /* í */
                            {0xc3b3,      98u},  /* ó */
                            {0xc3b6,      99u},  /* ö */
                            {0xc591,      100u}, /* ő */
                            {0xc3ba,      101u}, /* ú */
                            {0xc3bc,      102u}, /* ü */
                            {0xc5b1,      103u}, /* ű */
                            {0xf09f9882,  104u}, /* Face with tears of joy */
                            {0xf09f98a2,  105u}, /* Crying face */
                            {0xf09f9883,  106u}, /* Smiling face with open mouth */
                            {0xf09f85b1,  107u}, /* B Button */
                            {0xc2b0,      108u}, /* Degree sign */
};

/* This function converts an UTF-8 string to an array of pointers to the character data which should be drawn */
void convert_utf8_string_to_character_data(const uint8_t* output_data_array[], String text_in) {

  /* Make our String fit the output */
  if(text_in.length() >= 256) text_in.remove(255);

  uint16_t output_position = 0u;

  for(uint16_t i = 0u ; text_in[i] != '\0' ; i++) {

    /* If the character is ASCII, fetch it from the ASCII table */
    if(text_in[i] < 127) {
      output_data_array[output_position] = ledmatrix_ascii_table[(uint8_t)text_in[i]];
      output_position++;
      continue;
    }

    /* If the character is UTF-8, it needs further handling */

    uint8_t current_char = (uint8_t)text_in[i];

    /* Determine the byte length of the UTF-8 character */
    /* Check how many bits are 1 before we encounter a 0 */
    uint8_t utf8_length = 0u;
    uint8_t bit_offset = 7;
    while(current_char & (1 << bit_offset)) {
      if(bit_offset == 0) break;
      utf8_length++;
      bit_offset--;
    }

    /* Check that length doesn't exceed 4 bytes */
    /* If the length exceeds 4 bytes then something went really wrong, we just leave the string as it is and return */
    if(utf8_length > 4u) break;

    /* Check if the requested length doesn't go over the end of our string */
    if(i + utf8_length > text_in.length()) break;

    // Serial.printf("UTF-8 len: %d\n", utf8_length);

    /* Combine the calculated number of bytes together to get our UTF-8 character */
    uint32_t utf8_char = 0u;
    uint8_t combine_shift = (utf8_length * 8) - 8;
    for(uint8_t j = i ; j < i + utf8_length ; j++) {
      utf8_char |= ((uint8_t)text_in[j] << combine_shift);
      combine_shift -= 8;
    }

    // Serial.printf("UTF-8 char: 0x%x\n", utf8_char);

    /* Search the Look Up Table for a matching character */
    bool character_match_found = false;
    for(uint8_t k = 0u ; k < UTF8_LUT_SIZE ; k++) {
      if(utf8_lut[k][0] == utf8_char) {
        uint8_t character_data_index = utf8_lut[k][1];
        output_data_array[output_position] = ledmatrix_letter_font[character_data_index];
        character_match_found = true;
        break;
      }
    }

    /* If we don't have the requested character, print a space */
    if(character_match_found == false) {
      output_data_array[output_position] = ledmatrix_ascii_table[32];
    }


    output_position++;

    /* Step the iterator forward to the end of the current UTF-8 character */
    i += utf8_length - 1;

  }

  /* Terminate the output data with NULL */
  output_data_array[output_position] = NULL;

}
