#include "ledmatrix_main.h"


LedmatrixStateManager::LedmatrixStateManager(
    RotaryencoderHAL &rotaryencoderhal,
    LedmatrixHal &ledmatrixhal,
    LedmatrixGFX &ledmatrixgfx,
    LedmatrixSnake &ledmatrixsnake,
    LedmatrixTetris &tetris,
    LedmatrixScreens &ledmatrixscreens,
    LedmatrixCountdownTimer &ledmatrixcountdowntimer,
    LedmatrixMenu &ledmatrixmenu,
    MuteManager &mutemanager,
    Webweather &webweather,
    Settings &settings
) :
fRotaryencoderHAL(rotaryencoderhal),
fLedmatrixHal(ledmatrixhal),
fLedmatrixGFX(ledmatrixgfx),
fSnake(ledmatrixsnake),
fTetris(tetris),
fLedmatrixScreens(ledmatrixscreens),
fLedmatrixCountdownTimer(ledmatrixcountdowntimer),
fLedmatrixMenu(ledmatrixmenu),
fMuteManager(mutemanager),
fWebweather(webweather),
fSettings(settings)
{
    ;
}

void LedmatrixStateManager::read_settings() {
    mute_when_screen_is_off = fSettings.read_byte_s(SETT_MUTE_WHEN_SCREEN_IS_OFF);
}

void LedmatrixStateManager::set_mute_when_screen_is_off(bool state) {
    mute_when_screen_is_off = state;
}

bool LedmatrixStateManager::get_mute_when_screen_is_off() {
    return mute_when_screen_is_off;
}

void LedmatrixStateManager::set_current_screen(uint8_t requested_screen) {
    current_screen = requested_screen;
}

uint8_t LedmatrixStateManager::get_current_screen() {
    return current_screen;
}

void LedmatrixStateManager::show_text(String text, uint8_t screen_after_text) {
    screen_after_text_scroll = screen_after_text;
    fLedmatrixGFX.setup_text_scroll(text);
    set_current_screen(SCREEN_SHOWTEXT);
}

void LedmatrixStateManager::display_on() {
    bool global_mute = fSettings.read_byte_s(SETT_MUTE);
    if(!global_mute && mute_when_screen_is_off) fMuteManager.unmute();

    fLedmatrixHal.startup();
    set_current_screen(SCREEN_CLOCK);
}

void LedmatrixStateManager::display_off() {
    bool global_mute = fSettings.read_byte_s(SETT_MUTE);
    if(!global_mute && mute_when_screen_is_off) fMuteManager.mute();

    set_current_screen(SCREEN_LEDMATRIX_SHUTDOWN);
}

bool LedmatrixStateManager::get_display_on() {
    return fLedmatrixHal.get_on_state();
}

void LedmatrixStateManager::display_game_over_and_score(uint16_t score, uint16_t hiscore) {
    char score_text_buffer[100];
    sprintf_P(score_text_buffer, PSTR(" Game Over! Score: %d  Best: %d"), score, hiscore);
    show_text(String(score_text_buffer));
}

void LedmatrixStateManager::delayed_state_timer_setup(uint8_t next_state, uint16_t timer) {
    delayed_state_timer_next_state = next_state;
    delayed_state_timer_time = timer;
}

void LedmatrixStateManager::delayed_state_timer_tick() {
    if(delayed_state_timer_time == 0) {
        set_current_screen(delayed_state_timer_next_state);
        return;
    }
    delayed_state_timer_time--;
}

void LedmatrixStateManager::task() {

    static uint8_t previous_screen = 255u;

    /* If the display is off, do no processing */
    if(fLedmatrixHal.get_on_state() == false) return;

    static uint32_t display_delay_cycles = 0;
    if(display_delay_cycles > 0) {
        display_delay_cycles--;
        return;
    }

    if(fLedmatrixGFX.is_transitioning()) {
        if(fLedmatrixGFX.render_framebuffer_transition_to_screen()) fLedmatrixGFX.end_transition();
        display_delay_cycles = 6u;
        return;
    }

    /* Things we have to do when transitioning into a state */
    if(previous_screen != current_screen) {
        previous_screen = current_screen;

        switch(current_screen) {
        case SCREEN_LOADING:
            if(welcomewizard_active) return;
            fLedmatrixGFX.render_text_to_framebuffer("aura", 0, 1);
            fLedmatrixGFX.do_transition(0u, fLedmatrixGFX.TR_BOTTOM_TO_TOP);
            break;

        case SCREEN_CLOCK:
            fLedmatrixScreens.draw_clock(fLedmatrixGFX.LEDMATRIX_DO_TRANSITION);
            break;

        case SCREEN_YEAR:
            fLedmatrixScreens.draw_year();
            delayed_state_timer_setup(SCREEN_DATE, 600u);
            break;

        case SCREEN_DATE:
            fLedmatrixScreens.draw_date();
            delayed_state_timer_setup(SCREEN_CLOCK, 600u);
            break;

        case SCREEN_TEMPERATURE:
            fLedmatrixScreens.draw_inside_temperature();
            if(fWebweather.get_enabled()) {
                delayed_state_timer_setup(SCREEN_WEATHER, 1200u);
            } else {
                delayed_state_timer_setup(SCREEN_CLOCK, 1200u);
            }
            break;

        case SCREEN_WEATHER:
            fLedmatrixScreens.draw_weather();
            delayed_state_timer_setup(SCREEN_CLOCK, 1200u);
            break;

        case SCREEN_SNAKE:
            fSnake.new_game();
            break;

        case SCREEN_TETRIS:
            fTetris.new_game();
            break;

        case SCREEN_LEDMATRIX_SHUTDOWN:
            fLedmatrixGFX.draw_shutdown();
            break;

        case SCREEN_SHOWTEXT:
            //ledmatrix_show_text();
            //ledmatrix_delayed_state_timer_setup(SCREEN_CLOCK, 240u);
            break;

        case SCREEN_COUNTDOWN_TIMER:
            fLedmatrixCountdownTimer.draw();
            fLedmatrixGFX.do_transition();
            break;

        case SCREEN_WELCOME:
            fLedmatrixGFX.clear_framebuffer();
            fLedmatrixGFX.render_text_to_framebuffer("Hello.", 1, 0);
            fLedmatrixGFX.do_transition();
            if(welcomewizard_active) {
                delayed_state_timer_setup(SCREEN_WELCOMEWIZARD, 1000u);
            } else {
                delayed_state_timer_setup(SCREEN_CLOCK, 1400u);
            }

            break;

        case SCREEN_WELCOMEWIZARD:
            fLedmatrixGFX.clear_framebuffer();
            fLedmatrixGFX.draw_num(1, 0, LEDMATRIX_ICON_WIFI);
            fLedmatrixGFX.draw_num(10, 0, LEDMATRIX_ICON_UP);
            fLedmatrixGFX.do_transition();
            break;

        case SCREEN_MENU:
            fLedmatrixMenu.draw_menu();
            break;

        case SCREEN_BRIGHTNESSADJUST:
            fLedmatrixScreens.draw_brightness_adjust_screen();
            break;

        case SCREEN_SETALARM:
            fLedmatrixScreens.draw_alarm_setting_screen();
            break;

        case SCREEN_SETMUTE:
            fLedmatrixScreens.draw_mute_setting_screen();
            break;

        case SCREEN_CLOCKSTYLE_SELECT:
            fLedmatrixScreens.draw_clockstyle_select_screen();
            break;

        case SCREEN_CLOCKTYPE_SELECT:
            fLedmatrixScreens.draw_clocktype_select_screen();
            break;

        }

        return;

    }

    /* Things we have to do periodically, when in a state */
    switch(current_screen) {
        case SCREEN_LOADING:
            fLedmatrixScreens.draw_loading_anim();
            display_delay_cycles = 10u;
            break;

        case SCREEN_CLOCK:
            fLedmatrixScreens.screen_clock_periodic();
            display_delay_cycles = 10u;
            break;

        case SCREEN_YEAR:
        case SCREEN_DATE:
        case SCREEN_TEMPERATURE:
        case SCREEN_WEATHER:
        case SCREEN_WELCOME:
            delayed_state_timer_tick();
            break;

        case SCREEN_SHOWTEXT:
            if(fLedmatrixGFX.scroll_text() == false) {
                set_current_screen(screen_after_text_scroll);
            } else {
                display_delay_cycles = 17u;
            }
            break;

        case SCREEN_SNAKE:
            if(fSnake.task() == false) {
                display_game_over_and_score(fSnake.get_score(), fSnake.get_hiscore());
            }
            break;

        case SCREEN_TETRIS:
            if(fTetris.task() == false) {
                display_game_over_and_score(fTetris.get_score(), fTetris.get_hiscore());
            }
            break;

        case SCREEN_COUNTDOWN_TIMER:
            fLedmatrixCountdownTimer.task();
            display_delay_cycles = 10u;
            break;

        case SCREEN_LEDMATRIX_SHUTDOWN:
            if(fLedmatrixHal.get_on_state() == true) {
                fLedmatrixHal.shutdown();
            }
            break;

        case SCREEN_WELCOMEWIZARD:
            break;

        case SCREEN_MENU:
            fLedmatrixMenu.task();
            display_delay_cycles = 7u;
            break;

        case SCREEN_BRIGHTNESSADJUST:
            break;

        case SCREEN_SETALARM:
            break;

        case SCREEN_SETMUTE:
            break;

        case SCREEN_CLOCKSTYLE_SELECT:
            fLedmatrixScreens.blink_clockstyle_select_screen();
            break;

        case SCREEN_CLOCKTYPE_SELECT:
            break;

    }
}
