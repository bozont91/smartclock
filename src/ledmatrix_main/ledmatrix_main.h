#ifndef LEDMATRIX_MAIN_H
#define LEDMATRIX_MAIN_H

#include <Arduino.h>
#include "../rotaryencoder/rotaryencoder.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../ledmatrix_font/ledmatrix_font.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"
#include "../ledmatrix_snake/ledmatrix_snake.h"
#include "../ledmatrix_tetris/ledmatrix_tetris.h"
#include "../ledmatrix_screens/ledmatrix_screens.h"
#include "../ledmatrix_countdown_timer/ledmatrix_countdown_timer.h"
#include "../ledmatrix_menu/ledmatrix_menu.h"
#include "../ntp/ntp.h"
#include "../beep/beep.h"
#include "../beep/mutemanager.h"
#include "../internet_weather/internet_weather.h"


class LedmatrixStateManager {

public:

    LedmatrixStateManager(
        RotaryencoderHAL &rotaryencoderhal,
        LedmatrixHal &ledmatrixhal,
        LedmatrixGFX &ledmatrixgfx,
        LedmatrixSnake &ledmatrixsnake,
        LedmatrixTetris &tetris,
        LedmatrixScreens &ledmatrixscreens,
        LedmatrixCountdownTimer &ledmatrixcountdowntimer,
        LedmatrixMenu &ledmatrixmenu,
        MuteManager &mutemanager,
        Webweather &webweather,
        Settings &settings
    );

    enum ledmatrix_screens {
        SCREEN_LOADING,
        SCREEN_CLOCK,
        SCREEN_DATE,
        SCREEN_YEAR,
        SCREEN_TEMPERATURE,
        SCREEN_WEATHER,
        SCREEN_SNAKE,
        SCREEN_LEDMATRIX_SHUTDOWN,
        SCREEN_SHOWTEXT,
        SCREEN_COUNTDOWN_TIMER,
        SCREEN_WELCOME,
        SCREEN_WELCOMEWIZARD,
        SCREEN_MENU,
        SCREEN_BRIGHTNESSADJUST,
        SCREEN_SETALARM,
        SCREEN_TETRIS,
        SCREEN_SETMUTE,
        SCREEN_CLOCKSTYLE_SELECT,
        SCREEN_CLOCKTYPE_SELECT
    };

    void read_settings();
    uint8_t get_current_screen();
    void set_current_screen(uint8_t requested_screen);
    void show_text(String text, uint8_t screen_after_text = SCREEN_CLOCK);
    void display_on();
    void display_off();
    bool get_display_on();
    void set_mute_when_screen_is_off(bool state);
    bool get_mute_when_screen_is_off();
    void task();

    bool welcomewizard_active = false;

private:

    uint8_t current_screen = SCREEN_LOADING;
    uint8_t delayed_state_timer_next_state = 0u;
    uint16_t delayed_state_timer_time = 0u;
    uint8_t screen_after_text_scroll = SCREEN_CLOCK;
    bool mute_when_screen_is_off = false;

    void delayed_state_timer_setup(uint8_t next_state, uint16_t timer);
    void delayed_state_timer_tick();
    void display_game_over_and_score(uint16_t score, uint16_t hiscore);

    RotaryencoderHAL &fRotaryencoderHAL;
    LedmatrixHal &fLedmatrixHal;
    LedmatrixGFX &fLedmatrixGFX;
    LedmatrixSnake &fSnake;
    LedmatrixTetris &fTetris;
    LedmatrixScreens &fLedmatrixScreens;
    LedmatrixCountdownTimer &fLedmatrixCountdownTimer;
    LedmatrixMenu &fLedmatrixMenu;
    MuteManager &fMuteManager;
    Webweather &fWebweather;
    Settings &fSettings;

};


#endif /* LEDMATRIX_MAIN_H */
