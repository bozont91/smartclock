#ifndef TCP_REMOTE_H
#define TCP_REMOTE_H

#include <Arduino.h>
#include "../common.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../beep/mutemanager.h"
#include "../ntp/ntp.h"
#include "../settings/settings.h"
#include "../deviceclock/deviceclock.h"
#include "../clockmanager/clockmanager.h"
#include "../alarm/alarm.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../ledmatrix_screens/ledmatrix_screens.h"
#include "../ledmatrix_snake/ledmatrix_snake.h"
#include "../ledmatrix_countdown_timer/ledmatrix_countdown_timer.h"
#include "../display_auto_onoff/display_auto_onoff.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../mqtt/mqtt.h"
#include "../adafruitio/adafruitio.h"
#include "../temperature/temperature.h"
#include "../ledmatrix_tetris/ledmatrix_tetris.h"
#include "../internet_weather/internet_weather.h"
#include "../ota/ota.h"
#include "../esp_hal/esp_hal.h"
#include "../buttonhandler/buttonhandler.h"
#include "aura_protocol.h"
#include "../crypto/curve25519-donna.h"
#include "../crypto/sha256.h"


class TcpRemote {

public:

    TcpRemote(
        Logger &log,
        Beep &beep,
        NtpManager &ntp,
        Settings &settings,
        DeviceClock &deviceclock,
        ClockManager &clockmanager,
        AlarmManager &alarmmanager,
        LedmatrixHal &ledmatrixhal,
        LedmatrixScreens &ledmatrixscreens,
        LedmatrixSnake &snake,
        LedmatrixCountdownTimer &ledmatrixcountdowntimer,
        DisplayAutoOnOff &displayautoonoff,
        LedmatrixStateManager &ledmatrixmain,
        MqttManager &mqttmanager,
        AdafruitIoIntegration &adafruitiointegration,
        TemperatureManager &temperaturemanager,
        LedmatrixTetris &tetris,
        Webweather &webweather,
        AuraOta &ota,
        EspHal &esphal,
        ButtonHandler &buttonhandler,
        MuteManager &mutemanager,
        AuraProtocol &ap
    );
    void init();
    void task();

private:

    enum tcp_remote_states_e {
        TCPREMOTE_WAIT_FOR_CLIENT = 0u,
        TCPREMOTE_KEY_EXCHANGE,
        TCPREMOTE_SERVE_CLIENT
    };

    static const uint8_t TCPREMOTE_CLIENT_TIMEOUT = 60u;
    static const uint8_t TCPREMOTE_MSGBUFFER_SIZE = 128u;

    uint8_t tcpremote_state = TCPREMOTE_WAIT_FOR_CLIENT;
    uint8_t *tcpremote_msgbuffer;
    uint8_t tcpremote_msgbuffer_cnt = 0u;;
    uint32_t client_last_activity_timestamp = 0u;
    String client_ip_str;

    void process_message(AuraProtocol::message *msg);
    void build_and_send(AuraProtocol::message* msg);
    void build_and_send(uint8_t msg_id, uint8_t msg_type, uint8_t *payload, uint8_t payload_size);

    bool validate_bool_payload(uint8_t* payload, uint8_t payload_len);
    bool validate_string_payload(uint8_t* payload, uint8_t payload_len, uint8_t max_len);
    bool validate_int_payload(uint8_t* payload, uint8_t payload_len, int8_t min_val, int8_t max_val);
    bool validate_uint_payload(uint8_t* payload, uint8_t payload_len, uint8_t min_val, uint8_t max_val);
    bool validate_time_payload(uint8_t* payload, uint8_t payload_len);
    bool validate_var_len_uint_payload(uint8_t payload_len, uint8_t number_of_bytes);
    bool validate_timestamp_payload(uint8_t* payload, uint8_t payload_len, uint8_t tolerance);

    void process_key_exchange_message(AuraProtocol::message *msg);
    void print_key(const uint8_t* key);
    void generate_keypair();
    inline void generate_shared_key(const uint8_t* received_public);
    void cleanup_encryption_keys();
    static const uint8_t TCPREMOTE_KEY_SIZE = 32u;
    uint8_t* key_public = NULL;
    uint8_t* key_secret = NULL;
    uint8_t* key_shared = NULL;

    void encrypt(uint8_t* data, uint32_t data_len);

    Logger &fLog;
    Beep &fBeep;
    NtpManager &fNtp;
    Settings &fSettings;
    DeviceClock &fDeviceClock;
    ClockManager &fClockManager;
    AlarmManager &fAlarmManager;
    LedmatrixHal &fLedmatrixHal;
    LedmatrixScreens &fLedmatrixScreens;
    LedmatrixSnake &fSnake;
    LedmatrixCountdownTimer &fLedmatrixCountdownTimer;
    DisplayAutoOnOff &fDisplayAutoOnOff;
    LedmatrixStateManager &fLedmatrixMain;
    MqttManager &fMqttManager;
    AdafruitIoIntegration &fAdafruitIoIntegration;
    TemperatureManager &fTemperatureManager;
    LedmatrixTetris &fTetris;
    Webweather &fWebweather;
    AuraOta &fOta;
    EspHal &fEspHal;
    ButtonHandler &fButtonHandler;
    MuteManager &fMuteManager;
    AuraProtocol &fAuraProtocol;

};


#endif /* TCP_REMOTE_H */
