#ifndef AURA_PROTOCOL_H
#define AURA_PROTOCOL_H

/*
Packet layout:
-----------------------
Header    | Message size  | Message ID | Message type     | Payload size | Payload                             | Checksum (CRC32)        | Double LF - packet terminator
          | incl header   |            | req/resp/change  |              | as many bytes as the size indicates | for the preceeding data | not part of the CRC'd data, but part of length

0x42 0x91   0x0b            0x01         0xE0               0x01           0x69 0x.. 0x..                        0xfa 0xfe 0x55 0xc4       0x0A 0x0A
-----------------------
*/

#ifndef AURA_UNITTEST
    #include <Arduino.h>
    #include "../common.h"
    #include "../log/log.h"
    #include "../crypto/crc32.h"
#else
    #include "../../tests/mock/log.h"
    #include <stdint.h>
    #include <cstddef>
    #include <cstdlib>
    #include <string.h>
    #include <stdio.h>
    #include "../crypto/crc32.h"
#endif

class AuraProtocol {

public:

    static const uint16_t AP_MESSAGE_HEADER =  0x4291;

    enum msg_type_e {
        MSG_TYPE_MIN = 0xDF,

        REQUEST = 0xE0,
        RESPONSE = 0xE1,
        CHANGE_VALUE = 0xE2,

        MSG_TYPE_MAX
    };

    enum msg_e {
        KEEPALIVE,

        START_SNAKE,
        START_TETRIS,
        GAME_UP,
        GAME_DOWN,
        GAME_LEFT,
        GAME_RIGHT,

        DEVICECLOCK,
        DEVICETEMP,
        TEMPSENSOR_PRESENT,
        TEMPSENSOR_CALIBRATION_VALUE,
        TEMPERATURE_UNIT,

        DISPLAY_ON_OFF,
        DISPLAY_BRIGHTNESS,

        DISPLAY_AUTO_ONOFF_ENABLED,
        DISPLAY_AUTO_ONOFF_OFF_TIME,
        DISPLAY_AUTO_ONOFF_ON_TIME,
        DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS,

        SHOW_WEATHER,
        SHOW_DATE,
        SHOW_TEXT,

        ALARM_TIME,
        ALARM_DISABLE,
        ALARM_ONLY_ON_WEEKDAYS,

        TIMEZONE,
        CLOCK_STYLE,
        COUNTDOWNTIMER_START,
        HOURLY_BEEP_ENABLED,
        DISPLAY_INVERT,
        FACTORY_RESET,
        MUTE_DEVICE,
        DEBUGINFO,

        MQTT_CLIENT_ENABLED,
        MQTT_BROKER_ADDRESS,
        MQTT_BROKER_PORT,

        ADAFRUITIO_ENABLED,
        ADAFRUITIO_USERNAME,
        ADAFRUITIO_API_KEY,

        WEBWEATHER_ENABLED,
        WEBWEATHER_LOCATION,
        WEBWEATHER_API_KEY,

        FORCE_NTP_SYNC,
        SHOW_DETAILED_WEATHER,
        SHOW_DETAILED_DATE,
        ALARM_SNOOZE_TIME,
        ALARM_SOUND,
        DST_ENABLED,

        MQTT_BROKER_USERNAME,
        MQTT_BROKER_PASSWORD,

        OTA_ENABLED,
        OTA_PASSWORD,

        REBOOT,

        MQTT_CLIENT_STATUS,
        ADAFRUITIO_CLIENT_STATUS,
        WEATHER_CLIENT_STATUS,

        ENCRYPTION_KEY_EXCHANGE,

        MAGIC_NUMBERS_ENABLED,
        MUTE_WHEN_SCREEN_IS_OFF,

        CLOCK_TYPE,

        BUTTON_SHORTPRESS_ACTION,
        BUTTON_LONGPRESS_ACTION,

        MUTE_SCHEDULE_ENABLED,
        MUTE_ON_TIME,
        MUTE_OFF_TIME,

        MSG_ID_MAX
    };

    typedef struct message_repr_s {
        uint16_t header = AP_MESSAGE_HEADER;
        uint8_t size = 0u;
        uint8_t msg_id = 0u;
        uint8_t msg_type = 0u;
        uint8_t payload_size = 0u;
        uint8_t *payload = NULL;
        uint32_t crc32 = 0u;
    } message;

    enum msg_start_indexes_e {
        /* 2 byte */
        MSG_STARTINDEX_HEADER = 0,
        /* 1 byte */
        MSG_STARTINDEX_MSGSIZE = 2,
        /* 1 byte */
        MSG_STARTINDEX_MSG_ID = 3,
        /* 1 byte */
        MSG_STARTINDEX_MSG_TYPE = 4,
        /* 1 byte */
        MSG_STARTINDEX_PAYLOAD_SIZE = 5,
        /* n byte */
        MSG_STARTINDEX_PAYLOAD = 6
    };

    AuraProtocol(Logger &log);
    uint8_t *build(message* msg_to_build);
    message *parse(uint8_t *data);
    void discard_msg(message* msg_to_discard);
    void log_raw_message(uint8_t* raw_msg, uint8_t size);

private:
    Logger &fLog;

};

#endif /* AURA_PROTOCOL_H */
