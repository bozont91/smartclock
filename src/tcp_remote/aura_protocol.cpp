#include "aura_protocol.h"


AuraProtocol::AuraProtocol(Logger &log) : fLog(log) {
    crc32* crc32_i = crc32::getInstance();
    crc32_i->generate_table();
}

uint8_t* AuraProtocol::build(message* msg_to_build) {
    /* Calculate the final size */
    /* Header:2 Size:1 CmdId:1 CmdType:1 PaySize:1 Payload:varies CRC:4 LF:1 LF:1*/
    uint8_t f_size = 12u + msg_to_build->payload_size;

    /* Allocate memory for the return buffer */
    uint8_t *f_message = (uint8_t*)malloc(f_size*sizeof(uint8_t));
    memset(f_message, 0u, f_size);

    /* Fill in the header */
    memcpy(f_message, &AP_MESSAGE_HEADER, sizeof(AP_MESSAGE_HEADER));

    /* Fill the size */
    f_message[MSG_STARTINDEX_MSGSIZE] = f_size;
    msg_to_build->size = f_size;

    /* Fill the message ID */
    f_message[MSG_STARTINDEX_MSG_ID] = msg_to_build->msg_id;

    /* Fill the message type */
    f_message[MSG_STARTINDEX_MSG_TYPE] = msg_to_build->msg_type;

    /* Fill the payload size */
    f_message[MSG_STARTINDEX_PAYLOAD_SIZE] = msg_to_build->payload_size;

    /* Copy the payload */
    if(msg_to_build->payload_size > 0) {
        if(msg_to_build->payload) {
            memcpy(f_message + MSG_STARTINDEX_PAYLOAD, msg_to_build->payload, msg_to_build->payload_size);
        } else {
            /* Payload was null, so we set the size accordingly and don't copy anything*/
            f_message[MSG_STARTINDEX_PAYLOAD_SIZE] = 0;
        }
    }

    /* Fill the CRC */
    uint8_t msg_length_without_crc = MSG_STARTINDEX_PAYLOAD + msg_to_build->payload_size;
    crc32* crc32_i = crc32::getInstance();
    uint32_t f_crc32 = crc32_i->update(0, f_message, msg_length_without_crc);
    memcpy(f_message + msg_length_without_crc, &f_crc32, sizeof(uint32_t));
    msg_to_build->crc32 = f_crc32;

    /* Add a LF at the end */
    f_message[f_size - 2] = '\n';
    f_message[f_size - 1] = '\n';

    return f_message;
}

AuraProtocol::message* AuraProtocol::parse(uint8_t *data) {
    if(!data) return NULL;

    message* ret = (message*)malloc(sizeof(message));

    /* Extract and check the header */
    uint16_t header;
    memcpy(&header, data, sizeof(AP_MESSAGE_HEADER));
    /* ToDo: check byte order here */
    if(header != AP_MESSAGE_HEADER) {
        fLog.log(LOG_TCPREMOTE, L_ERROR, F("Header check failed! act='0x%x' exp='0x%x'"), header, AP_MESSAGE_HEADER);
        free(ret);
        return NULL;
    }
    ret->header = header;

    /* Get the message size */
    uint8_t f_size = data[MSG_STARTINDEX_MSGSIZE];
    ret->size = f_size;

    /* Check the CRC */
    uint32_t received_crc32;
    /* This is how many bytes we have to step back from the end of the received data to get what we calculate the CRC32 on */
    /* (4 bytes for the received CRC, 2 bytes for the terminating chars) */
    uint8_t crc_data_offset_from_the_end = 6;

    memcpy(&received_crc32, data + f_size - crc_data_offset_from_the_end, sizeof(uint32_t));
    crc32* crc32_i = crc32::getInstance();
    uint32_t calculated_crc32 = crc32_i->update(0, data, f_size - crc_data_offset_from_the_end);
    if(received_crc32 != calculated_crc32) {
        fLog.log(LOG_TCPREMOTE, L_ERROR, F("CRC check failed! act='0x%x' exp='0x%x'"), received_crc32, calculated_crc32);
        free(ret);
        return NULL;
    }
    ret->crc32 = received_crc32;

    /* Get the message ID */
    ret->msg_id = data[MSG_STARTINDEX_MSG_ID];
    if(ret->msg_id >= MSG_ID_MAX) {
        fLog.log(LOG_TCPREMOTE, L_ERROR, F("Unknown message ID received! msg_id='0x%x'"), ret->msg_id);
        free(ret);
        return NULL;
    }

    /* Get the message type */
    ret->msg_type = data[MSG_STARTINDEX_MSG_TYPE];
    if(ret->msg_type >= MSG_TYPE_MAX || ret->msg_type <= MSG_TYPE_MIN) {
        fLog.log(LOG_TCPREMOTE, L_ERROR, F("Unknown message type received! msg_type='0x%x'"), ret->msg_type);
        free(ret);
        return NULL;
    }

    /* Get the payload size */
    uint8_t f_payload_size = data[MSG_STARTINDEX_PAYLOAD_SIZE];
    ret->payload_size = f_payload_size;

    /* Get the payload */
    if(f_payload_size > 0) {
        uint8_t *f_payload = (uint8_t*)malloc(f_payload_size * sizeof(uint8_t));
        memcpy(f_payload, data + MSG_STARTINDEX_PAYLOAD, f_payload_size);
        ret->payload = f_payload;
    } else {
        ret->payload = NULL;
    }

    return ret;
}

void AuraProtocol::discard_msg(message* msg_to_discard) {
    if(!msg_to_discard) return;
    if(msg_to_discard->payload) free(msg_to_discard->payload);
    free(msg_to_discard);
}

void AuraProtocol::log_raw_message(uint8_t* raw_msg, uint8_t size) {
    fLog.log(LOG_TCPREMOTE, L_DEBUG, F("Raw message:"));
    for(int i = 0 ; i < size ; i++) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("0x%02x "), raw_msg[i]);
    }
}
