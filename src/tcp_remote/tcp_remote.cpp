#include "tcp_remote.h"


WiFiServer tcpremote_server(TCP_REMOTE_PORT);
WiFiClient tcpremote_client;

TcpRemote::TcpRemote(
    Logger &log,
    Beep &beep,
    NtpManager &ntp,
    Settings &settings,
    DeviceClock &deviceclock,
    ClockManager &clockmanager,
    AlarmManager &alarmmanager,
    LedmatrixHal &ledmatrixhal,
    LedmatrixScreens &ledmatrixscreens,
    LedmatrixSnake &snake,
    LedmatrixCountdownTimer &ledmatrixcountdowntimer,
    DisplayAutoOnOff &displayautoonoff,
    LedmatrixStateManager &ledmatrixmain,
    MqttManager &mqttmanager,
    AdafruitIoIntegration &adafruitiointegration,
    TemperatureManager &temperaturemanager,
    LedmatrixTetris &tetris,
    Webweather &webweather,
    AuraOta &ota,
    EspHal &esphal,
    ButtonHandler &buttonhandler,
    MuteManager &mutemanager,
    AuraProtocol &ap
) :
fLog(log),
fBeep(beep),
fNtp(ntp),
fSettings(settings),
fDeviceClock(deviceclock),
fClockManager(clockmanager),
fAlarmManager(alarmmanager),
fLedmatrixHal(ledmatrixhal),
fLedmatrixScreens(ledmatrixscreens),
fSnake(snake),
fLedmatrixCountdownTimer(ledmatrixcountdowntimer),
fDisplayAutoOnOff(displayautoonoff),
fLedmatrixMain(ledmatrixmain),
fMqttManager(mqttmanager),
fAdafruitIoIntegration(adafruitiointegration),
fTemperatureManager(temperaturemanager),
fTetris(tetris),
fWebweather(webweather),
fOta(ota),
fEspHal(esphal),
fButtonHandler(buttonhandler),
fMuteManager(mutemanager),
fAuraProtocol(ap)
{
    tcpremote_msgbuffer = (uint8_t*)malloc(TCPREMOTE_MSGBUFFER_SIZE * sizeof(uint8_t));
}

void TcpRemote::init() {
    tcpremote_server.begin();
}

void TcpRemote::generate_keypair() {
    if(key_public || key_secret) return;

    key_secret = (uint8_t*)malloc(TCPREMOTE_KEY_SIZE * sizeof(uint8_t));
    key_public = (uint8_t*)malloc(TCPREMOTE_KEY_SIZE * sizeof(uint8_t));
    /* Generate private key */
    for(uint8_t i = 0; i < TCPREMOTE_KEY_SIZE; i++) {
        key_secret[i] = fEspHal.get_true_random_number();
    }
    /* Generate public key */
    uint8_t basepoint[TCPREMOTE_KEY_SIZE] = {9};
    curve25519_donna(key_public, key_secret, basepoint);
}

inline void TcpRemote::generate_shared_key(const uint8_t* received_public) {
    if(key_shared || !key_secret || !key_public) return;

    /* Calculate shared secret using curve25519 */
    uint8_t* key_shared_tmp = (uint8_t*)malloc(TCPREMOTE_KEY_SIZE * sizeof(uint8_t));
    curve25519_donna(key_shared_tmp, key_secret, received_public);

    /* Apply SHA256 to the calculated shared key */
    key_shared = (uint8_t*)malloc(TCPREMOTE_KEY_SIZE * sizeof(uint8_t));
    Sha256* fSha256 = new Sha256();
    fSha256->update(key_shared_tmp, TCPREMOTE_KEY_SIZE);
    fSha256->final(key_shared);
    delete fSha256;

    free(key_shared_tmp);
}

void TcpRemote::cleanup_encryption_keys() {
    if(key_shared) {
        memset(key_shared, 0x00, TCPREMOTE_KEY_SIZE);
        free(key_shared);
        key_shared = NULL;
    }

    if(key_public) {
        memset(key_public, 0x00, TCPREMOTE_KEY_SIZE);
        free(key_public);
        key_public = NULL;
    }

    if(key_secret) {
        memset(key_secret, 0x00, TCPREMOTE_KEY_SIZE);
        free(key_secret);
        key_secret = NULL;
    }
}

void TcpRemote::print_key(const uint8_t* key) {
    if(!key) return;

    for(uint8_t i = 0; i < TCPREMOTE_KEY_SIZE; i++) {
        char buf[10];
        sprintf(buf, "%02x ", key[i]);
        Serial.print(buf);
    }
    Serial.println();
}

void TcpRemote::encrypt(uint8_t* data, uint32_t data_len) {
    if(!key_shared) return;

    uint8_t key_step = 0u;
    for(uint16_t i = 0; i < data_len; i++) {
        data[i] = data[i] ^ key_shared[key_step];
        key_step++;
        if(key_step == TCPREMOTE_KEY_SIZE) key_step = 0u;
    }
}

void TcpRemote::task() {

    switch(tcpremote_state) {
        case TCPREMOTE_WAIT_FOR_CLIENT:
            tcpremote_client = tcpremote_server.available();
            if (tcpremote_client) {
                tcpremote_state = TCPREMOTE_KEY_EXCHANGE;
                client_last_activity_timestamp = device_uptime;
                client_ip_str = tcpremote_client.remoteIP().toString();
                fLog.log(LOG_TCPREMOTE, L_INFO, F("Client connected (%s)"), client_ip_str.c_str());

                generate_keypair();
                build_and_send(AuraProtocol::ENCRYPTION_KEY_EXCHANGE, AuraProtocol::REQUEST, key_public, TCPREMOTE_KEY_SIZE);
                fLog.log(LOG_TCPREMOTE, L_INFO, F("Performing key exchange"));
            }
        break;

        case TCPREMOTE_SERVE_CLIENT:
        case TCPREMOTE_KEY_EXCHANGE:

            if(client_last_activity_timestamp + TCPREMOTE_CLIENT_TIMEOUT < device_uptime) {
                fLog.log(LOG_TCPREMOTE, L_INFO, F("Disconnecting client at %s due to inactivity"), client_ip_str.c_str());
                tcpremote_client.stop();
                tcpremote_state = TCPREMOTE_WAIT_FOR_CLIENT;
                cleanup_encryption_keys();
                return;
            }

            if(!tcpremote_client.connected()) {
                tcpremote_client.stop();
                tcpremote_state = TCPREMOTE_WAIT_FOR_CLIENT;
                fLog.log(LOG_TCPREMOTE, L_INFO, F("Client at %s disconnected"), client_ip_str.c_str());
                cleanup_encryption_keys();
                return;
            }

            if(tcpremote_client.available()) {
                bool packet_found = false;

                /* Receive data until we find \n\n */
                while(tcpremote_client.available()) {
                    tcpremote_msgbuffer[tcpremote_msgbuffer_cnt] = tcpremote_client.read();

                    if(tcpremote_msgbuffer_cnt > 0 && tcpremote_msgbuffer[tcpremote_msgbuffer_cnt] == '\n' && tcpremote_msgbuffer[tcpremote_msgbuffer_cnt - 1] == '\n') {
                        packet_found = true;
                        break;
                    }

                    tcpremote_msgbuffer_cnt++;

                    if(tcpremote_msgbuffer_cnt == TCPREMOTE_MSGBUFFER_SIZE) {
                        tcpremote_msgbuffer_cnt = 0;
                        fLog.log(LOG_TCPREMOTE, L_ERROR, F("Rx buffer overflow!"));
                        return;
                    }
                }

                if(!packet_found) {
                    fLog.log(LOG_TCPREMOTE, L_INFO, F("Received %d bytes but no packet was found!"), tcpremote_msgbuffer_cnt);
                    tcpremote_msgbuffer_cnt = 0;
                    return;
                }

                fLog.log(LOG_TCPREMOTE, L_DEBUG, F("(%s) received %d bytes"), client_ip_str.c_str(), tcpremote_msgbuffer_cnt);

                /* fAuraProtocol.log_raw_message((uint8_t*)tcpremote_msgbuffer, tcpremote_msgbuffer_cnt); */

                /* The minimal valid protcol message is 10 bytes long at least */
                if(tcpremote_msgbuffer_cnt < 10) return;

                /* If the shared key is available it means we had a successful security handshake, so we have to decrypt the message */
                /* We don't decrypt the message terminating bytes */
                if(key_shared) encrypt(tcpremote_msgbuffer, tcpremote_msgbuffer_cnt - 1);

                tcpremote_msgbuffer_cnt = 0;

                AuraProtocol::message *msg_recv = fAuraProtocol.parse((uint8_t*)tcpremote_msgbuffer);
                if(msg_recv == NULL) return;

                if(tcpremote_state == TCPREMOTE_KEY_EXCHANGE) {
                    process_key_exchange_message(msg_recv);
                } else {
                    process_message(msg_recv);
                }

                fAuraProtocol.discard_msg(msg_recv);
                client_last_activity_timestamp = device_uptime;
            }

        break;

    }
}

void TcpRemote::process_key_exchange_message(AuraProtocol::message *msg) {
    if(!(msg->msg_id == AuraProtocol::ENCRYPTION_KEY_EXCHANGE && msg->msg_type == AuraProtocol::RESPONSE)) return;

    fLog.log(LOG_TCPREMOTE, L_INFO, F("encryption_key_exchange response"));
    if(msg->payload_size != TCPREMOTE_KEY_SIZE) {
        fLog.log(LOG_TCPREMOTE, L_ERROR, F("Invalid response size"));
        return;
    }

    generate_shared_key(msg->payload);

    fLog.log(LOG_TCPREMOTE, L_INFO, F("Key exchange successful"));
    tcpremote_state = TCPREMOTE_SERVE_CLIENT;
    fBeep.start_music(clientconnected_chime, MUSIC_BLOCK_CLIENTCONNECTED_CHIME_SIZE);
}

void TcpRemote::process_message(AuraProtocol::message *msg) {

    uint8_t msg_id = msg->msg_id;
    uint8_t msg_type = msg->msg_type;
    uint8_t *msg_payload = msg->payload;
    uint8_t msg_payload_size = msg->payload_size;

    /* fLog.log(LOG_TCPREMOTE, L_INFO, F("msg_id: %d  msg_type: %d"), msg_id, msg_type); */

    if(msg_id == AuraProtocol::KEEPALIVE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("keepalive request"));
        return;
    }

    if(msg_id == AuraProtocol::START_SNAKE && msg_type == AuraProtocol::REQUEST) {
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_SNAKE);
        return;
    }
    if(msg_id == AuraProtocol::START_TETRIS && msg_type == AuraProtocol::REQUEST) {
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_TETRIS);
        return;
    }
    if(msg_id == AuraProtocol::GAME_UP && msg_type == AuraProtocol::REQUEST) {
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SNAKE) fSnake.set_direction(fSnake.SNAKE_UP);
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) fTetris.request_game_input(fTetris.TETRIS_UP);
        return;
    }
    if(msg_id == AuraProtocol::GAME_LEFT && msg_type == AuraProtocol::REQUEST) {
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SNAKE) fSnake.set_direction(fSnake.SNAKE_LEFT);
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) fTetris.request_game_input(fTetris.TETRIS_ROTATE);
        return;
    }
    if(msg_id == AuraProtocol::GAME_DOWN && msg_type == AuraProtocol::REQUEST) {
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SNAKE) fSnake.set_direction(fSnake.SNAKE_DOWN);
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) fTetris.request_game_input(fTetris.TETRIS_DOWN);
        return;
    }
    if(msg_id == AuraProtocol::GAME_RIGHT && msg_type == AuraProtocol::REQUEST) {
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_SNAKE) fSnake.set_direction(fSnake.SNAKE_RIGHT);
        if(fLedmatrixMain.get_current_screen() == fLedmatrixMain.SCREEN_TETRIS) fTetris.do_quickfall();
        return;
    }

    if(msg_id == AuraProtocol::DEVICECLOCK && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("deviceclock request called"));

        uint32_t payload = fDeviceClock.get();
        build_and_send(AuraProtocol::DEVICECLOCK, AuraProtocol::RESPONSE, (uint8_t*)&payload, 4u);
        return;
    }

    if(msg_id == AuraProtocol::DEVICETEMP && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("devicetemp request called"));

        float payload = fTemperatureManager.get_temperature();
        build_and_send(AuraProtocol::DEVICETEMP, AuraProtocol::RESPONSE, (uint8_t*)&payload, 4u);
        return;
    }

    if(msg_id == AuraProtocol::TEMPSENSOR_PRESENT && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("tempsensor_present request called"));

        uint8_t payload = fTemperatureManager.get_sensor_present();
        build_and_send(AuraProtocol::TEMPSENSOR_PRESENT, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_ON_OFF && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_on_off request called"));

        if(fLedmatrixMain.get_display_on()) {
            fLedmatrixMain.display_off();
            fBeep.play_negative_acknowledge();
        } else {
            fLedmatrixMain.display_on();
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::DEBUGINFO && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("debuginfo request called"));

        char msgbuf[100];
        AuraProtocol::message msg_dbg;
        msg_dbg.msg_id = AuraProtocol::DEBUGINFO;
        msg_dbg.msg_type = AuraProtocol::RESPONSE;
        msg_dbg.payload = (uint8_t*)msgbuf;

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Device uptime: %u seconds"), device_uptime);
        build_and_send(&msg_dbg);

        time_t now = fDeviceClock.get();
        struct tm ts = *localtime(&now);
        char timebuf[80];
        strftime(timebuf, sizeof(timebuf), "%a %Y-%m-%d %H:%M:%S", &ts);
        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Device clock: %s"), timebuf);
        build_and_send(&msg_dbg);

        now = fClockManager.device_startup_time;
        ts = *localtime(&now);
        strftime(timebuf, sizeof(timebuf), "%a %Y-%m-%d %H:%M:%S", &ts);
        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Device on since: %s"), timebuf);
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Last reset reason: %s"), ESP.getResetReason().c_str());
        build_and_send(&msg_dbg);

        now = fNtp.get_last_sync_unix_timestamp();
        ts = *localtime(&now);
        strftime(timebuf, sizeof(timebuf), "%H:%M  ", &ts);
        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Last NTP sync: %s"), timebuf);
        build_and_send(&msg_dbg);

        now = fNtp.get_next_sync_unix_timestamp();
        ts = *localtime(&now);
        strftime(timebuf, sizeof(timebuf), "%H:%M  ", &ts);
        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Next NTP sync: %s"), timebuf);
        build_and_send(&msg_dbg);

        if(fTemperatureManager.get_sensor_present() == true) {
            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Device temperature: %.1f°%c"), fTemperatureManager.get_temperature(), fTemperatureManager.get_temperature_postfix_as_char());
        } else {
            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Device temperature: (sensor not present)"));
        }
        build_and_send(&msg_dbg);

        if(fWebweather.get_enabled()) {
            if(fWebweather.get_data_available()) {
                msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Online weather: %d°%c (%s)"), fWebweather.get_temp(), fTemperatureManager.get_temperature_postfix_as_char(), fWebweather.get_location().c_str());
            } else {
                msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Online weather: (not available)"));
            }
            build_and_send(&msg_dbg);
        }

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("CPU ID: 0x%x"), ESP.getChipId());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("CPU speed: %u MHz"), ESP.getCpuFreqMHz());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("CPU utilization: %.1f%%"), cpu_current_load);
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Free heap: %d bytes (Fragmentation: %d%%)"), system_get_free_heap_size(), ESP.getHeapFragmentation());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("MAC address: %s"), WiFi.macAddress().c_str());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("WiFi network: %s"), WiFi.SSID().c_str());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("IPv4 address: %s"), WiFi.localIP().toString().c_str());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Default gateway: %s"), WiFi.gatewayIP().toString().c_str());
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("WiFi RSSI: %d dBm"), WiFi.RSSI());
        build_and_send(&msg_dbg);

        if(fMqttManager.get_enabled()) {

            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("MQTT broker: %s:%d"), fMqttManager.get_broker_address(), fMqttManager.get_broker_port());
            build_and_send(&msg_dbg);

            String mqtt_status = "Disconnected";
            if(fMqttManager.get_connected()) mqtt_status = "Connected";
            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("MQTT client status: %s"), mqtt_status.c_str());
            build_and_send(&msg_dbg);

            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("MQTT published messages: %d"), fMqttManager.get_sent_message_cnt());
            build_and_send(&msg_dbg);

            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("MQTT received messages: %d"), fMqttManager.get_received_message_cnt());
            build_and_send(&msg_dbg);

        }

        if(fAdafruitIoIntegration.get_enabled()) {
            String aio_status = "Disconnected";
            if(fAdafruitIoIntegration.get_connected()) aio_status = "Connected";
            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Adafruit IO: %s"), aio_status.c_str());
            build_and_send(&msg_dbg);

            msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Adafruit IO received messages: %d"), fAdafruitIoIntegration.get_received_message_cnt());
            build_and_send(&msg_dbg);
        }

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Software version: %s %s (%s)"), SOFTWARE_VERSION, SOFTWARE_RELEASE_TYPE, SOFTWARE_COMMIT_ID);
        build_and_send(&msg_dbg);

        msg_dbg.payload_size = sprintf_P(msgbuf, PSTR("Build date: %s"), SOFTWARE_BUILD_DATE);
        build_and_send(&msg_dbg);

        return;
    }

    if(msg_id == AuraProtocol::SHOW_WEATHER && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("show_weather request called"));
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_TEMPERATURE);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::SHOW_DETAILED_WEATHER && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("show_detailed_weather request called"));

        if(!fWebweather.get_enabled()) return;

        char* detailed_weather = (char*)malloc(200);
        fLedmatrixScreens.get_detailed_weather_string(detailed_weather, 200);
        fLedmatrixMain.show_text(detailed_weather);
        free(detailed_weather);

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::SHOW_DATE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("show_date request called"));
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_YEAR);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::SHOW_DETAILED_DATE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("show_detailed_date request called"));

        char* detailed_date = (char*)malloc(100);
        fLedmatrixScreens.get_detailed_date_string(detailed_date, 100);
        fLedmatrixMain.show_text(detailed_date);
        free(detailed_date);

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_BRIGHTNESS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_brightness request called"));

        uint8_t payload = fLedmatrixHal.get_brightness();
        build_and_send(AuraProtocol::DISPLAY_BRIGHTNESS, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_BRIGHTNESS && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_brightness change called"));
        if(!validate_uint_payload(msg_payload, msg_payload_size, MAX7219_LEDMATRIX_MIN_BRIGHTNESS, MAX7219_LEDMATRIX_MAX_BRIGHTNESS)) return;

        uint8_t brightnessval = msg_payload[0];
        if(fLedmatrixHal.get_brightness() == brightnessval) return;

        fLedmatrixHal.set_brightness(brightnessval);
        fSettings.write(SETT_BRIGHTNESS, &brightnessval);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::ALARM_TIME && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_time request called"));

        uint8_t payload[2];
        payload[0] = fAlarmManager.get_hour();
        payload[1] = fAlarmManager.get_minute();
        build_and_send(AuraProtocol::ALARM_TIME, AuraProtocol::RESPONSE, payload, 2u);
        return;
    }

    if(msg_id == AuraProtocol::ALARM_TIME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_time change called"));
        if(!validate_time_payload(msg_payload, msg_payload_size)) return;

        uint8_t alarm_hour = msg_payload[0];
        uint8_t alarm_min = msg_payload[1];

        fAlarmManager.set_alarm(alarm_hour, alarm_min);
        fAlarmManager.enable();

        uint8_t alarm_enabled = true;
        fSettings.write(SETT_ALARM_ENABLED, &alarm_enabled);
        fSettings.write(SETT_ALARM_HOUR, &alarm_hour);
        fSettings.write(SETT_ALARM_MINUTE, &alarm_min);

        fLedmatrixScreens.clock_force_update();
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::ALARM_DISABLE && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_disable change called"));
        fAlarmManager.disable();
        uint8_t alarm_enabled = false;
        fSettings.write(SETT_ALARM_ENABLED, &alarm_enabled);
        fLedmatrixScreens.clock_force_update();
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::ALARM_ONLY_ON_WEEKDAYS && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_only_on_weekdays change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t only_on_weekdays = msg_payload[0];
        if(only_on_weekdays == 1) {
            fSettings.write(SETT_ALARM_ONLY_ON_WEEKDAYS, &only_on_weekdays);
            fAlarmManager.set_only_on_weekdays_option(true);
            fBeep.play_acknowledge();
        } else if(only_on_weekdays == 0) {
            fSettings.write(SETT_ALARM_ONLY_ON_WEEKDAYS, &only_on_weekdays);
            fAlarmManager.set_only_on_weekdays_option(false);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::ALARM_ONLY_ON_WEEKDAYS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_only_on_weekdays request called"));

        uint8_t payload = fAlarmManager.get_only_on_weekdays_option();
        build_and_send(AuraProtocol::ALARM_ONLY_ON_WEEKDAYS, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::ALARM_SNOOZE_TIME && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_snooze_time request called"));

        uint8_t payload = fAlarmManager.get_snooze_time();
        build_and_send(AuraProtocol::ALARM_SNOOZE_TIME, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::ALARM_SNOOZE_TIME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_snooze_time change called"));
        if(!validate_uint_payload(msg_payload, msg_payload_size, 0, UINT8_MAX)) return;

        uint8_t snooze_time_new = msg_payload[0];
        fAlarmManager.set_snooze_time(snooze_time_new);
        fSettings.write(SETT_ALARM_SNOOZE_TIME, &snooze_time_new);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::ALARM_SOUND && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_sound request called"));

        uint8_t payload = fBeep.get_alarm_sound();
        build_and_send(AuraProtocol::ALARM_SOUND, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::ALARM_SOUND && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("alarm_sound change called"));
        if(!validate_uint_payload(msg_payload, msg_payload_size, 0, Beep::ALARMSOUND_MAX)) return;

        uint8_t alarm_sound_new = msg_payload[0];
        if(fBeep.get_alarm_sound() == alarm_sound_new) {
            fBeep.play_negative_acknowledge();
            return;
        }

        fSettings.write(SETT_ALARM_SOUND, &alarm_sound_new);
        fBeep.set_alarm_sound(alarm_sound_new);
        fBeep.play_acknowledge();
        return;
    }

    /* Info: Currently the max payload is 256 bytes which limits the length of displayable text */
    if(msg_id == AuraProtocol::SHOW_TEXT && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("show_text request called"));
        String text_to_show = String((char*)msg_payload);
        fLedmatrixMain.show_text(text_to_show);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::CLOCK_STYLE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("clock_style request called"));

        uint8_t payload = fLedmatrixScreens.get_clock_style();
        build_and_send(AuraProtocol::CLOCK_STYLE, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::CLOCK_STYLE && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("clock_style change called"));
        if(!validate_uint_payload(msg_payload, msg_payload_size, 0, LedmatrixScreens::CLOCK_STYLE_MAX)) return;

        uint8_t clock_style = msg_payload[0];
        if(fLedmatrixScreens.get_clock_style() == clock_style) {
            fBeep.play_negative_acknowledge();
            return;
        }

        fSettings.write(SETT_CLOCK_STYLE, &clock_style);
        fLedmatrixScreens.set_clock_style(clock_style);
        fLedmatrixScreens.clock_force_update();
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::CLOCK_TYPE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("clock_type request called"));

        uint8_t payload = fLedmatrixScreens.get_clock_type();
        build_and_send(AuraProtocol::CLOCK_TYPE, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::CLOCK_TYPE && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("clock_type change called"));
        if(!validate_uint_payload(msg_payload, msg_payload_size, 0, LedmatrixScreens::CLOCK_TYPE_MAX)) return;

        uint8_t clock_type = msg_payload[0];
        fSettings.write(SETT_CLOCK_TYPE, &clock_type);
        fLedmatrixScreens.set_clock_type(clock_type);
        fLedmatrixScreens.clock_force_update();
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::COUNTDOWNTIMER_START && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("countdowntimer_start change called"));
        if(!validate_var_len_uint_payload(msg_payload_size, sizeof(uint32_t))) return;

        uint32_t countdown_seconds;
        memcpy(&countdown_seconds, msg_payload, sizeof(uint32_t));
        fLedmatrixCountdownTimer.set(countdown_seconds);
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_COUNTDOWN_TIMER);
        fBeep.play_acknowledge();
        fLog.log(LOG_TCPREMOTE, L_INFO, F("Starting countdown timer from %u seconds"), countdown_seconds);
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_enabled request called"));

        uint8_t payload = fDisplayAutoOnOff.get_enabled();
        build_and_send(AuraProtocol::DISPLAY_AUTO_ONOFF_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_OFF_TIME && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_off_time request called"));

        uint8_t payload[2];
        payload[0] = fDisplayAutoOnOff.get_off_hour();
        payload[1] = fDisplayAutoOnOff.get_off_minute();
        build_and_send(AuraProtocol::DISPLAY_AUTO_ONOFF_OFF_TIME, AuraProtocol::RESPONSE, payload, 2u);
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_ON_TIME && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_on_time request called"));

        uint8_t payload[2];
        payload[0] = fDisplayAutoOnOff.get_on_hour();
        payload[1] = fDisplayAutoOnOff.get_on_minute();
        build_and_send(AuraProtocol::DISPLAY_AUTO_ONOFF_ON_TIME, AuraProtocol::RESPONSE, payload, 2u);
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t displayonoffenabled = msg_payload[0];
        if(displayonoffenabled == 1) {
            fSettings.write(SETT_SCREENONOFF_ENABLED, &displayonoffenabled);
            fDisplayAutoOnOff.set_enabled(true);
            fBeep.play_acknowledge();
        } else if(displayonoffenabled == 0) {
            fSettings.write(SETT_SCREENONOFF_ENABLED, &displayonoffenabled);
            fDisplayAutoOnOff.set_enabled(false);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_OFF_TIME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_off_time change called"));
        if(!validate_time_payload(msg_payload, msg_payload_size)) return;

        uint8_t hour = msg_payload[0];
        uint8_t minute = msg_payload[1];
        fDisplayAutoOnOff.set_off_time(hour, minute);

        fSettings.write(SETT_SCREENOFF_HOUR, &hour);
        fSettings.write(SETT_SCREENOFF_MINUTE, &minute);

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_ON_TIME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_on_time change called"));
        if(!validate_time_payload(msg_payload, msg_payload_size)) return;

        uint8_t hour = msg_payload[0];
        uint8_t minute = msg_payload[1];
        fDisplayAutoOnOff.set_on_time(hour, minute);

        fSettings.write(SETT_SCREENON_HOUR, &hour);
        fSettings.write(SETT_SCREENON_MINUTE, &minute);

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_only_on_weekdays change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t only_on_weekdays = msg_payload[0];
        if(only_on_weekdays == 1) {
            fSettings.write(SETT_SCREENOFF_ONLY_ON_WEEKDAYS, &only_on_weekdays);
            fDisplayAutoOnOff.set_option(fDisplayAutoOnOff.ONLY_ON_WEEKDAYS);
            fBeep.play_acknowledge();
        } else if(only_on_weekdays == 0) {
            fSettings.write(SETT_SCREENOFF_ONLY_ON_WEEKDAYS, &only_on_weekdays);
            fDisplayAutoOnOff.set_option(fDisplayAutoOnOff.EVERYDAY);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_auto_onoff_only_on_weekdays request called"));

        uint8_t payload = fDisplayAutoOnOff.get_only_on_weekdays_option();
        build_and_send(AuraProtocol::DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::HOURLY_BEEP_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("hourly_beep_enabled request called"));

        uint8_t payload = fClockManager.hourlyBeepGetEnabled();
        build_and_send(AuraProtocol::HOURLY_BEEP_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::HOURLY_BEEP_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("hourly_beep_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t hourlybeepenabled = msg_payload[0];
        uint8_t settings_hourly_beep_enabled;
        if(hourlybeepenabled == 1) {
            settings_hourly_beep_enabled = true;
            fSettings.write(SETT_HOURLY_BEEP, &settings_hourly_beep_enabled);
            fClockManager.hourlyBeepSetEnabledState(true);
            fBeep.play_acknowledge();
        } else if(hourlybeepenabled == 0) {
            settings_hourly_beep_enabled = false;
            fSettings.write(SETT_HOURLY_BEEP, &settings_hourly_beep_enabled);
            fClockManager.hourlyBeepSetEnabledState(false);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::TIMEZONE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("timezone request called"));

        uint8_t payload = fClockManager.getTimezone();
        build_and_send(AuraProtocol::TIMEZONE, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::TIMEZONE && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("timezone change called"));
        if(!validate_int_payload(msg_payload, msg_payload_size, ClockManager::timezones::UTC_MINUS_12, ClockManager::timezones::UTC_PLUS_12)) return;

        int8_t timezone = msg_payload[0];
        fClockManager.changeTimezone(timezone);
        fSettings.write(SETT_TIMEZONE, (uint8_t*)&timezone);
        client_last_activity_timestamp = device_uptime;
        fLedmatrixScreens.clock_force_update();
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::DISPLAY_INVERT && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("display_invert request called"));
        if(fLedmatrixHal.get_display_inverted()) {
            fLedmatrixHal.deinvert_display();
        } else {
            fLedmatrixHal.invert_display();
        }
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::MQTT_CLIENT_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_client_enabled request called"));

        uint8_t payload = fMqttManager.get_enabled();
        build_and_send(AuraProtocol::MQTT_CLIENT_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
    }

    if(msg_id == AuraProtocol::MQTT_CLIENT_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_client_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t mqttclient_req_state = msg_payload[0];

        /* If the client is already enabled and an enable is requested -> restart the client */
        if((mqttclient_req_state == 1) && fMqttManager.get_enabled()) {
            fMqttManager.restart();
        }

        uint8_t mqtt_enabled = true;
        /* If the client is disabled and an enable is requested -> enable */
        if(mqttclient_req_state == 1 && !fMqttManager.get_enabled()) {
            fMqttManager.enable();
            mqtt_enabled = true;
            fSettings.write(SETT_MQTT_CLIENT_ENABLED, &mqtt_enabled);

        /* If the client is enabled and a disable is requested -> disable */
        } else if(mqttclient_req_state != 1 && fMqttManager.get_enabled()) {
            fMqttManager.disable();
            mqtt_enabled = false;
            fSettings.write(SETT_MQTT_CLIENT_ENABLED, &mqtt_enabled);
        }

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::MQTT_BROKER_ADDRESS && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_broker_address change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_MQTT_BROKER_ADDRESS))) return;

        fSettings.write(SETT_MQTT_BROKER_ADDRESS, (uint8_t *)msg_payload, false, msg_payload_size);
        fMqttManager.set_broker_address((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::MQTT_BROKER_ADDRESS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_broker_address request called"));

        uint8_t setting_len = (uint8_t)fSettings.get_setting_length(SETT_MQTT_BROKER_ADDRESS);
        uint8_t payload[setting_len];
        fSettings.read_block(SETT_MQTT_BROKER_ADDRESS, payload);
        uint8_t payload_len = strnlen((const char*)payload, setting_len);
        build_and_send(AuraProtocol::MQTT_BROKER_ADDRESS, AuraProtocol::RESPONSE, payload, payload_len);
        return;
    }

    if(msg_id == AuraProtocol::MQTT_BROKER_PORT && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_broker_port change called"));
        if(!validate_var_len_uint_payload(msg_payload_size, sizeof(uint16_t))) return;

        uint16_t broker_port;
        /* ToDo: check endianness */
        memcpy(&broker_port, msg_payload, sizeof(uint16_t));
        fSettings.write(SETT_MQTT_BROKER_PORT, (uint8_t*)&broker_port);
        fMqttManager.set_broker_port(broker_port);
        return;
    }

    if(msg_id == AuraProtocol::MQTT_BROKER_PORT && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_broker_port request called"));

        uint8_t setting_len = (uint8_t)fSettings.get_setting_length(SETT_MQTT_BROKER_PORT);
        uint8_t payload[setting_len];
        fSettings.read_block(SETT_MQTT_BROKER_PORT, payload);
        build_and_send(AuraProtocol::MQTT_BROKER_PORT, AuraProtocol::RESPONSE, payload, setting_len);
        return;
    }

    if(msg_id == AuraProtocol::MQTT_BROKER_USERNAME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_broker_username change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_MQTT_BROKER_USERNAME))) return;

        if(msg_payload[0] == '\0') {
            fMqttManager.set_authentication_used(false);
            fSettings.write(SETT_MQTT_BROKER_USERNAME, (uint8_t *)msg_payload, false, 1);
            return;
        }

        fSettings.write(SETT_MQTT_BROKER_USERNAME, (uint8_t *)msg_payload, false, msg_payload_size);
        fMqttManager.set_authentication_used(true);
        fMqttManager.set_broker_username((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::MQTT_BROKER_PASSWORD && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mqtt_broker_password change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_MQTT_BROKER_PASSWORD))) return;

        if(msg_payload[0] == '\0') {
            fMqttManager.set_authentication_used(false);
            fSettings.write(SETT_MQTT_BROKER_PASSWORD, (uint8_t *)msg_payload, false, 1);
            return;
        }

        fSettings.write(SETT_MQTT_BROKER_PASSWORD, (uint8_t *)msg_payload, false, msg_payload_size);
        fMqttManager.set_authentication_used(true);
        fMqttManager.set_broker_password((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::FACTORY_RESET && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("factory_reset request called"));
        if(!validate_timestamp_payload(msg_payload, msg_payload_size, 2u)) return;
        fSettings.do_factory_reset();
        return;
    }

    if(msg_id == AuraProtocol::TEMPSENSOR_CALIBRATION_VALUE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("tempsensor_calibration_value request called"));

        int8_t payload = fTemperatureManager.get_calibration_value();
        build_and_send(AuraProtocol::TEMPSENSOR_CALIBRATION_VALUE, AuraProtocol::RESPONSE, (uint8_t*)&payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::TEMPSENSOR_CALIBRATION_VALUE && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("tempsensor_calibration_value change called"));
        if(!validate_int_payload(msg_payload, msg_payload_size, -10, 10)) return;

        int8_t temp_cal_value = (int8_t)msg_payload[0];
        fTemperatureManager.set_calibration_value(temp_cal_value);
        fSettings.write(SETT_TEMPERATURE_SENSOR_CALIBRATION, (uint8_t*)&temp_cal_value);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::ADAFRUITIO_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("adafruitio_enabled request called"));

        uint8_t payload = fAdafruitIoIntegration.get_enabled();
        build_and_send(AuraProtocol::ADAFRUITIO_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::ADAFRUITIO_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("adafruitio_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t aio_req_state = msg_payload[0];

        /* If the client is already enabled and an enable is requested -> restart the client */
        if((aio_req_state == 1) && fAdafruitIoIntegration.get_enabled()) {
            fAdafruitIoIntegration.restart();
        }

        uint8_t aio_enabled = false;
        /* If the client is disabled and an enable is requested -> enable */
        if(aio_req_state == 1 && !fAdafruitIoIntegration.get_enabled()) {
            fAdafruitIoIntegration.enable();
            aio_enabled = true;
            fSettings.write(SETT_ADAFRUITIO_ENABLED, &aio_enabled);

        /* If the client is enabled and a disable is requested -> disable */
        } else if(aio_req_state != 1 && fAdafruitIoIntegration.get_enabled()) {
            fAdafruitIoIntegration.disable();
            aio_enabled = false;
            fSettings.write(SETT_ADAFRUITIO_ENABLED, &aio_enabled);
        }
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::ADAFRUITIO_USERNAME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("adafruitio_username change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_ADAFRUITIO_USERNAME))) return;

        fSettings.write(SETT_ADAFRUITIO_USERNAME, msg_payload, false, msg_payload_size);
        fAdafruitIoIntegration.set_username((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::ADAFRUITIO_API_KEY && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("adafruitio_api_key change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_ADAFRUITIO_API_KEY))) return;

        fSettings.write(SETT_ADAFRUITIO_API_KEY, msg_payload, false, msg_payload_size);
        fAdafruitIoIntegration.set_api_key((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::TEMPERATURE_UNIT && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("temperature_unit request called"));

        uint8_t payload = fTemperatureManager.get_temperature_unit();
        build_and_send(AuraProtocol::TEMPERATURE_UNIT, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::TEMPERATURE_UNIT && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("temperature_unit change called"));
        if(!validate_uint_payload(msg_payload, msg_payload_size, 0, TU_MAX - 1)) return;

        uint8_t temp_unit_value = msg_payload[0];
        fTemperatureManager.set_temperature_unit(temp_unit_value);
        fWebweather.set_temperature_unit(temp_unit_value);
        fSettings.write(SETT_TEMPERATURE_UNIT, &temp_unit_value);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::WEBWEATHER_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("webweather_enabled request called"));

        uint8_t payload = fWebweather.get_enabled();
        build_and_send(AuraProtocol::WEBWEATHER_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
    }

    if(msg_id == AuraProtocol::WEBWEATHER_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("webweather_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t ww_req_state = msg_payload[0];

        /* If the client is already enabled and an enable is requested -> request a sync */
        if((ww_req_state == 1) && fWebweather.get_enabled()) {
            fWebweather.request_resync();
        }

        uint8_t ww_enabled = false;
        /* If the client is disabled and an enable is requested -> enable */
        if(ww_req_state == 1 && !fWebweather.get_enabled()) {
            fWebweather.enable();
            ww_enabled = true;
            fSettings.write(SETT_WEBWEATHER_ENABLED, &ww_enabled);

        /* If the client is enabled and a disable is requested -> disable */
        } else if(ww_req_state != 1 && fWebweather.get_enabled()) {
            fWebweather.disable();
            ww_enabled = false;
            fSettings.write(SETT_WEBWEATHER_ENABLED, &ww_enabled);
        }

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::WEBWEATHER_LOCATION && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("webweather_location change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_OPENWEATHERMAP_LOCATION))) return;

        fSettings.write(SETT_OPENWEATHERMAP_LOCATION, msg_payload, false, msg_payload_size);
        fWebweather.set_location((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::WEBWEATHER_LOCATION && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("webweather_location request called"));

        uint8_t setting_len = (uint8_t)fSettings.get_setting_length(SETT_OPENWEATHERMAP_LOCATION);
        uint8_t payload[setting_len];
        fSettings.read_block(SETT_OPENWEATHERMAP_LOCATION, payload);
        uint8_t payload_len = strnlen((const char*)payload, setting_len);
        build_and_send(AuraProtocol::WEBWEATHER_LOCATION, AuraProtocol::RESPONSE, payload, payload_len);
        return;
    }

    if(msg_id == AuraProtocol::WEBWEATHER_API_KEY && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("webweather_api_key change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_OPENWEATHERMAP_API_KEY))) return;

        fSettings.write(SETT_OPENWEATHERMAP_API_KEY, msg_payload, false, msg_payload_size);
        fWebweather.set_api_key((char*)msg_payload);
        return;
    }

    if(msg_id == AuraProtocol::MUTE_DEVICE && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_device request called"));

        uint8_t payload = fMuteManager.is_muted();
        build_and_send(AuraProtocol::MUTE_DEVICE, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::MUTE_DEVICE && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_device change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t mute = msg_payload[0];
        uint8_t settings_mute;
        if(mute == 1) {
            settings_mute = true;
            fSettings.write(SETT_MUTE, &settings_mute);
            fMuteManager.mute();
        } else if(mute == 0) {
            settings_mute = false;
            fSettings.write(SETT_MUTE, &settings_mute);
            fMuteManager.unmute();
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::MUTE_WHEN_SCREEN_IS_OFF && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_when_screen_is_off request called"));

        uint8_t payload = fLedmatrixMain.get_mute_when_screen_is_off();
        build_and_send(AuraProtocol::MUTE_WHEN_SCREEN_IS_OFF, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::MUTE_WHEN_SCREEN_IS_OFF && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_when_screen_is_off change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t mute_when_screen_is_off_req = msg_payload[0];
        uint8_t settings_val;
        if(mute_when_screen_is_off_req == 1) {
            settings_val = true;
            fSettings.write(SETT_MUTE_WHEN_SCREEN_IS_OFF, &settings_val);
            fLedmatrixMain.set_mute_when_screen_is_off(true);
            fBeep.play_acknowledge();
            fLog.log(LOG_TCPREMOTE, L_INFO, F("Mute while display is off enabled"));
        } else if(mute_when_screen_is_off_req == 0) {
            settings_val = false;
            fSettings.write(SETT_MUTE_WHEN_SCREEN_IS_OFF, &settings_val);
            fLedmatrixMain.set_mute_when_screen_is_off(false);
            fBeep.play_acknowledge();
            fLog.log(LOG_TCPREMOTE, L_INFO, F("Mute while display is off disabled"));
        }
        return;
    }

    if(msg_id == AuraProtocol::FORCE_NTP_SYNC && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("force_ntp_sync request called"));

        static uint32_t ntp_last_force_update = 0u;
        if(ntp_last_force_update + 5u > device_uptime) {
            fLog.log(LOG_TCPREMOTE, L_INFO, F("Forced NTP sync is under cooldown"));
            fBeep.play_negative_acknowledge();
        } else {
            ntp_last_force_update = device_uptime;
            fNtp.request_sync();
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::DST_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("dst_enabled request called"));

        uint8_t payload = fClockManager.getDstEnabled();
        build_and_send(AuraProtocol::DST_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::DST_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("dst_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t dst_enabled = msg_payload[0];
        if(dst_enabled == 1) {
            fSettings.write(SETT_DST_ENABLED, &dst_enabled);
            fClockManager.setDstEnabled(true);
            fLedmatrixScreens.clock_force_update();
            fBeep.play_acknowledge();
        } else if(dst_enabled == 0) {
            fSettings.write(SETT_DST_ENABLED, &dst_enabled);
            fClockManager.setDstEnabled(false);
            fLedmatrixScreens.clock_force_update();
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::OTA_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("ota_enabled request called"));

        uint8_t payload = fOta.get_enabled();
        build_and_send(AuraProtocol::OTA_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::OTA_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("ota_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t ota_enabled = msg_payload[0];
        if(ota_enabled == 1) {
            fSettings.write(SETT_OTA_ENABLED, &ota_enabled);
            fOta.set_enabled(true);
            fBeep.play_acknowledge();
        } else if(ota_enabled == 0) {
            fSettings.write(SETT_OTA_ENABLED, &ota_enabled);
            fOta.set_enabled(false);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::OTA_PASSWORD && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("ota_password change called"));
        if(!validate_string_payload(msg_payload, msg_payload_size, fSettings.get_setting_length(SETT_OTA_PASSWORD))) return;

        bool ota_use_pass;
        if(msg_payload[0] == '\0') {
            ota_use_pass = false;
            fSettings.write(SETT_OTA_USE_PASSWORD, (uint8_t*)&ota_use_pass);
            fOta.set_password(NULL);
            return;
        }

        ota_use_pass = true;
        fSettings.write(SETT_OTA_USE_PASSWORD, (uint8_t*)&ota_use_pass);
        fSettings.write(SETT_OTA_PASSWORD, (uint8_t *)msg_payload, false, msg_payload_size);
        fOta.set_password((char*)msg_payload);

        return;
    }

    if(msg_id == AuraProtocol::REBOOT && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("reboot request called"));
        if(!validate_timestamp_payload(msg_payload, msg_payload_size, 2u)) return;
        fEspHal.system_reboot();
        return;
    }

    if(msg_id == AuraProtocol::MQTT_CLIENT_STATUS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("MQTT client status request called"));
        uint8_t payload = fMqttManager.get_client_state();
        build_and_send(AuraProtocol::MQTT_CLIENT_STATUS, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::ADAFRUITIO_CLIENT_STATUS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("AdafruitIO client status request called"));
        uint8_t payload = fAdafruitIoIntegration.get_client_state();
        build_and_send(AuraProtocol::ADAFRUITIO_CLIENT_STATUS, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::WEATHER_CLIENT_STATUS && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("Weather client status request called"));
        uint8_t payload = fWebweather.get_sync_state();
        build_and_send(AuraProtocol::WEATHER_CLIENT_STATUS, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::MAGIC_NUMBERS_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("magic_numbers_enabled request called"));
        uint8_t payload = fLedmatrixScreens.magic_numbers_get_enabled();
        build_and_send(AuraProtocol::MAGIC_NUMBERS_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::MAGIC_NUMBERS_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("magic_numbers_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t magicnumbers_enabled = msg_payload[0];
        if(magicnumbers_enabled == 1) {
            fSettings.write(SETT_MAGIC_NUMBERS_ENABLED, &magicnumbers_enabled);
            fLedmatrixScreens.magic_numbers_set_enabled(true);
            fBeep.play_acknowledge();
        } else if(magicnumbers_enabled == 0) {
            fSettings.write(SETT_MAGIC_NUMBERS_ENABLED, &magicnumbers_enabled);
            fLedmatrixScreens.magic_numbers_set_enabled(false);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::BUTTON_SHORTPRESS_ACTION && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("button_shortpress_action request called"));
        uint8_t payload = fButtonHandler.get_shortpress_action();
        build_and_send(AuraProtocol::BUTTON_SHORTPRESS_ACTION, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::BUTTON_SHORTPRESS_ACTION && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("button_shortpress_action change called"));
        if(!validate_int_payload(msg_payload, msg_payload_size, 0u, ButtonHandler::BTN_ACT_MAX)) return;

        uint8_t shortpress_action = msg_payload[0];
        fSettings.write(SETT_BUTTON_SHORTPRESS_ACTION, &shortpress_action);
        fButtonHandler.set_shortpress_action(shortpress_action);
        fBeep.play_acknowledge();

        return;
    }

    if(msg_id == AuraProtocol::BUTTON_LONGPRESS_ACTION && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_DEBUG, F("button_longpress_action request called"));
        uint8_t payload = fButtonHandler.get_longpress_action();
        build_and_send(AuraProtocol::BUTTON_LONGPRESS_ACTION, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::BUTTON_LONGPRESS_ACTION && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("button_longpress_action change called"));
        if(!validate_int_payload(msg_payload, msg_payload_size, 0u, ButtonHandler::BTN_ACT_MAX)) return;

        uint8_t longpress_action = msg_payload[0];
        fSettings.write(SETT_BUTTON_LONGPRESS_ACTION, &longpress_action);
        fButtonHandler.set_longpress_action(longpress_action);
        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::MUTE_SCHEDULE_ENABLED && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_schedule_enabled request called"));

        uint8_t payload = fMuteManager.get_mute_schedule_enabled();
        build_and_send(AuraProtocol::MUTE_SCHEDULE_ENABLED, AuraProtocol::RESPONSE, &payload, 1u);
        return;
    }

    if(msg_id == AuraProtocol::MUTE_OFF_TIME && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_off_time request called"));

        uint8_t payload[2];
        payload[0] = fMuteManager.get_mute_off_hour();
        payload[1] = fMuteManager.get_mute_off_minute();
        build_and_send(AuraProtocol::MUTE_OFF_TIME, AuraProtocol::RESPONSE, payload, 2u);
        return;
    }

    if(msg_id == AuraProtocol::MUTE_ON_TIME && msg_type == AuraProtocol::REQUEST) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_on_time request called"));

        uint8_t payload[2];
        payload[0] = fMuteManager.get_mute_on_hour();
        payload[1] = fMuteManager.get_mute_on_minute();
        build_and_send(AuraProtocol::MUTE_ON_TIME, AuraProtocol::RESPONSE, payload, 2u);
        return;
    }

    if(msg_id == AuraProtocol::MUTE_SCHEDULE_ENABLED && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_schedule_enabled change called"));
        if(!validate_bool_payload(msg_payload, msg_payload_size)) return;

        uint8_t mutescheduleenabled = msg_payload[0];
        if(mutescheduleenabled == 1) {
            fSettings.write(SETT_MUTE_SCHEDULE_ENABLED, &mutescheduleenabled);
            fMuteManager.set_mute_schedule_enabled(true);
            fBeep.play_acknowledge();
        } else if(mutescheduleenabled == 0) {
            fSettings.write(SETT_MUTE_SCHEDULE_ENABLED, &mutescheduleenabled);
            fMuteManager.set_mute_schedule_enabled(false);
            fBeep.play_acknowledge();
        }
        return;
    }

    if(msg_id == AuraProtocol::MUTE_OFF_TIME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_off_time change called"));
        if(!validate_time_payload(msg_payload, msg_payload_size)) return;

        uint8_t hour = msg_payload[0];
        uint8_t minute = msg_payload[1];
        fMuteManager.set_mute_off_time(hour, minute);

        fSettings.write(SETT_MUTE_OFF_HOUR, &hour);
        fSettings.write(SETT_MUTE_OFF_MINUTE, &minute);

        fBeep.play_acknowledge();
        return;
    }

    if(msg_id == AuraProtocol::MUTE_ON_TIME && msg_type == AuraProtocol::CHANGE_VALUE) {
        fLog.log(LOG_TCPREMOTE, L_INFO, F("mute_on_time change called"));
        if(!validate_time_payload(msg_payload, msg_payload_size)) return;

        uint8_t hour = msg_payload[0];
        uint8_t minute = msg_payload[1];
        fMuteManager.set_mute_on_time(hour, minute);

        fSettings.write(SETT_MUTE_ON_HOUR, &hour);
        fSettings.write(SETT_MUTE_ON_MINUTE, &minute);

        fBeep.play_acknowledge();
        return;
    }

}

bool TcpRemote::validate_bool_payload(uint8_t* payload, uint8_t payload_len) {
    bool result = true;
    if(payload_len != 1) result = false;
    else if(payload[0] != 0 && payload[0] != 1) result = false;

    if(result) return true;

    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (bool)"));
    return false;
}

bool TcpRemote::validate_string_payload(uint8_t* payload, uint8_t payload_len, uint8_t max_len) {
    bool end_present;

    /* Check if we fit in the max length */
    if(payload_len > max_len) goto validation_failed;

    /* Check if the data contains a '\0' somewhere */
    end_present = false;
    for(uint8_t i = 0; i < max_len ; i++) {
        if(payload[i] == '\0') end_present = true;
    }
    if(!end_present) goto validation_failed;

    /* Check if the string length exceeds the max length */
    if(strnlen((char*)payload, 254) > payload_len) goto validation_failed;

    return true;

validation_failed:
    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (string)"));
    return false;
}

bool TcpRemote::validate_int_payload(uint8_t* payload, uint8_t payload_len, int8_t min_val, int8_t max_val) {
    bool result = true;
    if(payload_len != 1) result = false;
    else if((int8_t)payload[0] < min_val || (int8_t)payload[0] > max_val) result = false;

    if(result) return true;

    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (int)"));
    return false;
}

bool TcpRemote::validate_uint_payload(uint8_t* payload, uint8_t payload_len, uint8_t min_val, uint8_t max_val) {
    bool result = true;
    if(payload_len != 1) result = false;
    else if(payload[0] < min_val || payload[0] > max_val) result = false;

    if(result) return true;

    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (uint)"));
    return false;
}

bool TcpRemote::validate_time_payload(uint8_t* payload, uint8_t payload_len) {
    bool result = true;
    if(payload_len != 2) result = false;
    else if(payload[0] > 23 || payload[1] > 59) result = false;

    if(result) return true;

    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (time)"));
    return false;
}

bool TcpRemote::validate_var_len_uint_payload(uint8_t payload_len, uint8_t number_of_bytes) {
    fLog.log(LOG_TCPREMOTE, L_DEBUG, F("Payload len: %u  Bytes: %u"), payload_len, number_of_bytes);
    bool result = true;
    if(payload_len != number_of_bytes) result = false;

    if(result) return true;

    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (var len uint)"));
    return false;
}

bool TcpRemote::validate_timestamp_payload(uint8_t* payload, uint8_t payload_len, uint8_t tolerance) {
    uint32_t extracted_timestamp = 0u;
    uint32_t current_time = fDeviceClock.get();

    if(payload_len == sizeof(uint32_t)) {
        memcpy(&extracted_timestamp, payload, sizeof(uint32_t));
        if((current_time <= extracted_timestamp + tolerance) && (current_time >= extracted_timestamp - tolerance)) {
            return true;
        }
    }

    fLog.log(LOG_TCPREMOTE, L_ERROR, F("Payload validation failed! (timestamp); exp='%u' act='%u'"), current_time, extracted_timestamp);
    return false;
}

void TcpRemote::build_and_send(uint8_t msg_id, uint8_t msg_type, uint8_t *payload, uint8_t payload_size) {
    AuraProtocol::message msg;
    msg.msg_id = msg_id;
    msg.msg_type = msg_type;
    msg.payload = payload;
    msg.payload_size = payload_size;
    build_and_send(&msg);
}

void TcpRemote::build_and_send(AuraProtocol::message* msg) {
    uint8_t* raw_message = fAuraProtocol.build(msg);

    /* If the shared key is available it means we had a successful security handshake, so we encrypt the message */
    /* We don't encrypt the message terminating bytes */
    if(key_shared) encrypt(raw_message, msg->size - 2);

    tcpremote_client.write(raw_message, msg->size);
    /* fAuraProtocol.log_raw_message(raw_message, msg->size); */
    free(raw_message);
}
