#include "temperature.h"

TemperatureManager::TemperatureManager(Logger &log, Settings &settings) :
fLog(log),
fSettings(settings)
{
    ;
}

void TemperatureManager::read_settings() {
    set_calibration_value((int8_t)fSettings.read_byte_s(SETT_TEMPERATURE_SENSOR_CALIBRATION));
    set_temperature_unit(fSettings.read_byte_s(SETT_TEMPERATURE_UNIT));
}

void TemperatureManager::read_sensor() {
    if(sensor_present == false) return;

    if(temperature_unit == TU_FAHRENHEIT) {
        device_temperature = lm75a.getTemperatureInFarenheit() + calibration_value;
    } else {
        device_temperature = lm75a.getTemperature() + calibration_value;
    }
}

float TemperatureManager::get_temperature() {
    return device_temperature;
}

void TemperatureManager::log_temperature() {
    fLog.log(LOG_DEVICETEMPERATURE, L_INFO, F("Device temperature: %.1f°%c"), device_temperature, get_temperature_postfix_as_char());
}

void TemperatureManager::init() {
    fLog.log(LOG_DEVICETEMPERATURE, L_INFO, F("Initializing sensor..."), device_temperature);
    lm75a.begin();
    if(!lm75a.isConnected()) {
        fLog.log(LOG_DEVICETEMPERATURE, L_INFO, F("Sensor is not present!"));
        sensor_present = false;
        return;
    }
    fLog.log(LOG_DEVICETEMPERATURE, L_INFO, F("Sensor ID: 0x%02x"), lm75a.getProdId());
    sensor_present = true;
}

void TemperatureManager::set_calibration_value(int8_t new_cal_value) {
    calibration_value = new_cal_value;
    fLog.log(LOG_DEVICETEMPERATURE, L_INFO, F("Setting calibration value: %d"), new_cal_value);
}

int8_t TemperatureManager::get_calibration_value() {
    return calibration_value;
}

bool TemperatureManager::get_sensor_present() {
    return sensor_present;
}

uint8_t TemperatureManager::get_temperature_unit() {
    return temperature_unit;
}

void TemperatureManager::set_temperature_unit(uint8_t tu_new) {
    if(tu_new == temperature_unit) return;

    temperature_unit = tu_new;
    read_sensor();
    notify_subscribers(device_temperature);
}

char TemperatureManager::get_temperature_postfix_as_char() {
    return (temperature_unit == TU_FAHRENHEIT) ? 'F' : 'C';
}

void TemperatureManager::subscribe(TemperatureSubscriber *sub) {
    subscriber_list.push_back(sub);
}

void TemperatureManager::unsubscribe(TemperatureSubscriber *sub) {
    subscriber_list.remove(sub);
}

void TemperatureManager::notify_subscribers(const int16_t temperature) {
    for(auto &i : subscriber_list) {
        i->temp_change_callback(temperature);
    }
}

void TemperatureManager::task() {

    if(sensor_present == false) return;

    read_sensor();

    float temp_difference = device_temperature - device_temperature_prev;
    /* abs() doesn't seem to work for float values */
    if(temp_difference >= TEMPERATURE_CHANGE_TRESHOLD || temp_difference <= ((float)-1.0 * TEMPERATURE_CHANGE_TRESHOLD)) {
        log_temperature();

        /* If the whole part of the temperature changes - notify subscribers */
        int16_t device_temperature_wh = (int16_t)device_temperature;
        if(device_temperature_wh != device_temperature_wh_prev) {
            notify_subscribers(device_temperature_wh);
            device_temperature_wh_prev = device_temperature_wh;
        }

        device_temperature_prev = device_temperature;
    }
}
