#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include <Arduino.h>
#include <list>
#include "LM75A_driver.h"
#include "../log/log.h"
#include "../settings/settings.h"


class TemperatureSubscriber {
public:
    virtual ~TemperatureSubscriber(){};
    virtual void temp_change_callback(const int16_t temperature) = 0;
};


class TemperaturePublisher {
public:
    virtual ~TemperaturePublisher(){};
    virtual void subscribe(TemperatureSubscriber *sub) = 0;
    virtual void unsubscribe(TemperatureSubscriber *sub) = 0;
    virtual void notify_subscribers(const int16_t temperature) = 0;
};


class TemperatureManager : public TemperaturePublisher {

public:

    TemperatureManager(Logger &log, Settings &settings);
    void read_settings();
    float get_temperature();
    void set_calibration_value(int8_t new_cal_value);
    int8_t get_calibration_value();
    bool get_sensor_present();
    uint8_t get_temperature_unit();
    void set_temperature_unit(uint8_t tu_new);
    char get_temperature_postfix_as_char();
    void subscribe(TemperatureSubscriber *sub) override;
    void unsubscribe(TemperatureSubscriber *sub) override;
    void init();
    void task();

private:

    const float TEMPERATURE_CHANGE_TRESHOLD = 0.2f;

    M2M_LM75A lm75a;
    float device_temperature = 0.0f;
    float device_temperature_prev = 0.0f;
    int16_t device_temperature_wh_prev = 0;
    int8_t calibration_value = 0;
    bool sensor_present = false;
    uint8_t temperature_unit = TU_CELSIUS;
    std::list<TemperatureSubscriber*> subscriber_list;

    void log_temperature();
    void read_sensor();
    void notify_subscribers(const int16_t temperature) override;

    Logger &fLog;
    Settings &fSettings;

};

#endif /* TEMPERATURE_H */
