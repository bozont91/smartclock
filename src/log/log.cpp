#include "log.h"

WiFiServer tcplogger_server(TCP_LOGGER_PORT);
WiFiClient tcplogger_client;

Logger::Logger(DeviceClock &deviceclock) :
fDeviceClock(deviceclock)
{
    tcplogger_server.begin();
}

void Logger::log(uint8_t log_dest, uint8_t log_level, const __FlashStringHelper *fmt, ... ) {

    /* Check the log level */
    if(log_level > GLOBAL_LOG_LEVEL) return;

    /* Print the arguments to the buffer (2x in size to fit the arguments) */
    PGM_P fmt_p = reinterpret_cast<PGM_P>(fmt);
    size_t message_size = strlen_P(fmt_p) * 2;

    /* Have a minimum buffer size of 100 - for short messages with long arguments */
    if(message_size < 100) message_size = 100;

    char* message = (char*)malloc(message_size);
    va_list args;
    va_start(args, fmt);
    vsnprintf_P(message, message_size, fmt_p, args);
    va_end(args);

    /* Print the timestamp to a buffer (40 bytes) */
    time_t now = fDeviceClock.get();
    struct tm ts = *localtime(&now);
    char* timebuf = (char*)malloc(40);
    strftime(timebuf, 40, "%a %Y-%m-%d %H:%M:%S", &ts);

    /* Get the flash pointer to the TAG and print it to a buffer (40 bytes) */
    PGM_P tag_p = log_get_tag(log_dest);
    char* tag_buf = (char*)malloc(strlen_P(tag_p) + 1);
    sprintf_P(tag_buf, "%s", tag_p);

    /* Assemble the output logmessage */
    char* logmessage_out = (char*)malloc(LOG_BUFFER_SIZE);
    sprintf(logmessage_out, "%s (%c) [%s] %s", timebuf, get_loglevel_char(log_level), tag_buf, message);

    free(timebuf);
    free(tag_buf);
    free(message);

    /* Send the logmessage */
    Serial.println(logmessage_out);

    if(tcplogger_state == TCPLOGGER_SERVE_CLIENT) {
        tcplogger_client.println(logmessage_out);
    }

    free(logmessage_out);

}

void Logger::task() {

    switch(tcplogger_state) {
        case TCPLOGGER_WAIT_FOR_CLIENT:
            tcplogger_client = tcplogger_server.available();
            if (tcplogger_client) {
                tcplogger_state = TCPLOGGER_SERVE_CLIENT;
                tcplogger_client.println("\naura SmartClock");
                tcplogger_client.println("---------------");
                tcplogger_client.printf("Software version: %s %s (%s)\n", SOFTWARE_VERSION, SOFTWARE_RELEASE_TYPE, SOFTWARE_COMMIT_ID);
                tcplogger_client.printf("Build date: %s\n", SOFTWARE_BUILD_DATE);
                tcplogger_client.printf("Device uptime: %u seconds\n", device_uptime);
                tcplogger_client.println("---------------");
                log(LOG_SYSTEM, L_INFO, F("TCP log client connected (%s)"), tcplogger_client.remoteIP().toString().c_str());
            }
        break;

        case TCPLOGGER_SERVE_CLIENT:
            if(!tcplogger_client.connected()) {
                tcplogger_client.stop();
                tcplogger_state = TCPLOGGER_WAIT_FOR_CLIENT;
                log(LOG_SYSTEM, L_INFO, F("TCP log client disconnected"));
            }
        break;
    }
}

char Logger::get_loglevel_char(uint8_t log_level) {
    if(log_level == L_INFO) return 'i';
    if(log_level == L_ERROR) return 'e';
    if(log_level == L_DEBUG) return 'd';
    return ' ';
}

const char logtag_1[] PROGMEM =  "SYSTEM";
const char logtag_2[] PROGMEM =  "NTP";
const char logtag_3[] PROGMEM =  "WEATHER";
const char logtag_4[] PROGMEM =  "LEDMATRIX";
const char logtag_5[] PROGMEM =  "TCPREMOTE";
const char logtag_6[] PROGMEM =  "WEBSERVER";
const char logtag_7[] PROGMEM =  "ALARM";
const char logtag_8[] PROGMEM =  "DEVICETEMP";
const char logtag_9[] PROGMEM =  "DISPLAYAUTOONOFF";
const char logtag_10[] PROGMEM = "SETTINGS";
const char logtag_11[] PROGMEM = "WELCOMEWIZARD";
const char logtag_12[] PROGMEM = "CLOCKMANAGER";
const char logtag_13[] PROGMEM = "MQTT";
const char logtag_14[] PROGMEM = "ROTARYENCODER";
const char logtag_15[] PROGMEM = "ADAFRUITIO";
const char logtag_16[] PROGMEM = "BUTTONHANDLER";
const char logtag_17[] PROGMEM = "TETRIS";
const char logtag_18[] PROGMEM = "SNAKE";
const char logtag_19[] PROGMEM = "MDNS";
const char logtag_20[] PROGMEM = "BEEP";
const char logtag_21[] PROGMEM = "OTA";
const char logtag_22[] PROGMEM = "WIFICHECKER";
const char logtag_23[] PROGMEM = "JSON";
const char logtag_24[] PROGMEM = "MUTEMANAGER";
const char logtag_25[] PROGMEM = "UNKNOWN";

const char* const log_tag_map[] PROGMEM = {
    logtag_1, logtag_2, logtag_3, logtag_4, logtag_5,
    logtag_6, logtag_7, logtag_8, logtag_9, logtag_10,
    logtag_11, logtag_12, logtag_13, logtag_14, logtag_15,
    logtag_16, logtag_17, logtag_18, logtag_19, logtag_20,
    logtag_21, logtag_22, logtag_23, logtag_24, logtag_25
};

PGM_P Logger::log_get_tag(uint8_t tag) {
    if(tag >= LOG_DEST_MAX) return log_tag_map[LOG_UNKNOWN];
    return log_tag_map[tag];
}
