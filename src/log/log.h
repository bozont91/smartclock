#ifndef LOG_H
#define LOG_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "../common.h"
#include "../deviceclock/deviceclock.h"

enum log_dest_e {
    LOG_SYSTEM,
    LOG_NTP,
    LOG_WEATHER,
    LOG_LEDMATRIX,
    LOG_TCPREMOTE,
    LOG_WEBSERVER,
    LOG_ALARM,
    LOG_DEVICETEMPERATURE,
    LOG_DISPLAYAUTOONOFF,
    LOG_SETTINGS,
    LOG_WELCOMEWIZARD,
    LOG_CLOCKMANAGER,
    LOG_MQTT,
    LOG_ROTARYENCODER,
    LOG_ADAFRUITIO,
    LOG_BUTTONHANDLER,
    LOG_TETRIS,
    LOG_SNAKE,
    LOG_MDNS,
    LOG_BEEP,
    LOG_OTA,
    LOG_WIFICHECKER,
    LOG_JSON,
    LOG_MUTEMANAGER,

    LOG_UNKNOWN,
    LOG_DEST_MAX
};

enum log_level_e {
    L_ERROR,
    L_INFO,
    L_DEBUG,
    L_VERBOSE,
};


class Logger {

public:

    Logger(DeviceClock &deviceclock);
    void log(uint8_t log_dest, uint8_t log_level, const __FlashStringHelper *fmt, ... );
    void task();

private:

    #define LOG_BUFFER_SIZE 256u

    enum tcp_logger_states_e {
        TCPLOGGER_WAIT_FOR_CLIENT = 0u,
        TCPLOGGER_SERVE_CLIENT
    };

    char get_loglevel_char(uint8_t log_level);
    PGM_P log_get_tag(uint8_t tag);
    uint8_t tcplogger_state = TCPLOGGER_WAIT_FOR_CLIENT;

    DeviceClock &fDeviceClock;

};

#endif /* LOG_H */
