#include "ntp.h"

NtpManager::NtpManager(DeviceClock &deviceclock, Logger &log, ClockManager &clockmanager) :
fDeviceClock(deviceclock),
fLog(log),
fClockManager(clockmanager)
{
    memset(packet_buffer, 0x00, NTP_PACKET_SIZE);
}

bool NtpManager::start_sync() {

    if(!ntp_udp_socket.begin(npt_local_port)) {
        fLog.log(LOG_NTP, L_ERROR, F("Socket creation failed"));
        return false;
    }

    IPAddress ntp_server_ip;
    if(!WiFi.hostByName(ntp_server_address, ntp_server_ip)) {
        fLog.log(LOG_NTP, L_ERROR, F("NTP server (%s) cannot be found"), ntp_server_address);
        return false;
    }

    if(!send_ntp_packet(ntp_server_ip)) {
        fLog.log(LOG_NTP, L_ERROR, F("Could not communicate with server (%s)"), ntp_server_address);
        return false;
    }

    return true;
}

void NtpManager::end_sync() {
    ntp_udp_socket.stop();
}


void NtpManager::request_sync() {
    sync_requested = true;
}

bool NtpManager::process_ntp_response() {

    int received_bytes = ntp_udp_socket.parsePacket();

    if(received_bytes == 0) {
        fLog.log(LOG_NTP, L_INFO, F("Waiting for NTP response"));
    } else {
        fLog.log(LOG_NTP, L_INFO, F("Response received (%d bytes)"), received_bytes);
    }

    if(received_bytes == NTP_PACKET_SIZE) {

        /* NTP packet received */
        ntp_udp_socket.read(packet_buffer, NTP_PACKET_SIZE);

        /* The timestamp starts at byte 40 of the received packet and is four bytes, */
        /* or two words, long. First, extract the two words: */
        uint16_t highWord = word(packet_buffer[40], packet_buffer[41]);
        uint16_t lowWord = word(packet_buffer[42], packet_buffer[43]);

        memset(packet_buffer, 0x00, NTP_PACKET_SIZE);

        /* Combine the four bytes (two words) into a long integer */
        /* this is NTP time (seconds since Jan 1 1900): */
        uint32_t secsSince1900 = highWord << 16 | lowWord;

        /* Unix time starts on Jan 1 1970. In seconds, that's 2208988800: */
        const uint32_t seventyYears = 2208988800UL;
        /* Subtract seventy years: */
        uint32_t device_main_clock_new = secsSince1900 - seventyYears;

        /* For DST switching testing */
        /* 2019.10.27. 2:59:40 UTC+1 DST*/
        //device_main_clock_new = 1572137980;

        /* 2020.03.29 1:59 UTC+1 No DST */
        //device_main_clock_new = 1585443580;

        fDeviceClock.set(device_main_clock_new);
        fClockManager.ntpPostSyncCorrectForTimezoneAndDst();

        /* Only do these at first sync */
        if(!fClockManager.isClockSynchronizedToNtp()) {
            fClockManager.setClockSynchronizedToNtp();
            fClockManager.device_startup_time = fDeviceClock.get() - device_uptime;
        }

        fLog.log(LOG_NTP, L_INFO, F("Updated NTP time: %lu"), fDeviceClock.get());

        return true;
    }

    return false;
}

bool NtpManager::send_ntp_packet(IPAddress& address) {

    /* Set all bytes in the buffer to 0 */
    memset(packet_buffer, 0, NTP_PACKET_SIZE);

    /* Initialize values needed to form NTP request */
    packet_buffer[0] = 0b11100011;  // LI, Version, Mode
    packet_buffer[1] = 0;           // Stratum, or type of clock
    packet_buffer[2] = 6;           // Polling Interval
    packet_buffer[3] = 0xEC;        // Peer Clock Precision

    /* 8 bytes of zero for Root Delay & Root Dispersion */
    packet_buffer[12]  = 49;
    packet_buffer[13]  = 0x4E;
    packet_buffer[14]  = 49;
    packet_buffer[15]  = 52;

    /* Send the packet */
    if(!ntp_udp_socket.beginPacket(address, 123)) return false;
    if(!ntp_udp_socket.write(packet_buffer, NTP_PACKET_SIZE)) return false;
    if(!ntp_udp_socket.endPacket()) return false;

    return true;
}

uint32_t NtpManager::get_last_sync_unix_timestamp() {
    return fDeviceClock.get() - (device_uptime - last_successful_ntp_update);
}

uint32_t NtpManager::get_next_sync_unix_timestamp() {
    return get_last_sync_unix_timestamp() + NTP_SYNC_FREQUENCY_HOURS*3600u;
}

uint32_t NtpManager::get_next_sync_time() {
    /* Returns the time of the next sync in device uptime */
    return last_successful_ntp_update + NTP_SYNC_FREQUENCY_HOURS*3600u;
}

void NtpManager::task() {
    static uint8_t state = NTP_INIT;
    static uint32_t sync_failure_cnt = 0u;
    static uint32_t sync_failure_timestamp = 0u;

    switch(state) {
        case NTP_INIT:
            request_sync();
            state = NTP_SLEEP;
            break;

        case NTP_SLEEP:
            if(device_uptime >= get_next_sync_time() || sync_requested) {
                sync_requested = false;
                fLog.log(LOG_NTP, L_INFO, F("Starting NTP sync..."));
                if(start_sync()) {
                    state = NTP_SYNC_IN_PROGRESS;
                } else {
                    sync_failure_timestamp = device_uptime;
                    state = NTP_SYNC_FAILED;
                }
                return;
            }
            break;

        case NTP_SYNC_IN_PROGRESS:
            if(process_ntp_response()) {
                state = NTP_SLEEP;
                fLog.log(LOG_NTP, L_INFO, F("NTP update successful"));
                sync_failure_cnt = 0;
                last_successful_ntp_update = device_uptime;
                end_sync();
                return;
            } else {
                sync_failure_cnt++;
            }

            if(sync_failure_cnt == 5) {
                sync_failure_cnt = 0;
                sync_failure_timestamp = device_uptime;
                state = NTP_SYNC_FAILED;
                fLog.log(LOG_NTP, L_ERROR, F("NTP sync failed, retrying in %lu seconds..."), NTP_DELAY_SECONDS_ON_FAILURE);
                end_sync();
            }
            break;

        case NTP_SYNC_FAILED:
            /* If the sync failed, we wait a predefined time before trying again */
            if(sync_failure_timestamp + NTP_DELAY_SECONDS_ON_FAILURE < device_uptime) {
                fLog.log(LOG_NTP, L_INFO, F("Retrying NTP sync..."));
                start_sync();
                state = NTP_SYNC_IN_PROGRESS;
            }
            break;
    }

}
