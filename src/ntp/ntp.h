#ifndef NTP_H
#define NTP_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "../log/log.h"
#include "../deviceclock/deviceclock.h"
#include "../clockmanager/clockmanager.h"

class NtpManager {

public:

    NtpManager(DeviceClock &deviceclock, Logger &log, ClockManager &clockmanager);

    void request_sync();
    uint32_t get_next_sync_unix_timestamp();
    uint32_t get_last_sync_unix_timestamp();
    void task();

private:

    static const uint8_t NTP_DELAY_SECONDS_ON_FAILURE = 60u;
    static const uint8_t NTP_PACKET_SIZE = 48u;
    const char* ntp_server_address = "pool.ntp.org";
    static const uint16_t npt_local_port = 2390;

    enum ntp_state_e {
        NTP_INIT,
        NTP_SLEEP,
        NTP_SYNC_IN_PROGRESS,
        NTP_SYNC_FAILED,
        NTP_COOLDOWN
    };

    WiFiUDP ntp_udp_socket;
    uint8_t packet_buffer[NTP_PACKET_SIZE];
    uint32_t last_successful_ntp_update = 0;
    bool sync_requested = false;

    bool start_sync();
    void end_sync();
    bool send_ntp_packet(IPAddress& address);
    bool process_ntp_response();
    uint32_t get_next_sync_time();

    DeviceClock &fDeviceClock;
    Logger &fLog;
    ClockManager &fClockManager;

};


#endif /* NTP_H */
