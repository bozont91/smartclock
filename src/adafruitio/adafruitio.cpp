#include "adafruitio.h"

WiFiClient AdafruitIoWifiClient;
PubSubClient AdafruitIoMqttclient(AdafruitIoWifiClient);


void AdafruitIoIntegration::handle_data_received(char* topic, byte* payload, unsigned int length) {
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("AdafruitIO message received; topic='%s' length='%u'"), topic, length);
    received_message_cnt++;

    if(length >= 150u) length = 150u;

    char* rx_payload = (char*)malloc((length + 1) * sizeof(char));
    memcpy(rx_payload, payload, length);
    rx_payload[length] = '\0';
    String payload_str = String(rx_payload);
    free(rx_payload);

    if(payload_str == F("showweather")) {
        fLog.log(LOG_ADAFRUITIO, L_INFO, F("Called: showweather"));
        fBeep.play_acknowledge();
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_TEMPERATURE);
        return;
    }

    if(payload_str == F("playmusic")) {
        fLog.log(LOG_ADAFRUITIO, L_INFO, F("Called: playmusic"));
        fBeep.start_music(stacys_mom, MUSIC_BLOCK_STACYS_MOM_SIZE);
        return;
    }

    if(payload_str.indexOf(F("showtext")) >= 0) {
        fLog.log(LOG_ADAFRUITIO, L_INFO, F("Called: showtext"));
        String texttoshow = payload_str.substring(payload_str.indexOf("showtext")+9);
        fLedmatrixMain.show_text(texttoshow);
        fBeep.play_acknowledge();
        return;
    }

    if(payload_str == F("displayoff")) {
        fLog.log(LOG_ADAFRUITIO, L_INFO, F("Called: displayoff"));
        if(fLedmatrixMain.get_display_on()) {
            fLedmatrixMain.display_off();
            fBeep.play_negative_acknowledge();
        }
        return;
    }

    if(payload_str == F("displayon")) {
        fLog.log(LOG_ADAFRUITIO, L_INFO, F("Called: displayon"));
        if(!fLedmatrixMain.get_display_on()) {
            fLedmatrixMain.display_on();
            fBeep.play_acknowledge();
        }
        return;
    }
}

AdafruitIoIntegration::AdafruitIoIntegration(
    mqtt_callback_function_p mqtt_callback_fn,
    Logger &log, Beep &beep,
    LedmatrixStateManager &ledmatrixmain,
    Settings &settings
) :
fLog(log),
fBeep(beep),
fLedmatrixMain(ledmatrixmain),
fSettings(settings)
{
    sprintf(aio_client_id, "aurasmartclock_%x", ESP.getChipId());
    AdafruitIoMqttclient.setServer("io.adafruit.com", 1883);
    AdafruitIoMqttclient.setCallback(mqtt_callback_fn);
    memset(aio_username, 0x00, AIO_USERNAME_LENGTH);
    memset(aio_api_key, 0x00, AIO_APIKEY_LENGTH);
}

void AdafruitIoIntegration::read_settings() {
    bool client_enabled = (bool)fSettings.read_byte_s(SETT_ADAFRUITIO_ENABLED);
    if(client_enabled) {
        char username[fSettings.get_setting_length(SETT_ADAFRUITIO_USERNAME)];
        fSettings.read_block(SETT_ADAFRUITIO_USERNAME, (uint8_t *)username);
        char api_key[fSettings.get_setting_length(SETT_ADAFRUITIO_API_KEY)];
        fSettings.read_block(SETT_ADAFRUITIO_API_KEY, (uint8_t *)api_key);
        set_username(username);
        set_api_key(api_key);
        enable();
    }
}

void AdafruitIoIntegration::set_username(char* username_in) {
    strcpy(aio_username, username_in);
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Setting username: %s"), username_in);
}

void AdafruitIoIntegration::set_api_key(char* api_key_in) {
    strcpy(aio_api_key, api_key_in);
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Setting API key"));
}

bool AdafruitIoIntegration::get_enabled() {
    return client_enabled;
}

void AdafruitIoIntegration::enable() {
    client_enabled = true;
    client_state = AIO_CLIENT_CONNECTING;
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Service enabled"));
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Client ID: %s"), aio_client_id);
}

void AdafruitIoIntegration::disable() {
    client_enabled = false;
    client_state = AIO_CLIENT_DISABLED;
    if(AdafruitIoMqttclient.connected()) AdafruitIoMqttclient.disconnect();
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Service disabled"));
}

void AdafruitIoIntegration::restart() {
    if(!client_enabled) return;
    client_state = AIO_CLIENT_CONNECTING;
    if(AdafruitIoMqttclient.connected()) AdafruitIoMqttclient.disconnect();
    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Reloading configuration and restarting"));
}

bool AdafruitIoIntegration::get_connected() {
    return AdafruitIoMqttclient.connected();
}

bool AdafruitIoIntegration::connect() {
    if(AdafruitIoMqttclient.connected()) return true;

    fLog.log(LOG_ADAFRUITIO, L_INFO, F("Connecting to AdafruitIO"));
    if(AdafruitIoMqttclient.connect(aio_client_id, aio_username, aio_api_key)) {
        String subscribe_feed = String(aio_username);
        subscribe_feed += "/feeds/aura";
        AdafruitIoMqttclient.subscribe(subscribe_feed.c_str());
        return true;
    }

    return false;
}

uint32_t AdafruitIoIntegration::get_received_message_cnt() {
    return received_message_cnt;
}

uint8_t AdafruitIoIntegration::get_client_state() {
    return client_state;
}

void AdafruitIoIntegration::task() {

    static uint32_t disconnect_cooldown_start = 0u;

    switch(client_state) {
        case AIO_CLIENT_DISABLED:
            break;

        case AIO_CLIENT_CONNECTING:
            if(connect()) {
                client_state = AIO_CLIENT_CONNECTED;
                fLog.log(LOG_ADAFRUITIO, L_INFO, F("Successfully connected"));
            } else {
                client_state = AIO_CLIENT_DISCONNECTED;
                fLog.log(LOG_ADAFRUITIO, L_ERROR, F("Could not connect to AdafruitIO"));
            }
            break;

        case AIO_CLIENT_CONNECTED:
            if(!AdafruitIoMqttclient.connected()) {
                client_state = AIO_CLIENT_DISCONNECTED;
                fLog.log(LOG_ADAFRUITIO, L_ERROR, F("Connection lost with AdafruitIO"));
                return;
            }
            AdafruitIoMqttclient.loop();
            break;

        case AIO_CLIENT_DISCONNECTED:
            if(connect()) {
                client_state = AIO_CLIENT_CONNECTED;
                fLog.log(LOG_ADAFRUITIO, L_INFO, F("Reconnected to AdafruitIO"));
            } else {
                client_state = AIO_CLIENT_CONNECTION_FAILED;
                fLog.log(LOG_ADAFRUITIO, L_ERROR, F("Reconnect failed, retrying soon"));
                disconnect_cooldown_start = device_uptime;
            }
            break;

        case AIO_CLIENT_CONNECTION_FAILED:
            if(disconnect_cooldown_start + DISCONNECT_COOLDOWN_TIME < device_uptime) {
                client_state = AIO_CLIENT_CONNECTING;
            }
            break;

    }
}
