#ifndef ADAFRUITIO_H
#define ADAFRUITIO_H

#include <Arduino.h>
#include <PubSubClient.h>
#include "../common.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../settings/settings.h"


class AdafruitIoIntegration {

public:
    AdafruitIoIntegration(
        mqtt_callback_function_p mqtt_callback_fn,
        Logger &log,
        Beep &beep,
        LedmatrixStateManager &ledmatrixmain,
        Settings &settings
    );

    enum client_state {
        AIO_CLIENT_DISABLED,
        AIO_CLIENT_CONNECTING,
        AIO_CLIENT_CONNECTED,
        AIO_CLIENT_DISCONNECTED,
        AIO_CLIENT_CONNECTION_FAILED
    };

    void read_settings();
    bool connect();
    void set_username(char* username_in);
    void set_api_key(char* api_key_in);
    bool get_enabled();
    void enable();
    void disable();
    void restart();
    bool get_connected();
    uint8_t get_client_state();
    void handle_data_received(char* topic, byte* payload, unsigned int length);
    uint32_t get_received_message_cnt();
    void task();

private:
    #define AIO_CLIENTID_LENGTH 32u
    #define AIO_USERNAME_LENGTH 32u
    #define AIO_APIKEY_LENGTH 64u

    char aio_client_id[AIO_CLIENTID_LENGTH];
    char aio_username[AIO_USERNAME_LENGTH];
    char aio_api_key[AIO_APIKEY_LENGTH];

    bool client_enabled = false;

    #define DISCONNECT_COOLDOWN_TIME 60u

    uint8_t client_state = AIO_CLIENT_DISABLED;
    uint32_t received_message_cnt = 0u;

    Logger &fLog;
    Beep &fBeep;
    LedmatrixStateManager &fLedmatrixMain;
    Settings &fSettings;

};


#endif /* ADAFRUITIO_H */
