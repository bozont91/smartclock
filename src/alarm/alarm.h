#ifndef ALARM_H
#define ALARM_H

#include <Arduino.h>
#include "../deviceclock/deviceclock.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../clockmanager/clockmanager.h"
#include "../settings/settings.h"

class AlarmManager {

public:
    AlarmManager(DeviceClock &deviceclock, Logger &log, Beep &beep, ClockManager &clockmanager, Settings &settings);
    void read_settings();
    void enable();
    void disable();
    void set_alarm(uint8_t hour, uint8_t min);
    uint8_t get_hour();
    uint8_t get_minute();
    bool get_enabled();
    bool get_is_alarm_in_progress();
    void dismiss_alarm();
    void set_only_on_weekdays_option(bool value);
    bool get_only_on_weekdays_option();
    void snooze_start();
    void snooze_cancel();
    bool is_snoozing();
    void set_snooze_time(uint8_t snooze_time);
    uint8_t get_snooze_time();
    void task();

    static const uint8_t SNOOZE_TIME_MIN_DEFAULT = 5u;

private:

    enum alarm_states_e {
        ALARM_DISABLED,
        ALARM_WAIT,
        ALARM_TRIGGERED,
        ALARM_SNOOZE,
        ALARM_STATE_MAX
    };

    uint8_t alarm_state = ALARM_DISABLED;
    uint8_t alarm_state_requested = ALARM_DISABLED;

    uint32_t alarm_last_triggered = 0u;
    uint8_t alarm_hour = 0u;
    uint8_t alarm_min = 0u;
    bool only_on_weekdays_option = false;
    uint32_t snooze_start_time = 0u;
    uint8_t snooze_time_min = SNOOZE_TIME_MIN_DEFAULT;

    bool check_alarm_day();
    bool alarm_check();
    bool snooze_check();
    void request_state(uint8_t state);

    static const uint16_t ALARM_AUTO_TURNOFF_TIMEOUT_SEC = 300u;
    void auto_turnoff_check();

    DeviceClock &fDeviceClock;
    Logger &fLog;
    Beep &fBeep;
    ClockManager &fClockManager;
    Settings &fSettings;

};

#endif /* ALARM_H */
