#include "alarm.h"


AlarmManager::AlarmManager(DeviceClock &deviceclock, Logger &log, Beep &beep, ClockManager &clockmanager, Settings &settings) :
fDeviceClock(deviceclock),
fLog(log),
fBeep(beep),
fClockManager(clockmanager),
fSettings(settings)
{
    ;
}

void AlarmManager::read_settings() {
    set_alarm(fSettings.read_byte_s(SETT_ALARM_HOUR), fSettings.read_byte_s(SETT_ALARM_MINUTE));
    set_only_on_weekdays_option(fSettings.read_byte_s(SETT_ALARM_ONLY_ON_WEEKDAYS));
    set_snooze_time(fSettings.read_byte_s(SETT_ALARM_SNOOZE_TIME));

    bool alarm_enabled = fSettings.read_byte_s(SETT_ALARM_ENABLED);
    if(alarm_enabled) {
        enable();
    } else {
        disable();
    }
}

/**
 * Enables the alarm.
 *
 * @return void
 */
void AlarmManager::enable() {
    alarm_last_triggered = 0u;
    alarm_state = ALARM_WAIT;
    fLog.log(LOG_ALARM, L_INFO, F("Alarm enabled"));
}

/**
 * Disables the alarm.
 *
 * @return void
 */
void AlarmManager::disable() {
    alarm_last_triggered = 0u;
    alarm_state = ALARM_DISABLED;
    fBeep.disable_alarm();
    fLog.log(LOG_ALARM, L_INFO, F("Alarm disabled"));
}

/**
 * Sets the alarm to the provided hour and minute.
 *
 * @param hour Hour to set the alarm to
 * @param minute Minute to set the alarm to
 *
 * @return void
 */
void AlarmManager::set_alarm(uint8_t hour, uint8_t min) {
    alarm_last_triggered = 0u;
    alarm_hour = hour;
    alarm_min = min;
    fLog.log(LOG_ALARM, L_INFO, F("Setting alarm to %02d:%02d"), alarm_hour, alarm_min);
}

/**
 * Returns the hour the alarm is set to.
 *
 * @return Alarm hour
 */
uint8_t AlarmManager::get_hour() {
    return alarm_hour;
}

/**
 * Returns the minute the alarm is set to.
 *
 * @return Alarm minute
 */
uint8_t AlarmManager::get_minute() {
    return alarm_min;
}

/**
 * Returns whether the alarm is enabled or not.
 *
 * @return 'true' if the alarm is enabled
 */
bool AlarmManager::get_enabled() {
    return (alarm_state != ALARM_DISABLED);
}

/**
 * Sets the Only on Weekdays option on or off.
 *
 * @param value bool value with the desired state
 *
 * @return void
 */
void AlarmManager::set_only_on_weekdays_option(bool value) {
    only_on_weekdays_option = value;
}

/**
 * Returns the status of the Only on Weekdays option.
 *
 * @return 'true' if Only on Weekdays is on, 'false' otherwise
 */
bool AlarmManager::get_only_on_weekdays_option() {
    return only_on_weekdays_option;
}

/**
 * Returns wether the alarm has been triggered.
 *
 * @return 'true' if an alarm is currently in progress
 */
bool AlarmManager::get_is_alarm_in_progress() {
    return (alarm_state == ALARM_TRIGGERED);
}

/**
 * Dismisses the alarm when it's been triggered.
 *
 * @return void
 */
void AlarmManager::dismiss_alarm() {
    request_state(ALARM_WAIT);
    fBeep.disable_alarm();
}

/**
 * Checks whether the alarm should be turned off automatically.
 * If the alarm has been ringing for ALARM_AUTO_TURNOFF_TIMEOUT_SEC seconds, and hasn't been dismissed,
 * this function will dismiss it automatically.
 *
 * @return void
 */
void AlarmManager::auto_turnoff_check() {
    if(fDeviceClock.get() > alarm_last_triggered + ALARM_AUTO_TURNOFF_TIMEOUT_SEC) {
        dismiss_alarm();
        fLog.log(LOG_ALARM, L_INFO, F("Alarm has not been turned off manually for %d seconds, turning it off"), ALARM_AUTO_TURNOFF_TIMEOUT_SEC);
    }
}

/**
 * Returns if the alarm should trigger on the current day.
 *
 * @return 'true' if the alarm should trigger on the current day, 'false' otherwise
 */
bool AlarmManager::check_alarm_day() {
    if(!only_on_weekdays_option) return true;

    if(fClockManager.isWeekend()) return false;

    return true;
}

/**
 * Checks whether the alarm should be triggered.
 *
 * @return 'true' if it's time to trigger the alarm
 */
bool AlarmManager::alarm_check() {

    if(!fClockManager.isClockSynchronizedToNtp()) return false;

    if(alarm_hour == fClockManager.getHour() && alarm_min == fClockManager.getMinute() && check_alarm_day()) {
        if(alarm_last_triggered+60 < fDeviceClock.get()) {
            return true;
        }
    }

    return false;
}

/**
 * Checks whether the snooze time has expired.
 *
 * @return 'true' if the snoozing has ended
 */
bool AlarmManager::snooze_check() {
    if(snooze_start_time + ((uint32_t)snooze_time_min * (uint32_t)60u) <= fDeviceClock.get()) {
        return true;
    }
    return false;
}

/**
 * Starts the snoozing by dismissing the alarm and setting the snooze start time.
 *
 * @return void
 */
void AlarmManager::snooze_start() {
    fLog.log(LOG_ALARM, L_INFO, F("Snoozing for %d minutes"), snooze_time_min);
    dismiss_alarm();
    snooze_start_time = fDeviceClock.get();
    request_state(ALARM_SNOOZE);
}

/**
 * Cancels the snooze timer if it's active.
 *
 * @return void
 */
void AlarmManager::snooze_cancel() {
    if(alarm_state == ALARM_SNOOZE) {
        alarm_state = ALARM_WAIT;
        fLog.log(LOG_ALARM, L_INFO, F("Snooze timer cancelled"));
    }
}

/**
 * Checks whether the snooze time is active.
 *
 * @return 'true' if the snooze timer is active
 */
bool AlarmManager::is_snoozing() {
    return (alarm_state == ALARM_SNOOZE);
}

/**
 * Sets the snooze time to the desired value.
 *
 * @param snooze_time time to set
 *
 * @return void
 */
void AlarmManager::set_snooze_time(uint8_t snooze_time) {
    snooze_time_min = snooze_time;
    fLog.log(LOG_ALARM, L_INFO, F("Setting snooze time to %d minutes"), snooze_time_min);
}

/**
 * Checks whether the snooze time is active.
 *
 * @return the actual 'snooze_time' value in minutes
 */
uint8_t AlarmManager::get_snooze_time() {
    return snooze_time_min;
}

/**
 * Requests a desired AlarmManager state.
 *
 * @return void
 */
void AlarmManager::request_state(uint8_t state) {
    alarm_state_requested = state;
}

/**
 * The periodic task for AlarmManager.
 *
 * @return void
 */
void AlarmManager::task() {

    switch(alarm_state) {
        case ALARM_DISABLED:
            break;

        case ALARM_WAIT:
            if(alarm_check()) {
                fLog.log(LOG_ALARM, L_INFO, F("The alarm has been triggered"));
                fBeep.enable_alarm();
                alarm_state = ALARM_TRIGGERED;
                alarm_last_triggered = fDeviceClock.get();
            }
            break;

        case ALARM_TRIGGERED:
            auto_turnoff_check();
            if(alarm_state_requested == ALARM_WAIT || alarm_state_requested == ALARM_SNOOZE) {
                alarm_state = alarm_state_requested;
                alarm_state_requested = ALARM_STATE_MAX;
            }
            break;

        case ALARM_SNOOZE:
            if(snooze_check()) {
                fLog.log(LOG_ALARM, L_INFO, F("Snoozing has ended - triggering the alarm again"));
                fBeep.enable_alarm();
                alarm_last_triggered = fDeviceClock.get();
                alarm_state = ALARM_TRIGGERED;
            }
            break;
    }

}
