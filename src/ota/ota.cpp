#include "ota.h"

AuraOta::AuraOta(
    Logger &log,
    Settings &settings
):
fLog(log),
fSettings(settings)
{
    ;
}

void AuraOta::init() {
    read_settings();
    callback_init();
}

void AuraOta::read_settings() {
    bool ota_use_pass = (bool)fSettings.read_byte_s(SETT_OTA_USE_PASSWORD);
    if(ota_use_pass) {
        uint8_t ota_pass_len = fSettings.get_setting_length(SETT_OTA_PASSWORD);
        char ota_pass[ota_pass_len];
        fSettings.read_block(SETT_OTA_PASSWORD, (uint8_t *)ota_pass);
        set_password(ota_pass);
        memset(ota_pass, 0, ota_pass_len);
    }

    bool ota_enabled = (bool)fSettings.read_byte_s(SETT_OTA_ENABLED);
    set_enabled(ota_enabled);
}

void AuraOta::callback_init() {
    ArduinoOTA.onStart([&]() {
        this->on_start();
    });
    ArduinoOTA.onEnd([&]() {
        this->on_end();
    });
    ArduinoOTA.onProgress([&](unsigned int progress, unsigned int total) {
        this->on_progress(progress, total);
    });
    ArduinoOTA.onError([&](ota_error_t error) {
        this->on_error(error);
    });
}

void AuraOta::on_start() {
    fLog.log(LOG_OTA, L_INFO, F("Over the Air update started..."));
}

void AuraOta::on_end() {
    fLog.log(LOG_OTA, L_INFO, F("Over the Air update finished"));
}

void AuraOta::on_progress(unsigned int progress, unsigned int total) {
    fLog.log(LOG_OTA, L_INFO, F("Update progress: %u%%"), (progress / (total / 100)));
}

void AuraOta::on_error(ota_error_t error) {
    String error_reason = "";
    if (error == OTA_AUTH_ERROR) error_reason = "Auth failed";
    else if (error == OTA_BEGIN_ERROR) error_reason = "Begin failed";
    else if (error == OTA_CONNECT_ERROR) error_reason = "Connect failed";
    else if (error == OTA_RECEIVE_ERROR) error_reason = "Receive failed";
    else if (error == OTA_END_ERROR) error_reason = "End failed";

    fLog.log(LOG_OTA, L_ERROR, F("Update failed: (%u) %s"), error, error_reason.c_str());
}

void AuraOta::set_password(char* pass) {
    ArduinoOTA.setPassword(pass);
    if(pass) {
        fLog.log(LOG_OTA, L_INFO, F("Password authentication set"));
    } else {
        fLog.log(LOG_OTA, L_INFO, F("Password authentication disabled"));
    }
}

bool AuraOta::get_enabled() {
    return service_enabled;
}

void AuraOta::set_enabled(bool status) {
    if(status) {
        ArduinoOTA.setHostname("aura SmartClock");
        ArduinoOTA.begin();
        service_enabled = true;
        fLog.log(LOG_OTA, L_INFO, F("Service enabled"));
    } else {
        /* ArduinoOTA doesn't have an end() method yet, so the service may still be advertising after it's been disabled */
        /* The device however won't accept any sessions */
        service_enabled = false;
        fLog.log(LOG_OTA, L_INFO, F("Service disabled"));
    }
}

void AuraOta::task() {
    if(!service_enabled) return;
    ArduinoOTA.handle();
}
