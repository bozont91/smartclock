#ifndef AURA_OTA_H
#define AURA_OTA_H

#include <Arduino.h>
#include <ArduinoOTA.h>
#include "../common.h"
#include "../log/log.h"
#include "../settings/settings.h"

class AuraOta {

public:
    AuraOta(Logger &log, Settings &settings);
    void init();
    void read_settings();
    void on_start();
    void on_end();
    void on_progress(unsigned int progress, unsigned int total);
    void on_error(ota_error_t error);
    void set_password(char* pass);
    bool get_enabled();
    void set_enabled(bool status);
    void task();

private:
    bool service_enabled = false;

    void callback_init();

    Logger &fLog;
    Settings &fSettings;

};

#endif /* AURA_OTA_H */
