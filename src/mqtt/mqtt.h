#ifndef MQTT_H
#define MQTT_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "../common.h"
#include "../log/log.h"
#include "../deviceclock/deviceclock.h"
#include "../clockmanager/clockmanager.h"
#include "../alarm/alarm.h"
#include "../temperature/temperature.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../beep/beep.h"
#include "../settings/settings.h"

extern void mqtt_callback(char* topic, byte* payload, unsigned int length);

enum mqtt_topics {
    MQTT_TOPIC_TEMPERATURE,
    MQTT_TOPIC_CLOCK,
    MQTT_TOPIC_DEVICEUPTIME,
    MQTT_TOPIC_SOFTWAREVERSION,
    MQTT_TOPIC_SOFTWAREBUILDDATE,
    MQTT_TOPIC_ALARMTIME
};


class MqttManager {

public:

    MqttManager(
        Logger &logger,
        DeviceClock &deviceclock,
        ClockManager &clockmanager,
        AlarmManager& alarmammanager,
        TemperatureManager &temperaturemanager,
        LedmatrixHal &ledmatrixhal,
        LedmatrixStateManager &ledmatrixmain,
        Beep &beep,
        Settings &settings
    );

    enum client_state {
        MQTT_CLIENT_DISABLED,
        MQTT_CLIENT_CONNECTING,
        MQTT_CLIENT_CONNECTED,
        MQTT_CLIENT_DISCONNECTED,
        MQTT_CLIENT_CONNECTION_FAILED
    };

    void read_settings();
    bool connect();
    bool get_connected();
    uint8_t get_client_state();
    void publish(uint8_t topic, bool retained, const char *fmt, ... );
    void handle_receive(char* topic, byte* payload, unsigned int length);
    char* get_received_data();
    char* get_received_topic();
    bool get_enabled();
    void enable();
    void disable();
    void restart();
    void set_broker_address(char* broker_address);
    void set_broker_port(uint16_t broker_port);
    char* get_broker_address();
    uint16_t get_broker_port();
    uint32_t get_sent_message_cnt();
    uint32_t get_received_message_cnt();
    void set_authentication_used(bool use_auth);
    void set_broker_username(char* broker_username);
    void set_broker_password(char* broker_password);
    void task();

private:

    bool client_enabled = false;
    uint8_t client_state = MQTT_CLIENT_DISABLED;

    #define MQTT_CLIENTID_SIZE 32u
    #define MQTT_SERVERADDRESS_SIZE 64u
    #define MQTT_BROKER_USERNAME_SIZE 32u
    #define MQTT_BROKER_PASSWORD_SIZE 32u

    char mqtt_client_id[MQTT_CLIENTID_SIZE];
    char mqtt_server_address[MQTT_SERVERADDRESS_SIZE];
    char mqtt_broker_username[MQTT_BROKER_USERNAME_SIZE];
    char mqtt_broker_password[MQTT_BROKER_PASSWORD_SIZE];

    uint16_t mqtt_server_port = 0u;

    #define MQTT_MAX_RX_BYTES 100u
    bool mqtt_data_received = false;
    char mqtt_rx_topic[MQTT_MAX_RX_BYTES];
    char mqtt_rx_payload[MQTT_MAX_RX_BYTES];

    #define DISCONNECT_COOLDOWN_TIME 60u

    bool authentication_used = false;
    uint32_t sent_message_cnt = 0u;
    uint32_t received_message_cnt = 0u;

    void periodic_publish();
    void handle_received_messages();
    void publish_initial_data();

    Logger &fLog;
    DeviceClock &fDeviceClock;
    ClockManager &fClockManager;
    AlarmManager &fAlarmManager;
    TemperatureManager &fTemperatureManager;
    LedmatrixHal &fLedmatrixHal;
    LedmatrixStateManager &fLedmatrixMain;
    Beep &fBeep;
    Settings &fSettings;


};

#endif /* MQTT_H */
