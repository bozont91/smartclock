#include "mqtt.h"


WiFiClient MqttWifiClient;
PubSubClient mqttclient(MqttWifiClient);

void MqttManager::handle_receive(char* topic, byte* payload, unsigned int length) {
    if(length >= MQTT_MAX_RX_BYTES) length = MQTT_MAX_RX_BYTES - 1;

    memcpy(mqtt_rx_payload, payload, length);
    mqtt_rx_payload[length] = '\0';
    strcpy(mqtt_rx_topic, topic);

    fLog.log(LOG_MQTT, L_INFO, F("Message received; topic='%s' length='%u' message='%s'"), topic, length, mqtt_rx_payload);
    mqtt_data_received = true;
}

char* MqttManager::get_received_data() {
    if(mqtt_data_received == false) return NULL;

    mqtt_data_received = false;
    return mqtt_rx_payload;
}

char* MqttManager::get_received_topic() {
    return mqtt_rx_topic;
}

MqttManager::MqttManager(
    Logger &logger,
    DeviceClock &deviceclock,
    ClockManager &clockmanager,
    AlarmManager& alarmammanager,
    TemperatureManager &temperaturemanager,
    LedmatrixHal &ledmatrixhal,
    LedmatrixStateManager &ledmatrixmain,
    Beep &beep,
    Settings &settings
) :
fLog(logger),
fDeviceClock(deviceclock),
fClockManager(clockmanager),
fAlarmManager(alarmammanager),
fTemperatureManager(temperaturemanager),
fLedmatrixHal(ledmatrixhal),
fLedmatrixMain(ledmatrixmain),
fBeep(beep),
fSettings(settings)
{
    sprintf(mqtt_client_id, "aurasmartclock_%x", ESP.getChipId());
    mqttclient.setCallback(mqtt_callback);
    memset(mqtt_server_address, 0x00, MQTT_SERVERADDRESS_SIZE);
    memset(mqtt_rx_topic, 0x00, MQTT_MAX_RX_BYTES);
    memset(mqtt_rx_payload, 0x00, MQTT_MAX_RX_BYTES);
    memset(mqtt_broker_username, 0x00, MQTT_BROKER_USERNAME_SIZE);
    memset(mqtt_broker_password, 0x00, MQTT_BROKER_PASSWORD_SIZE);
}

void MqttManager::read_settings() {
    bool mqtt_client_enabled = (bool)fSettings.read_byte_s(SETT_MQTT_CLIENT_ENABLED);
    if(mqtt_client_enabled) {
        char broker_address[fSettings.get_setting_length(SETT_MQTT_BROKER_ADDRESS)];
        fSettings.read_block(SETT_MQTT_BROKER_ADDRESS, (uint8_t *)broker_address);
        uint16_t broker_port;
        fSettings.read_block(SETT_MQTT_BROKER_PORT, (uint8_t *)&broker_port);
        set_broker_address(broker_address);
        set_broker_port(broker_port);

        char broker_username[fSettings.get_setting_length(SETT_MQTT_BROKER_USERNAME)];
        fSettings.read_block(SETT_MQTT_BROKER_USERNAME, (uint8_t *)broker_username);
        char broker_password[fSettings.get_setting_length(SETT_MQTT_BROKER_PASSWORD)];
        fSettings.read_block(SETT_MQTT_BROKER_PASSWORD, (uint8_t *)broker_password);

        if(broker_username[0] == '\0' || broker_password[0] == '\0') {
            set_authentication_used(false);
        } else {
            set_authentication_used(true);
            set_broker_username(broker_username);
            set_broker_password(broker_password);
            memset(broker_password, 0xFF, fSettings.get_setting_length(SETT_MQTT_BROKER_PASSWORD));
        }

        enable();
    }
}

bool MqttManager::get_enabled() {
    return client_enabled;
}

void MqttManager::enable() {
    client_enabled = true;
    client_state = MQTT_CLIENT_CONNECTING;
    fLog.log(LOG_MQTT, L_INFO, F("Client enabled"));
    fLog.log(LOG_MQTT, L_INFO, F("Client ID: %s"), mqtt_client_id);
}

void MqttManager::disable() {
    client_enabled = false;
    client_state = MQTT_CLIENT_DISABLED;
    if(mqttclient.connected()) mqttclient.disconnect();
    fLog.log(LOG_MQTT, L_INFO, F("Client disabled"));
}

void MqttManager::restart() {
    if(!client_enabled) return;
    client_state = MQTT_CLIENT_CONNECTING;
    if(mqttclient.connected()) mqttclient.disconnect();
    fLog.log(LOG_MQTT, L_INFO, F("Reloading configuration and restarting"));
}

void MqttManager::set_broker_address(char* broker_address) {
    strcpy(mqtt_server_address, broker_address);
    mqttclient.setServer(mqtt_server_address, mqtt_server_port);
    fLog.log(LOG_MQTT, L_INFO, F("Setting broker address: %s"), broker_address);
}

void MqttManager::set_broker_port(uint16_t broker_port) {
    mqtt_server_port = broker_port;
    mqttclient.setServer(mqtt_server_address, mqtt_server_port);
    fLog.log(LOG_MQTT, L_INFO, F("Setting broker port: %d"), broker_port);
}

char* MqttManager::get_broker_address() {
    return mqtt_server_address;
}

uint16_t MqttManager::get_broker_port() {
    return mqtt_server_port;
}

uint32_t MqttManager::get_sent_message_cnt() {
    return sent_message_cnt;
}

uint32_t MqttManager::get_received_message_cnt() {
    return received_message_cnt;
}

bool MqttManager::get_connected() {
    return mqttclient.connected();
}

void MqttManager::set_authentication_used(bool use_auth) {

    if(use_auth == authentication_used) return;

    authentication_used = use_auth;
    if(use_auth) {
        fLog.log(LOG_MQTT, L_INFO, F("Username/password authentication enabled"));
    } else {
        fLog.log(LOG_MQTT, L_INFO, F("Username/password authentication disabled"));
    }
}

void MqttManager::set_broker_username(char* broker_username) {
    strcpy(mqtt_broker_username, broker_username);
    fLog.log(LOG_MQTT, L_INFO, F("Setting broker username: %s"), broker_username);
}

void MqttManager::set_broker_password(char* broker_password) {
    strcpy(mqtt_broker_password, broker_password);
    fLog.log(LOG_MQTT, L_INFO, F("Setting broker password"));
}

bool MqttManager::connect() {
    if(mqttclient.connected()) return true;

    fLog.log(LOG_MQTT, L_INFO, F("Connecting to broker (%s:%d)"), mqtt_server_address, mqtt_server_port);

    bool connection_result;

    if(authentication_used) {
        connection_result = mqttclient.connect(mqtt_client_id, mqtt_broker_username, mqtt_broker_password);
    } else {
        connection_result = mqttclient.connect(mqtt_client_id);
    }

    if(connection_result) {
        mqttclient.subscribe("aura_smartclock/showtext");
        mqttclient.subscribe("aura_smartclock/setbrightness");
        mqttclient.subscribe("aura_smartclock/displayon");
        mqttclient.subscribe("aura_smartclock/displayoff");
        return true;
    }

    return false;
}

void MqttManager::publish(uint8_t topic, bool retained, const char *fmt, ... ) {

    if(!mqttclient.connected()) return;

    char message[200u];
    va_list args;
    va_start(args, fmt);
    vsnprintf(message, 200u, fmt, args);
    va_end(args);

    String topic_str = "aura_smartclock/";
    if(topic == MQTT_TOPIC_TEMPERATURE) {
        topic_str += "temperature";
    } else if(topic == MQTT_TOPIC_CLOCK) {
        topic_str += "clock";
    } else if(topic == MQTT_TOPIC_DEVICEUPTIME) {
        topic_str += "device_uptime";
    } else if(topic == MQTT_TOPIC_SOFTWAREVERSION) {
        topic_str += "software_version";
    } else if(topic == MQTT_TOPIC_SOFTWAREBUILDDATE) {
        topic_str += "software_build_date";
    } else if(topic == MQTT_TOPIC_ALARMTIME) {
        topic_str += "alarm_time";
    } else {
        topic_str += "unknown";
    }

    sent_message_cnt++;
    mqttclient.publish(topic_str.c_str(), message, retained);

}

void MqttManager::periodic_publish() {

    static float last_temperature = 0;
    float current_temperature = fTemperatureManager.get_temperature();
    float temp_difference = last_temperature - current_temperature;
    /* abs() doesn't seem to work for float values */
    if(temp_difference >= (float)0.2 || temp_difference <= (float)-0.2) {
        publish(MQTT_TOPIC_TEMPERATURE, false, "%.1f", current_temperature);
        last_temperature = current_temperature;
    }

    static uint8_t last_minute = 61u;
    uint8_t current_minute = fClockManager.getMinute();
    if(current_minute != last_minute) {
        publish(MQTT_TOPIC_CLOCK, false, "%02u:%02u", fClockManager.getHour(), fClockManager.getMinute());
        publish(MQTT_TOPIC_DEVICEUPTIME, false, "%u", device_uptime);
        last_minute = current_minute;
    }

    static uint8_t last_alarm_hour = 25u;
    static uint8_t last_alarm_min = 61u;
    uint8_t current_alarm_hour = fAlarmManager.get_hour();
    uint8_t current_alarm_min = fAlarmManager.get_minute();
    if(current_alarm_hour != last_alarm_hour || current_alarm_min != last_alarm_min) {
        publish(MQTT_TOPIC_ALARMTIME, false, "%02d:%02d", current_alarm_hour, current_alarm_min);
        last_alarm_hour = current_alarm_hour;
        last_alarm_min = current_alarm_min;
    }

}

void MqttManager::handle_received_messages() {
    char* received_data = get_received_data();
    if(received_data == NULL) return;

    received_message_cnt++;
    char* topic = get_received_topic();

    if(strcmp(topic, "aura_smartclock/showtext") == 0) {
        fLedmatrixMain.show_text(received_data);
        fBeep.play_acknowledge();
        return;
    }

    if(strcmp(topic, "aura_smartclock/setbrightness") == 0) {
        if(strlen(received_data) == 1) {
            uint8_t req_brightness = received_data[0] - '0';
            if(req_brightness <= MAX7219_LEDMATRIX_MAX_BRIGHTNESS) {
                fLedmatrixHal.set_brightness(req_brightness);
                /* fSettings.write(SETT_BRIGHTNESS, &req_brightness); */
                fBeep.play_acknowledge();
                return;
            }
        }
        fLog.log(LOG_MQTT, L_ERROR, F("Invalid payload for 'setbrightness'"));
        fBeep.play_negative_acknowledge();
        return;
    }

    if(strcmp(topic, "aura_smartclock/displayon") == 0) {
        if(!fLedmatrixMain.get_display_on()) {
            fLedmatrixMain.display_on();
            fBeep.play_acknowledge();
        }
        return;
    }

    if(strcmp(topic, "aura_smartclock/displayoff") == 0) {
        if(fLedmatrixMain.get_display_on()) {
            fLedmatrixMain.display_off();
            fBeep.play_negative_acknowledge();
        }
        return;
    }

}

void MqttManager::publish_initial_data() {
    publish(MQTT_TOPIC_SOFTWAREVERSION, true, "%s %s (%s)", SOFTWARE_VERSION, SOFTWARE_RELEASE_TYPE, SOFTWARE_COMMIT_ID);
    publish(MQTT_TOPIC_SOFTWAREBUILDDATE, true, "%s", SOFTWARE_BUILD_DATE);
    publish(MQTT_TOPIC_TEMPERATURE, false, "%.1f", fTemperatureManager.get_temperature());
}

uint8_t MqttManager::get_client_state() {
    return client_state;
}

void MqttManager::task() {
    static uint32_t disconnect_cooldown_start = 0u;

    switch(client_state) {
        case MQTT_CLIENT_DISABLED:
            break;

        case MQTT_CLIENT_CONNECTING:
            if(connect()) {
                client_state = MQTT_CLIENT_CONNECTED;
                fLog.log(LOG_MQTT, L_INFO, F("Successfully connected"));
                publish_initial_data();
            } else {
                client_state = MQTT_CLIENT_DISCONNECTED;
                fLog.log(LOG_MQTT, L_ERROR, F("Could not connect to broker"));
            }
            break;

        case MQTT_CLIENT_CONNECTED:
            if(!mqttclient.connected()) {
                client_state = MQTT_CLIENT_DISCONNECTED;
                fLog.log(LOG_MQTT, L_ERROR, F("Connection lost with broker"));
                return;
            }
            mqttclient.loop();
            periodic_publish();
            handle_received_messages();
            break;

        case MQTT_CLIENT_DISCONNECTED:
            if(connect()) {
                client_state = MQTT_CLIENT_CONNECTED;
                fLog.log(LOG_MQTT, L_INFO, F("Reconnected to broker"));
                publish_initial_data();
            } else {
                client_state = MQTT_CLIENT_CONNECTION_FAILED;
                fLog.log(LOG_MQTT, L_ERROR, F("Reconnect failed, retrying soon"));
                disconnect_cooldown_start = device_uptime;
            }
            break;

        case MQTT_CLIENT_CONNECTION_FAILED:
            if(disconnect_cooldown_start + DISCONNECT_COOLDOWN_TIME < device_uptime) {
                client_state = MQTT_CLIENT_CONNECTING;
            }
            break;

    }

}
