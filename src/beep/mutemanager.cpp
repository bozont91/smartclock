#include "mutemanager.h"

MuteManager::MuteManager(
    Beep &beep,
    Logger &log,
    ClockManager &clockmanager,
    Settings &settings
) :
fBeep(beep),
fLog(log),
fClockManager(clockmanager),
fSettings(settings)
{
    ;
}

void MuteManager::read_settings() {
    if((bool)fSettings.read_byte_s(SETT_MUTE)) mute();

    uint8_t on_hr = fSettings.read_byte_s(SETT_MUTE_ON_HOUR);
    uint8_t on_min = fSettings.read_byte_s(SETT_MUTE_ON_MINUTE);
    uint8_t off_hr = fSettings.read_byte_s(SETT_MUTE_OFF_HOUR);
    uint8_t off_min = fSettings.read_byte_s(SETT_MUTE_OFF_MINUTE);
    set_mute_on_time(on_hr, on_min);
    set_mute_off_time(off_hr, off_min);

    if(fSettings.read_byte_s(SETT_MUTE_SCHEDULE_ENABLED) == 1) {
        set_mute_schedule_enabled(true);
    } else {
        set_mute_schedule_enabled(false);
    }
}

void MuteManager::mute() {
    fBeep.mute();
    fLog.log(LOG_MUTEMANAGER, L_INFO, F("Device muted"));
}

void MuteManager::unmute() {
    fBeep.unmute();
    fLog.log(LOG_MUTEMANAGER, L_INFO, F("Device unmuted"));
}

bool MuteManager::is_muted() {
    return fBeep.is_muted();
}

bool MuteManager::get_mute_schedule_enabled() {
    return mute_schedule_enabled;
}

void MuteManager::set_mute_schedule_enabled(bool state) {
    mute_schedule_enabled = state;
    if(state) {
        fLog.log(LOG_MUTEMANAGER, L_INFO, F("Mute scheduling enabled"));
    } else {
        fLog.log(LOG_MUTEMANAGER, L_INFO, F("Mute scheduling disabled"));
    }
}

uint8_t MuteManager::get_mute_off_hour() {
    return mute_off_hour;
}

uint8_t MuteManager::get_mute_off_minute() {
    return mute_off_minute;
}

void MuteManager::set_mute_off_time(uint8_t hour, uint8_t minute) {
    if(hour > 23 || minute > 59) return;
    mute_off_hour = hour;
    mute_off_minute = minute;
    fLog.log(LOG_MUTEMANAGER, L_INFO, F("Setting mute off time: %02d:%02d"), hour, minute);
}

uint8_t MuteManager::get_mute_on_hour() {
    return mute_on_hour;
}

uint8_t MuteManager::get_mute_on_minute() {
    return mute_on_minute;
}

void MuteManager::set_mute_on_time(uint8_t hour, uint8_t minute) {
    if(hour > 23 || minute > 59) return;
    mute_on_hour = hour;
    mute_on_minute = minute;
    fLog.log(LOG_MUTEMANAGER, L_INFO, F("Setting mute on time: %02d:%02d"), hour, minute);
}

void MuteManager::task() {
    static uint32_t cooldown_timer = 0u;
    if(mute_schedule_enabled == false || !fClockManager.isClockSynchronizedToNtp() || cooldown_timer > device_uptime) return;

    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    if(hour == mute_off_hour && minute == mute_off_minute) {
        if(is_muted()) {
            cooldown_timer = device_uptime;
            fLog.log(LOG_MUTEMANAGER, L_INFO, F("Scheduled mute deactivated"));
            unmute();
        }
    }

    if(hour == mute_on_hour && minute == mute_on_minute) {
        if(!is_muted()) {
            cooldown_timer = device_uptime;
            fLog.log(LOG_MUTEMANAGER, L_INFO, F("Scheduled mute activated"));
            mute();
        }
    }
}
