#include "beep.h"

Beep::Beep(Settings &settings, Logger &log) : fSettings(settings), fLog(log) {
    pinMode(PIN_BUZZER, OUTPUT);
    digitalWrite(PIN_BUZZER, LOW);
}

void Beep::read_settings() {
    set_alarm_sound(fSettings.read_byte_s(SETT_ALARM_SOUND));
}

/**
 * Mutes the device except for the alarms.
 *
 * @return void
 */
void Beep::mute() {
    device_muted = true;
}

/**
 * Unmutes the device.
 *
 * @return void
 */
void Beep::unmute() {
    device_muted = false;
}

/**
 * Returns whether the device is muted.
 *
 * @return bool - true if the device is muted, false otherwise
 */
bool Beep::is_muted() {
    return device_muted;
}

/**
 * Plays a short beep notification.
 *
 * @return void
 */
void Beep::beep_short() {
    play_tone(transpose_musical_note(MUSICAL_G5, 3), DURATION_SIXTEENTH);
}

/**
 * Sets the tone generator timer ISR callback.
 *
 * @return void
 */
void Beep::set_tonegen_isr(void (*fn)(void)) {
    tonegen_isr_function = fn;
    timer0_attachInterrupt(tonegen_isr_function);
}

/**
 * Disables currently playing alarm.
 *
 * @return void
 */
void Beep::disable_alarm() {
    alarm_in_progress = false;
    stop_music();
}

/**
 * Enables the alarm music playback.
 *
 * @return void
 */
void Beep::enable_alarm() {
    alarm_in_progress = true;
}

/**
 * Plays an acknowledge notification sound.
 *
 * @return void
 */
void Beep::play_acknowledge() {
    if(!device_muted) start_music(okay_chime, MUSIC_BLOCK_OKAY_CHIME_SIZE);
}

/**
 * Plays a notification sound indicating failure.
 *
 * @return void
 */
void Beep::play_negative_acknowledge() {
    if(!device_muted) start_music(fail_chime, MUSIC_BLOCK_FAIL_CHIME_SIZE);
}

/**
 * Plays the requested frequency for the provided time if the device is not muted
 *
 * @param uint32_t frequency - The requested frequency in Hz.
 * @param uint32_t duration - The requested duration in Task Cycle counts.
 *
 * @return void
 */
void Beep::play_tone(uint32_t frequency, uint32_t duration) {
    if(!device_muted) play_tone_internal(frequency, duration);
}

/**
 * Plays the requested frequency for the provided time.
 *
 * @param uint32_t frequency - The requested frequency in Hz.
 * @param uint32_t duration - The requested duration in Task Cycle counts.
 *
 * @return void
 */
void Beep::play_tone_internal(uint32_t frequency, uint32_t duration) {

    /* Calculate timer value to achieve the desired frequency */
    beep_tone_generator_timer_value = (uint32_t)F_CPU / frequency;

    tone_tick_cnt = duration;
    beep_tone_generator_active = true;

    timer0_write(ESP.getCycleCount()+beep_tone_generator_timer_value);
}

/**
 * Initializes playback for the provided music_block if the device is not muted.
 *
 * @param music_block* music_block_in - Pointer to a music_block
 * @param uint32_t music_block_size - size of the provided music_block
 *
 * @return void
 */
void Beep::start_music(music_block* music_block_in, uint32_t music_block_size) {
    if(!device_muted) start_music_internal(music_block_in, music_block_size);
}

/**
 * Initializes playback for the provided music_block.
 * A music_block contains a musical piece with all the tones and timings.
 * This function provides a facility to initialize playback for such a structure.
 *
 * @param music_block* music_block_in - Pointer to a music_block
 * @param uint32_t music_block_size - size of the provided music_block
 *
 * @return void
 */
void Beep::start_music_internal(music_block* music_block_in, uint32_t music_block_size) {
    stop_music();
    current_music_block = music_block_in;
    current_music_block_length = music_block_size;
    music_play_in_progress = true;
    music_state = 0u;
    music_play_handler();
}

/**
 * Stops the currently playing music.
 *
 * @return void
 */
void Beep::stop_music() {
    beep_tone_generator_active = false;
    music_play_in_progress = false;
    digitalWrite(PIN_BUZZER, LOW);
}

/**
 * Handles music playback.
 * Should be called periodically when playing music.
 * Playback is initialized by start_music(), and this function takes all the
 * notes and timings from the current music_block and plays them back via the tone generator.
 *
 * @return 'true' if the music playback is not finished and the function should be called again, 'false' is the playback has finished.
 */
bool Beep::music_play_handler() {

    if(music_state == current_music_block_length) {
        music_state = 0u;
        /* Returns false if the music has finished */
        return false;
    }

    music_block current_note;
    memcpy_P(&current_note, &current_music_block[music_state], sizeof(music_block));

    uint32_t frequency_to_play = current_note.note;
    uint32_t duration = current_note.duration;

    /* Transpose the notes for a better sound */
    frequency_to_play = transpose_musical_note(frequency_to_play, 2);

    if(frequency_to_play != 0) {
        /* Play the next tone */
        play_tone_internal(frequency_to_play, duration);
        beep_tone_generator_pause = false;
    } else {
        /* Wait out the pause in the music */
        tone_tick_cnt = duration;
        beep_tone_generator_pause = true;
    }

    music_state++;

    /* Returns true if it should be called again for the next note */
    return true;
}

uint8_t Beep::get_alarm_sound() {
    return alarm_sound;
}

void Beep::set_alarm_sound(uint8_t alarm_sound_new) {
    if(alarm_sound_new >= ALARMSOUND_MAX) return;
    alarm_sound = alarm_sound_new;
    fLog.log(LOG_BEEP, L_INFO, F("Setting alarm sound: %d"), alarm_sound);
}

void Beep::start_alarm_music() {
    switch(alarm_sound) {
        case ALARMSOUND_ELISE:
            start_music_internal(furelise, MUSIC_BLOCK_FUR_ELISE_SIZE);
            break;

        case ALARMSOUND_SUBURBIA:
            start_music_internal(jesus_of_suburbia, MUSIC_BLOCK_JESUS_OF_SUBURBIA_SIZE);
            break;

        case ALARMSOUND_STACY:
            start_music_internal(stacys_mom, MUSIC_BLOCK_STACYS_MOM_SIZE);
            break;

        case ALARMSOUND_ILLFLY:
            start_music_internal(ill_fly, MUSIC_BLOCK_ILL_FLY_SIZE - 1);
            break;

        case ALARMSOUND_EVERYDAY:
            start_music_internal(everyday, MUSIC_BLOCK_EVERYDAY_SIZE);
            break;

        default:
            start_music_internal(jesus_of_suburbia, MUSIC_BLOCK_JESUS_OF_SUBURBIA_SIZE);
            break;
    }
}

/**
 * The periodic task for Beep.
 *
 * @return void
 */
void Beep::task() {
    if(alarm_in_progress && !music_play_in_progress) {
        start_alarm_music();
    }

    if(beep_tone_generator_active || beep_tone_generator_pause) {
        if(tone_tick_cnt == 0) {
            beep_tone_generator_active = false;
            digitalWrite(PIN_BUZZER, LOW);
            if(music_play_in_progress) music_play_in_progress = music_play_handler();
        }
        tone_tick_cnt--;
    }
}
