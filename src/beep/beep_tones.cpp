#include "beep_tones.h"

#define pause_eighth  {.note = MUSICAL_SILENCE, .duration = DURATION_EIGHTH}
#define pause_quarter {.note = MUSICAL_SILENCE, .duration = DURATION_QUARTER}

uint32_t transpose_musical_note(uint32_t note, int8_t octaves) {
    uint32_t retVal = 0u;
    if(octaves > 0) {
        retVal = note * (2 * octaves);
        //Serial.printf("Octave transpose up; base=%lu shift_octave=%d calc_freq=%lu\r\n", note, octaves, retVal);
    } else if(octaves < 0) {
        octaves = abs(octaves) + 1;
        retVal = note / (2 * octaves);
        //Serial.printf("Octave transpose down; base=%lu shift_octave=%d calc_freq=%lu\r\n", note, octaves, retVal);
    }
    return retVal;
}


/* Startup chime */

#define startupchime_1 {.note = MUSICAL_C6, .duration = DURATION_SIXTEENTH}
#define startupchime_2 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define startupchime_3 {.note = MUSICAL_G6, .duration = DURATION_EIGHTH}

PROGMEM music_block startup_chime[MUSIC_BLOCK_STARTUP_CHIME_SIZE] = {startupchime_1, startupchime_2, startupchime_3};


/* Welcome chime */

#define welcomechime_1 {.note = MUSICAL_C6, .duration = DURATION_EIGHTH}
#define welcomechime_2 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define welcomechime_3 {.note = MUSICAL_C6, .duration = DURATION_EIGHTH}
#define welcomechime_4 {.note = MUSICAL_G6, .duration = DURATION_SIXTEENTH}

PROGMEM music_block welcome_chime[MUSIC_BLOCK_WELCOME_CHIME_SIZE] = {welcomechime_1, welcomechime_2, welcomechime_3, welcomechime_4};


/* Okay chime */

#define okaychime_1 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define okaychime_2 {.note = MUSICAL_G6, .duration = DURATION_SIXTEENTH}

PROGMEM music_block okay_chime[MUSIC_BLOCK_OKAY_CHIME_SIZE] = {okaychime_1, okaychime_2};


/* Failure chime */

#define failchime_1 {.note = MUSICAL_G6, .duration = DURATION_SIXTEENTH}
#define failchime_2 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}

PROGMEM music_block fail_chime[MUSIC_BLOCK_FAIL_CHIME_SIZE] = {failchime_1, failchime_2};


/* Client connected chime */

#define MUSIC_BLOCK_CLIENTCONNECTED_CHIME_SIZE 2u
PROGMEM music_block clientconnected_chime[MUSIC_BLOCK_CLIENTCONNECTED_CHIME_SIZE] = {okaychime_1, okaychime_1};


/* Powerup chime */

#define powerup_1 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define powerup_2 {.note = MUSICAL_G6, .duration = DURATION_SIXTEENTH}
#define powerup_3 {.note = MUSICAL_E6, .duration = DURATION_EIGHTH}

PROGMEM music_block powerup_chime[MUSIC_BLOCK_POWERUP_CHIME_SIZE] = {powerup_1, powerup_2, powerup_3};

/* Strong powerup chime */

#define strongpowerup_1 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define strongpowerup_2 {.note = MUSICAL_E6, .duration = DURATION_SIXTEENTH}
#define strongpowerup_3 {.note = MUSICAL_G6, .duration = DURATION_EIGHTH}

PROGMEM music_block strong_powerup_chime[MUSIC_BLOCK_STRONG_POWERUP_CHIME_SIZE] = {strongpowerup_1, strongpowerup_2, strongpowerup_3};

/* Stronger powerup chime */

#define strongerpowerup_1 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define strongerpowerup_2 {.note = MUSICAL_G6, .duration = DURATION_SIXTEENTH}
#define strongerpowerup_3 {.note = MUSICAL_E6, .duration = DURATION_SIXTEENTH}
#define strongerpowerup_4 {.note = MUSICAL_G6, .duration = DURATION_EIGHTH}

PROGMEM music_block stronger_powerup_chime[MUSIC_BLOCK_STRONGER_POWERUP_CHIME_SIZE] = {strongerpowerup_1, strongerpowerup_2, strongerpowerup_3, strongerpowerup_4};


/* Snake collision chime */

#define snakecollision_1 {.note = MUSICAL_E6, .duration = DURATION_SIXTEENTH}
#define snakecollision_2 {.note = MUSICAL_G6, .duration = DURATION_SIXTEENTH}
#define snakecollision_3 {.note = MUSICAL_D6, .duration = DURATION_SIXTEENTH}
#define snakecollision_4 {.note = MUSICAL_C6, .duration = DURATION_EIGHTH}

PROGMEM music_block snake_collision_chime[MUSIC_BLOCK_SNAKECOLLISION_CHIME_SIZE] = {snakecollision_1, snakecollision_2, snakecollision_3, snakecollision_4};


/* Beethoven - Für Elise */

#define fe_1 {.note = MUSICAL_E5, .duration = DURATION_EIGHTH}
#define fe_2 {.note = MUSICAL_D_SHARP_5, .duration = DURATION_EIGHTH}
#define fe_3 {.note = MUSICAL_B4, .duration = DURATION_EIGHTH}
#define fe_4 {.note = MUSICAL_D5, .duration = DURATION_EIGHTH}
#define fe_5 {.note = MUSICAL_C5, .duration = DURATION_EIGHTH}
#define fe_6 {.note = MUSICAL_A4, .duration = DURATION_EIGHTH}

PROGMEM music_block furelise[MUSIC_BLOCK_FUR_ELISE_SIZE] = {fe_1, fe_2, fe_1, fe_2, fe_1, fe_3, fe_4, fe_5, fe_6, pause_eighth};


/* Green Day - Jesus of Suburbia */

#define jesus_1 {.note = MUSICAL_C_SHARP_5, .duration = DURATION_QUARTER}
#define jesus_2 {.note = MUSICAL_A_SHARP_4, .duration = DURATION_QUARTER}
#define jesus_3 {.note = MUSICAL_F5, .duration = DURATION_EIGHTH}
#define jesus_4 {.note = MUSICAL_F_SHARP_5, .duration = DURATION_EIGHTH}
#define jesus_5 {.note = MUSICAL_D_SHARP_5, .duration = DURATION_EIGHTH}
#define jesus_6 {.note = MUSICAL_C_SHARP_5, .duration = DURATION_QUARTER}
#define jesus_7 {.note = MUSICAL_C_SHARP_5, .duration = DURATION_EIGHTH}
#define jesus_8 {.note = MUSICAL_A_SHARP_4, .duration = DURATION_EIGHTH}


PROGMEM music_block jesus_of_suburbia[MUSIC_BLOCK_JESUS_OF_SUBURBIA_SIZE] = {
    jesus_1, pause_eighth, jesus_1, pause_eighth, jesus_1,
    /* I'm    the      son      of       rage      and     love  */
    jesus_3, jesus_3, jesus_3, jesus_4, jesus_3, jesus_5, jesus_6,
    jesus_2, pause_eighth, jesus_2, pause_eighth, jesus_8,
    /* The     je      sus      of       su        bur     bi       a  */
    jesus_7, jesus_3, jesus_3, jesus_3, jesus_4, jesus_3, jesus_5, jesus_6
};


/* Fountains of Wayne - Stacy's Mom guitar solo */

#define stacy_1  {.note = MUSICAL_C5, .duration = DURATION_EIGHTH}
#define stacy_2  {.note = MUSICAL_D5, .duration = DURATION_EIGHTH}
#define stacy_3  {.note = MUSICAL_E5, .duration = DURATION_EIGHTH}
#define stacy_4  {.note = MUSICAL_D5, .duration = DURATION_QUARTER}
#define stacy_5  {.note = MUSICAL_B4, .duration = DURATION_EIGHTH}
#define stacy_6  {.note = MUSICAL_G4, .duration = DURATION_EIGHTH}
#define stacy_7  {.note = MUSICAL_A4, .duration = DURATION_QUARTER}
#define stacy_8  {.note = MUSICAL_A4, .duration = DURATION_EIGHTH}
#define stacy_9  {.note = MUSICAL_G4, .duration = DURATION_EIGHTH}
#define stacy_10 {.note = MUSICAL_B4, .duration = DURATION_QUARTER}

PROGMEM music_block stacys_mom[MUSIC_BLOCK_STACYS_MOM_SIZE] = {
    stacy_1, stacy_2, stacy_3, stacy_4, stacy_5, stacy_6, stacy_7,
    stacy_8, stacy_8, stacy_9, stacy_10, stacy_8, stacy_9,

    stacy_1, stacy_2, stacy_3, stacy_4, stacy_5, stacy_6,
    stacy_5, stacy_5, stacy_1, stacy_5, stacy_1, stacy_5, stacy_1, stacy_5, stacy_1,

    stacy_1, stacy_2, stacy_3, stacy_4, stacy_5, stacy_6,
    stacy_7, stacy_8, stacy_8, stacy_9, stacy_10, stacy_8, stacy_9,

    stacy_5, stacy_1, stacy_5, stacy_1, stacy_5, stacy_1, stacy_5, stacy_1,
    stacy_2, stacy_3, stacy_2, stacy_3, stacy_2, stacy_3, stacy_2, stacy_3
};


/* I'll fly with you */

#define fly_1  {.note = MUSICAL_F_SHARP_4, .duration = DURATION_QUARTER}
#define fly_2  {.note = MUSICAL_F_SHARP_4, .duration = DURATION_EIGHTH}
#define fly_3  {.note = MUSICAL_D5, .duration = DURATION_EIGHTH}
#define fly_4  {.note = MUSICAL_C_SHARP_5, .duration = DURATION_QUARTER}
#define fly_5  {.note = MUSICAL_C_SHARP_5, .duration = DURATION_EIGHTH}
#define fly_6  {.note = MUSICAL_B4, .duration = DURATION_QUARTER}
#define fly_7  {.note = MUSICAL_B4, .duration = DURATION_EIGHTH}
#define fly_8  {.note = MUSICAL_A4, .duration = DURATION_EIGHTH}

PROGMEM music_block ill_fly[MUSIC_BLOCK_ILL_FLY_SIZE] = {
    fly_1, pause_quarter, fly_1, fly_2, fly_3,
    fly_4, pause_quarter, fly_4, fly_5, fly_3,
    fly_6, pause_quarter, fly_6, fly_7, fly_8,
    fly_6, fly_6, fly_7, fly_8, fly_7, fly_8,
    fly_1
};


/* A timeless classic */

#define day_1  {.note = MUSICAL_G5, .duration = DURATION_QUARTER}
#define day_2  {.note = MUSICAL_D5, .duration = DURATION_QUARTER}
#define day_3  {.note = MUSICAL_E5, .duration = DURATION_QUARTER}
#define day_4  {.note = MUSICAL_E5, .duration = DURATION_EIGHTH}
#define day_5  {.note = MUSICAL_B4, .duration = DURATION_QUARTER}
#define day_6  {.note = MUSICAL_D5, .duration = DURATION_EIGHTH}

PROGMEM music_block everyday[MUSIC_BLOCK_EVERYDAY_SIZE] = {
    day_1, day_1, day_2, day_3, day_4,
    day_2, pause_quarter, day_3, day_6,
    day_3, day_6, day_5, day_2, pause_quarter
};
