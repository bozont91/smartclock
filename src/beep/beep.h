#ifndef BEEP_H
#define BEEP_H

#include <Arduino.h>
#include "../common.h"
#include "beep_tones.h"
#include "../log/log.h"
#include "../settings/settings.h"

class Beep {

public:
    Beep(Settings &settings, Logger &log);

    enum alarm_sounds {
        ALARMSOUND_ELISE,
        ALARMSOUND_SUBURBIA,
        ALARMSOUND_STACY,
        ALARMSOUND_ILLFLY,
        ALARMSOUND_EVERYDAY,
        ALARMSOUND_MAX
    };

    /* These are public, because they are used in the timer0 ISR */
    volatile bool beep_tone_generator_active = false;
    volatile uint32_t beep_tone_generator_timer_value = 0u;

    bool music_play_in_progress = false;

    void read_settings();
    void beep_short();
    void disable_alarm();
    void enable_alarm();
    void play_tone(uint32_t frequency, uint32_t duration);
    void play_negative_acknowledge();
    void play_acknowledge();
    void start_music(music_block* music_block_in, uint32_t music_block_size);
    void stop_music();
    void set_tonegen_isr(void (*fn)(void));
    uint8_t get_alarm_sound();
    void set_alarm_sound(uint8_t alarm_sound_new);
    void task();

private:
    bool alarm_in_progress = false;
    bool beep_tone_generator_pause = false;
    uint32_t tone_tick_cnt = 0;
    music_block* current_music_block = NULL;
    uint32_t current_music_block_length = 0u;
    uint8_t music_state = 0u;
    bool device_muted = false;
    uint8_t alarm_sound = ALARMSOUND_SUBURBIA;

    void mute();
    void unmute();
    bool is_muted();

    void start_alarm_music();
    bool music_play_handler();
    void play_tone_internal(uint32_t frequency, uint32_t duration);
    void start_music_internal(music_block* music_block_in, uint32_t music_block_size);
    void (*tonegen_isr_function)(void) = NULL;

    friend class MuteManager;

    Settings &fSettings;
    Logger &fLog;

};

#endif /* BEEP_H */
