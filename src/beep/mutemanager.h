#ifndef MUTEMANAGER_H
#define MUTEMANAGER_H

#include <Arduino.h>
#include "../beep/beep.h"
#include "../log/log.h"
#include "../clockmanager/clockmanager.h"
#include "../settings/settings.h"


class MuteManager {

public:
    MuteManager(
        Beep &beep,
        Logger &log,
        ClockManager &clockmanager,
        Settings &settings
    );

    void read_settings();
    void mute();
    void unmute();
    bool is_muted();
    bool get_mute_schedule_enabled();
    void set_mute_schedule_enabled(bool state);
    uint8_t get_mute_off_hour();
    uint8_t get_mute_off_minute();
    void set_mute_off_time(uint8_t hour, uint8_t minute);
    uint8_t get_mute_on_hour();
    uint8_t get_mute_on_minute();
    void set_mute_on_time(uint8_t hour, uint8_t minute);
    void task();

private:

    bool mute_schedule_enabled = false;
    uint8_t mute_off_hour = 0u;
    uint8_t mute_off_minute = 0u;
    uint8_t mute_on_hour = 0u;
    uint8_t mute_on_minute = 0u;

    Beep &fBeep;
    Logger &fLog;
    ClockManager &fClockManager;
    Settings &fSettings;

};

#endif /* MUTEMANAGER_H */
