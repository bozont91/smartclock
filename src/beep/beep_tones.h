#ifndef BEEP_TONES_H
#define BEEP_TONES_H

#include <Arduino.h>

/* Definition for musical notes and their respective frequencies in Hz */
/* https://pages.mtu.edu/~suits/notefreqs.html */
#define MUSICAL_SILENCE 0

#define MUSICAL_F4 349
#define MUSICAL_F_SHARP_4 370
#define MUSICAL_G4 392
#define MUSICAL_G_SHARP_4 415

#define MUSICAL_A4 440
#define MUSICAL_A_SHARP_4 466
#define MUSICAL_B4 494
#define MUSICAL_C5 523
#define MUSICAL_C_SHARP_5 554
#define MUSICAL_D5 587
#define MUSICAL_D_SHARP_5 622
#define MUSICAL_E5 659
#define MUSICAL_F5 698
#define MUSICAL_F_SHARP_5 740
#define MUSICAL_G5 784
#define MUSICAL_G_SHARP_5 831

#define MUSICAL_A5 880
#define MUSICAL_A_SHARP_5 932
#define MUSICAL_B5 988
#define MUSICAL_C6 1047
#define MUSICAL_C_SHARP_6 1109
#define MUSICAL_D6 1175
#define MUSICAL_D_SHARP_6 1245
#define MUSICAL_E6 1319
#define MUSICAL_F6 1397
#define MUSICAL_F_SHARP_6 1480
#define MUSICAL_G6 1568
#define MUSICAL_G_SHARP_6 1661

#define MUSICAL_A6 1760

/* Definition for musical timing */
#define DURATION_SIXTEENTH 50u
#define DURATION_EIGHTH (DURATION_SIXTEENTH * 2u)
#define DURATION_QUARTER (DURATION_SIXTEENTH * 4u)
#define DURATION_HALF (DURATION_SIXTEENTH * 8u)
#define DURATION_FULL (DURATION_SIXTEENTH * 16u)


typedef struct music_block_s {
    uint32_t note;
    uint32_t duration;
} music_block;

uint32_t transpose_musical_note(uint32_t note, int8_t octaves);

#define MUSIC_BLOCK_STARTUP_CHIME_SIZE 3u
extern music_block startup_chime[MUSIC_BLOCK_STARTUP_CHIME_SIZE];

#define MUSIC_BLOCK_WELCOME_CHIME_SIZE 4u
extern music_block welcome_chime[MUSIC_BLOCK_WELCOME_CHIME_SIZE];

#define MUSIC_BLOCK_OKAY_CHIME_SIZE 2u
extern music_block okay_chime[MUSIC_BLOCK_OKAY_CHIME_SIZE];

#define MUSIC_BLOCK_FAIL_CHIME_SIZE 2u
extern music_block fail_chime[MUSIC_BLOCK_FAIL_CHIME_SIZE];

#define MUSIC_BLOCK_JESUS_OF_SUBURBIA_SIZE 25u
extern music_block jesus_of_suburbia[MUSIC_BLOCK_JESUS_OF_SUBURBIA_SIZE];

#define MUSIC_BLOCK_FUR_ELISE_SIZE 10u
extern music_block furelise[MUSIC_BLOCK_FUR_ELISE_SIZE];

#define MUSIC_BLOCK_STACYS_MOM_SIZE 57u
extern music_block stacys_mom[MUSIC_BLOCK_STACYS_MOM_SIZE];

#define MUSIC_BLOCK_POWERUP_CHIME_SIZE 3u
extern music_block powerup_chime[MUSIC_BLOCK_POWERUP_CHIME_SIZE];

#define MUSIC_BLOCK_STRONG_POWERUP_CHIME_SIZE 3u
extern music_block strong_powerup_chime[MUSIC_BLOCK_STRONG_POWERUP_CHIME_SIZE];

#define MUSIC_BLOCK_STRONGER_POWERUP_CHIME_SIZE 4u
extern music_block stronger_powerup_chime[MUSIC_BLOCK_STRONGER_POWERUP_CHIME_SIZE];

#define MUSIC_BLOCK_SNAKECOLLISION_CHIME_SIZE 4u
extern music_block snake_collision_chime[MUSIC_BLOCK_SNAKECOLLISION_CHIME_SIZE];

#define MUSIC_BLOCK_CLIENTCONNECTED_CHIME_SIZE 2u
extern music_block clientconnected_chime[MUSIC_BLOCK_CLIENTCONNECTED_CHIME_SIZE];

#define MUSIC_BLOCK_ILL_FLY_SIZE 22u
extern music_block ill_fly[MUSIC_BLOCK_ILL_FLY_SIZE];

#define MUSIC_BLOCK_EVERYDAY_SIZE 14u
extern music_block everyday[MUSIC_BLOCK_EVERYDAY_SIZE];

#endif /* BEEP_TONES_H */
