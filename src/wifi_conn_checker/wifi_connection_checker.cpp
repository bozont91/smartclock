#include "wifi_connection_checker.h"

WifiConnectionChecker::WifiConnectionChecker(
    TickerScheduler &ostask,
    Logger &log
) :
osTask(ostask),
fLog(log)
{
    ;
}

uint32_t WifiConnectionChecker::get_first_wifi_connection_time() {
    return wifi_first_conn_time;
}

const char* WifiConnectionChecker::wifi_status_to_string(wl_status_t status) {
    if(status > WIFI_STATUS_STR_SIZE) return NULL;
    return wifi_status_str[status];
}

void WifiConnectionChecker::log_ip_address() {
    char IP[16];
    WiFi.localIP().toString().toCharArray(IP, 16);
    fLog.log(LOG_WIFICHECKER, L_INFO, F("IP address: %s"), IP);
}

void WifiConnectionChecker::task() {

    if(WiFi.status() == last_wifi_status) return;
    last_wifi_status = WiFi.status();

    fLog.log(LOG_WIFICHECKER, L_INFO, F("WiFi status changed; state='%d (%s)'"), last_wifi_status, wifi_status_to_string((wl_status_t)last_wifi_status));

    if(last_wifi_status == WL_CONNECTED) {
        is_wifi_connected = true;
    } else {
        is_wifi_connected = false;
    }

    if(previous_is_wifi_connected == is_wifi_connected) return;
    previous_is_wifi_connected = is_wifi_connected;

    if(is_wifi_connected) {

        if(is_first_connection) {
            fLog.log(LOG_WIFICHECKER, L_INFO, F("WiFi connected"));
            log_ip_address();
            is_first_connection = false;
            wifi_first_conn_time = device_uptime;
            return;
        }

        fLog.log(LOG_WIFICHECKER, L_INFO, F("WiFi connection restored"));
        log_ip_address();

        fLog.log(LOG_WIFICHECKER, L_INFO, F("Enabling network dependent tasks"));
        osTask.enable(OS_TASK_MDNS);
        osTask.enable(OS_TASK_NTP);
        osTask.enable(OS_TASK_WEBWEATHER);
        osTask.enable(OS_TASK_WEBSERVER);
        osTask.enable(OS_TASK_TCPREMOTE);
        osTask.enable(OS_TASK_MQTT);
        osTask.enable(OS_TASK_ADAFRUITIO);
        osTask.enable(OS_TASK_OTA);

    } else {

        fLog.log(LOG_WIFICHECKER, L_ERROR, F("WiFi connection lost! Disabling network dependent tasks."));
        osTask.disable(OS_TASK_MDNS);
        osTask.disable(OS_TASK_WEBWEATHER);
        osTask.disable(OS_TASK_WEBSERVER);
        osTask.disable(OS_TASK_TCPREMOTE);
        osTask.disable(OS_TASK_NTP);
        osTask.disable(OS_TASK_MQTT);
        osTask.disable(OS_TASK_ADAFRUITIO);
        osTask.disable(OS_TASK_OTA);

    }
}
