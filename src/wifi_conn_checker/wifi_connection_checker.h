#ifndef WIFI_CONNECTION_CHECKER_H
#define WIFI_CONNECTION_CHECKER_H

#include <ESP8266WiFi.h>
#include <TickerScheduler.h>
#include "../common.h"
#include "../log/log.h"


class WifiConnectionChecker {

public:
    WifiConnectionChecker(
        TickerScheduler &ostask,
        Logger &log
    );
    uint32_t get_first_wifi_connection_time();
    void task();

private:
    uint8_t last_wifi_status = WL_DISCONNECTED;
    bool is_first_connection = true;
    uint32_t wifi_first_conn_time = 0u;
    bool is_wifi_connected = false;
    bool previous_is_wifi_connected = false;

    static const uint8_t WIFI_STATUS_STR_SIZE = 8u;
    const char* const wifi_status_str[WIFI_STATUS_STR_SIZE] = {
        "Idle",
        "SSID not available",
        "Scan completed",
        "Connected",
        "Connection failed",
        "Connection lost",
        "Password authentication failed",
        "Disconnected"
    };

    const char* wifi_status_to_string(wl_status_t status);
    void log_ip_address();

    TickerScheduler &osTask;
    Logger &fLog;

};

#endif /* WIFI_CONNECTION_CHECKER_H */
