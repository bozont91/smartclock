#ifndef PROFILER_H
#define PROFILER_H

#include <Arduino.h>
#include "../common.h"

volatile float cpu_current_load = 0.0f;

const char* os_task_str_names[] = {
    "2ms",
    "100ms",
    "200ms",
    "1000ms",
    "ntp",
    "webserver",
    "webweather",
    "tcpremote",
    "mdns",
    "mqtt",
    "adafruitio",
    "ota"
};

#define PROFILING_FREQUENCY_SECONDS 5u

#define OS_TASK_IDLE    OS_TASK_MAX
#define P_MAX           OS_TASK_MAX+1

volatile uint8_t profiler_current_task = OS_TASK_IDLE;
volatile uint32_t profiler_data[P_MAX];
volatile bool profiler_data_lock = false;

#define P_START(x) profiler_current_task=x
#define P_END() profiler_current_task=OS_TASK_IDLE

#ifdef PROFILER_ENABLED
double tasks_max_percent[P_MAX];
double peak_load = 0;
#endif

void profiler_init() {
    for(uint32_t i = 0; i < P_MAX; i++) profiler_data[i] = 0u;
    #ifdef PROFILER_ENABLED
    memset(tasks_max_percent, 0, OS_TASK_MAX*sizeof(double));
    #endif
}

void profiler_loop() {

    /* Wait for the next profiling window */
    static uint32_t next_calc = PROFILING_FREQUENCY_SECONDS;
    if(device_uptime < next_calc) return;

    next_calc = device_uptime + PROFILING_FREQUENCY_SECONDS;

    profiler_data_lock = true;

    /* Calculate the total samples taken */
    uint32_t total_samples_taken = 0u;
    for(uint32_t i = 0u ; i < P_MAX ; i++) total_samples_taken += profiler_data[i];
    if(total_samples_taken == 0) return;

    /* Calculate the idle and load percentage */
    double idle_percent = (double)(profiler_data[OS_TASK_IDLE] * 100) / total_samples_taken;
    double load_percent = 100 - idle_percent;
    cpu_current_load = load_percent;

    /* Log detailed data for each task */
    #ifdef PROFILER_ENABLED

        if(load_percent > peak_load) peak_load = load_percent;

        Serial.println("\r\nProfiler data:");
        Serial.printf("Total samples: %u\r\n", total_samples_taken);
        Serial.printf("IDLE: %u (%.1lf%%)  |  BUSY: %u (%.1lf%%)  Peak: %.1lf%%\r\n", profiler_data[OS_TASK_IDLE], idle_percent, total_samples_taken-profiler_data[OS_TASK_IDLE], load_percent, peak_load);

        double task_percent;
        for(uint8_t i = 0 ; i < OS_TASK_MAX ; i++) {
            task_percent = (double)(profiler_data[i] * 100) / total_samples_taken;
            if(task_percent > tasks_max_percent[i]) tasks_max_percent[i] = task_percent;

            Serial.printf("%-20s\t %u (%.1lf%%)   \tMax: %.1lf%%\r\n", os_task_str_names[i], profiler_data[i], task_percent, tasks_max_percent[i]);
        }

    #endif

    /* Clear the results */
    for(uint32_t i = 0; i < P_MAX; i++) profiler_data[i] = 0u;

    profiler_data_lock = false;
}

#endif /* PROFILER_H */
