#ifndef WELCOMEWIZARD_H
#define WELCOMEWIZARD_H

#include <Arduino.h>
#include "../esp_hal/esp_hal.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../settings/settings.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"
#include "../webserver/webserver.h"

class WelcomeWizard {

public:
    WelcomeWizard(
        EspHal &esphal,
        Logger &log,
        Beep &beep,
        Settings &settings,
        LedmatrixStateManager &ledmatrixmain,
        LedmatrixGFX &ledmatrixgfx
    );
    void begin();
    void task();
    void handle_sta_connected(WiFiEventSoftAPModeStationConnected sta_info);

private:
    void handle_webserver_clients();
    void test_wifi_connection();
    void save_data_and_reboot();
    String build_http_ok_response(String response_data);
    void prepare_html_page(String& html_template);
    int8_t perform_wifi_scan();

    enum conn_test_status_e {
        TST_IDLE = 0,
        TST_RUNNING,
        TST_FAILED,
        TST_SUCCESS
    };

    uint8_t conn_test_status = TST_IDLE;
    String wifinetworkname = "";
    String wifipassword = "";
    int8 timezone = 0;

    bool reboot_requested = false;
    uint32_t reboot_time = 0u;
    void request_reboot(uint32_t seconds_to_wait);

    EspHal &fEspHal;
    Logger &fLog;
    Beep &fBeep;
    Settings &fSettings;
    LedmatrixStateManager &fLedmatrixMain;
    LedmatrixGFX &fLedmatrixGFX;

};


#endif /* WELCOMEWIZARD_H */
