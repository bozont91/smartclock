#include "welcomewizard.h"


WiFiServer webserver_welcomewizard(80);
WiFiClient webserver_welcomewizard_client;


static const char welcomewizard_html_template[] PROGMEM = R"=====(
HTTP/1.1 200 OK
Content-Type: text/html
Connection: close

<!DOCTYPE HTML>

<html>
<head>
<style>
html, body {
height: 100%;
background-repeat: no-repeat;
background-attachment: fixed;
background-image: linear-gradient(to right top, #082d66, #0063a2, #0097ab, #00c879, #a8eb12);
}
</style>
<title>aura SmartClock - Welcome Wizard</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body>
<script>
function getTimezone() {
var d = new Date();
return d.getTimezoneOffset() / -60;
}
function loadData(url, callback) {
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
 if(this.readyState == 4 && this.status == 200) {
 callback.apply(xhttp);
}
};
xhttp.open("GET", url, true);
xhttp.send();
}
var tm;
function submit_network(){
 document.getElementById('conn_result').innerHTML = "Connecting...";
 document.getElementById('btn_conn').disabled = true;
 loadData('submitnetwork?network='+document.getElementById('network').value+'&password='+document.getElementById('password').value+'&timezone='+getTimezone(), null);
 tm = setInterval(function() {loadData("getconnstatus", function(){
  document.getElementById("conn_result").innerHTML = this.responseText;
  clearInterval(tm);
  document.getElementById('btn_conn').disabled = false;
 })}, 500);
}
</script>
<p><h2><b>aura SmartClock</b></h2></p>
<i><p>Welcome Wizard</p></i>
<br>
<div align="center">
<b><p>Select WiFi network:</p></b>
<select id="network">
%WIFINETWORKS%
</select>
<b><p>Passphrase:</p></b>
<input type="password" id="password">
<br><br>
<p id="conn_result"></p>
<input type="submit" value="Connect" onclick="submit_network()" id="btn_conn">
</div>
</body>
</html>

)=====";



WelcomeWizard::WelcomeWizard(
    EspHal &esphal,
    Logger &log,
    Beep &beep,
    Settings &settings,
    LedmatrixStateManager &ledmatrixmain,
    LedmatrixGFX &ledmatrixgfx
) :
fEspHal(esphal),
fLog(log),
fBeep(beep),
fSettings(settings),
fLedmatrixMain(ledmatrixmain),
fLedmatrixGFX(ledmatrixgfx)
{
    ;
}

void WelcomeWizard::begin() {
    webserver_welcomewizard.begin();
}

void WelcomeWizard::handle_sta_connected(WiFiEventSoftAPModeStationConnected sta_info) {
    fLog.log(LOG_SYSTEM, L_INFO, F("WiFi AP STA connected: %02X:%02X:%02X:%02X:%02X:%02X  Id: %d"), MAC2STR(sta_info.mac), sta_info.aid);
}

int8_t WelcomeWizard::perform_wifi_scan() {
    fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Scanning for available WiFi networks..."));
    int8_t scan_result;
    do {
        scan_result = WiFi.scanNetworks();
        yield();
    } while(scan_result == WIFI_SCAN_FAILED || scan_result == WIFI_SCAN_RUNNING);

    fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Available networks (%u):"), scan_result);
    for(int8_t i = 0; i < scan_result; i++) {
        String scanned_ssid = WiFi.SSID(i);
        fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("\t(%d) %s"), i+1, scanned_ssid.c_str());
    }

    return scan_result;
}

String WelcomeWizard::build_http_ok_response(String response_data) {
    String response = (String)FPSTR(html_ok_response_template);
    return response + response_data + "\n";
}

void WelcomeWizard::prepare_html_page(String& html_template) {

    int8_t number_of_networks = perform_wifi_scan();
    char networkname_buffer[100];
    String networkname_html = "";

    for(int8_t i = 0; i < number_of_networks; i++) {
        String scanned_ssid = WiFi.SSID(i);
        sprintf_P(networkname_buffer, PSTR("<option value=\"%s\">%s</option>"), scanned_ssid.c_str(), scanned_ssid.c_str());
        networkname_html += networkname_buffer;
    }

    html_template.replace(F("%WIFINETWORKS%"), networkname_html);
}

void WelcomeWizard::handle_webserver_clients() {

    webserver_welcomewizard_client = webserver_welcomewizard.available();
    if(!webserver_welcomewizard_client) return;
    fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Client connected"));

    while(webserver_welcomewizard_client.connected()) {

        if(!webserver_welcomewizard_client.available()) continue;

        String line = webserver_welcomewizard_client.readStringUntil('\r');
        //Serial.println(line);

        if((line.indexOf(F("GET")) < 0)) break;
        String called_handle = line;
        called_handle.remove(called_handle.indexOf(" HTTP"));
        called_handle = called_handle.substring(called_handle.indexOf("GET ") + 4);
        fLog.log(LOG_WELCOMEWIZARD, L_DEBUG, F("Called handle: %s"), called_handle.c_str());

        if(called_handle.indexOf("/submitnetwork") >= 0 && conn_test_status != TST_RUNNING) {

            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Called submitnetwork"));

            /* Decode the received data to have the correct parameters */
            line = Webserver::url_decode(line);

            /* Extract the SSID and the password from the response */
            wifinetworkname = line.substring(line.indexOf("=")+1);
            wifipassword = wifinetworkname.substring(wifinetworkname.indexOf('=')+1);
            String timezone_str = wifipassword.substring(wifipassword.indexOf('=')+1);

            timezone_str.remove(timezone_str.indexOf("HTTP")-1);
            wifipassword.remove(wifipassword.indexOf("&"));
            timezone = timezone_str.toInt();

            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Timezone from setup device: UTC %d"), timezone);

            wifinetworkname.remove(wifinetworkname.indexOf("&"));
            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("WiFi network: %s"), wifinetworkname.c_str());

            /* Replace the password with *-s in logging */
            String wifipassword_redacted;
            for(uint8_t i = 0 ; i < wifipassword.length() ; i++) {
                wifipassword_redacted += '*';
            }
            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("WiFi password: %s"), wifipassword_redacted.c_str());

            fBeep.beep_short();

            /* Try connecting to the provided AP */
            WiFi.begin(wifinetworkname, wifipassword);
            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Testing connection to the provided AP"));

            conn_test_status = TST_RUNNING;

            String response = String(FPSTR(html_ok_response_template));
            webserver_welcomewizard_client.println(response);

            fLedmatrixGFX.clear_framebuffer();
            fLedmatrixGFX.draw_num(1, 0, LEDMATRIX_ICON_WIFI);
            fLedmatrixGFX.render_framebuffer_to_screen();
            fLedmatrixMain.set_current_screen(LedmatrixStateManager::SCREEN_LOADING);

            break;
        }

        if(called_handle.indexOf("/getconnstatus") >= 0) {
            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Called getconnstatus; status='%d'"), conn_test_status);
            String response;

            if(conn_test_status == TST_FAILED) {
                response = build_http_ok_response("Connection failed, please try again!");
                conn_test_status = TST_IDLE;
            } else if(conn_test_status == TST_SUCCESS) {
                response = build_http_ok_response("Connection successful! The device will reboot now.");
                conn_test_status = TST_IDLE;
            } else {
                response = String(FPSTR(html_not_ok_response_template));
            }

            webserver_welcomewizard_client.println(response);
            break;
        }

        if(called_handle == "/") {
            fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Serving webpage root"));
            String ww_html = String(FPSTR(welcomewizard_html_template));
            prepare_html_page(ww_html);
            webserver_welcomewizard_client.println(ww_html);
            break;
        }

        fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Serving not found page"));
        String response = build_http_ok_response(FPSTR(html_404_template));
        webserver_welcomewizard_client.println(response);
        break;

    }

}

void WelcomeWizard::test_wifi_connection() {

    static uint8_t connection_attempt_counter = 0u;

    if(WiFi.isConnected() == false) {
        connection_attempt_counter++;
        if(connection_attempt_counter == 15) {
            fLog.log(LOG_WELCOMEWIZARD, L_ERROR, F("Failed to connect to the provided AP"));
            conn_test_status = TST_FAILED;
            fLedmatrixMain.show_text("Connection failed, please try again!", LedmatrixStateManager::SCREEN_WELCOMEWIZARD);
            fBeep.beep_short();
            WiFi.disconnect();
            connection_attempt_counter = 0u;
        }
        return;
    }

    fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Successfully connected to the provided AP"));
    conn_test_status = TST_SUCCESS;
    fBeep.start_music(welcome_chime, MUSIC_BLOCK_WELCOME_CHIME_SIZE);
    request_reboot(3);
}

void WelcomeWizard::save_data_and_reboot() {

    /* Convert SSID to char array and save to NVM */
    uint8_t wifinetworkname_bytes = wifinetworkname.length()+1;
    char wifinetworkname_c[wifinetworkname_bytes];
    wifinetworkname.toCharArray(wifinetworkname_c, wifinetworkname_bytes);
    fSettings.write(SETT_SSID, (uint8_t *)&wifinetworkname_c, true, wifinetworkname_bytes);

    /* Convert WiFi password to char array and save to NVM */
    uint8_t wifipassword_bytes = wifipassword.length()+1;
    char wifipassword_c[wifipassword_bytes];
    wifipassword.toCharArray(wifipassword_c, wifipassword_bytes);
    fSettings.write(SETT_WIFI_PASS, (uint8_t *)&wifipassword_c, true, wifipassword_bytes);

    /* Write the default values to NVM */
    fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Initializing NVM with default values"));
    fSettings.initialize_with_default_values();

    /* Write the timezone information to NVM */
    fSettings.write(SETT_TIMEZONE, (uint8_t *)&timezone, true);

    webserver_welcomewizard_client.stop();
    WiFi.softAPdisconnect(true);
    WiFi.disconnect();
    fEspHal.system_reboot();
}

void WelcomeWizard::request_reboot(uint32_t seconds_to_wait) {
    reboot_requested = true;
    reboot_time = device_uptime + seconds_to_wait;
    fLog.log(LOG_WELCOMEWIZARD, L_INFO, F("Rebooting in %u seconds..."), seconds_to_wait);
}

void WelcomeWizard::task() {
    handle_webserver_clients();
    if(conn_test_status == TST_RUNNING) test_wifi_connection();
    if(reboot_requested && reboot_time <= device_uptime) save_data_and_reboot();
}
