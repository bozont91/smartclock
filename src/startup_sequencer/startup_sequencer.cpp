#include "startup_sequencer.h"


StartupSequencer::StartupSequencer(
    TickerScheduler &ostask,
    WifiConnectionChecker &wificonnectionchecker,
    Logger &log,
    NtpManager &ntp,
    Webweather &webweather,
    ClockManager &clockmanager,
    LedmatrixStateManager &ledmatrixmain,
    Beep &beep,
    TcpRemote &tcpremote,
    RotaryencoderHAL &rotaryencoderhal,
    ButtonHandler &buttonhandler,
    mDnsManager &mdnsmanager,
    Webserver &webserver,
    MqttManager &mqtt,
    AdafruitIoIntegration &adafruitiointegration,
    AuraOta &ota
) :
osTask(ostask),
fWifiConnectionChecker(wificonnectionchecker),
fLog(log),
fNtp(ntp),
fWebWeather(webweather),
fClockManager(clockmanager),
fLedmatrixMain(ledmatrixmain),
fBeep(beep),
fTcpRemote(tcpremote),
fRotaryencoderHAL(rotaryencoderhal),
fButtonHandler(buttonhandler),
fmDnsManager(mdnsmanager),
fWebserver(webserver),
fMqttManager(mqtt),
fAdafruitIoIntegration(adafruitiointegration),
fOta(ota)
{
    ;
}

void StartupSequencer::task() {

    /* If the startup has already finished or we haven't connected to WiFi yet - return */
    if(startup_finished || fWifiConnectionChecker.get_first_wifi_connection_time() == 0) return;

    /* If we haven't reached the time for the next startup stage - return */
    if(next_stage_time > device_uptime) return;

    switch(startup_state) {
        case STS_INIT:
            startup_state++;
            next_stage_time = device_uptime;
            break;

        case STS_NTP:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - NTP"));
            osTask.enable(OS_TASK_NTP);
            fNtp.task();
            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_WEATHER_AND_WELCOMESCREEN:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - Weather & Welcome screen"));
            fWebWeather.task();
            osTask.enable(OS_TASK_WEBWEATHER);

            if(fClockManager.isClockSynchronizedToNtp()) {
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_WELCOME);
                fBeep.start_music(welcome_chime, MUSIC_BLOCK_WELCOME_CHIME_SIZE);
                first_ntp_sync_successful = true;
            }

            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_TCPREMOTE_ENCODER:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - TCPremote and encoder"));
            fTcpRemote.init();
            osTask.enable(OS_TASK_TCPREMOTE);
            fRotaryencoderHAL.set_enabled(true);
            fButtonHandler.set_enabled(true);

            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_MDNS:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - MDNS"));
            osTask.enable(OS_TASK_MDNS);
            fmDnsManager.task();
            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_WEBSERVER:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - Webserver"));
            fWebserver.begin();
            osTask.enable(OS_TASK_WEBSERVER);
            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_MQTT:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - MQTT"));
            osTask.enable(OS_TASK_MQTT);
            fMqttManager.task();
            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_ADAFRUITIO:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - AdafruitIO"));
            osTask.enable(OS_TASK_ADAFRUITIO);
            fAdafruitIoIntegration.task();
            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_OTA:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - OTA"));
            osTask.enable(OS_TASK_OTA);
            fOta.init();
            startup_state++;
            next_stage_time = device_uptime + 1;
            break;

        case STS_NTP_CHECK:
            fLog.log(LOG_SYSTEM, L_DEBUG, F("SSeq - NTP check"));
            if(first_ntp_sync_successful) {
                startup_state++;
                next_stage_time = device_uptime + 1;
                return;
            }

            if(fClockManager.isClockSynchronizedToNtp()) {
                fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_WELCOME);
                fBeep.start_music(welcome_chime, MUSIC_BLOCK_WELCOME_CHIME_SIZE);
                startup_state++;
                next_stage_time = device_uptime + 1;
            }
            break;

        case STS_MAX:
            fLog.log(LOG_SYSTEM, L_INFO, F("Startup finished in %lu seconds"), device_uptime);
            startup_finished = true;
            break;
    }
}
