#ifndef STARTUP_SEQUENCER_H
#define STARTUP_SEQUENCER_H

#include <Arduino.h>
#include <TickerScheduler.h>
#include "../common.h"
#include "../wifi_conn_checker/wifi_connection_checker.h"
#include "../log/log.h"
#include "../ntp/ntp.h"
#include "../internet_weather/internet_weather.h"
#include "../clockmanager/clockmanager.h"
#include "../ledmatrix_main/ledmatrix_main.h"
#include "../beep/beep.h"
#include "../tcp_remote/tcp_remote.h"
#include "../rotaryencoder/rotaryencoder.h"
#include "../buttonhandler/buttonhandler.h"
#include "../mdns/mdns.h"
#include "../webserver/webserver.h"
#include "../mqtt/mqtt.h"
#include "../adafruitio/adafruitio.h"
#include "../ota/ota.h"

class StartupSequencer {

public:
    StartupSequencer(
        TickerScheduler &ostask,
        WifiConnectionChecker &wificonnectionchecker,
        Logger &log,
        NtpManager &ntp,
        Webweather &webweather,
        ClockManager &clockmanager,
        LedmatrixStateManager &ledmatrixmain,
        Beep &beep,
        TcpRemote &tcpremote,
        RotaryencoderHAL &rotaryencoderhal,
        ButtonHandler &buttonhandler,
        mDnsManager &mdnsmanager,
        Webserver &webserver,
        MqttManager &mqtt,
        AdafruitIoIntegration &adafruitiointegration,
        AuraOta &ota
    );
    void task();

private:
    enum startup_state_e {
        STS_INIT,
        STS_NTP,
        STS_WEATHER_AND_WELCOMESCREEN,
        STS_TCPREMOTE_ENCODER,
        STS_MDNS,
        STS_WEBSERVER,
        STS_MQTT,
        STS_ADAFRUITIO,
        STS_OTA,
        STS_NTP_CHECK,
        STS_MAX
    } startup_states;

    uint32_t next_stage_time = 0u;
    bool startup_finished = false;
    uint8_t startup_state = STS_INIT;
    bool first_ntp_sync_successful = false;

    TickerScheduler &osTask;
    WifiConnectionChecker &fWifiConnectionChecker;
    Logger &fLog;
    NtpManager &fNtp;
    Webweather &fWebWeather;
    ClockManager &fClockManager;
    LedmatrixStateManager &fLedmatrixMain;
    Beep &fBeep;
    TcpRemote &fTcpRemote;
    RotaryencoderHAL &fRotaryencoderHAL;
    ButtonHandler &fButtonHandler;
    mDnsManager &fmDnsManager;
    Webserver &fWebserver;
    MqttManager &fMqttManager;
    AdafruitIoIntegration &fAdafruitIoIntegration;
    AuraOta &fOta;

};

#endif /* STARTUP_SEQUENCER_H */
