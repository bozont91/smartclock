#ifndef CRC32_H
#define CRC32_H

#ifndef AURA_UNITTEST
    #include <Arduino.h>
#else
    #include <stdio.h>
    #include <stdint.h>
#endif

class crc32 {
private:
    static crc32* instance;
    uint32_t crc32_table[256];
    crc32();

public:
    static crc32* getInstance() {
        if (instance == 0) instance = new crc32();
        return instance;
    }

    void generate_table();
    uint32_t update(uint32_t initial, const void* buf, size_t len);
};


#endif /* CRC32_H */
