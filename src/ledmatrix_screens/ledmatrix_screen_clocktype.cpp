#include "ledmatrix_screens.h"

void LedmatrixScreens::draw_clocktype_select_screen() {
    fLedmatrixGFX.clear_framebuffer();
    fLedmatrixGFX.draw_num(3, 0, 1);
    fLedmatrixGFX.draw_num(8, 0, 2);
    fLedmatrixGFX.draw_num(18, 0, 2);
    fLedmatrixGFX.draw_num(24, 0, 4);

    if(ledmatrix_clock_type == CLOCK_TYPE_12H) update_clocktype_select_screen(true, false);
    else if(ledmatrix_clock_type == CLOCK_TYPE_24H) update_clocktype_select_screen(false, false);

    fLedmatrixGFX.do_transition();
}

void LedmatrixScreens::update_clocktype_select_screen(uint8_t is_12h_selected, bool do_render) {
    clocktype_select_screen_is_12h_selected = is_12h_selected;

    if(is_12h_selected) {
        fLedmatrixGFX.clear_column_framebuffer(16);
        fLedmatrixGFX.clear_column_framebuffer(30);
        fLedmatrixGFX.fill_column_framebuffer(2);
        fLedmatrixGFX.fill_column_framebuffer(14);
    } else {
        fLedmatrixGFX.fill_column_framebuffer(16);
        fLedmatrixGFX.fill_column_framebuffer(30);
        fLedmatrixGFX.clear_column_framebuffer(2);
        fLedmatrixGFX.clear_column_framebuffer(14);
    }

    if(do_render) fLedmatrixGFX.render_framebuffer_to_screen();
}

void LedmatrixScreens::calculate_clocktype_setting_from_encoder(uint8_t enc_direction) {
    if(enc_direction == RotaryencoderHAL::RE_CLOCKWISE && clocktype_select_screen_is_12h_selected) {
        update_clocktype_select_screen(false);
        return;
    } else if(enc_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE && !clocktype_select_screen_is_12h_selected) {
        update_clocktype_select_screen(true);
        return;
    }

    fBeep.beep_short();
}

void LedmatrixScreens::handle_clocktype_selection() {
    fBeep.play_acknowledge();

    uint8_t clock_type_curr = get_clock_type();
    if((clock_type_curr == CLOCK_TYPE_12H && clocktype_select_screen_is_12h_selected) ||
       (clock_type_curr == CLOCK_TYPE_24H && !clocktype_select_screen_is_12h_selected)) {
        return;
    }

    if(clocktype_select_screen_is_12h_selected) set_clock_type(CLOCK_TYPE_12H);
    else set_clock_type(CLOCK_TYPE_24H);

    uint8_t sett_val = get_clock_type();
    fSettings.write(SETT_CLOCK_TYPE, &sett_val);
}
