#include "../ledmatrix_screens/ledmatrix_screens.h"

void LedmatrixScreens::draw_alarm_setting_screen() {

    fLedmatrixGFX.clear_framebuffer();

    alarmsettings_hour_selected = true;
    alarmsetting_hour = fAlarmManager.get_hour();
    alarmsetting_minute = fAlarmManager.get_minute();

    uint8_t hour_tens = alarmsetting_hour/10;
    uint8_t hour_ones = alarmsetting_hour%10;
    uint8_t minute_tens = alarmsetting_minute/10;
    uint8_t minute_ones = alarmsetting_minute%10;

    uint8_t display_x_offset = 3u;

    if(hour_tens > 0) fLedmatrixGFX.draw_num(display_x_offset, 0, hour_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 6, 0, hour_ones);
    fLedmatrixGFX.draw_num(display_x_offset + 11, 0, LEDMATRIX_ICON_COLON);
    fLedmatrixGFX.draw_num(display_x_offset + 14, 0, minute_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 20, 0, minute_ones);

    fLedmatrixGFX.fill_column_framebuffer(0);
    fLedmatrixGFX.fill_column_framebuffer(1);

    fLedmatrixGFX.do_transition();
}

void LedmatrixScreens::update_alarm_setting_screen(uint8_t hour, uint8_t minute, bool hours_selected) {

    fLedmatrixGFX.clear_framebuffer();

    uint8_t hour_tens = hour/10;
    uint8_t hour_ones = hour%10;
    uint8_t minute_tens = minute/10;
    uint8_t minute_ones = minute%10;

    uint8_t display_x_offset = 3u;

    if(hour_tens > 0) fLedmatrixGFX.draw_num(display_x_offset, 0, hour_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 6, 0, hour_ones);
    fLedmatrixGFX.draw_num(display_x_offset + 11, 0, LEDMATRIX_ICON_COLON);
    fLedmatrixGFX.draw_num(display_x_offset + 14, 0, minute_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 20, 0, minute_ones);

    if(hours_selected) {
        fLedmatrixGFX.fill_column_framebuffer(0);
        fLedmatrixGFX.fill_column_framebuffer(1);
    } else {
        fLedmatrixGFX.fill_column_framebuffer(31);
        fLedmatrixGFX.fill_column_framebuffer(30);
    }

    fLedmatrixGFX.render_framebuffer_to_screen();
}

void LedmatrixScreens::calculate_alarm_setting_from_encoder(uint8_t enc_direction) {
    if(enc_direction == RotaryencoderHAL::RE_CLOCKWISE) {

        if(alarmsettings_hour_selected) {
            alarmsetting_hour++;
            if(alarmsetting_hour == 24u) alarmsetting_hour = 0u;
        } else {
            alarmsetting_minute++;
            if(alarmsetting_minute == 60u) alarmsetting_minute = 0u;
        }

        update_alarm_setting_screen(alarmsetting_hour, alarmsetting_minute, alarmsettings_hour_selected);

    } else if(enc_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) {

        if(alarmsettings_hour_selected) {
            alarmsetting_hour--;
            if(alarmsetting_hour == 255u) alarmsetting_hour = 23u;
        } else {
            alarmsetting_minute--;
            if(alarmsetting_minute == 255u) alarmsetting_minute = 59u;
        }

        update_alarm_setting_screen(alarmsetting_hour, alarmsetting_minute, alarmsettings_hour_selected);
    }
}


bool LedmatrixScreens::handle_alarm_setting_selection() {
    /* Switch from adjusting the hour to adjusting the minutes */
    if(alarmsettings_hour_selected) {
        alarmsettings_hour_selected = false;
        update_alarm_setting_screen(alarmsetting_hour, alarmsetting_minute, false);
        fBeep.beep_short();
        return false;
    }

    uint8_t alarm_hour = alarmsetting_hour;
    uint8_t alarm_min = alarmsetting_minute;

    fAlarmManager.set_alarm(alarm_hour, alarm_min);
    fAlarmManager.enable();

    uint8_t alarm_enabled = true;
    if(fSettings.read_byte_s(SETT_ALARM_HOUR) != alarm_hour) {
        fSettings.write(SETT_ALARM_HOUR, &alarm_hour, false);
    }

    if(fSettings.read_byte_s(SETT_ALARM_MINUTE) != alarm_min) {
        fSettings.write(SETT_ALARM_MINUTE, &alarm_min);
    }

    if(fSettings.read_byte_s(SETT_ALARM_ENABLED) == false) {
        fSettings.write(SETT_ALARM_ENABLED, &alarm_enabled);
    }

    fBeep.play_acknowledge();
    return true;
}
