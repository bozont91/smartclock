#include "ledmatrix_screens.h"

void LedmatrixScreens::draw_weather()
{
    fLedmatrixGFX.clear_framebuffer();

    if(!fWebWeather.get_data_available()) {
        fLedmatrixGFX.draw_num(3, 0, LEDMATRIX_ICON_DASH);
        fLedmatrixGFX.draw_num(9, 0, LEDMATRIX_ICON_DASH);
        fLedmatrixGFX.draw_num(15, 0, LEDMATRIX_ICON_CELSIUS);
        fLedmatrixGFX.do_transition();
        return;
    }

    int16_t temperature = fWebWeather.get_temp();

    if(temperature < 0) {
        temperature = abs(temperature);
        fLedmatrixGFX.setxy_framebuffer(0, 4, true);
        fLedmatrixGFX.setxy_framebuffer(1, 4, true);
    }

    uint8_t temperature_tens = temperature/10;
    uint8_t temperature_ones = temperature%10;

    if(temperature_tens > 0) {
        fLedmatrixGFX.draw_num(3, 0, temperature_tens);
        fLedmatrixGFX.draw_num(9, 0, temperature_ones);
        fLedmatrixGFX.draw_num(15, 0, LEDMATRIX_ICON_CELSIUS);
    } else {
        fLedmatrixGFX.draw_num(3, 0, temperature_ones);
        fLedmatrixGFX.draw_num(9, 0, LEDMATRIX_ICON_CELSIUS);
    }

    fLedmatrixGFX.draw_num(24, 0, fWebWeather.get_weather_icon());

    fLedmatrixGFX.do_transition();
}

void LedmatrixScreens::get_detailed_weather_string(char *dest, size_t size) {
    char temp_postfix = fTemperatureManager.get_temperature_postfix_as_char();

    char inside_temp_str[32] = "";
    if(fTemperatureManager.get_sensor_present()) {
        snprintf_P(inside_temp_str, 32u, PSTR("Inside: %d°%c | "), (int8_t)fTemperatureManager.get_temperature(), temp_postfix);
    }

    snprintf_P(dest, size, PSTR("%s%s  %d°%c %s  Feels like: %d°%c  Rain: %d mm  Humidity: %d%%  Wind: %.1f km/h  Pressure: %d hPa"),
                    inside_temp_str,
                    fWebWeather.get_location().c_str(), fWebWeather.get_temp(), temp_postfix, fWebWeather.get_state_as_text().c_str(),
                    fWebWeather.get_temp_feels_like(), temp_postfix,
                    fWebWeather.get_rain(),
                    fWebWeather.get_humidity(), (float)fWebWeather.get_wind_speed()*(float)3.6, fWebWeather.get_pressure()
            );

}
