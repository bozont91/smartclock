#include "../ledmatrix_screens/ledmatrix_screens.h"

void LedmatrixScreens::draw_mute_setting_screen() {
    fLedmatrixGFX.clear_framebuffer();
    fLedmatrixGFX.draw_num(0, 0, LEDMATRIX_ICON_SOUND);
    fLedmatrixGFX.draw_num(13, 0, LEDMATRIX_ICON_YES);
    fLedmatrixGFX.draw_num(23, 0, LEDMATRIX_ICON_NO);

    if(fMuteManager.is_muted()) {
        update_mute_setting_screen(false, false);
    } else {
        update_mute_setting_screen(true, false);
    }

    fLedmatrixGFX.do_transition();
}

void LedmatrixScreens::update_mute_setting_screen(bool yes_selected, bool do_render) {
    if(yes_selected) {
        fLedmatrixGFX.clear_column_framebuffer(22);
        fLedmatrixGFX.clear_column_framebuffer(31);
        fLedmatrixGFX.fill_column_framebuffer(12);
        fLedmatrixGFX.fill_column_framebuffer(21);
    } else {
        fLedmatrixGFX.fill_column_framebuffer(22);
        fLedmatrixGFX.fill_column_framebuffer(31);
        fLedmatrixGFX.clear_column_framebuffer(12);
        fLedmatrixGFX.clear_column_framebuffer(21);
    }

    if(do_render) fLedmatrixGFX.render_framebuffer_to_screen();
}

void LedmatrixScreens::calculate_mute_setting_from_encoder(uint8_t enc_direction) {
    if(enc_direction == RotaryencoderHAL::RE_CLOCKWISE) {
        if(mutesetting_menu_yes_selected) {
            mutesetting_menu_yes_selected = false;
            update_mute_setting_screen(mutesetting_menu_yes_selected);
            fMuteManager.mute();
        } else {
            fBeep.beep_short();
        }
    } else if(enc_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) {
        if(mutesetting_menu_yes_selected) {
            fBeep.beep_short();
        } else {
            mutesetting_menu_yes_selected = true;
            update_mute_setting_screen(mutesetting_menu_yes_selected);
            fMuteManager.unmute();
        }
    }
}


void LedmatrixScreens::handle_mute_setting_selection() {
    bool is_muted = fMuteManager.is_muted();

    if(fSettings.read_byte_s(SETT_MUTE) != is_muted) {
        fSettings.write(SETT_MUTE, (uint8_t*)&is_muted);
        fLog.log(LOG_LEDMATRIX, L_INFO, F("Changing mute state to: %d"), is_muted);
    }

    fBeep.play_acknowledge();
}
