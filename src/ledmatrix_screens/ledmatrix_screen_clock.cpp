#include "ledmatrix_screens.h"

void LedmatrixScreens::clock_force_update() {
    fLog.log(LOG_LEDMATRIX, L_DEBUG, F("Clock update requested"));
    clock_update_requested = true;
}

void LedmatrixScreens::set_clock_type(uint8_t type) {
    if(type >= CLOCK_TYPE_MAX) return;
    ledmatrix_clock_type = type;
    fLog.log(LOG_LEDMATRIX, L_INFO, F("Setting clock type: %d"), ledmatrix_clock_type);
}

uint8_t LedmatrixScreens::get_clock_type() {
    return ledmatrix_clock_type;
}

void LedmatrixScreens::set_clock_style(uint8_t style) {
    if(style >= CLOCK_STYLE_MAX) {
        ledmatrix_clock_style = CLOCK_STYLE_BIG;
        alarm_dot_x_position = 31u;
    } else {
        ledmatrix_clock_style = style;
        if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_INSIDE_TEMP || ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_OUTSIDE_TEMP) {
            alarm_dot_x_position = 19u;
        } else {
            alarm_dot_x_position = 31u;
        }
    }

    fLog.log(LOG_LEDMATRIX, L_INFO, F("Setting clock style: %d"), ledmatrix_clock_style);
}

uint8_t LedmatrixScreens::get_clock_style() {
    return ledmatrix_clock_style;
}

uint8_t LedmatrixScreens::convert_hour_to_12h_format(uint8_t hour) {
    if(hour > 12) hour = hour-12;
    if(hour == 0) hour = 12;
    return hour;
}

void LedmatrixScreens::draw_clock(uint8_t transition_type) {
    clock_update_requested = false;

    /* This is why the blink's not working on the second display clock */
    fLedmatrixGFX.clear_framebuffer();

    switch(ledmatrix_clock_style) {
        case CLOCK_STYLE_BIG:
            draw_big_clock();
            break;

        case CLOCK_STYLE_SMALL:
        case CLOCK_STYLE_SMALL_WITH_INSIDE_TEMP:
        case CLOCK_STYLE_SMALL_WITH_OUTSIDE_TEMP:
            draw_small_clock();
            break;

        case CLOCK_STYLE_SMALL_WITH_SECONDS:
            draw_small_clock_with_seconds();
            break;

        default:
            break;
    }

    /* Draw a dot in the corner if the alarm is enabled */
    if(fAlarmManager.get_enabled() && !fAlarmManager.is_snoozing()) {
        fLedmatrixGFX.setxy_framebuffer(alarm_dot_x_position, 7, true);
    }

    if(fLedmatrixGFX.LEDMATRIX_DO_TRANSITION == transition_type) {
        fLedmatrixGFX.do_transition();
    } else {
        fLedmatrixGFX.render_framebuffer_to_screen();
    }

}

void inline LedmatrixScreens::draw_big_clock() {
    if(ledmatrix_clock_type == CLOCK_TYPE_24H) draw_24h_clock();
    else if(ledmatrix_clock_type == CLOCK_TYPE_12H) draw_12h_clock();
}

void inline LedmatrixScreens::draw_24h_clock() {
    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    uint8_t hour_tens = hour/10;
    uint8_t hour_ones = hour%10;
    uint8_t minute_tens = minute/10;
    uint8_t minute_ones = minute%10;

    uint8_t display_x_offset = 3u;

    if(hour_tens > 0) fLedmatrixGFX.draw_num(display_x_offset, 0, hour_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 6, 0, hour_ones);
    fLedmatrixGFX.draw_num(display_x_offset + 11, 0, LEDMATRIX_ICON_COLON);
    fLedmatrixGFX.draw_num(display_x_offset + 14, 0, minute_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 20, 0, minute_ones);

    update_displayed_numbers(hour, minute);
}

void inline LedmatrixScreens::draw_12h_clock() {
    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    bool is_am = true;

    if(hour >= 12) is_am = false;
    hour = convert_hour_to_12h_format(hour);

    uint8_t hour_tens = hour/10;
    uint8_t hour_ones = hour%10;
    uint8_t minute_tens = minute/10;
    uint8_t minute_ones = minute%10;

    if(hour_tens != 1) {
        /* Draw space if there would be a 0 */
        fLedmatrixGFX.draw_num(0, 0, LEDMATRIX_ICON_EMPTY);
    } else {
        fLedmatrixGFX.draw_num(0, 0, hour_tens);
    }
    fLedmatrixGFX.draw_num(6, 0, hour_ones);
    fLedmatrixGFX.draw_num(11, 0, LEDMATRIX_ICON_COLON);
    fLedmatrixGFX.draw_num(14, 0, minute_tens);
    fLedmatrixGFX.draw_num(20, 0, minute_ones);

    if(is_am) {
        fLedmatrixGFX.draw_num(26, 0, LEDMATRIX_ICON_AM);
    } else {
        fLedmatrixGFX.draw_num(26, 0, LEDMATRIX_ICON_PM);
    }

    update_displayed_numbers(hour, minute);
}

void inline LedmatrixScreens::draw_small_clock() {
    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    if(ledmatrix_clock_type == CLOCK_TYPE_12H) hour = convert_hour_to_12h_format(hour);

    uint8_t hour_tens = hour/10;
    uint8_t hour_ones = hour%10;
    uint8_t minute_tens = minute/10;
    uint8_t minute_ones = minute%10;

    uint8_t display_x_offset = 1u;
    uint8_t display_y_offset = 2u;

    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL) {
        display_x_offset = 7;
    }

    if(hour_tens > 0) fLedmatrixGFX.draw_small_num(display_x_offset, display_y_offset, hour_tens);
    fLedmatrixGFX.draw_small_num(display_x_offset + 4, display_y_offset, hour_ones);
    fLedmatrixGFX.draw_small_num(display_x_offset + 7, display_y_offset, LEDMATRIX_SMALL_ICON_COLON);
    fLedmatrixGFX.draw_small_num(display_x_offset + 10, display_y_offset, minute_tens);
    fLedmatrixGFX.draw_small_num(display_x_offset + 14, display_y_offset, minute_ones);

    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL) {
        update_displayed_numbers(hour, minute);
        return;
    }

    int8_t temperature = 0u;

    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_INSIDE_TEMP) {
        if(!fTemperatureManager.get_sensor_present()) return;
        temperature = (int8_t)fTemperatureManager.get_temperature();
    } else if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_OUTSIDE_TEMP) {
        if(!fWebWeather.get_enabled()) return;

        if(!fWebWeather.get_data_available()) {
            fLedmatrixGFX.draw_small_num(23, display_y_offset, LEDMATRIX_SMALL_ICON_DASH);
            fLedmatrixGFX.draw_small_num(27, display_y_offset, LEDMATRIX_SMALL_ICON_DASH);
            return;
        }

        if(small_clock_weather_state_show_timer > 0) {
            fLedmatrixGFX.draw_num(SMALL_CLOCK_WEATHER_ICON_X_OFFSET, 0, fWebWeather.get_weather_icon());
            return;
        }

        temperature = (int8_t)fWebWeather.get_temp();
    }

    bool temp_is_negative = false;
    if(temperature < 0) {
        temperature = abs(temperature);
        temp_is_negative = true;
    }

    uint8_t temperature_tens = temperature/10;
    uint8_t temperature_ones = temperature%10;

    if(temperature_tens > 0) {
        fLedmatrixGFX.draw_small_num(23, display_y_offset, temperature_tens);
        fLedmatrixGFX.draw_small_num(27, display_y_offset, temperature_ones);
        fLedmatrixGFX.draw_small_num(30, display_y_offset, LEDMATRIX_SMALL_ICON_CELSIUS);
        if(temp_is_negative) {
            fLedmatrixGFX.setxy_framebuffer(20, 4, true);
            fLedmatrixGFX.setxy_framebuffer(21, 4, true);
        }
    } else {
        fLedmatrixGFX.draw_small_num(27, display_y_offset, temperature_ones);
        fLedmatrixGFX.draw_small_num(30, display_y_offset, LEDMATRIX_SMALL_ICON_CELSIUS);
        if(temp_is_negative) {
            fLedmatrixGFX.setxy_framebuffer(24, 4, true);
            fLedmatrixGFX.setxy_framebuffer(25, 4, true);
        }
    }

    update_displayed_numbers(hour, minute, temperature);
}

void inline LedmatrixScreens::draw_small_clock_with_seconds() {
    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    if(ledmatrix_clock_type == CLOCK_TYPE_12H) hour = convert_hour_to_12h_format(hour);

    uint8_t hour_tens = hour/10;
    uint8_t hour_ones = hour%10;
    uint8_t minute_tens = minute/10;
    uint8_t minute_ones = minute%10;

    uint8_t seconds = fClockManager.getSecond();
    uint8_t second_tens = seconds/10;
    uint8_t second_ones = seconds%10;

    uint8_t display_x_offset = 2u;
    uint8_t display_y_offset = 2u;

    if(hour_tens > 0) fLedmatrixGFX.draw_small_num(display_x_offset, display_y_offset, hour_tens);
    fLedmatrixGFX.draw_small_num(display_x_offset + 4, display_y_offset, hour_ones);
    fLedmatrixGFX.draw_small_num(display_x_offset + 7, display_y_offset, LEDMATRIX_SMALL_ICON_COLON);
    fLedmatrixGFX.draw_small_num(display_x_offset + 10, display_y_offset, minute_tens);
    fLedmatrixGFX.draw_small_num(display_x_offset + 14, display_y_offset, minute_ones);
    fLedmatrixGFX.draw_small_num(display_x_offset + 17, display_y_offset, LEDMATRIX_SMALL_ICON_COLON);
    fLedmatrixGFX.draw_small_num(display_x_offset + 20, display_y_offset, second_tens);
    fLedmatrixGFX.draw_small_num(display_x_offset + 24, display_y_offset, second_ones);

    update_displayed_numbers(hour, minute, seconds);
}

void LedmatrixScreens::clock_smooth_transition_start() {
    clock_smooth_transition_in_progress = true;
}

void LedmatrixScreens::clock_smooth_transition_task() {

    static uint8_t line_counter = 0u;
    uint8_t hour = fClockManager.getHour();
    uint8_t minute = fClockManager.getMinute();

    /* Calculate the previous minute */
    int8_t minute_prev = minute-1;
    int8_t hour_prev = hour;
    if(minute_prev < 0) {
        minute_prev = 59;
        hour_prev--;
        if(hour_prev < 0) hour_prev = 23;
    }

    uint8_t hour_tens_prev = hour_prev/10;
    uint8_t hour_ones_prev = hour_prev%10;
    uint8_t minute_tens_prev = minute_prev/10;
    uint8_t minute_ones_prev = minute_prev%10;

    uint8_t hour_tens = hour/10;
    uint8_t hour_ones = hour%10;
    uint8_t minute_tens = minute/10;
    uint8_t minute_ones = minute%10;

    /* Draw one line of the clock */
    uint8_t display_x_offset = 3u;
    if(hour_tens > 0 && hour_tens != hour_tens_prev) fLedmatrixGFX.draw_single_line_of_num(display_x_offset, 0, hour_tens, line_counter);
    if(hour_tens == 0 && hour_tens_prev > 0) fLedmatrixGFX.draw_single_line_of_num(display_x_offset, 0, LEDMATRIX_ICON_EMPTY, line_counter);
    if(hour_ones != hour_ones_prev) fLedmatrixGFX.draw_single_line_of_num(display_x_offset + 6, 0, hour_ones, line_counter);
    fLedmatrixGFX.draw_num(display_x_offset + 11, 0, LEDMATRIX_ICON_COLON);
    if(minute_tens != minute_tens_prev) fLedmatrixGFX.draw_single_line_of_num(display_x_offset + 14, 0, minute_tens, line_counter);
    if(minute_ones != minute_ones_prev) fLedmatrixGFX.draw_single_line_of_num(display_x_offset + 20, 0, minute_ones, line_counter);

    fLedmatrixGFX.render_framebuffer_to_screen();

    line_counter++;

    if(line_counter == fLedmatrixGFX.LEDMATRIX_HEIGHT) {
        line_counter = 0u;
        clock_smooth_transition_in_progress = false;
        update_displayed_numbers(hour, minute);
        return;
    }

}

void LedmatrixScreens::temp_change_callback(const int16_t temperature) {
    (void)temperature;
    fLog.log(LOG_LEDMATRIX, L_DEBUG, F("Temperature change notification received; temp='%d'"), temperature);
    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_INSIDE_TEMP) {
        clock_force_update();
    }
}

void LedmatrixScreens::weather_change_callback(const int16_t temperature, const uint8_t state_icon, bool validity) {
    static int16_t temperature_prev = 0;
    static bool validity_prev = false;
    static uint8_t state_icon_prev = LEDMATRIX_ICON_EMPTY;
    fLog.log(LOG_LEDMATRIX, L_DEBUG, F("Weather change notification received; temp='%d' state_icon='%d' validity='%d'"), temperature, state_icon, validity);

    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_OUTSIDE_TEMP) {

        /* If the temperature changes but the weather state remains the same, or the validity changes - simply refresh the displayed temp */
        if((temperature != temperature_prev && state_icon == state_icon_prev) || validity_prev != validity) {
            clock_force_update();
            temperature_prev = temperature;
            state_icon_prev = state_icon;
            validity_prev = validity;
            return;
        }

        /* If the data is valid and the weather state changes */
        if(validity && (state_icon != state_icon_prev)) {
            fLog.log(LOG_LEDMATRIX, L_DEBUG, F("Requesting to display weather update"));
            small_clock_weather_update_draw_request = true;
            state_icon_prev = state_icon;
            temperature_prev = temperature;
            return;
        }

    }
}

/* Draws the weather icon overwriting the outside temperature */
void LedmatrixScreens::draw_small_clock_weather_update() {
    fLedmatrixGFX.clear_framebuffer(22u, 0u);
    fLedmatrixGFX.draw_num(SMALL_CLOCK_WEATHER_ICON_X_OFFSET, 0, fWebWeather.get_weather_icon());
    fLedmatrixGFX.do_transition(22u);
    fBeep.start_music(okay_chime, MUSIC_BLOCK_OKAY_CHIME_SIZE);
}

/* Restores the small clock to the original state (temp display) */
void LedmatrixScreens::draw_small_clock_weather_update_restore() {
    fLedmatrixGFX.clear_framebuffer();
    draw_small_clock();
    fLedmatrixGFX.do_transition(19u);
}

/* Handles the timings and requests for the weather update on the 'small clock with outside temp' */
void LedmatrixScreens::handle_small_clock_weather_notifications() {

    if(ledmatrix_clock_style != CLOCK_STYLE_SMALL_WITH_OUTSIDE_TEMP) return;

    if(small_clock_weather_state_show_timer > 0) {
        small_clock_weather_state_show_timer--;
        if(small_clock_weather_state_show_timer == 0) draw_small_clock_weather_update_restore();
    }

    if(small_clock_weather_update_draw_request) {
        draw_small_clock_weather_update();
        small_clock_weather_update_draw_request = false;
        /* Show the update for 10 seconds */
        small_clock_weather_state_show_timer = 500u;
    }
}

void LedmatrixScreens::screen_clock_periodic() {

    /* Transition periodic call */
    if(clock_smooth_transition_in_progress) {
        clock_smooth_transition_task();
        return;
    }

    handle_small_clock_weather_notifications();

    static uint32_t last_update = 0u;
    uint32_t device_clock = fDeviceClock.get();

    /* The small clock with seconds needs to be updated every second */
    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_SECONDS) {
        if(clock_update_requested || last_update != device_clock) {
            last_update = device_clock;
            draw_clock(fLedmatrixGFX.LEDMATRIX_NO_TRANSITION);
        }
    } else {

        /* The other types of clocks need to be updated every minute */
        if((device_clock%60 == 0 && last_update != device_clock) || clock_update_requested) {
            last_update = device_clock;
            /* ToDo: implement smooth transitions for the 12h clock */
            if(clock_smooth_transition_enabled && ledmatrix_clock_style == CLOCK_STYLE_BIG && ledmatrix_clock_type == CLOCK_TYPE_24H && !clock_update_requested) {
                clock_smooth_transition_start();
            } else {
                draw_clock(fLedmatrixGFX.LEDMATRIX_NO_TRANSITION);
            }
        }

    }

    /* Blink the dot in the corner if a snooze is in progress */
    static uint32_t last_blink_change = 0u;
    static bool alarm_dot_blink_state = false;
    if(fAlarmManager.is_snoozing() && last_blink_change != device_clock) {
        last_blink_change = device_clock;
        alarm_dot_blink_state = !alarm_dot_blink_state;
        fLedmatrixHal.set_pixel(alarm_dot_x_position, 7, alarm_dot_blink_state);
    }

    /* Check for pretty numbers if enabled */
    if(magic_numbers_enabled) magic_number_check();
}
