#ifndef LEDMATRIX_SCREENS_H
#define LEDMATRIX_SCREENS_H

#include <Arduino.h>
#include "../ledmatrix_font/ledmatrix_font.h"
#include "../clockmanager/clockmanager.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"
#include "../alarm/alarm.h"
#include "../ledmatrix_hal/ledmatrix_hal.h"
#include "../rotaryencoder/rotaryencoder.h"
#include "../beep/beep.h"
#include "../temperature/temperature.h"
#include "../deviceclock/deviceclock.h"
#include "../internet_weather/internet_weather.h"
#include "../settings/settings.h"
#include "../beep/beep.h"
#include "../beep/mutemanager.h"
#include "../log/log.h"


class LedmatrixScreens : public TemperatureSubscriber, public WeatherSubscriber {

public:

    enum CLOCK_STYLE_e {
        CLOCK_STYLE_BIG,
        CLOCK_STYLE_SMALL,
        CLOCK_STYLE_SMALL_WITH_INSIDE_TEMP,
        CLOCK_STYLE_SMALL_WITH_OUTSIDE_TEMP,
        CLOCK_STYLE_SMALL_WITH_SECONDS,
        CLOCK_STYLE_MAX
    };

    enum CLOCK_TYPE_e {
        CLOCK_TYPE_12H,
        CLOCK_TYPE_24H,
        CLOCK_TYPE_MAX
    };

    LedmatrixScreens(
        ClockManager &clockmanager,
        LedmatrixGFX &ledmatrixgfx,
        AlarmManager &alarmmanager,
        LedmatrixHal &fLedledmatrixhal,
        DeviceClock &deviceclock,
        TemperatureManager &tempmanager,
        Webweather &webweather,
        Settings &settings,
        Beep &beep,
        MuteManager &mutemanager,
        Logger &log
    );
    void read_settings();
    void temp_change_callback(const int16_t temperature) override;
    void weather_change_callback(const int16_t temperature, const uint8_t state_icon, bool validity) override;
    void clock_force_update();
    void set_clock_type(uint8_t type);
    uint8_t get_clock_type();
    void set_clock_style(uint8_t style);
    uint8_t get_clock_style();
    void draw_clock(uint8_t transition_type);
    void draw_loading_anim();
    void screen_clock_periodic();
    void draw_year();
    void draw_date();
    void draw_inside_temperature();
    void draw_weather();
    void get_detailed_weather_string(char *dest, size_t size);
    void get_detailed_date_string(char *dest, size_t size);

    void calculate_brightness_setting_from_encoder(uint8_t enc_direction);
    void draw_brightness_adjust_screen();
    void update_brightness_adjust_screen();
    void handle_brightness_setting_selection();

    void calculate_alarm_setting_from_encoder(uint8_t enc_direction);
    void draw_alarm_setting_screen();
    void update_alarm_setting_screen(uint8_t hour, uint8_t minute, bool hours_selected);
    bool handle_alarm_setting_selection();

    void calculate_mute_setting_from_encoder(uint8_t enc_direction);
    void draw_mute_setting_screen();
    void update_mute_setting_screen(bool yes_selected, bool do_render=true);
    void handle_mute_setting_selection();

    void calculate_clockstyle_setting_from_encoder(uint8_t enc_direction);
    void draw_clockstyle_select_screen();
    void update_clockstyle_select_screen();
    void blink_clockstyle_select_screen();
    void handle_clockstyle_selection();

    void calculate_clocktype_setting_from_encoder(uint8_t enc_direction);
    void draw_clocktype_select_screen();
    void update_clocktype_select_screen(uint8_t is_12h_selected, bool do_render=true);
    void handle_clocktype_selection();

    void magic_numbers_set_enabled(bool state);
    bool magic_numbers_get_enabled();


private:

    uint8_t ledmatrix_clock_type = CLOCK_TYPE_24H;
    uint8_t ledmatrix_clock_style = CLOCK_STYLE_BIG;
    bool clock_update_requested = false;
    uint8_t alarm_dot_x_position = 31u;

    uint32_t clockstyle_select_blink_delay_cycles = 0u;
    bool clockstyle_select_blink_state = true;

    bool small_clock_weather_update_draw_request = false;
    uint32_t small_clock_weather_state_show_timer = 0u;
    static const uint8_t SMALL_CLOCK_WEATHER_ICON_X_OFFSET = 24u;

    uint8_t convert_hour_to_12h_format(uint8_t hour);
    void inline draw_big_clock();
    void inline draw_24h_clock();
    void inline draw_12h_clock();
    void inline draw_small_clock();
    void inline draw_small_clock_with_seconds();
    void draw_small_clock_weather_update();
    void draw_small_clock_weather_update_restore();
    void handle_small_clock_weather_notifications();

    bool clock_smooth_transition_enabled = true;
    bool clock_smooth_transition_in_progress = false;
    void clock_smooth_transition_start();
    void clock_smooth_transition_task();

    typedef struct displayed_numbers_st {
        uint8_t size;
        uint8_t numbers[3];
        bool changed;
    } displayed_numbers_s;

    displayed_numbers_s displayed_numbers = {.size = 0, .numbers = {255, 255, 255}, .changed = false};
    void update_displayed_numbers(uint8_t one, uint8_t two);
    void update_displayed_numbers(uint8_t one, uint8_t two, uint8_t three);

    bool magic_numbers_enabled = false;
    inline void magic_number_notify();
    void magic_number_check();


    bool clocktype_select_screen_is_12h_selected = false;

    uint8_t alarmsetting_hour = 0u;
    uint8_t alarmsetting_minute = 0u;
    bool alarmsettings_hour_selected = true;

    bool mutesetting_menu_yes_selected = true;

    ClockManager &fClockManager;
    LedmatrixGFX &fLedmatrixGFX;
    AlarmManager &fAlarmManager;
    LedmatrixHal &fLedmatrixHal;
    DeviceClock &fDeviceClock;
    TemperatureManager &fTemperatureManager;
    Webweather &fWebWeather;
    Settings &fSettings;
    Beep &fBeep;
    MuteManager &fMuteManager;
    Logger &fLog;

};


#endif /* LEDMATRIX_SCREENS_H */
