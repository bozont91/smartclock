#include "ledmatrix_screens.h"

void LedmatrixScreens::draw_loading_anim() {

    static uint8_t wait_cycles = 0;
    static const uint8_t anim_x_offset = 15u;
    static const uint8_t anim_y_offset = 4u;

    if(wait_cycles != 0) {
        wait_cycles--;
        return;
    } else {
        wait_cycles = 5;
    }

    static bool led_states[4] = {0,0,0,0};
    static uint8_t anim_cnt = 0;

    if(anim_cnt < 4) {
        led_states[anim_cnt] = true;
    } else if(anim_cnt < 8) {
        led_states[anim_cnt-4] = false;
    }

    anim_cnt++;
    if(anim_cnt == 8) anim_cnt = 0;

    fLedmatrixHal.set_pixel(anim_x_offset  , anim_y_offset, led_states[0]);
    fLedmatrixHal.set_pixel(anim_x_offset+1, anim_y_offset, led_states[1]);
    fLedmatrixHal.set_pixel(anim_x_offset+2, anim_y_offset, led_states[2]);
    fLedmatrixHal.set_pixel(anim_x_offset+3, anim_y_offset, led_states[3]);

}
