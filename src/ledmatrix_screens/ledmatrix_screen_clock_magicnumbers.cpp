#include "ledmatrix_screens.h"

void LedmatrixScreens::update_displayed_numbers(uint8_t one, uint8_t two) {
    if(one == displayed_numbers.numbers[0] && two == displayed_numbers.numbers[1]) return;

    displayed_numbers.size = 2u;
    displayed_numbers.numbers[0] = one;
    displayed_numbers.numbers[1] = two;
    displayed_numbers.changed = true;
}

void LedmatrixScreens::update_displayed_numbers(uint8_t one, uint8_t two, uint8_t three) {
    if(one == displayed_numbers.numbers[0] && two == displayed_numbers.numbers[1] && three == displayed_numbers.numbers[2]) return;

    displayed_numbers.size = 3u;
    displayed_numbers.numbers[0] = one;
    displayed_numbers.numbers[1] = two;
    displayed_numbers.numbers[2] = three;
    displayed_numbers.changed = true;
}

void LedmatrixScreens::magic_numbers_set_enabled(bool state) {
    magic_numbers_enabled = state;
    if(state) fLog.log(LOG_LEDMATRIX, L_INFO, F("Magic numbers enabled"));
    else fLog.log(LOG_LEDMATRIX, L_INFO, F("Magic numbers disabled"));
}

bool LedmatrixScreens::magic_numbers_get_enabled() {
    return magic_numbers_enabled;
}

inline void LedmatrixScreens::magic_number_notify() {
    fBeep.start_music(ill_fly, MUSIC_BLOCK_ILL_FLY_SIZE);
    fLog.log(LOG_LEDMATRIX, L_INFO, F("Magic number found!"));
}

void LedmatrixScreens::magic_number_check() {
    if(!displayed_numbers.changed) return;
    displayed_numbers.changed = false;

    uint8_t first = displayed_numbers.numbers[0];
    uint8_t second = displayed_numbers.numbers[1];

    if(first == 16 && second == 20) {
        magic_number_notify();
        return;
    }

    if(ledmatrix_clock_style == CLOCK_STYLE_SMALL_WITH_SECONDS) return;

    if(displayed_numbers.size == 2) {
        if(first == second) magic_number_notify();
        return;
    }

    if(displayed_numbers.size == 3) {
        uint8_t third = displayed_numbers.numbers[2];
        if(first == second && second == third) magic_number_notify();
        if((first + 1) == second && (second + 1) == third) magic_number_notify();
        if((first - 1) == second && (second - 1) == third) magic_number_notify();
        if(first == 11 && second == 22 && third == 33) magic_number_notify();
        return;
    }
}
