#include "ledmatrix_screens.h"

void LedmatrixScreens::draw_brightness_adjust_screen() {
    fLedmatrixGFX.clear_framebuffer();
    fLedmatrixGFX.render_text_to_framebuffer("-", 2, 0);

    fLedmatrixGFX.draw_num(13, 0, fLedmatrixHal.get_brightness() + 1);

    fLedmatrixGFX.render_text_to_framebuffer("+", 25, 0);
    fLedmatrixGFX.do_transition();
}

void LedmatrixScreens::update_brightness_adjust_screen() {
    for(uint8_t i = 0 ; i < 5 ; i++) {
        fLedmatrixGFX.clear_column_framebuffer(13 + i);
    }
    fLedmatrixGFX.draw_num(13, 0, fLedmatrixHal.get_brightness() + 1);
    fLedmatrixGFX.render_framebuffer_to_screen();
}

void LedmatrixScreens::calculate_brightness_setting_from_encoder(uint8_t enc_direction) {
    if(enc_direction == RotaryencoderHAL::RE_CLOCKWISE) {
        if(fLedmatrixHal.get_brightness() == MAX7219_LEDMATRIX_MAX_BRIGHTNESS) {
            fBeep.beep_short();
        } else {
            fLedmatrixHal.increase_brightness();
            update_brightness_adjust_screen();
        }
    } else if(enc_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) {
        if(fLedmatrixHal.get_brightness() == MAX7219_LEDMATRIX_MIN_BRIGHTNESS) {
            fBeep.beep_short();
        } else {
            fLedmatrixHal.decrease_brightness();
            update_brightness_adjust_screen();
        }
    }
}

void LedmatrixScreens::handle_brightness_setting_selection() {
    fLog.log(LOG_LEDMATRIX, L_INFO, F("Saving selected brightness"));
    uint8_t brightnessval = fLedmatrixHal.get_brightness();

    if(fSettings.read_byte_s(SETT_BRIGHTNESS) != brightnessval) {
        fSettings.write(SETT_BRIGHTNESS, &brightnessval);
    }

    fBeep.beep_short();
}
