#include "ledmatrix_screens.h"

void LedmatrixScreens::draw_inside_temperature() {

    int8_t temperature = (int8_t)fTemperatureManager.get_temperature();

    fLedmatrixGFX.clear_framebuffer();
    fLedmatrixGFX.render_text_to_framebuffer("in", 0, 0);

    if(fTemperatureManager.get_sensor_present() == true) {

        if(temperature < 0) {
            temperature = abs(temperature);
            fLedmatrixGFX.setxy_framebuffer(13, 4, true);
            fLedmatrixGFX.setxy_framebuffer(14, 4, true);
        }

        fLedmatrixGFX.draw_num(16, 0, temperature/10);
        fLedmatrixGFX.draw_num(22, 0, temperature%10);
        fLedmatrixGFX.draw_num(28, 0, LEDMATRIX_ICON_CELSIUS);

    } else {

        fLedmatrixGFX.render_text_to_framebuffer("--", 17, 0);
        fLedmatrixGFX.draw_num(28, 0, LEDMATRIX_ICON_CELSIUS);

    }

    fLedmatrixGFX.do_transition();
}
