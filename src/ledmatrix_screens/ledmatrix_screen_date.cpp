#include "ledmatrix_screens.h"

void LedmatrixScreens::draw_year() {

    uint8_t display_x_offset = 3u;

    uint16_t year = fClockManager.getYear();
    uint8_t year_thousand = year/1000;
    uint8_t year_hundred = (year/100)%10;
    uint8_t year_tens = (year - year_thousand*1000 - year_hundred*100) / 10;
    uint8_t year_ones = year%10;

    fLedmatrixGFX.clear_framebuffer();
    fLedmatrixGFX.draw_num(display_x_offset, 0, year_thousand);
    fLedmatrixGFX.draw_num(display_x_offset + 6, 0, year_hundred);
    fLedmatrixGFX.draw_num(display_x_offset + 12, 0, year_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 18, 0, year_ones);

    fLedmatrixGFX.do_transition();

}

void LedmatrixScreens::draw_date() {

    uint8_t display_x_offset = 3u;

    fLedmatrixGFX.clear_framebuffer();

    uint8_t month = fClockManager.getMonth();
    uint8_t day = fClockManager.getDay();

    uint8_t month_tens = month/10;
    uint8_t month_ones = month%10;
    uint8_t day_tens = day/10;
    uint8_t day_ones = day%10;

    fLedmatrixGFX.draw_num(display_x_offset, 0, month_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 6, 0, month_ones);
    fLedmatrixGFX.draw_num(display_x_offset + 11, 0, LEDMATRIX_ICON_DOT);
    fLedmatrixGFX.draw_num(display_x_offset + 14, 0, day_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 20, 0, day_ones);
    fLedmatrixGFX.draw_num(display_x_offset + 25, 0, LEDMATRIX_ICON_DOT);

    fLedmatrixGFX.do_transition();

}

void LedmatrixScreens::get_detailed_date_string(char *dest, size_t size) {
    snprintf_P(dest, size, PSTR("%d. %s %d. %s"),
                                    fClockManager.getYear(),
                                    fClockManager.getMonthName().c_str(),
                                    fClockManager.getDay(),
                                    fClockManager.getDayOfWeekName().c_str()
                                );
}
