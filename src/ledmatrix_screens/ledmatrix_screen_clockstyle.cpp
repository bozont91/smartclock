#include "ledmatrix_screens.h"

#define DELAY_CYCLES_BLINK_CLOCK 900u
#define DELAY_CYCLES_BLINK_ARROWS 400u

void LedmatrixScreens::draw_clockstyle_select_screen() {
    draw_clock(fLedmatrixGFX.LEDMATRIX_DO_TRANSITION);
    clockstyle_select_blink_delay_cycles = DELAY_CYCLES_BLINK_CLOCK / 3;
    clockstyle_select_blink_state = true;
}

void LedmatrixScreens::update_clockstyle_select_screen() {
    draw_clock(fLedmatrixGFX.LEDMATRIX_NO_TRANSITION);
    clockstyle_select_blink_delay_cycles = DELAY_CYCLES_BLINK_CLOCK;
    clockstyle_select_blink_state = true;
}

void LedmatrixScreens::blink_clockstyle_select_screen() {
    if(clockstyle_select_blink_delay_cycles != 0) {
        clockstyle_select_blink_delay_cycles--;
        return;
    }

    if(clockstyle_select_blink_state) {
        clockstyle_select_blink_delay_cycles = DELAY_CYCLES_BLINK_ARROWS;
        fLedmatrixGFX.clear_framebuffer();
        uint8_t clock_style = get_clock_style();
        if(clock_style != 0) fLedmatrixGFX.draw_num(0,0,LEDMATRIX_ICON_LEFT_ARROW);
        if(clock_style != CLOCK_STYLE_MAX - 1) fLedmatrixGFX.draw_num(24,0,LEDMATRIX_ICON_RIGHT_ARROW);
        fLedmatrixGFX.render_framebuffer_to_screen();
        clockstyle_select_blink_state = false;
    } else {
        clockstyle_select_blink_delay_cycles = DELAY_CYCLES_BLINK_CLOCK;
        update_clockstyle_select_screen();
        clockstyle_select_blink_state = true;
    }
}

void LedmatrixScreens::calculate_clockstyle_setting_from_encoder(uint8_t enc_direction) {
    if(enc_direction == RotaryencoderHAL::RE_CLOCKWISE) {
        uint8_t clockstyle_new = get_clock_style() + 1;
        if(clockstyle_new < CLOCK_STYLE_MAX) {
            set_clock_style(clockstyle_new);
            update_clockstyle_select_screen();
        } else {
            fBeep.beep_short();
        }
    } else if(enc_direction == RotaryencoderHAL::RE_COUNTERCLOCKWISE) {
        uint8_t clockstyle_new = get_clock_style() - 1;
        if(clockstyle_new < CLOCK_STYLE_MAX) {
            set_clock_style(clockstyle_new);
            update_clockstyle_select_screen();
        } else {
            fBeep.beep_short();
        }
    }
}

void LedmatrixScreens::handle_clockstyle_selection() {
    uint8_t current_clockstyle = fSettings.read_byte_s(SETT_CLOCK_STYLE);
    uint8_t selected_clockstyle = get_clock_style();

    if(current_clockstyle != selected_clockstyle) {
        fSettings.write(SETT_CLOCK_STYLE, (uint8_t*)&selected_clockstyle);
        fLog.log(LOG_LEDMATRIX, L_INFO, F("Changing clock style to: %d"), selected_clockstyle);
    }

    fBeep.play_acknowledge();
}
