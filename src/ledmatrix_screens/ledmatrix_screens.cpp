#include "ledmatrix_screens.h"


LedmatrixScreens::LedmatrixScreens(
    ClockManager &clockmanager,
    LedmatrixGFX &ledmatrixgfx,
    AlarmManager &alarmmanager,
    LedmatrixHal &ledmatrixhal,
    DeviceClock &deviceclock,
    TemperatureManager &tempmanager,
    Webweather &webweather,
    Settings &settings,
    Beep &beep,
    MuteManager &mutemanager,
    Logger &log
):
fClockManager(clockmanager),
fLedmatrixGFX(ledmatrixgfx),
fAlarmManager(alarmmanager),
fLedmatrixHal(ledmatrixhal),
fDeviceClock(deviceclock),
fTemperatureManager(tempmanager),
fWebWeather(webweather),
fSettings(settings),
fBeep(beep),
fMuteManager(mutemanager),
fLog(log)
{
    fTemperatureManager.subscribe(this);
    fWebWeather.subscribe(this);
}

void LedmatrixScreens::read_settings() {
    set_clock_type(fSettings.read_byte_s(SETT_CLOCK_TYPE));
    set_clock_style(fSettings.read_byte_s(SETT_CLOCK_STYLE));
    magic_numbers_set_enabled(fSettings.read_byte_s(SETT_MAGIC_NUMBERS_ENABLED));
}
