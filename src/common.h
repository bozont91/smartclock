#ifndef COMMON_H
#define COMMON_H

/* Software version */
#define SOFTWARE_VERSION "v1.2"
#define SOFTWARE_RELEASE_TYPE "main"
#define SOFTWARE_BUILD_DATE __DATE__ " " __TIME__

#ifndef SOFTWARE_COMMIT_ID
#define SOFTWARE_COMMIT_ID "n/a"
#endif

/* Pin mapping */
#define PIN_BUZZER              16u
#define PIN_BUTTON              2u
#define PIN_HW_SPI_CS           15u
#define PIN_HW_SPI_CLK          14u
#define PIN_HW_SPI_MOSI         13u
#define PIN_ROTARYENCODER_DT    12u
#define PIN_ROTARYENCODER_CLK   0u

/* Screen definitions */
#define MAX7219_LEDMATRIX_ROWS              8u
#define MAX7219_LEDMATRIX_COLS              32u
#define MAX7219_LEDMATRIX_MODULES           4u
#define MAX7219_LEDMATRIX_MAX_BRIGHTNESS    6u
#define MAX7219_LEDMATRIX_MIN_BRIGHTNESS    0u

/* OS tasks for normal operation */
#define OS_TASK_2MS                 0u
#define OS_TASK_100MS               1u
#define OS_TASK_200MS               2u
#define OS_TASK_1000MS              3u
#define OS_TASK_NTP                 4u
#define OS_TASK_WEBSERVER           5u
#define OS_TASK_WEBWEATHER          6u
#define OS_TASK_TCPREMOTE           7u
#define OS_TASK_MDNS                8u
#define OS_TASK_MQTT                9u
#define OS_TASK_ADAFRUITIO          10u
#define OS_TASK_OTA                 11u
#define OS_TASK_MAX                 12u

/* OS tasks for WelcomeWizard */
#define OS_TASK_WW_WELCOMEWIZARD    0u
#define OS_TASK_WW_2MS              1u

/* WelcomeWizard WiFi AP */
#define WW_WIFI_AP_SSID "aura SmartClock"
#define WW_WIFI_AP_PASS "supersecret"

/* Sync frequencies */
#define NTP_SYNC_FREQUENCY_HOURS 4u
#define WEATHER_SYNC_FREQUENCY_MINUTES 20u

/* Log level */
#ifndef GLOBAL_LOG_LEVEL
    #define GLOBAL_LOG_LEVEL L_INFO
#endif

/* Ports */
#define TCP_REMOTE_PORT 42069u
#define TCP_LOGGER_PORT 42042u

/* Profiler */
#ifdef PROFILER_ENABLED
    #define TIMER_OVERFLOWS_TO_A_SECOND 2000u
#else
    #define TIMER_OVERFLOWS_TO_A_SECOND 200u
#endif

/* Extern variables */
extern volatile uint32_t device_uptime;
extern volatile uint32_t general_timer;
extern volatile float cpu_current_load;

/* Callbacks */
typedef void (*callback_function)(void);
typedef void (*mqtt_callback_function_p)(char* topic, byte* payload, unsigned int length);

/* Temperature */
enum temperature_units {
    TU_CELSIUS,
    TU_FAHRENHEIT,
    TU_MAX
};

float convert_celsius_to_fahrenheit(float input);

#endif /* COMMON_H */
