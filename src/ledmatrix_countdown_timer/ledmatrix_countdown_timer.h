#ifndef LEDMATRIX_COUNTDOWN_TIMER_H
#define LEDMATRIX_COUNTDOWN_TIMER_H

#include <Arduino.h>
#include "../common.h"
#include "../beep/beep.h"
#include "../ledmatrix_font/ledmatrix_font.h"
#include "../ledmatrix_gfx/ledmatrix_gfx.h"

class LedmatrixCountdownTimer {

public:

    LedmatrixCountdownTimer(Beep &beep, LedmatrixGFX &ledmatrixgfx);

    void set(uint32_t value);
    void cancel();
    void draw();
    void task();

    volatile uint32_t timer_value = 0u;
    volatile uint8_t state = COUNTDOWNTIMER_OFF;

    enum countdown_timer_states_e {
        COUNTDOWNTIMER_OFF,
        COUNTDOWNTIMER_START,
        COUNTDOWNTIMER_RUN,
        COUNTDOWNTIMER_FINISHED
    };

private:

    static const uint8_t WAIT_CYCLES_BEFORE_START = 20u;

    Beep &fBeep;
    LedmatrixGFX &fLedmatrixGFX;

};

#endif /* LEDMATRIX_COUNTDOWN_TIMER_H */
