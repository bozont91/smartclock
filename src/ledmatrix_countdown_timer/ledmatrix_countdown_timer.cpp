#include "ledmatrix_countdown_timer.h"


LedmatrixCountdownTimer::LedmatrixCountdownTimer(Beep &beep, LedmatrixGFX &ledmatrixgfx) :
fBeep(beep),
fLedmatrixGFX(ledmatrixgfx)
{
    ;
}

void LedmatrixCountdownTimer::set(uint32_t value) {
    /* The value is processed in the timer ISR so we have to multiply it by TIMER_OVERFLOWS_TO_A_SECOND */
    timer_value = value * TIMER_OVERFLOWS_TO_A_SECOND;
    state = COUNTDOWNTIMER_START;
}

void LedmatrixCountdownTimer::cancel() {
    state = COUNTDOWNTIMER_OFF;
}

void LedmatrixCountdownTimer::draw() {

    uint32_t countdown_timer_seconds = timer_value / TIMER_OVERFLOWS_TO_A_SECOND;

    uint8_t minute_tens = countdown_timer_seconds / 60 / 10;
    uint8_t minute_ones = countdown_timer_seconds / 60 % 10;
    uint8_t second_tens = countdown_timer_seconds % 60 / 10;
    uint8_t second_ones = countdown_timer_seconds % 60 % 10;

    uint8_t display_x_offset = 3u;

    fLedmatrixGFX.clear_framebuffer();

    fLedmatrixGFX.draw_num(display_x_offset, 0, minute_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 6, 0, minute_ones);
    fLedmatrixGFX.draw_num(display_x_offset + 11, 0, LEDMATRIX_ICON_COLON);
    fLedmatrixGFX.draw_num(display_x_offset + 14, 0, second_tens);
    fLedmatrixGFX.draw_num(display_x_offset + 20, 0, second_ones);
}

void LedmatrixCountdownTimer::task() {

    switch(state) {
        case COUNTDOWNTIMER_OFF:
            return;
            break;

        case COUNTDOWNTIMER_START:
            /* A little cosmetic stuff here */
            /* We wait a little before starting to count down */
            /* so that the user can see the value they set the timer to */
            static uint32_t wait_cycles_before_start = WAIT_CYCLES_BEFORE_START;
            if(wait_cycles_before_start > 0) {
                wait_cycles_before_start--;
                return;
            }
            wait_cycles_before_start = WAIT_CYCLES_BEFORE_START;
            state = COUNTDOWNTIMER_RUN;
            break;

        case COUNTDOWNTIMER_RUN:

            static uint32_t countdown_timer_value_prev = 0u;

            if(timer_value == 0u) {
                fBeep.enable_alarm();
                state = COUNTDOWNTIMER_FINISHED;
                countdown_timer_value_prev = 0u;
                return;
            }

            if(timer_value / TIMER_OVERFLOWS_TO_A_SECOND != countdown_timer_value_prev / TIMER_OVERFLOWS_TO_A_SECOND) {
                countdown_timer_value_prev = timer_value;
                this->draw();
                fLedmatrixGFX.render_framebuffer_to_screen();
            }

            break;

        case COUNTDOWNTIMER_FINISHED:

            static bool blink_state = false;
            static uint32_t blink_cnt = 0u;

            if(blink_cnt > 0) {
                blink_cnt--;
                return;
            }

            /* 1 second between blinks */
            blink_cnt = 50u;

            if(blink_state == false) {
                fLedmatrixGFX.clear_framebuffer();
                fLedmatrixGFX.render_framebuffer_to_screen();
                blink_state = true;
            } else {
                fLedmatrixGFX.clear_framebuffer();
                this->draw();
                fLedmatrixGFX.render_framebuffer_to_screen();
                blink_state = false;
            }

            break;

    }

}
