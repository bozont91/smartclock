#ifndef DEVICECLOCK_H
#define DEVICECLOCK_H

#include <Arduino.h>
#include <time.h>

class DeviceClock {

public:
    DeviceClock();
    uint32_t get();
    void set(uint32_t);
    void add(uint32_t amount);
    void subtract(uint32_t amount);
    void change(int32_t amount);
    void IRAM_ATTR increment();

private:
    time_t device_main_clock = 0u;

};

#endif /* DEVICECLOCK_H */
