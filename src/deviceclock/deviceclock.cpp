#include "deviceclock.h"

DeviceClock::DeviceClock() {
    ;
}

void DeviceClock::set(uint32_t device_clock_new) {
    device_main_clock = device_clock_new;
}

void IRAM_ATTR DeviceClock::increment() {
    device_main_clock++;
}

uint32_t DeviceClock::get() {
    return device_main_clock;
}

void DeviceClock::add(uint32_t amount) {
    device_main_clock += amount;
}

void DeviceClock::subtract(uint32_t amount) {
    device_main_clock -= amount;
}

void DeviceClock::change(int32_t amount) {
    device_main_clock += amount;
}
