#include "mdns.h"

mDnsManager::mDnsManager(Logger &log) : fLog(log) {
    ;
}

void mDnsManager::task() {

    switch(mdnsmanager_state) {
        case MDNS_START:
            if(MDNS.begin("aura")) {
                fLog.log(LOG_MDNS, L_INFO, F("Service started successfully"));
                MDNS.addService("http", "tcp", 80);
                MDNS.addService("aura_remote", "tcp", TCP_REMOTE_PORT);
                MDNS.update();
                mdnsmanager_state = MDNS_RUNNING;
            } else {
                fLog.log(LOG_MDNS, L_ERROR, F("Failed to start service"));
                mdnsmanager_state = MDNS_FAILED;
            }
            break;

        case MDNS_RUNNING:
            if(!MDNS.update()) {
                mdnsmanager_state = MDNS_FAILED;
                fLog.log(LOG_MDNS, L_ERROR, F("Update failed"));
            }
            break;

        case MDNS_FAILED:
            fLog.log(LOG_MDNS, L_INFO, F("Restarting service"));
            mdnsmanager_state = MDNS_START;
            break;

    }

}
