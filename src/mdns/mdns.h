#ifndef MDNSMANAGER_H
#define MDNSMANAGER_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include "../common.h"
#include "../log/log.h"

class mDnsManager {

public:
    mDnsManager(Logger &log);
    void task();

private:
    enum mdnsman_states_e {
        MDNS_START,
        MDNS_RUNNING,
        MDNS_FAILED
    };

    uint8_t mdnsmanager_state = MDNS_START;

    Logger &fLog;

};

#endif /* MDNSMANAGER_H */
