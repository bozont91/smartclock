#include "ledmatrix_hal.h"

LedmatrixHal::LedmatrixHal(Logger &log, Settings &settings) :
fLog(log),
fSettings(settings)
{
    ledmatrix = MAX7219_Driver(PIN_HW_SPI_MOSI, PIN_HW_SPI_CLK, PIN_HW_SPI_CS, LEDMATRIX_MODULES);
    clear_onscreen_framebuffer();
}

void LedmatrixHal::read_settings() {
    set_brightness(fSettings.read_byte_s(SETT_BRIGHTNESS));
}

bool LedmatrixHal::get_on_state() {
    return display_on;
}

void LedmatrixHal::startup() {
    uint8_t devices = ledmatrix.getDeviceCount();
    for(uint8_t address = 0 ; address < devices ; address++) {
        ledmatrix.shutdown(address, false);
        ledmatrix.clearDisplay(address);
    }
    set_brightness(ledmatrix_brightness);
    clear_onscreen_framebuffer();
    fLog.log(LOG_LEDMATRIX, L_INFO, F("Display ON"));
    display_on = true;
}

void LedmatrixHal::shutdown() {
    uint8_t devices = ledmatrix.getDeviceCount();
    for(uint8_t address = 0 ; address < devices ; address++) {
        ledmatrix.shutdown(address, true);
    }
    fLog.log(LOG_LEDMATRIX, L_INFO, F("Display OFF"));
    display_on = false;
}

void LedmatrixHal::clear_onscreen_framebuffer() {
    memset(ledmatrix_framebuffer_onscreen, false, (LEDMATRIX_COLS * LEDMATRIX_ROWS));
}

void LedmatrixHal::set_pixel(uint8_t x, uint8_t y, bool value) {

    if(x >= LEDMATRIX_COLS || y >= LEDMATRIX_ROWS) return;

    if(display_inverted) value = !value;

    /* Only render the pixel if there's a change */
    if(ledmatrix_framebuffer_onscreen[x][y] != value) {
        uint8_t module_number = x/8;
        uint8_t x_inside_module = x%8;
        ledmatrix.setLed(module_number, 7-y, 7-x_inside_module, value);
        ledmatrix_framebuffer_onscreen[x][y] = value;
    }

}

void LedmatrixHal::invert_pixel(uint8_t x, uint8_t y) {
    set_pixel(x, y, !ledmatrix_framebuffer_onscreen[x][y]);
}

uint8_t LedmatrixHal::get_brightness() {
    return ledmatrix_brightness;
}


void LedmatrixHal::set_brightness(uint8_t requested_brightness) {

    if(requested_brightness > LEDMATRIX_MAX_BRIGHTNESS) return;
    ledmatrix_brightness = requested_brightness;

    uint8_t devices = ledmatrix.getDeviceCount();
    for(uint8_t address = 0 ; address < devices ; address++) {
        ledmatrix.setIntensity(address, requested_brightness);
    }

    fLog.log(LOG_LEDMATRIX, L_INFO, F("Setting brightness: %d"), ledmatrix_brightness);

}


void LedmatrixHal::increase_brightness() {
    set_brightness(ledmatrix_brightness+1);
}


void LedmatrixHal::decrease_brightness() {
    set_brightness(ledmatrix_brightness-1);
}

bool LedmatrixHal::get_display_inverted() {
    return display_inverted;
}

void LedmatrixHal::invert_display() {

    fLog.log(LOG_LEDMATRIX, L_INFO, F("Invert display"));
    for(uint8_t i = 0 ; i < LEDMATRIX_COLS ; i++) {
        for(uint8_t j = 0 ; j < LEDMATRIX_ROWS ; j++) {
            set_pixel(i, j, !ledmatrix_framebuffer_onscreen[i][j]);
        }
    }

    display_inverted = true;
}

void LedmatrixHal::deinvert_display() {

    display_inverted = false;

    fLog.log(LOG_LEDMATRIX, L_INFO, F("Deinvert display"));
    for(uint8_t i = 0 ; i < LEDMATRIX_COLS ; i++) {
        for(uint8_t j = 0 ; j < LEDMATRIX_ROWS ; j++) {
            set_pixel(i, j, !ledmatrix_framebuffer_onscreen[i][j]);
        }
    }
}
