#include "max7219_driver.h"

MAX7219_Driver::MAX7219_Driver(uint8_t dataPin, uint8_t clkPin, uint8_t csPin, uint8_t numDevices) :
    SPI_MOSI(dataPin),
    SPI_CLK(clkPin),
    SPI_CS(csPin),
    maxDevices(numDevices)
{

    /* Restrict the maximum number of devices to 8 */
    if(maxDevices > 8) {
        maxDevices = 8;
    }

    /* Initialize the SPI module */
    pinMode(SPI_CS, OUTPUT);
    digitalWrite(SPI_CS, HIGH);
    SPI.begin();
    SPI.setFrequency(SPI_CLOCK_FREQUENCY_HZ);

    /* Clear the status buffer */
    memset(status, 0u, 64u);

    /* Initialize all modules */
    for(int i = 0 ; i < maxDevices ; i++) {
        spiTransfer(i, OP_DISPLAYTEST, 0);
        /* Scanlimit is set to max on startup */
        setScanLimit(i, 7);
        /* Decode is done in source */
        spiTransfer(i, OP_DECODEMODE, 0);
        clearDisplay(i);
        /* We go into shutdown-mode on startup */
        shutdown(i, true);
    }
}

MAX7219_Driver::MAX7219_Driver() {

}

uint8_t MAX7219_Driver::getDeviceCount() {
    return maxDevices;
}

void MAX7219_Driver::shutdown(uint8_t addr, bool shutdown_state) {
    if(addr >= maxDevices) return;

    if(shutdown_state) {
        spiTransfer(addr, OP_SHUTDOWN, 0);
    } else {
        spiTransfer(addr, OP_SHUTDOWN, 1);
    }
}

void MAX7219_Driver::setScanLimit(uint8_t addr, uint8_t limit) {
    if(addr >= maxDevices) return;

    if(limit < 8) {
        spiTransfer(addr, OP_SCANLIMIT, limit);
    }
}

void MAX7219_Driver::setIntensity(uint8_t addr, uint8_t intensity) {
    if(addr >= maxDevices) return;

    if(intensity < 16) {
        spiTransfer(addr, OP_INTENSITY,intensity);
    }
}

void MAX7219_Driver::clearDisplay(uint8_t addr) {
    if(addr >= maxDevices) return;

    uint8_t offset = addr * 8;
    for(uint8_t i = 0 ; i < 8 ; i++) {
        status[offset+i] = 0;
        spiTransfer(addr, i+1, status[offset+i]);
    }
}

void MAX7219_Driver::setLed(uint8_t addr, uint8_t row, uint8_t column, bool state) {

    if(addr >= maxDevices) return;
    if(row > 7 || column > 7) return;

    uint8_t offset = addr * 8;
    uint8_t val =  B10000000 >> column;

    if(state) {
        status[offset+row] |= val;
    } else {
        status[offset+row] &= ~val;
    }

    spiTransfer(addr, row+1, status[offset+row]);
}

void MAX7219_Driver::spiTransfer(uint8_t addr, volatile uint8_t opcode, volatile uint8_t data) {
    /* Create an array with the data to shift out */
    uint8_t offset = addr * 2;
    uint8_t maxbytes = maxDevices * 2;

    /* Clear the spi_output_buffer */
    memset(spi_output_buffer, 0u, maxbytes);

    /* Put our device data into the array */
    spi_output_buffer[offset + 1] = opcode;
    spi_output_buffer[offset] = data;

    /* Enable the SPI bus */
    digitalWrite(SPI_CS, LOW);

    /* Write the data to the SPI bus */
    for(uint8_t i = maxbytes ; i > 0 ; i--) {
        SPI.transfer(spi_output_buffer[i-1]);
    }

    /* Disable the SPI bus */
    digitalWrite(SPI_CS, HIGH);
}
