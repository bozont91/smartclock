#ifndef LEDMATRIX_HAL_H
#define LEDMATRIX_HAL_H

#include <Arduino.h>
#include "max7219_driver.h"
#include "../log/log.h"
#include "../settings/settings.h"

class LedmatrixHal {

public:

    LedmatrixHal(Logger &log, Settings &settings);
    void startup();
    void shutdown();
    bool get_on_state();
    uint8_t get_brightness();
    void set_brightness(uint8_t requested_brightness);
    void increase_brightness();
    void decrease_brightness();
    void set_pixel(uint8_t x, uint8_t y, bool value);
    void invert_pixel(uint8_t x, uint8_t y);
    void invert_display();
    void deinvert_display();
    bool get_display_inverted();
    void read_settings();

private:

    uint8_t LEDMATRIX_ROWS = MAX7219_LEDMATRIX_ROWS;
    uint8_t LEDMATRIX_COLS = MAX7219_LEDMATRIX_COLS;
    uint8_t LEDMATRIX_MODULES = MAX7219_LEDMATRIX_MODULES;
    uint8_t LEDMATRIX_MAX_BRIGHTNESS = MAX7219_LEDMATRIX_MAX_BRIGHTNESS;

    MAX7219_Driver ledmatrix;
    bool display_on = false;
    uint8_t ledmatrix_brightness = 1u;
    uint8_t ledmatrix_framebuffer_onscreen[MAX7219_LEDMATRIX_COLS][MAX7219_LEDMATRIX_ROWS];
    bool display_inverted = false;

    void clear_onscreen_framebuffer();

    Logger &fLog;
    Settings &fSettings;

};


#endif /* LEDMATRIX_HAL_H */
