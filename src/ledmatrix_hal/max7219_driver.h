/* aura SmartClock*/
/* MAX7219 Driver*/
/* This code is based on: https://github.com/wayoda/LedControl */
/* The original repository is not maintained anymore. */
/* This is a heavily modified/optimized version of the original */


#ifndef MAX7219_DRIVER_H
#define MAX7219_DRIVER_H

#include <Arduino.h>
#include <SPI.h>


class MAX7219_Driver {
    private :

        #define OP_NOOP   0
        #define OP_DIGIT0 1
        #define OP_DIGIT1 2
        #define OP_DIGIT2 3
        #define OP_DIGIT3 4
        #define OP_DIGIT4 5
        #define OP_DIGIT5 6
        #define OP_DIGIT6 7
        #define OP_DIGIT7 8
        #define OP_DECODEMODE  9
        #define OP_INTENSITY   10
        #define OP_SCANLIMIT   11
        #define OP_SHUTDOWN    12
        #define OP_DISPLAYTEST 15

        /* 10 MHz */
        #define SPI_CLOCK_FREQUENCY_HZ 10000000u

        uint8_t spi_output_buffer[16];
        void spiTransfer(uint8_t addr, uint8_t opcode, uint8_t data);

        /* We keep track of the led-status for all 8 devices in this array */
        uint8_t status[64];

        uint8_t SPI_MOSI;
        uint8_t SPI_CLK;
        uint8_t SPI_CS;

        /* The maximum number of devices we use */
        uint8_t maxDevices;

    public:

        MAX7219_Driver(uint8_t dataPin, uint8_t clkPin, uint8_t csPin, uint8_t numDevices = 1);
        MAX7219_Driver();

        uint8_t getDeviceCount();
        void shutdown(uint8_t addr, bool shutdown_state);
        void setScanLimit(uint8_t addr, uint8_t limit);
        void setIntensity(uint8_t addr, uint8_t intensity);
        void clearDisplay(uint8_t addr);
        void setLed(uint8_t addr, uint8_t row, uint8_t col, bool state);

};

#endif /* MAX7219_DRIVER_H */
