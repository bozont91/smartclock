#ifndef CLOCKMANAGER_H
#define CLOCKMANAGER_H

#include "../deviceclock/deviceclock.h"
#include "../log/log.h"
#include "../beep/beep.h"
#include "../settings/settings.h"

class ClockManager {

public:
    ClockManager(DeviceClock &deviceclock, Logger &log, Beep &beep, Settings &settings);
    void read_settings();
    void changeTimezone(int8_t timezone_id);
    void adjustToTimezone();
    int8_t getTimezone();
    void ntpPostSyncCorrectForTimezoneAndDst();
    uint16_t getYear();
    uint8_t getMonth();
    uint8_t getDay();
    uint8_t getHour();
    uint8_t getMinute();
    uint8_t getSecond();
    uint8_t getDayOfWeek();
    String getMonthName();
    String getDayOfWeekName();
    bool isWeekend();
    void calculateNextDstSwitchTime();
    void adjustTimeForDst();
    void hourlyBeepSetEnabledState(bool state);
    bool hourlyBeepGetEnabled();
    bool isClockSynchronizedToNtp();
    void setClockSynchronizedToNtp();
    void setDstEnabled(bool state);
    bool getDstEnabled();
    void task();

    uint32_t device_startup_time = 0u;

    struct timezones {
        enum timezones_e {
            UTC_MINUS_12 = -12,
            UTC_MINUS_11,
            UTC_MINUS_10,
            UTC_MINUS_9,
            UTC_MINUS_8,
            UTC_MINUS_7,
            UTC_MINUS_6,
            UTC_MINUS_5,
            UTC_MINUS_4,
            UTC_MINUS_3,
            UTC_MINUS_2,
            UTC_MINUS_1,
            UTC_PLUS_0,
            UTC_PLUS_1,
            UTC_PLUS_2,
            UTC_PLUS_3,
            UTC_PLUS_4,
            UTC_PLUS_5,
            UTC_PLUS_6,
            UTC_PLUS_7,
            UTC_PLUS_8,
            UTC_PLUS_9,
            UTC_PLUS_10,
            UTC_PLUS_11,
            UTC_PLUS_12,
        };
    };

    struct days_of_week {
        enum days_of_week_e {
            SUNDAY,
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY
        };
    };

private:
    struct tm ts;
    int8_t timezone = timezones::UTC_PLUS_0;
    time_t getDstStartOrEndTimestampForYear(uint32_t year, bool forDstStart);
    time_t dst_start_timestamp = 0u;
    time_t dst_end_timestamp = 0u;
    time_t next_dst_change_timestamp = UINT32_MAX;
    bool hourly_beep_enabled = true;
    void inline refresh_time_struct();
    bool clock_synchronized_to_ntp = false;
    bool clock_adjusted_to_dst = false;
    bool dst_enabled = true;

    DeviceClock &fDeviceClock;
    Logger &fLog;
    Beep &fBeep;
    Settings &fSettings;

};

#endif /* CLOCKMANAGER_H */
