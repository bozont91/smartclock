#include "clockmanager.h"

ClockManager::ClockManager(DeviceClock &deviceclock, Logger &log, Beep &beep, Settings &settings) :
fDeviceClock(deviceclock),
fLog(log),
fBeep(beep),
fSettings(settings)
{
    memset(&ts, 0x00, sizeof(tm));
}

void ClockManager::read_settings() {
    changeTimezone(fSettings.read_byte_s(SETT_TIMEZONE));
    hourlyBeepSetEnabledState((bool)fSettings.read_byte_s(SETT_HOURLY_BEEP));
    setDstEnabled(fSettings.read_byte_s(SETT_DST_ENABLED));
}

void inline ClockManager::refresh_time_struct() {
    time_t device_main_clock = fDeviceClock.get();
    ts = *localtime(&device_main_clock);
}

void ClockManager::ntpPostSyncCorrectForTimezoneAndDst() {
    if(dst_enabled) {
        clock_adjusted_to_dst = false;
        adjustTimeForDst();
    }
    adjustToTimezone();
}

void ClockManager::changeTimezone(int8_t timezone_id) {

    /* Go back to UTC+0 */
    fDeviceClock.change((int32_t)timezone * 3600 * -1);

    /* Apply the new timezone */
    timezone = timezone_id;
    fDeviceClock.change((int32_t)timezone*3600);

    if(timezone_id >= 0) {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Setting timezone: UTC+%d"), timezone_id);
    } else {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Setting timezone: UTC%d"), timezone_id);
    }
}

void ClockManager::adjustToTimezone() {

    fDeviceClock.change((int32_t)timezone*3600);

    if(timezone >= 0) {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Adjusting to timezone: UTC+%d"), timezone);
    } else {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Adjusting to timezone: UTC%d"), timezone);
    }
}

int8_t ClockManager::getTimezone() {
    return timezone;
}

uint16_t ClockManager::getYear() {
    refresh_time_struct();
    /* tm_year is the years since 1900 */
    return ts.tm_year+1900;
}

uint8_t ClockManager::getMonth() {
    refresh_time_struct();
    /* tm_mon is the months since january, eg. january is 0 */
    return ts.tm_mon+1;
}

uint8_t ClockManager::getDay() {
    refresh_time_struct();
    return ts.tm_mday;
}

uint8_t ClockManager::getHour() {
    refresh_time_struct();
    return ts.tm_hour;
}

uint8_t ClockManager::getMinute() {
    refresh_time_struct();
    return ts.tm_min;
}

uint8_t ClockManager::getSecond() {
    refresh_time_struct();
    return ts.tm_sec;
}

uint8_t ClockManager::getDayOfWeek() {
    refresh_time_struct();
    return ts.tm_wday;
}

String ClockManager::getMonthName() {
    refresh_time_struct();
    char buf[15];
    strftime(buf, sizeof(buf), "%B", &ts);
    return String(buf);
}

String ClockManager::getDayOfWeekName() {
    refresh_time_struct();
    char buf[15];
    strftime(buf, sizeof(buf), "%A", &ts);
    return String(buf);
}

bool ClockManager::isWeekend() {
    uint8_t day_of_week = getDayOfWeek();
    return (day_of_week == days_of_week::SATURDAY || day_of_week == days_of_week::SUNDAY) ? true : false;
}

time_t ClockManager::getDstStartOrEndTimestampForYear(uint32_t year, bool forDstStart) {
    /* For EU */
    /* DST begins - Last Sunday of March */
    /* DST ends - Last Sunday of October */

    uint8_t dst_switch_month = 0u;
    uint8_t dst_switch_hour = 0u;
    if(forDstStart) {
        /* Set the month to March */
        dst_switch_month = 2;
        /* DST starts at 2AM, when we set the clock fowrard to 3AM */
        dst_switch_hour = 2;
    } else {
        /* Set the month to October */
        dst_switch_month = 9;
        /* DST ends at 3AM, when we set the clock back to 2AM */
        dst_switch_hour = 3;
    }

    time_t switch_of_dst_unix_time;
    struct tm ts_dst;
    struct tm ts_switch_of_dst;

    ts_dst.tm_year = year-1900;
    ts_dst.tm_mon = dst_switch_month;
    ts_dst.tm_mday = 31;
    ts_dst.tm_hour = dst_switch_hour;
    ts_dst.tm_min = 0;
    ts_dst.tm_sec = 0;

    uint8_t day_of_month = 31;
    uint8_t day_of_week = 6;

    /* Convert the time structure to unix time */
    switch_of_dst_unix_time = mktime(&ts_dst);
    /* Convert the specified unix time back to time structure */
    ts_switch_of_dst = *localtime(&switch_of_dst_unix_time);
    /* Get the current day of week for the specified date */
    day_of_week = ts_switch_of_dst.tm_wday;

    /* Decrease day and loop till we find Sunday */
    while(day_of_week != 0) {
        day_of_month--;
        ts_dst.tm_mday = day_of_month;
        switch_of_dst_unix_time = mktime(&ts_dst);
        ts_switch_of_dst = *localtime(&switch_of_dst_unix_time);
        day_of_week = ts_switch_of_dst.tm_wday;
    }

    return switch_of_dst_unix_time;
}

void ClockManager::calculateNextDstSwitchTime() {

    uint16_t year = getYear();
    dst_start_timestamp = getDstStartOrEndTimestampForYear(year, true);
    dst_end_timestamp = getDstStartOrEndTimestampForYear(year, false);

    /* If we're past the DST switch in October, we have to use the next year's timestamps */
    if(fDeviceClock.get() >= (uint32_t)dst_end_timestamp) {
        year++;
        dst_start_timestamp = getDstStartOrEndTimestampForYear(year, true);
        dst_end_timestamp = getDstStartOrEndTimestampForYear(year, false);
    }

    struct tm ts_switch_of_dst;
    ts_switch_of_dst = *localtime(&dst_start_timestamp);
    fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Start of DST - Last Sunday of March: %lu-%02u-%02u"), ts_switch_of_dst.tm_year+1900, ts_switch_of_dst.tm_mon+1, ts_switch_of_dst.tm_mday);
    ts_switch_of_dst = *localtime(&dst_end_timestamp);
    fLog.log(LOG_CLOCKMANAGER, L_INFO, F("End of DST - Last Sunday of October: %lu-%02u-%02u"), ts_switch_of_dst.tm_year+1900, ts_switch_of_dst.tm_mon+1, ts_switch_of_dst.tm_mday);

}

void ClockManager::adjustTimeForDst() {

    /* Calculate the DST switch times if they haven't been set up yet */
    if(dst_start_timestamp == 0 || dst_end_timestamp == 0) calculateNextDstSwitchTime();

    bool daylight_saving_time_active;

    time_t device_main_clock = (time_t)fDeviceClock.get();
    if(device_main_clock >= dst_start_timestamp && device_main_clock <= dst_end_timestamp) {
        daylight_saving_time_active = true;
        next_dst_change_timestamp = dst_end_timestamp;
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("DST is active"));
    } else {
        next_dst_change_timestamp = dst_start_timestamp;
        daylight_saving_time_active = false;
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("DST is not active"));
    }

    if(daylight_saving_time_active && !clock_adjusted_to_dst) {

        clock_adjusted_to_dst = true;
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Adjusting +1 hour for DST"));
        fDeviceClock.add(3600);

    } else if(!daylight_saving_time_active && clock_adjusted_to_dst) {

        clock_adjusted_to_dst = false;
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Adjusting -1 hour for DST"));
        fDeviceClock.subtract(3600);

    }
}

void ClockManager::setDstEnabled(bool state) {
    dst_enabled = state;
    if(dst_enabled) {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Using DST enabled"));
        if(clock_synchronized_to_ntp) {
            adjustTimeForDst();
        }
    } else {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Using DST disabled"));
        if(clock_synchronized_to_ntp && clock_adjusted_to_dst) {
            clock_adjusted_to_dst = false;
            fDeviceClock.subtract(3600);
        }
    }
}

bool ClockManager::getDstEnabled() {
    return dst_enabled;
}

void ClockManager::hourlyBeepSetEnabledState(bool state) {
    hourly_beep_enabled = state;
}

bool ClockManager::hourlyBeepGetEnabled() {
    return hourly_beep_enabled;
}

bool ClockManager::isClockSynchronizedToNtp() {
    return clock_synchronized_to_ntp;
}

void ClockManager::setClockSynchronizedToNtp() {
    clock_synchronized_to_ntp = true;
}

void ClockManager::task() {

    time_t device_main_clock = (time_t)fDeviceClock.get();

    if(dst_enabled && device_main_clock == next_dst_change_timestamp) {
        fLog.log(LOG_CLOCKMANAGER, L_INFO, F("Automatically switching DST"));
        calculateNextDstSwitchTime();
        adjustTimeForDst();
    }

    static time_t last_hourly_beep_timestamp = device_main_clock;

    if(hourly_beep_enabled && (last_hourly_beep_timestamp != device_main_clock) && (device_main_clock % 3600 == 0)) {
        last_hourly_beep_timestamp = device_main_clock;
        fBeep.beep_short();
    }
}
