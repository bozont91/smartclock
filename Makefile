.PHONY: build upload all build_debug all_debug clean tests doc flashdump

hardware_opts_min = esp8266:esp8266:d1_mini_clone:xtal=160
hardware_opts = ${hardware_opts_min},FlashMode=qio,FlashFreq=80
hardware_opts_debug = ${hardware_opts},dbg=Serial,lvl=HWDT

build_extra_flags = --build-property build.extra_flags=
git_commit_id = ${shell git rev-parse --short HEAD}
commit_id_opt = -DSOFTWARE_COMMIT_ID=\"${git_commit_id}\"
set_debug_logvevel_opt = -DGLOBAL_LOG_LEVEL=2

upload_port = /dev/ttyUSB0

dirs:
	mkdir -p build
	mkdir -p tests/build
	mkdir -p doxygen

build: dirs
	cd ..; \
		arduino-cli compile --fqbn ${hardware_opts} smartclock -v --output-dir smartclock/build/smartclock --warnings all ${build_extra_flags}${commit_id_opt}

upload: dirs
	cd ..; \
		arduino-cli upload -p ${upload_port} --fqbn ${hardware_opts} -i smartclock/build/smartclock.bin -v

all: dirs
	cd ..; \
		arduino-cli compile --fqbn ${hardware_opts} smartclock -v --output-dir smartclock/build/smartclock --warnings all ${build_extra_flags}${commit_id_opt} -u -p ${upload_port}

build_debug: dirs
	cd ..; \
		arduino-cli compile --fqbn ${hardware_opts_debug} smartclock -v --output-dir smartclock/build/smartclock --warnings all ${build_extra_flags}"${commit_id_opt} ${set_debug_logvevel_opt}"

all_debug: dirs
	cd ..; \
		arduino-cli compile --fqbn ${hardware_opts_debug} smartclock -v --output-dir smartclock/build/smartclock --warnings all -u -p ${upload_port} ${build_extra_flags}"${commit_id_opt} ${set_debug_logvevel_opt}"

clean:
	rm -rf build
	rm -rf tests/build
	rm -rf doxygen

debug:
	~/.arduino15/packages/esp8266/tools/xtensa-lx106-elf-gcc/3.0.3-gcc10.3-9bcba0b/bin/xtensa-lx106-elf-gdb


compiler_args = g++ -Wall -g -pthread -std=c++17 -DAURA_UNITTEST
test_include_dirs = -I mock/ -I mock/esp8266_core -I googletest/googletest/include/
coverage_flags = -fprofile-arcs -ftest-coverage
test_linked_libraries = -Llib -lauratest -lgtest
compile_command = ${compiler_args} ${test_include_dirs} ${coverage_flags}
testlib_sources =  $(wildcard tests/mock/esp8266_core/*.cpp)
testlib_sources += $(wildcard tests/mock/*.cpp)
testlib_sources += src/crypto/crc32.cpp
testlib_sources += src/json_parser/json_parser.cpp

tests: dirs

# Compile the mocks into a static library
	${compiler_args} -c ${test_include_dirs} ${testlib_sources}; \
	ar rvs libauratest.a WString.o stdlib_noniso.o core_esp8266_noniso.o Arduino.o crc32.o json_parser.o; \
	rm *.o; \
	mv libauratest.a tests/lib/libauratest.a; \

# Build the tests
	cd tests; \
	${compile_command} test_deviceclock.cpp ../src/deviceclock/deviceclock.cpp ${test_linked_libraries} -o build/test_deviceclock

	cd tests; \
	${compile_command} test_aura_protocol.cpp ../src/tcp_remote/aura_protocol.cpp ${test_linked_libraries} -o build/test_aura_protocol

	cd tests; \
	${compile_command} test_weather.cpp ../src/internet_weather/internet_weather.cpp ${test_linked_libraries} -o build/test_weather

	cd tests; \
	${compile_command} test_json_parser.cpp ../src/json_parser/json_parser.cpp ${test_linked_libraries} -o build/test_json_parser

# Run the tests
	./tests/build/test_aura_protocol
	./tests/build/test_deviceclock
	./tests/build/test_weather
	./tests/build/test_json_parser

coverage: tests
	cd tests; \
	gcov internet_weather.cpp; \
	gcov json_parser.cpp; \
	lcov -c --directory . --output-file coverage.info; \
	lcov -r coverage.info '/usr/include/*' -o coverage.info; \
	lcov -r coverage.info '*/googletest/*' -o coverage.info; \
	lcov -r coverage.info '*/mock/*' -o coverage.info; \
	genhtml coverage.info --output-directory coverage_report; \
	rm *.gcov; \
	rm *.gcda; \
	rm *.gcno; \
	rm *.info; \

doc: dirs
	doxygen doxygen.conf

flashdump:
	python3 ~/.arduino15/packages/esp8266/hardware/esp8266/3.0.1/tools/esptool/esptool.py --port /dev/ttyUSB0 --baud 921600 read_flash 0x00000 0x400000 dump.img

cppcheck:
	cppcheck --language=c++ --enable=all smartclock.ino src/*
