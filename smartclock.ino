/**@file smartclock.ino */

/*
  aura SmartClock
  Copyright (C) 2019-2021, Tamas Jozsi
*/

#include <ESP8266WiFi.h>
#include <LwipDhcpServer.h>
#include <TickerScheduler.h>
/* #include <GDBStub.h> */
/* #define PROFILER_ENABLED */

void IRAM_ATTR timer0_ISR();
void IRAM_ATTR timer1_ISR();
void IRAM_ATTR isr_rotaryencoder_clk();
void IRAM_ATTR isr_button_handler();
void adafruitio_mqtt_callback(char* topic, byte* payload, unsigned int length);
void welcomewizard_sta_connected(WiFiEventSoftAPModeStationConnected sta_info);
inline void os_task_2ms();
inline void os_task_100ms();
inline void os_task_200ms();
inline void os_task_1000ms();

#include "src/common.h"
TickerScheduler osTask(OS_TASK_MAX);

volatile uint32_t device_uptime = 0u;
volatile uint32_t general_timer = 0u;

#include "src/deviceclock/deviceclock.h"
#include "src/clockmanager/clockmanager.h"
#include "src/log/log.h"
#include "src/esp_hal/esp_hal.h"
#include "src/beep/beep_tones.h"
#include "src/beep/beep.h"
#include "src/beep/mutemanager.h"
#include "src/mqtt/mqtt.h"
#include "src/temperature/temperature.h"
#include "src/alarm/alarm.h"
#include "src/ledmatrix_hal/ledmatrix_hal.h"
#include "src/internet_weather/internet_weather.h"
#include "src/ntp/ntp.h"
#include "src/ledmatrix_font/ledmatrix_font.h"
#include "src/ledmatrix_gfx/ledmatrix_gfx.h"
#include "src/ledmatrix_snake/ledmatrix_snake.h"
#include "src/ledmatrix_screens/ledmatrix_screens.h"
#include "src/rotaryencoder/rotaryencoder.h" /* Note: this has to be after the Ledmatrix HAL, as we are reusing the SPI MISO as an input */
#include "src/ledmatrix_countdown_timer/ledmatrix_countdown_timer.h"
#include "src/ledmatrix_menu/ledmatrix_menu.h"
#include "src/ledmatrix_main/ledmatrix_main.h"
#include "src/display_auto_onoff/display_auto_onoff.h"
#include "src/adafruitio/adafruitio.h"
#include "src/settings/settings.h"
#include "src/tcp_remote/aura_protocol.h"
#include "src/tcp_remote/tcp_remote.h"
#include "src/welcomewizard/welcomewizard.h"
#include "src/webserver/webserver.h"
#include "src/ledmatrix_tetris/ledmatrix_tetris.h"
#include "src/buttonhandler/buttonhandler.h"
#include "src/rotaryencoder/encodermanager.h"
#include "src/mdns/mdns.h"
#include "src/ota/ota.h"
#include "src/profiler/profiler.h"
#include "src/wifi_conn_checker/wifi_connection_checker.h"
#include "src/startup_sequencer/startup_sequencer.h"

DeviceClock             fDeviceClock;
Logger                  fLog(fDeviceClock);
EspHal                  fEspHal(fLog);
JsonParser              fJsonParser(fLog);
Settings                fSettings(fEspHal, fLog);
Beep                    fBeep(fSettings, fLog);
mDnsManager             fmDnsManager(fLog);
ClockManager            fClockManager(fDeviceClock, fLog, fBeep, fSettings);
TemperatureManager      fTemperatureManager(fLog, fSettings);
AlarmManager            fAlarmManager(fDeviceClock, fLog, fBeep, fClockManager, fSettings);
MuteManager             fMuteManager(fBeep, fLog, fClockManager, fSettings);
LedmatrixHal            fLedmatrixHal(fLog, fSettings);
Webweather              fWebWeather(fLog, fSettings, fJsonParser);
NtpManager              fNtp(fDeviceClock, fLog, fClockManager);
LedmatrixGFX            fLedmatrixGFX(fLedmatrixHal);
LedmatrixSnake          fSnake(fEspHal, fBeep, fLedmatrixGFX, fLog, fSettings);
LedmatrixTetris         fTetris(fLedmatrixGFX, fBeep, fEspHal, fLog, fSettings);
RotaryencoderHAL        fRotaryencoderHAL(isr_rotaryencoder_clk, fLog);
LedmatrixScreens        fLedmatrixScreens(fClockManager, fLedmatrixGFX, fAlarmManager, fLedmatrixHal, fDeviceClock, fTemperatureManager, fWebWeather, fSettings, fBeep, fMuteManager, fLog);
LedmatrixCountdownTimer fLedmatrixCountdownTimer(fBeep, fLedmatrixGFX);
LedmatrixMenu           fLedmatrixMenu(fRotaryencoderHAL, fLog, fBeep, fLedmatrixGFX, fLedmatrixScreens);
LedmatrixStateManager   fLedmatrixMain(fRotaryencoderHAL, fLedmatrixHal, fLedmatrixGFX, fSnake, fTetris, fLedmatrixScreens, fLedmatrixCountdownTimer, fLedmatrixMenu, fMuteManager, fWebWeather, fSettings);
DisplayAutoOnOff        fDisplayAutoOnOff(fLog, fBeep, fLedmatrixMain, fDeviceClock, fClockManager, fSettings);
MqttManager             fMqttManager(fLog, fDeviceClock, fClockManager, fAlarmManager, fTemperatureManager, fLedmatrixHal, fLedmatrixMain, fBeep, fSettings);
AdafruitIoIntegration   fAdafruitIoIntegration(adafruitio_mqtt_callback, fLog, fBeep, fLedmatrixMain, fSettings);
AuraOta                 fOta(fLog, fSettings);
AuraProtocol            fAuraProtocol(fLog);
Webserver               fWebserver(fLog, fBeep, fNtp, fSettings, fTemperatureManager, fDeviceClock, fLedmatrixHal, fLedmatrixMain);
WelcomeWizard           fWelcomeWizard(fEspHal, fLog, fBeep, fSettings, fLedmatrixMain, fLedmatrixGFX);
ButtonHandler           fButtonHandler(fLog, fBeep, fSettings, fAlarmManager, fLedmatrixHal, fLedmatrixMain, fLedmatrixScreens, fLedmatrixCountdownTimer, fLedmatrixMenu, fTetris, fTemperatureManager, fWebWeather, fClockManager);
EncoderManager          fEncoderManager(fRotaryencoderHAL, fLedmatrixMain, fLedmatrixMenu, fSnake, fTetris, fLedmatrixScreens);
WifiConnectionChecker   fWifiConnectionChecker(osTask, fLog);
TcpRemote               fTcpRemote(fLog, fBeep, fNtp, fSettings, fDeviceClock, fClockManager, fAlarmManager, fLedmatrixHal, fLedmatrixScreens, fSnake, fLedmatrixCountdownTimer, fDisplayAutoOnOff, fLedmatrixMain, fMqttManager, fAdafruitIoIntegration, fTemperatureManager, fTetris, fWebWeather, fOta, fEspHal, fButtonHandler, fMuteManager, fAuraProtocol);
StartupSequencer        fStartupSequencer(osTask, fWifiConnectionChecker, fLog, fNtp, fWebWeather, fClockManager, fLedmatrixMain, fBeep, fTcpRemote, fRotaryencoderHAL, fButtonHandler, fmDnsManager, fWebserver, fMqttManager, fAdafruitIoIntegration, fOta);

void setup() {

    pinMode(PIN_BUTTON, INPUT_PULLUP);

    noInterrupts();

    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), isr_button_handler, FALLING);
    timer1_attachInterrupt(timer1_ISR);
    /* Timer input clock: 80MHz (independent of CPU clock)
        80MHz divided by 16 is 5MHz - timer runs at 5MHz
        Interrupt is at 50.000 cycles which is 10ms, timer counts downwards, interrupts at 0
        Refer to:  Arduino/cores/esp8266/Arduino.h
    */
    timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);
    #ifdef PROFILER_ENABLED
        /* 0.5ms */
        timer1_write(2500);
    #else
        /* 5ms */
        timer1_write(25000);
    #endif

    timer0_write(ESP.getCycleCount()+1);
    fBeep.set_tonegen_isr(timer0_ISR);
    timer0_isr_init();

    profiler_init();

    interrupts();

    Serial.begin(115200);
    /* gdbstub_init(); */
    /* Serial.setDebugOutput(true); */
    fLog.log(LOG_SYSTEM, L_INFO, F("ayy lmao 👽\n"));
    fLog.log(LOG_SYSTEM, L_INFO, F("aura SmartClock"));
    fLog.log(LOG_SYSTEM, L_INFO, F("---------------"));
    fLog.log(LOG_SYSTEM, L_INFO, F("Software version: %s %s (%s)"), SOFTWARE_VERSION, SOFTWARE_RELEASE_TYPE, SOFTWARE_COMMIT_ID);
    fLog.log(LOG_SYSTEM, L_INFO, F("Build date: %s"), SOFTWARE_BUILD_DATE);
    fLog.log(LOG_SYSTEM, L_INFO, F("ESP8266 Core: %s"), ESP.getCoreVersion().c_str());
    fLog.log(LOG_SYSTEM, L_INFO, F("CPU speed: %d MHz"), ESP.getCpuFreqMHz());
    fLog.log(LOG_SYSTEM, L_INFO, F("MAC address: %s"), WiFi.macAddress().c_str());
    fLog.log(LOG_SYSTEM, L_INFO, F("Last reset reason: %s"), ESP.getResetReason().c_str());

    fLedmatrixHal.startup();
    ledmatrix_ascii_table_init();

    fSettings.init();

    if(!fSettings.get_valid()) {

        fLedmatrixMain.welcomewizard_active = true;
        fLedmatrixMain.set_current_screen(fLedmatrixMain.SCREEN_WELCOME);
        fLog.log(LOG_SYSTEM, L_INFO, F("Device is uninitialized. Starting welcome wizard..."));

        fWelcomeWizard.begin();
        osTask.add(OS_TASK_WW_2MS,            2,    [&](void *) { fLedmatrixMain.task(); fBeep.task(); }, nullptr, false);
        osTask.add(OS_TASK_WW_WELCOMEWIZARD,  239,  [&](void *) { fWelcomeWizard.task(); }, nullptr, true);

        WiFi.mode(WIFI_AP_STA);
        WiFi.persistent(false);
        uint8_t softap_dhcp_mode = 0;
        dhcpSoftAP.set_dhcps_offer_option(OFFER_ROUTER, &softap_dhcp_mode);
        fLog.log(LOG_SYSTEM, L_INFO, F("WiFi AP SSID: %s"), WW_WIFI_AP_SSID);

        if(WiFi.softAP(WW_WIFI_AP_SSID, WW_WIFI_AP_PASS)) {
            fLog.log(LOG_SYSTEM, L_INFO, F("WiFi AP ready"));
            char IP[16];
            WiFi.softAPIP().toString().toCharArray(IP, 16);
            fLog.log(LOG_SYSTEM, L_INFO, F("WiFi AP address: %s"), IP);
        } else {
            fLog.log(LOG_SYSTEM, L_ERROR, F("Setting up WiFi AP failed."));
            fEspHal.system_reboot();
        }

        static WiFiEventHandler eventhandler_softap;
        eventhandler_softap = WiFi.onSoftAPModeStationConnected(welcomewizard_sta_connected);

    } else {

        fLedmatrixHal.read_settings();
        fBeep.read_settings();
        fMuteManager.read_settings();
        fClockManager.read_settings();
        fAlarmManager.read_settings();
        fLedmatrixMain.read_settings();
        fLedmatrixScreens.read_settings();
        fDisplayAutoOnOff.read_settings();
        fWebWeather.read_settings();
        fMqttManager.read_settings();
        fAdafruitIoIntegration.read_settings();
        fTemperatureManager.read_settings();
        fSnake.read_settings();
        fTetris.read_settings();
        fButtonHandler.read_settings();

        fTemperatureManager.init();
        fTemperatureManager.task();

        char *wifi_ssid = (char*)malloc(fSettings.get_setting_length(SETT_SSID) * sizeof(char));
        char *wifi_password = (char*)malloc(fSettings.get_setting_length(SETT_WIFI_PASS) * sizeof(char));
        fSettings.read_block(SETT_SSID, (uint8_t *)wifi_ssid);
        fSettings.read_block(SETT_WIFI_PASS, (uint8_t *)wifi_password);

        ESP.wdtFeed();
        fLog.log(LOG_SYSTEM, L_INFO, F("Connecting to WiFi network \"%s\""), wifi_ssid);
        WiFi.persistent(false);
        WiFi.mode(WIFI_STA);
        WiFi.hostname("auraSmartClock");
        WiFi.begin(wifi_ssid, wifi_password);
        memset(wifi_password, 0xFF, fSettings.get_setting_length(SETT_WIFI_PASS));
        free(wifi_ssid);
        free(wifi_password);

        osTask.add(OS_TASK_2MS,               2,    [&](void *) { P_START(OS_TASK_2MS); os_task_2ms(); P_END(); },        nullptr, true);
        osTask.add(OS_TASK_100MS,             97,   [&](void *) { P_START(OS_TASK_100MS); os_task_100ms(); P_END(); },    nullptr, true);
        osTask.add(OS_TASK_200MS,             197,  [&](void *) { P_START(OS_TASK_200MS); os_task_200ms(); P_END(); },    nullptr, true);
        osTask.add(OS_TASK_1000MS,            997,  [&](void *) { P_START(OS_TASK_1000MS); os_task_1000ms(); P_END(); },  nullptr, true);

        osTask.add(OS_TASK_NTP,               151,  [&](void *) { P_START(OS_TASK_NTP); fNtp.task(); P_END(); },                          nullptr, false);
        osTask.add(OS_TASK_WEBSERVER,         239,  [&](void *) { P_START(OS_TASK_WEBSERVER); fWebserver.task(); P_END(); },              nullptr, false);
        osTask.add(OS_TASK_WEBWEATHER,        1993, [&](void *) { P_START(OS_TASK_WEBWEATHER); fWebWeather.task(); P_END(); },            nullptr, false);
        osTask.add(OS_TASK_TCPREMOTE,         29,   [&](void *) { P_START(OS_TASK_TCPREMOTE); fTcpRemote.task(); P_END(); },              nullptr, false);
        osTask.add(OS_TASK_MDNS,              5003, [&](void *) { P_START(OS_TASK_MDNS); fmDnsManager.task(); P_END(); },                 nullptr, false);
        osTask.add(OS_TASK_MQTT,              419,  [&](void *) { P_START(OS_TASK_MQTT); fMqttManager.task(); P_END(); },                 nullptr, false);
        osTask.add(OS_TASK_ADAFRUITIO,        401,  [&](void *) { P_START(OS_TASK_ADAFRUITIO); fAdafruitIoIntegration.task(); P_END(); }, nullptr, false);
        osTask.add(OS_TASK_OTA,               113,  [&](void *) { P_START(OS_TASK_OTA); fOta.task(); P_END(); },                          nullptr, false);

        osTask.disable(OS_TASK_NTP);
        osTask.disable(OS_TASK_WEBSERVER);
        osTask.disable(OS_TASK_WEBWEATHER);
        osTask.disable(OS_TASK_TCPREMOTE);
        osTask.disable(OS_TASK_MDNS);
        osTask.disable(OS_TASK_MQTT);
        osTask.disable(OS_TASK_ADAFRUITIO);
        osTask.disable(OS_TASK_OTA);

    }

    fBeep.start_music(startup_chime, MUSIC_BLOCK_STARTUP_CHIME_SIZE);

}

void loop() {
    osTask.update();
    profiler_loop();
}

inline void os_task_2ms() {
    fLedmatrixMain.task();
    fBeep.task();
}

inline void os_task_100ms() {
    fButtonHandler.task();
    fRotaryencoderHAL.task();
}

inline void os_task_200ms() {
    fClockManager.task();
    fAlarmManager.task();
    fWifiConnectionChecker.task();
    fStartupSequencer.task();
}

inline void os_task_1000ms() {
    fDisplayAutoOnOff.task();
    fMuteManager.task();
    fTemperatureManager.task();
    fSettings.task();
    fLog.task();
}

void IRAM_ATTR timer1_ISR() {
    static uint16_t second_cnt = 0u;
    second_cnt++;
    if(second_cnt == TIMER_OVERFLOWS_TO_A_SECOND) {
        fDeviceClock.increment();
        device_uptime++;
        second_cnt = 0u;
    }

    if(fLedmatrixCountdownTimer.timer_value > 0 && fLedmatrixCountdownTimer.state == fLedmatrixCountdownTimer.COUNTDOWNTIMER_RUN) {
        fLedmatrixCountdownTimer.timer_value--;
    }

    general_timer++;

    if(!profiler_data_lock) profiler_data[profiler_current_task]++;
}

void IRAM_ATTR timer0_ISR() {

    /* Frequency generator for the buzzer */
    if(fBeep.beep_tone_generator_active) {

        static bool buzzer_pin_state = false;

        /* 50% duty cycle */
        if(buzzer_pin_state) {
            digitalWrite(PIN_BUZZER, LOW);
            buzzer_pin_state = false;
        } else {
            digitalWrite(PIN_BUZZER, HIGH);
            buzzer_pin_state = true;
        }
        /* Set the next interrupt to the provided frequency */
        timer0_write(ESP.getCycleCount()+fBeep.beep_tone_generator_timer_value);
    } else {
        timer0_write(ESP.getCycleCount());
    }

}

void IRAM_ATTR isr_rotaryencoder_clk() {
    fRotaryencoderHAL.handle_clk_interrupt();
}

void IRAM_ATTR isr_button_handler() {
    fButtonHandler.button_pressed = true;
}

void adafruitio_mqtt_callback(char* topic, byte* payload, unsigned int length) {
    fAdafruitIoIntegration.handle_data_received(topic, payload, length);
}

void mqtt_callback(char* topic, byte* payload, unsigned int length) {
    fMqttManager.handle_receive(topic, payload, length);
}

void welcomewizard_sta_connected(WiFiEventSoftAPModeStationConnected sta_info) {
    fWelcomeWizard.handle_sta_connected(sta_info);
}

float convert_celsius_to_fahrenheit(float input) {
    return input * 1.8f + 32.0f;
}
