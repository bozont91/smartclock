package com.bozont.aura3.ui.main;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bozont.aura3.MainActivity;
import com.bozont.aura3.R;

import static com.bozont.aura3.MainActivity.mTcpClient;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentAlarm extends Fragment implements View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    static TimePicker tp;
    static Switch sw_alarm_only_on_weekdays;

    public static FragmentAlarm newInstance(int index) {
        FragmentAlarm fragment = new FragmentAlarm();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    class GetAlarmThread implements Runnable {
        @Override
        public void run() {
            MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_TIME, MainActivity.message_type.REQUEST, null);
            MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_ONLY_ON_WEEKDAYS, MainActivity.message_type.REQUEST, null);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_alarm, container, false);

        tp = (TimePicker) root.findViewById(R.id.tp_alarm);
        tp.setIs24HourView(true);

        Button btn_setalarm = root.findViewById(R.id.btn_setalarm);
        btn_setalarm.setOnClickListener(this);

        Button btn_disablealarm = root.findViewById(R.id.btn_disablealarm);
        btn_disablealarm.setOnClickListener(this);

        sw_alarm_only_on_weekdays = root.findViewById(R.id.sw_alarm_only_on_weekdays);
        sw_alarm_only_on_weekdays.setOnClickListener(this);

        new Thread(new GetAlarmThread()).start();

        return root;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_setalarm:
                byte[] payload_at = new byte[2];
                payload_at[0] = (byte)tp.getHour();
                payload_at[1] = (byte)tp.getMinute();
                MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_TIME, MainActivity.message_type.CHANGE_VALUE, payload_at);
                MainActivity.vibrate();
                break;

            case R.id.btn_disablealarm:
                MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_DISABLE, MainActivity.message_type.CHANGE_VALUE, null);
                MainActivity.vibrate();
                break;

            case R.id.sw_alarm_only_on_weekdays:
                boolean h = sw_alarm_only_on_weekdays.isChecked();
                byte[] payload_wd = new byte[1];
                if(h) {
                    payload_wd[0] = 1;
                } else {
                    payload_wd[0] = 0;
                }
                MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_ONLY_ON_WEEKDAYS, MainActivity.message_type.CHANGE_VALUE, payload_wd);
                MainActivity.vibrate();
                break;
        }
    }




    public static void messageReceived(MainActivity.message_ids message_id, byte[] payload) {

        switch(message_id) {
            case ALARM_TIME:

                if(payload.length < 2) {
                    Log.d("aura_com", "Alarm response too small!");
                    return;
                } else if(payload[0] > 23 || payload[1] > 59) {
                    Log.d("aura_com", "Alarm response does not contain correct time information!");
                    return;
                }

                int alarm_hour = payload[0];
                int alarm_minute = payload[1];
                tp.setHour(alarm_hour);
                tp.setMinute(alarm_minute);
                break;

            case ALARM_ONLY_ON_WEEKDAYS:
                int onlyonweekdays = payload[0];
                if(onlyonweekdays == 1) {
                    sw_alarm_only_on_weekdays.setChecked(true);
                } else {
                    sw_alarm_only_on_weekdays.setChecked(false);
                }
                break;
        }

    }
}