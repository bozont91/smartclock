package com.bozont.aura3.ui.main;

import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bozont.aura3.MainActivity;
import com.bozont.aura3.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.bozont.aura3.MainActivity.mTcpClient;

class MainScreenDataHolder {

    enum card_type {
        INFOCARD,
        BUTTONCARD
    };

    public String bigtext = "";
    public String smalltext = "";
    public card_type type = card_type.INFOCARD;
}

public class FragmentMainNew extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static MainScreenDataHolder[] ms_data;
    private static MainScreenAdapter mainscreenadapter;
    CountDownTimer countDownTimerClockRefresh = null;
    CountDownTimer countDownTimerTempRefresh;

    private PageViewModel pageViewModel;
    private static View root;
    private static GridView gridView;
    private static int displayBrightness = 0;

    private static boolean temperature_sensor_present = false;
    public static long current_synced_unix_time = 0;

    public enum card_order {
        CARD_TIME(0),
        CARD_TEMP(1),
        CARD_SHOWTEXT(2),
        CARD_SHOWWEATHER(3),
        CARD_BRIGHTNESS(4),
        CARD_SHOWDATE(5),
        CARD_DISPLAYONOFF(6),
        CARD_STARTTIMER(7);

        private final int value;

        card_order(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateClockTimer();
        updateTemperatureTimer();
        Log.d("aura", "Main fragment started");

        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_BRIGHTNESS, MainActivity.message_type.REQUEST, null);

    }

    @Override
    public void onPause() {
        super.onPause();
        /* countDownTimerClockRefresh.cancel(); */
        countDownTimerTempRefresh.cancel();
        Log.d("aura", "Main fragment paused");
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_main_new, container, false);

        ms_data = new MainScreenDataHolder[card_order.values().length];

        ms_data[card_order.CARD_TIME.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_TIME.value].bigtext = "19:01";
        ms_data[card_order.CARD_TIME.value].smalltext = "1991.01.12";

        ms_data[card_order.CARD_TEMP.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_TEMP.value].bigtext = "-";
        ms_data[card_order.CARD_TEMP.value].smalltext = "Inside temperature";

        ms_data[card_order.CARD_SHOWTEXT.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_SHOWTEXT.value].smalltext = "Show text";
        ms_data[card_order.CARD_SHOWTEXT.value].type = MainScreenDataHolder.card_type.BUTTONCARD;

        ms_data[card_order.CARD_SHOWWEATHER.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_SHOWWEATHER.value].smalltext = "Show weather";
        ms_data[card_order.CARD_SHOWWEATHER.value].type = MainScreenDataHolder.card_type.BUTTONCARD;

        ms_data[card_order.CARD_BRIGHTNESS.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_BRIGHTNESS.value].bigtext = displayBrightness + "";
        ms_data[card_order.CARD_BRIGHTNESS.value].smalltext = "Screen brightness";

        ms_data[card_order.CARD_SHOWDATE.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_SHOWDATE.value].smalltext = "Show date";
        ms_data[card_order.CARD_SHOWDATE.value].type = MainScreenDataHolder.card_type.BUTTONCARD;

        ms_data[card_order.CARD_DISPLAYONOFF.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_DISPLAYONOFF.value].smalltext = "Display on/off";
        ms_data[card_order.CARD_DISPLAYONOFF.value].type = MainScreenDataHolder.card_type.BUTTONCARD;

        ms_data[card_order.CARD_STARTTIMER.value] = new MainScreenDataHolder();
        ms_data[card_order.CARD_STARTTIMER.value].smalltext = "Start timer";
        ms_data[card_order.CARD_STARTTIMER.value].type = MainScreenDataHolder.card_type.BUTTONCARD;


        gridView = root.findViewById(R.id.gridview);
        mainscreenadapter = new MainScreenAdapter(this.getContext(), ms_data);
        gridView.setAdapter(mainscreenadapter);

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, int position, long id) {

                if(position == card_order.CARD_SHOWWEATHER.value || position == card_order.CARD_TEMP.value) {
                    MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_DETAILED_WEATHER, MainActivity.message_type.REQUEST, null);
                    MainActivity.vibrate();
                }

                if(position == card_order.CARD_SHOWDATE.value || position == card_order.CARD_TIME.value) {
                    MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_DETAILED_DATE, MainActivity.message_type.REQUEST, null);
                    MainActivity.vibrate();
                }

                return true;
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                if(position == card_order.CARD_SHOWWEATHER.value) {

                    MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_WEATHER, MainActivity.message_type.REQUEST, null);
                    MainActivity.vibrate();

                } else if(position == card_order.CARD_SHOWDATE.value) {

                    MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_DATE, MainActivity.message_type.REQUEST, null);
                    MainActivity.vibrate();

                } else if(position == card_order.CARD_DISPLAYONOFF.value) {

                    MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_ON_OFF, MainActivity.message_type.REQUEST, null);
                    MainActivity.vibrate();

                } else if(position == card_order.CARD_STARTTIMER.value) {

                    final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
                    dialog.setContentView(R.layout.fragment_set_timer);
                    dialog.setTitle("Select time...");

                    final TimePicker tp_countdowntimer = dialog.findViewById(R.id.tp_timer);
                    tp_countdowntimer.setIs24HourView(true);
                    tp_countdowntimer.setHour(0);
                    tp_countdowntimer.setMinute(0);

                    Button dialogButton = dialog.findViewById(R.id.btn_starttimer);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int countdown_seconds = tp_countdowntimer.getHour()*60+tp_countdowntimer.getMinute();
                            byte[] payload = ByteBuffer.allocate(4).putInt(Integer.reverseBytes(countdown_seconds)).array();
                            MainActivity.sendAuraPacket(MainActivity.message_ids.COUNTDOWNTIMER_START, MainActivity.message_type.REQUEST, payload);
                            MainActivity.vibrate();
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                } else if(position == card_order.CARD_SHOWTEXT.value) {

                    final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
                    dialog.setContentView(R.layout.fragment_showtext_popup);
                    dialog.setTitle("Show text");
                    final EditText et_showtext = dialog.findViewById(R.id.et_showtext);
                    et_showtext.setSelection(et_showtext.getText().length());

                    Button btn_showtext_apply = dialog.findViewById(R.id.btn_showtext_apply);
                    btn_showtext_apply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String text_to_show = et_showtext.getText().toString() + '\0';
                            MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_TEXT, MainActivity.message_type.REQUEST, text_to_show.getBytes());

                            MainActivity.vibrate();
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                } else if(position == card_order.CARD_BRIGHTNESS.value) {

                    final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
                    dialog.setContentView(R.layout.fragment_setbrightness_popup);
                    dialog.setTitle("Set screen brightness");
                    final SeekBar sb_brightness = dialog.findViewById(R.id.seekbar_brightness);
                    sb_brightness.setProgress(displayBrightness);

                    Button btn_setbrightness_apply = dialog.findViewById(R.id.btn_setbrightness_apply);
                    btn_setbrightness_apply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("aura_dbg", "brightness progressbar: " + sb_brightness.getProgress());
                            displayBrightness = sb_brightness.getProgress();
                            byte[] payload = new byte[1];
                            payload[0] = (byte)displayBrightness;
                            MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_BRIGHTNESS, MainActivity.message_type.CHANGE_VALUE, payload);

                            ms_data[4].bigtext = displayBrightness + "";
                            mainscreenadapter.notifyDataSetChanged();
                            MainActivity.vibrate();
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                }
            }
        });


        return root;
    }

    public static void messageReceived(MainActivity.message_ids message_id, byte[] payload) {

        switch (message_id) {
            case DEVICECLOCK:

                long unixSeconds = 0;

                StringBuilder res = new StringBuilder();
                for (int i = 0; i < payload.length; i++) {
                    res.append(String.format("0x%02x ", payload[i]));
                }
                Log.d("aura_com", "Payload time: " + res.toString());
                for (int i = 0; i < payload.length; i++) {
                    unixSeconds += ((long) payload[i] & 0xffL) << (8 * i);
                }

                current_synced_unix_time = unixSeconds;

                Date date = new java.util.Date(unixSeconds * 1000L);
                SimpleDateFormat str_time = new java.text.SimpleDateFormat("HH:mm");
                SimpleDateFormat str_date = new java.text.SimpleDateFormat("yyyy. MM. dd.");
                str_time.setTimeZone(java.util.TimeZone.getTimeZone("GMT+0"));
                String formattedTime = str_time.format(date);
                str_date.setTimeZone(java.util.TimeZone.getTimeZone("GMT+0"));
                String formattedDate = str_date.format(date);

                ms_data[card_order.CARD_TIME.value].bigtext = formattedTime;
                ms_data[card_order.CARD_TIME.value].smalltext = formattedDate;
                mainscreenadapter.notifyDataSetChanged();
                break;

            case TEMPSENSOR_PRESENT:
                int sensor_present_rcv = payload[0] & 0xff;
                if(sensor_present_rcv == 1) {
                    temperature_sensor_present = true;
                } else {
                    temperature_sensor_present = false;
                }
                if(!temperature_sensor_present) {
                    ms_data[card_order.CARD_TEMP.value].bigtext = "-";
                    mainscreenadapter.notifyDataSetChanged();
                }
                break;

            case DEVICETEMP:
                if(!temperature_sensor_present) {
                    return;
                }
                float temperature = ByteBuffer.wrap(payload).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                ms_data[card_order.CARD_TEMP.value].bigtext = (int)Math.floor(temperature)+"°";
                mainscreenadapter.notifyDataSetChanged();
                break;

            case DISPLAY_BRIGHTNESS:
                int brightness_rcv = payload[0] & 0xff;
                ms_data[card_order.CARD_BRIGHTNESS.value].bigtext = brightness_rcv + "";
                displayBrightness = brightness_rcv;
                mainscreenadapter.notifyDataSetChanged();
                break;
        }
    }

    public void updateClockTimer() {
        if(countDownTimerClockRefresh != null) return;

        countDownTimerClockRefresh = new CountDownTimer(Long.MAX_VALUE, 2000) {

            public void onTick(long millisUntilFinished) {
                MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICECLOCK, MainActivity.message_type.REQUEST, null);
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    public void updateTemperatureTimer() {
        countDownTimerTempRefresh = new CountDownTimer(Long.MAX_VALUE, 5000) {

            public void onTick(long millisUntilFinished) {
                if (!temperature_sensor_present) {
                    MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPSENSOR_PRESENT, MainActivity.message_type.REQUEST, null);
                } else {
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICETEMP, MainActivity.message_type.REQUEST, null);
                }
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    public static void doInitialSync() {
        MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICECLOCK, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPSENSOR_PRESENT, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICETEMP, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_BRIGHTNESS, MainActivity.message_type.REQUEST, null);
    }
}
