package com.bozont.aura3.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bozont.aura3.R;

public class MainScreenAdapter extends BaseAdapter {

    private final Context mContext;
    private final MainScreenDataHolder[] data;

    public MainScreenAdapter(Context context, MainScreenDataHolder[] data) {
        this.mContext = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MainScreenDataHolder.card_type current_card_type = data[position].type;

        if (convertView == null) {
            if(current_card_type == MainScreenDataHolder.card_type.INFOCARD) {

                final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.fragment_main_tile, null);

            } else if(current_card_type == MainScreenDataHolder.card_type.BUTTONCARD) {

                final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.fragment_main_tile_simple, null);

            }
        }

        if(current_card_type == MainScreenDataHolder.card_type.INFOCARD) {

            final TextView bigtext = convertView.findViewById(R.id.tw_text_big);
            final TextView smalltext = convertView.findViewById(R.id.tw_text_small);
            try {
                bigtext.setText(data[position].bigtext);
                smalltext.setText(data[position].smalltext);
            }
            catch(Exception e) {
            }

        } else if(current_card_type == MainScreenDataHolder.card_type.BUTTONCARD) {

            final TextView smalltext = convertView.findViewById(R.id.tw_simple_text_small);
            smalltext.setText(data[position].smalltext);

        }

        return convertView;
    }

}