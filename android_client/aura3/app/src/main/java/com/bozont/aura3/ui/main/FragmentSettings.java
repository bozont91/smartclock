package com.bozont.aura3.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bozont.aura3.MainActivity;
import com.bozont.aura3.R;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentSettings extends Fragment implements View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private PageViewModel pageViewModel;
    static TextView tw_clockstyle;
    static int clock_style = 0;
    static ListView lw_clockstyle;
    static Switch sw_autodisplayonoff;
    static Switch sw_disable_hourlybeep;
    static TextView tw_autodisplayoff;
    static TextView tw_autodisplayon;
    static TextView tw_timezone;
    static ListView lw_timezones;
    static ListView lw_tempcal;
    static TextView tw_invert_display;
    static Switch sw_autodisplayonoff_onlyonweekdays;
    static Switch sw_mqtt;
    static TextView tw_factoryreset;
    static TextView tw_tempsensorcalibration;
    static Switch sw_aio;
    static Switch sw_tempunit;
    static Switch sw_webweather;
    static int displayoff_min = 0;
    static int displayoff_hour = 0;
    static int displayon_min = 0;
    static int displayon_hour = 0;
    static List<String> timezoneList;
    static ArrayAdapter<String> timezoneListArrayAdapter;
    static List<String> tempcalList;
    static ArrayAdapter<String> tempcalListArrayAdapter;
    static List<String> clockstyleList;
    static ArrayAdapter<String> clockstyleListArrayAdapter;
    static String timezone;
    static Switch sw_mutedevice;
    static TextView tw_force_ntp_sync;
    static TextView tw_snoozetime;
    static int snooze_time = 0;
    static EditText et_snoozetime;
    static TextView tw_alarmsound;
    static ListView lw_alarmsound;
    static List<String> alarmsoundList;
    static ArrayAdapter<String> alarmsoundListArrayAdapter;
    static int alarm_sound = 0;
    static Switch sw_dst;
    static Switch sw_ota;
    static EditText et_ota_pass;
    static TextView tw_reboot;
    static TextView tw_mqttstate;
    static TextView tw_aiostate;
    static TextView tw_weatherstate;
    static Switch sw_numerology_mode;
    static Switch sw_mutewhenscreenisoff;
    static Switch sw_clock_type;
    static TextView tw_configure_button;
    static int button_shortpress_action = 0;
    static int button_longpress_action = 0;
    static Switch sw_muteschedule;
    static TextView tw_muteschedule_on;
    static TextView tw_muteschedule_off;
    static int muteoff_hour = 0;
    static int muteoff_min = 0;
    static int muteon_hour = 0;
    static int muteon_min = 0;

    static float scale;

    static CountDownTimer mqtt_status_update_timer = null;
    static CountDownTimer aio_status_update_timer = null;
    static CountDownTimer weather_status_update_timer = null;

    private static String cfg_mqtt_broker_address = "";
    private static int cfg_mqtt_broker_port = 0;
    private static String cfg_weather_location = "";

    public static FragmentSettings newInstance(int index) {
        FragmentSettings fragment = new FragmentSettings();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);

    }

    @Override
    public void onPause() {
        super.onPause();
        if(mqtt_status_update_timer != null) mqtt_status_update_timer.cancel();
        if(aio_status_update_timer != null) aio_status_update_timer.cancel();
        if(weather_status_update_timer != null) weather_status_update_timer.cancel();
        Log.d("aura", "Settings fragment paused");
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);


        tw_clockstyle = root.findViewById(R.id.tw_clockstyle);
        tw_clockstyle.setOnClickListener(this);

        sw_autodisplayonoff = root.findViewById(R.id.sw_autodisplayonoff);
        sw_autodisplayonoff.setOnClickListener(this);

        tw_autodisplayoff = root.findViewById(R.id.tw_autodisplayoff);
        tw_autodisplayoff.setOnClickListener(this);

        tw_autodisplayon = root.findViewById(R.id.tw_autodisplayon);
        tw_autodisplayon.setOnClickListener(this);

        sw_disable_hourlybeep = root.findViewById(R.id.sw_disable_hourlybeep);
        sw_disable_hourlybeep.setOnClickListener(this);

        tw_timezone = root.findViewById(R.id.tw_timezone);
        tw_timezone.setOnClickListener(this);

        tw_invert_display = root.findViewById(R.id.tw_invert_display);
        tw_invert_display.setOnClickListener(this);

        sw_autodisplayonoff_onlyonweekdays = root.findViewById(R.id.sw_autodisplayonoff_onlyonweekdays);
        sw_autodisplayonoff_onlyonweekdays.setOnClickListener(this);

        sw_mqtt = root.findViewById(R.id.sw_mqtt);
        sw_mqtt.setOnClickListener(this);

        tw_factoryreset = root.findViewById(R.id.tw_factoryreset);
        tw_factoryreset.setOnClickListener(this);

        tw_tempsensorcalibration = root.findViewById(R.id.tw_tempsensorcalibration);
        tw_tempsensorcalibration.setOnClickListener(this);

        sw_aio = root.findViewById(R.id.sw_aio);
        sw_aio.setOnClickListener(this);

        sw_tempunit = root.findViewById(R.id.sw_tempunit);
        sw_tempunit.setOnClickListener(this);

        sw_webweather = root.findViewById(R.id.sw_webweather);
        sw_webweather.setOnClickListener(this);

        sw_mutedevice = root.findViewById(R.id.sw_mutedevice);
        sw_mutedevice.setOnClickListener(this);

        tw_force_ntp_sync = root.findViewById(R.id.tw_force_ntp_sync);
        tw_force_ntp_sync.setOnClickListener(this);

        tw_snoozetime = root.findViewById(R.id.tw_snoozetime);
        tw_snoozetime.setOnClickListener(this);

        tw_alarmsound = root.findViewById(R.id.tw_alarmsound);
        tw_alarmsound.setOnClickListener(this);

        sw_dst = root.findViewById(R.id.sw_dst);
        sw_dst.setOnClickListener(this);

        sw_ota = root.findViewById(R.id.sw_ota);
        sw_ota.setOnClickListener(this);

        tw_reboot = root.findViewById(R.id.tw_reboot);
        tw_reboot.setOnClickListener(this);

        tw_mqttstate = root.findViewById(R.id.tw_mqttstate);
        tw_mqttstate.setOnClickListener(this);

        tw_aiostate = root.findViewById(R.id.tw_aiostate);
        tw_aiostate.setOnClickListener(this);

        tw_weatherstate = root.findViewById(R.id.tw_weatherstate);
        tw_weatherstate.setOnClickListener(this);

        sw_numerology_mode = root.findViewById(R.id.sw_numerology_mode);
        sw_numerology_mode.setOnClickListener(this);

        sw_mutewhenscreenisoff = root.findViewById(R.id.sw_mutewhenscreenisoff);
        sw_mutewhenscreenisoff.setOnClickListener(this);

        sw_clock_type = root.findViewById(R.id.sw_clock_type);
        sw_clock_type.setOnClickListener(this);

        tw_configure_button = root.findViewById(R.id.tw_configure_button);
        tw_configure_button.setOnClickListener(this);

        sw_muteschedule = root.findViewById(R.id.sw_muteschedule);
        sw_muteschedule.setOnClickListener(this);

        tw_muteschedule_on = root.findViewById(R.id.tw_muteschedule_on);
        tw_muteschedule_on.setOnClickListener(this);

        tw_muteschedule_off = root.findViewById(R.id.tw_muteschedule_off);
        tw_muteschedule_off.setOnClickListener(this);

        set_mute_menu_expanded(false);
        set_autodisplayonoff_menu_expanded(false);

        MainActivity.sendAuraPacket(MainActivity.message_ids.CLOCK_TYPE, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.CLOCK_STYLE, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_OFF_TIME, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ON_TIME, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.HOURLY_BEEP_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.TIMEZONE, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_CLIENT_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPSENSOR_CALIBRATION_VALUE, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPERATURE_UNIT, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.WEBWEATHER_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_DEVICE, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_SNOOZE_TIME, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_SOUND, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.DST_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.OTA_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MAGIC_NUMBERS_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_WHEN_SCREEN_IS_OFF, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.BUTTON_SHORTPRESS_ACTION, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.BUTTON_LONGPRESS_ACTION, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_SCHEDULE_ENABLED, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_ON_TIME, MainActivity.message_type.REQUEST, null);
        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_OFF_TIME, MainActivity.message_type.REQUEST, null);

        scale = getResources().getDisplayMetrics().density;

        return root;
    }


    public static void messageReceived(MainActivity.message_ids message_id, byte[] payload) {


        switch(message_id) {
            case CLOCK_TYPE:
                int clock_type = payload[0];
                if (clock_type == 1) {
                    sw_clock_type.setChecked(true);
                } else {
                    sw_clock_type.setChecked(false);
                }
                break;

            case CLOCK_STYLE:
                clock_style = payload[0];
                break;

            case DISPLAY_AUTO_ONOFF_ENABLED:
                int autodisplayonoffenabled = payload[0];
                if (autodisplayonoffenabled == 1) {
                    sw_autodisplayonoff.setChecked(true);
                    set_autodisplayonoff_menu_expanded(true);
                } else {
                    sw_autodisplayonoff.setChecked(false);
                    set_autodisplayonoff_menu_expanded(false);
                }
                break;

            case DISPLAY_AUTO_ONOFF_ON_TIME:
                int on_hour = payload[0];
                int on_min = payload[1];
                displayon_hour = on_hour;
                displayon_min = on_min;
                tw_autodisplayon.setText(String.format("Turn on time: %d:%02d", on_hour, on_min));
                break;

            case DISPLAY_AUTO_ONOFF_OFF_TIME:
                int off_hour = payload[0];
                int off_min = payload[1];
                displayoff_hour = off_hour;
                displayoff_min = off_min;
                tw_autodisplayoff.setText(String.format("Turn off time: %d:%02d", off_hour, off_min));
                break;

            case DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS:
                int autodisplayonoffonlyonweekdays = payload[0];
                if(autodisplayonoffonlyonweekdays == 1) {
                    sw_autodisplayonoff_onlyonweekdays.setChecked(true);
                } else {
                    sw_autodisplayonoff_onlyonweekdays.setChecked(false);
                }
                break;

            case MUTE_DEVICE:
                int devicemuted = payload[0];
                if(devicemuted == 1) {
                    sw_mutedevice.setChecked(true);
                    set_mute_menu_expanded(false);
                } else {
                    sw_mutedevice.setChecked(false);
                    set_mute_menu_expanded(true);
                }
                break;

            case MUTE_WHEN_SCREEN_IS_OFF:
                int mutewhenscreenisoff = payload[0];
                if(mutewhenscreenisoff == 1) {
                    sw_mutewhenscreenisoff.setChecked(true);
                } else {
                    sw_mutewhenscreenisoff.setChecked(false);
                }
                break;

            case HOURLY_BEEP_ENABLED:
                int hourlybeep = payload[0];
                if(hourlybeep == 1) {
                    sw_disable_hourlybeep.setChecked(false);
                } else {
                    sw_disable_hourlybeep.setChecked(true);
                }
                break;

            case TIMEZONE:
                int timezone_i = payload[0];
                if(timezone_i >= 0) {
                    timezone = "UTC+"+timezone_i;
                } else {
                    timezone = "UTC"+timezone_i;
                }
                updateTimezoneButtonText();
                break;

            case MQTT_CLIENT_ENABLED:
                int mqttclientenabled = payload[0];
                if(mqttclientenabled == 1) {
                    sw_mqtt.setChecked(true);
                    mqtt_status_change_visibility(true);
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_BROKER_ADDRESS, MainActivity.message_type.REQUEST, null);
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_BROKER_PORT, MainActivity.message_type.REQUEST, null);
                } else {
                    sw_mqtt.setChecked(false);
                    mqtt_status_change_visibility(false);
                }
                break;

            case MQTT_BROKER_ADDRESS:
                cfg_mqtt_broker_address = new String(payload, StandardCharsets.UTF_8);
                break;

            case MQTT_BROKER_PORT:
                cfg_mqtt_broker_port = (payload[1] << 8) + payload[0];
                break;

            case TEMPSENSOR_CALIBRATION_VALUE:
                int tempcal = payload[0];
                updateTempeatureSensorCalibrationButtonText("" + tempcal);
                break;

            case ADAFRUITIO_ENABLED:
                int aioenabled = payload[0];
                if(aioenabled == 1) {
                    sw_aio.setChecked(true);
                    aio_status_change_visibility(true);
                } else {
                    sw_aio.setChecked(false);
                    aio_status_change_visibility(false);
                }
                break;

            case TEMPERATURE_UNIT:
                int gettempunit = payload[0];
                if(gettempunit == 1) {
                    sw_tempunit.setChecked(true);
                } else {
                    sw_tempunit.setChecked(false);
                }
                break;

            case WEBWEATHER_ENABLED:
                int webweatherenabled = payload[0];
                if(webweatherenabled == 1) {
                    sw_webweather.setChecked(true);
                    weather_status_change_visibility(true);
                    MainActivity.sendAuraPacket(MainActivity.message_ids.WEBWEATHER_LOCATION, MainActivity.message_type.REQUEST, null);
                } else {
                    sw_webweather.setChecked(false);
                    weather_status_change_visibility(false);
                }
                break;

            case WEBWEATHER_LOCATION:
                cfg_weather_location = new String(payload, StandardCharsets.UTF_8);
                break;

            case ALARM_SNOOZE_TIME:
                snooze_time = payload[0];
                tw_snoozetime.setText(String.format("Snooze time (%d min)", snooze_time));
                break;

            case ALARM_SOUND:
                alarm_sound = payload[0];
                break;

            case DST_ENABLED:
                int dstenabled = payload[0];
                sw_dst.setChecked(dstenabled == 1);
                break;

            case OTA_ENABLED:
                int otaenabled = payload[0];
                sw_ota.setChecked(otaenabled == 1);
                break;

            case MQTT_CLIENT_STATUS:
                final int MQTT_CLIENT_DISABLED = 0;
                final int MQTT_CLIENT_CONNECTING = 1;
                final int MQTT_CLIENT_CONNECTED = 2;
                final int MQTT_CLIENT_DISCONNECTED = 3;
                final int MQTT_CLIENT_CONNECTION_FAILED = 4;
                String mqtt_status_str = "";

                int mqtt_status = payload[0];
                switch(mqtt_status) {
                    case MQTT_CLIENT_DISABLED:
                        mqtt_status_str += "disabled";
                        break;

                    case MQTT_CLIENT_CONNECTING:
                        mqtt_status_str += "\uD83D\uDD35 connecting...";
                        break;

                    case MQTT_CLIENT_CONNECTED:
                        mqtt_status_str += "\uD83D\uDFE2 connected";
                        break;

                    case MQTT_CLIENT_DISCONNECTED:
                    case MQTT_CLIENT_CONNECTION_FAILED:
                        mqtt_status_str += "\uD83D\uDD34 connection failed";
                        break;
                }

                tw_mqttstate.setText(mqtt_status_str);
                break;

            case ADAFRUITIO_CLIENT_STATUS:
                final int AIO_CLIENT_DISABLED = 0;
                final int AIO_CLIENT_CONNECTING = 1;
                final int AIO_CLIENT_CONNECTED = 2;
                final int AIO_CLIENT_DISCONNECTED = 3;
                final int AIO_CLIENT_CONNECTION_FAILED = 4;
                String aio_status_str = "";

                int aio_status = payload[0];
                switch(aio_status) {
                    case AIO_CLIENT_DISABLED:
                        aio_status_str += "disabled";
                        break;

                    case AIO_CLIENT_CONNECTING:
                        aio_status_str += "\uD83D\uDD35 connecting...";
                        break;

                    case AIO_CLIENT_CONNECTED:
                        aio_status_str += "\uD83D\uDFE2 connected";
                        break;

                    case AIO_CLIENT_CONNECTION_FAILED:
                    case AIO_CLIENT_DISCONNECTED:
                        aio_status_str += "\uD83D\uDD34 connection failed";
                        break;
                }

                tw_aiostate.setText(aio_status_str);
                break;

            case WEATHER_CLIENT_STATUS:
                final int WW_DISABLED = 0;
                final int WW_SYNC_IN_PROGRESS = 1;
                final int WW_SLEEP = 2;
                final int WW_SYNC_FAILED = 3;
                String weather_status_str = "";

                int weather_status = payload[0];
                switch(weather_status) {
                    case WW_DISABLED:
                        weather_status_str += "disabled";
                        break;

                    case WW_SYNC_IN_PROGRESS:
                        weather_status_str += "\uD83D\uDD35 syncing...";
                        break;

                    case WW_SLEEP:
                        weather_status_str += "\uD83D\uDFE2 connected";
                        break;

                    case WW_SYNC_FAILED:
                        weather_status_str += "\uD83D\uDD34 sync failed";
                        break;
                }

                tw_weatherstate.setText(weather_status_str);

                break;

            case MAGIC_NUMBERS_ENABLED:
                int numerologymode_enabled = payload[0];
                sw_numerology_mode.setChecked(numerologymode_enabled == 1);
                break;

            case BUTTON_SHORTPRESS_ACTION:
                button_shortpress_action = payload[0];
                break;

            case BUTTON_LONGPRESS_ACTION:
                button_longpress_action = payload[0];
                break;

            case MUTE_SCHEDULE_ENABLED:
                int mutescheduleenabled = payload[0];
                if (mutescheduleenabled == 1) {
                    sw_muteschedule.setChecked(true);
                    tw_muteschedule_on.setVisibility(View.VISIBLE);
                    tw_muteschedule_off.setVisibility(View.VISIBLE);
                } else {
                    sw_muteschedule.setChecked(false);
                    tw_muteschedule_on.setVisibility(View.GONE);
                    tw_muteschedule_off.setVisibility(View.GONE);
                }
                break;

            case MUTE_ON_TIME:
                int rcv_mute_on_hour = payload[0];
                int rcv_mute_on_min = payload[1];
                muteon_hour = rcv_mute_on_hour;
                muteon_min = rcv_mute_on_min;
                tw_muteschedule_on.setText(String.format("Turn on time: %d:%02d", muteon_hour, muteon_min));
                break;

            case MUTE_OFF_TIME:
                int rcv_muteoff_hour = payload[0];
                int rcv_muteoff_min = payload[1];
                muteoff_hour = rcv_muteoff_hour;
                muteoff_min = rcv_muteoff_min;
                tw_muteschedule_off.setText(String.format("Turn off time: %d:%02d", muteoff_hour, muteoff_min));
                break;

        }

    }

    static void updateTimezoneButtonText() {
        tw_timezone.setText("Timezone ("+timezone+")");
    }

    static void updateTempeatureSensorCalibrationButtonText(String value) {
        tw_tempsensorcalibration.setText("Temperature calibration (" + value + ")");
    }

    @Override
    public void onClick(View v) {

        final Dialog dialog;
        Button dialogButton;
        final TimePicker tp_clock;

        switch (v.getId()) {
            case R.id.sw_clock_type:
                if(sw_clock_type.isChecked()) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.CLOCK_TYPE, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.CLOCK_TYPE, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.tw_clockstyle:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timezone);
                dialog.setTitle("Select clock style");

                clockstyleList = new ArrayList<String>();
                clockstyleListArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, clockstyleList);

                String[] listOfStyles = new String[] {
                        "Big clock",
                        "Small clock",
                        "Small clock with inside temp",
                        "Small clock with outside temp",
                        "Small clock with seconds"
                };

                for(int i = 0 ; i < listOfStyles.length ; i++) {
                    String curr_item = listOfStyles[i];
                    if(i == clock_style) curr_item += "  " + new String(Character.toChars(0x2714));
                    clockstyleList.add(curr_item);
                }

                lw_clockstyle = dialog.findViewById(R.id.lw_timezones);
                lw_clockstyle.setAdapter(clockstyleListArrayAdapter);

                lw_clockstyle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        clock_style = i;

                        byte[] payload = new byte[1];
                        payload[0] = (byte)i;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.CLOCK_STYLE, MainActivity.message_type.CHANGE_VALUE, payload);

                        MainActivity.vibrate();
                        dialog.dismiss();
                    }

                });

                dialog.show();
                break;

            case R.id.sw_autodisplayonoff:
                if(sw_autodisplayonoff.isChecked()) {
                    set_autodisplayonoff_menu_expanded(true);
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    set_autodisplayonoff_menu_expanded(false);
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.tw_autodisplayoff:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timer);
                dialog.setTitle("Set off time");

                tp_clock = dialog.findViewById(R.id.tp_timer);
                tp_clock.setIs24HourView(true);
                tp_clock.setHour(displayoff_hour);
                tp_clock.setMinute(displayoff_min);

                dialogButton = dialog.findViewById(R.id.btn_starttimer);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayoff_hour = tp_clock.getHour();
                        displayoff_min = tp_clock.getMinute();

                        byte[] payload = new byte[2];
                        payload[0] = (byte)displayoff_hour;
                        payload[1] = (byte)displayoff_min;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_OFF_TIME, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();
                        tw_autodisplayoff.setText(String.format("Turn off time: %02d:%02d", displayoff_hour, displayoff_min));

                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;

            case R.id.tw_autodisplayon:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timer);
                dialog.setTitle("Set on time");

                tp_clock = dialog.findViewById(R.id.tp_timer);
                tp_clock.setIs24HourView(true);
                tp_clock.setHour(displayon_hour);
                tp_clock.setMinute(displayon_min);

                dialogButton = dialog.findViewById(R.id.btn_starttimer);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayon_hour = tp_clock.getHour();
                        displayon_min = tp_clock.getMinute();

                        byte[] payload = new byte[2];
                        payload[0] = (byte)displayon_hour;
                        payload[1] = (byte)displayon_min;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ON_TIME, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();
                        tw_autodisplayon.setText(String.format("Turn on time: %02d:%02d",displayon_hour, displayon_min));

                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;

            case R.id.sw_mutedevice:
                boolean dm = sw_mutedevice.isChecked();
                if(dm) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_DEVICE, MainActivity.message_type.CHANGE_VALUE, payload);
                    set_mute_menu_expanded(false);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_DEVICE, MainActivity.message_type.CHANGE_VALUE, payload);
                    set_mute_menu_expanded(true);
                }
                MainActivity.vibrate();
                break;

            case R.id.sw_mutewhenscreenisoff:
                if(sw_mutewhenscreenisoff.isChecked()) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_WHEN_SCREEN_IS_OFF, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_WHEN_SCREEN_IS_OFF, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.sw_disable_hourlybeep:
                if(!sw_disable_hourlybeep.isChecked()) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.HOURLY_BEEP_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.HOURLY_BEEP_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.tw_timezone:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timezone);
                dialog.setTitle("Set timezone");

                timezoneList = new ArrayList<String>();
                timezoneListArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, timezoneList);
                timezoneList.add("UTC-12");
                timezoneList.add("UTC-11");
                timezoneList.add("UTC-10");
                timezoneList.add("UTC-9");
                timezoneList.add("UTC-8");
                timezoneList.add("UTC-7");
                timezoneList.add("UTC-6");
                timezoneList.add("UTC-5");
                timezoneList.add("UTC-4");
                timezoneList.add("UTC-3");
                timezoneList.add("UTC-2");
                timezoneList.add("UTC-1");
                timezoneList.add("UTC+0");
                timezoneList.add("UTC+1");
                timezoneList.add("UTC+2");
                timezoneList.add("UTC+3");
                timezoneList.add("UTC+4");
                timezoneList.add("UTC+5");
                timezoneList.add("UTC+6");
                timezoneList.add("UTC+7");
                timezoneList.add("UTC+8");
                timezoneList.add("UTC+9");
                timezoneList.add("UTC+10");
                timezoneList.add("UTC+11");
                timezoneList.add("UTC+12");

                lw_timezones = dialog.findViewById(R.id.lw_timezones);
                lw_timezones.setAdapter(timezoneListArrayAdapter);

                lw_timezones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        timezone = lw_timezones.getItemAtPosition(i).toString();
                        updateTimezoneButtonText();

                        String selected_timezone = timezone.substring(3);
                        int selected_timezone_i = Integer.decode(selected_timezone);

                        byte[] payload = new byte[1];
                        payload[0] = (byte)selected_timezone_i;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.TIMEZONE, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();

                        dialog.dismiss();
                    }

                });

                dialog.show();
                break;

            case R.id.tw_invert_display:
                MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_INVERT, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;

            case R.id.sw_autodisplayonoff_onlyonweekdays:
                boolean e = sw_autodisplayonoff_onlyonweekdays.isChecked();
                if(e) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.sw_mqtt:
                handle_mqtt_config_window();
                break;

            case R.id.tw_factoryreset:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("All settings including the WiFi credentials will be erased. You'll have to set the device up again. Are you sure?")
                        .setPositiveButton("Yes", factoryreset_dialogClickListener)
                        .setNegativeButton("No", factoryreset_dialogClickListener).show();
                MainActivity.vibrate();
                break;

            case R.id.tw_tempsensorcalibration:
                /* ToDo: Rework the timezone setting fragment to be general, this way it's a bit messy */
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timezone);
                dialog.setTitle("Select calibration value");

                tempcalList = new ArrayList<String>();
                tempcalListArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tempcalList);
                tempcalList.add("-5");
                tempcalList.add("-4");
                tempcalList.add("-3");
                tempcalList.add("-2");
                tempcalList.add("-1");
                tempcalList.add("0");
                tempcalList.add("+1");
                tempcalList.add("+2");
                tempcalList.add("+3");
                tempcalList.add("+4");
                tempcalList.add("+5");

                lw_tempcal = dialog.findViewById(R.id.lw_timezones);
                lw_tempcal.setAdapter(tempcalListArrayAdapter);

                lw_tempcal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String tempcal = lw_tempcal.getItemAtPosition(i).toString();
                        updateTempeatureSensorCalibrationButtonText(tempcal);

                        byte[] payload = new byte[1];
                        payload[0] = (byte)Integer.parseInt(tempcal);
                        MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPSENSOR_CALIBRATION_VALUE, MainActivity.message_type.CHANGE_VALUE, payload);

                        MainActivity.vibrate();
                        dialog.dismiss();
                    }

                });

                dialog.show();
                break;

            case R.id.sw_aio:
                handle_aio_config_window();
                break;

            case R.id.sw_tempunit:
                boolean h = sw_tempunit.isChecked();
                if(h) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPERATURE_UNIT, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPERATURE_UNIT, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.sw_webweather:
                handle_weather_config_window();
                break;

            case R.id.tw_force_ntp_sync:
                MainActivity.sendAuraPacket(MainActivity.message_ids.FORCE_NTP_SYNC, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;

            case R.id.tw_snoozetime:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_snoozetime_popup);
                dialog.setTitle("Snooze time");

                et_snoozetime = dialog.findViewById(R.id.et_snoozetime);
                et_snoozetime.setText(snooze_time + "");
                et_snoozetime.setSelection(et_snoozetime.getText().length());

                Button btn_snoozetime_apply = dialog.findViewById(R.id.btn_snoozetime_apply);
                btn_snoozetime_apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snooze_time = Integer.parseInt(et_snoozetime.getText().toString());
                        if(snooze_time > 120) snooze_time = 120;

                        byte[] payload = new byte[1];
                        payload[0] = (byte)snooze_time;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_SNOOZE_TIME, MainActivity.message_type.CHANGE_VALUE, payload);

                        tw_snoozetime.setText(String.format("Snooze time (%d min)", snooze_time));
                        MainActivity.vibrate();
                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;

            case R.id.tw_alarmsound:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timezone);
                dialog.setTitle("Select alarm sound");

                alarmsoundList = new ArrayList<String>();
                alarmsoundListArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, alarmsoundList);

                String[] listOfAlarmSounds = new String[] {
                        "Elise",
                        "Suburbia",
                        "Stacy",
                        "Fly",
                        "Everyday"
                };

                for(int j = 0 ; j < listOfAlarmSounds.length ; j++) {
                    String curr_item = listOfAlarmSounds[j];
                    if(j == alarm_sound) curr_item += "  " + new String(Character.toChars(0x2714));
                    alarmsoundList.add(curr_item);
                }

                lw_alarmsound = dialog.findViewById(R.id.lw_timezones);
                lw_alarmsound.setAdapter(alarmsoundListArrayAdapter);

                lw_alarmsound.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        alarm_sound = i;

                        byte[] payload = new byte[1];
                        payload[0] = (byte)i;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.ALARM_SOUND, MainActivity.message_type.CHANGE_VALUE, payload);

                        MainActivity.vibrate();
                        dialog.dismiss();
                    }

                });

                dialog.show();
                break;

            case R.id.sw_dst:
                boolean dststate = sw_dst.isChecked();
                if(dststate) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DST_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.DST_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.sw_ota:
                boolean otastate = sw_ota.isChecked();
                if(otastate) {

                    dialog = new Dialog(getContext(), R.style.CustomDialog);
                    dialog.setContentView(R.layout.fragment_ota_configure);
                    dialog.setTitle("Configure OTA password");

                    et_ota_pass = dialog.findViewById(R.id.et_ota_pass);

                    dialogButton = dialog.findViewById(R.id.btn_ota_apply);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String ota_pass = et_ota_pass.getText().toString() + '\0';
                            MainActivity.sendAuraPacket(MainActivity.message_ids.OTA_PASSWORD, MainActivity.message_type.CHANGE_VALUE, ota_pass.getBytes());

                            byte[] payload = new byte[1];
                            payload[0] = 1;
                            MainActivity.sendAuraPacket(MainActivity.message_ids.OTA_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);

                            MainActivity.vibrate();
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.OTA_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.tw_reboot:
                AlertDialog.Builder r_builder = new AlertDialog.Builder(getContext());
                r_builder.setMessage("The device will reboot. Proceed?")
                        .setPositiveButton("Yes", reboot_dialogClickListener)
                        .setNegativeButton("No", reboot_dialogClickListener).show();
                MainActivity.vibrate();
                break;

            case R.id.sw_numerology_mode:
                if(sw_numerology_mode.isChecked()) {
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MAGIC_NUMBERS_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MAGIC_NUMBERS_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.tw_configure_button:
                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_doublelist_popup);
                dialog.setTitle("Configure button actions");
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

                String[] listOfButtonActions = new String[] {
                        "Do nothing",
                        "Play music",
                        "Show weather",
                        "Show detailed weather",
                        "Show date",
                        "Show detailed date",
                        "Screen on/off",
                        "Configure alarm",
                        "Start snake",
                        "Start tetris",
                };

                TextView tw_shortpress_title = dialog.findViewById(R.id.tw_list_title_one);
                tw_shortpress_title.setText("Short press action:");

                TextView tw_longpress_title = dialog.findViewById(R.id.tw_list_title_two);
                tw_longpress_title.setText("Long press action:");

                List<String> shortpressactionList = new ArrayList<String>();
                ArrayAdapter<String> shortpressactionListArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, shortpressactionList);

                String[] listOfShortpressActions = listOfButtonActions;
                for(int i = 0 ; i < listOfShortpressActions.length ; i++) {
                    String curr_item = listOfShortpressActions[i];
                    if(i == button_shortpress_action) curr_item += "  " + new String(Character.toChars(0x2714));
                    shortpressactionList.add(curr_item);
                }

                ListView lw_shortpressactions = dialog.findViewById(R.id.lw_list_one);
                lw_shortpressactions.setAdapter(shortpressactionListArrayAdapter);
                setListViewHeightBasedOnChildren(lw_shortpressactions);

                lw_shortpressactions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        button_shortpress_action = i;

                        String[] listOfShortpressActions = listOfButtonActions;
                        shortpressactionList.clear();
                        for(int j = 0 ; j < listOfShortpressActions.length ; j++) {
                            String curr_item = listOfShortpressActions[j];
                            if(j == button_shortpress_action) curr_item += "  " + new String(Character.toChars(0x2714));
                            shortpressactionList.add(curr_item);
                        }
                        shortpressactionListArrayAdapter.notifyDataSetChanged();

                        byte[] payload = new byte[1];
                        payload[0] = (byte)i;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.BUTTON_SHORTPRESS_ACTION, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();
                    }
                });

                List<String> longpressactionList = new ArrayList<String>();
                ArrayAdapter<String> longpressactionListArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, longpressactionList);

                String[] listOfLongpressActions = listOfButtonActions;
                for(int i = 0 ; i < listOfLongpressActions.length ; i++) {
                    String curr_item = listOfLongpressActions[i];
                    if(i == button_longpress_action) curr_item += "  " + new String(Character.toChars(0x2714));
                    longpressactionList.add(curr_item);
                }

                ListView lw_longpressactions = dialog.findViewById(R.id.lw_list_two);
                lw_longpressactions.setAdapter(longpressactionListArrayAdapter);
                setListViewHeightBasedOnChildren(lw_longpressactions);

                lw_longpressactions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        button_longpress_action = i;

                        String[] listOfLongpressActions = listOfButtonActions;
                        longpressactionList.clear();
                        for(int j = 0 ; j < listOfLongpressActions.length ; j++) {
                            String curr_item = listOfLongpressActions[j];
                            if(j == button_longpress_action) curr_item += "  " + new String(Character.toChars(0x2714));
                            longpressactionList.add(curr_item);
                        }
                        longpressactionListArrayAdapter.notifyDataSetChanged();

                        byte[] payload = new byte[1];
                        payload[0] = (byte)i;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.BUTTON_LONGPRESS_ACTION, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();
                    }
                });

                Button btn_buttonaction_done = dialog.findViewById(R.id.btn_doublelist_done);
                btn_buttonaction_done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;

            case R.id.sw_muteschedule:
                if(sw_muteschedule.isChecked()) {
                    tw_muteschedule_on.setVisibility(View.VISIBLE);
                    tw_muteschedule_off.setVisibility(View.VISIBLE);
                    byte[] payload = new byte[1];
                    payload[0] = 1;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_SCHEDULE_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                } else {
                    tw_muteschedule_on.setVisibility(View.GONE);
                    tw_muteschedule_off.setVisibility(View.GONE);
                    byte[] payload = new byte[1];
                    payload[0] = 0;
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_SCHEDULE_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
                }
                MainActivity.vibrate();
                break;

            case R.id.tw_muteschedule_on:
                if(sw_mutedevice.isChecked()) return;

                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timer);
                dialog.setTitle("Set mute time");

                tp_clock = dialog.findViewById(R.id.tp_timer);
                tp_clock.setIs24HourView(true);
                tp_clock.setHour(muteon_hour);
                tp_clock.setMinute(muteon_min);

                dialogButton = dialog.findViewById(R.id.btn_starttimer);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        muteon_hour = tp_clock.getHour();
                        muteon_min = tp_clock.getMinute();

                        byte[] payload = new byte[2];
                        payload[0] = (byte)muteon_hour;
                        payload[1] = (byte)muteon_min;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_ON_TIME, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();
                        tw_muteschedule_on.setText(String.format("Mute at: %02d:%02d", muteon_hour, muteon_min));

                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;

            case R.id.tw_muteschedule_off:
                if(sw_mutedevice.isChecked()) return;

                dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timer);
                dialog.setTitle("Set unmute time");

                tp_clock = dialog.findViewById(R.id.tp_timer);
                tp_clock.setIs24HourView(true);
                tp_clock.setHour(muteoff_hour);
                tp_clock.setMinute(muteoff_min);

                dialogButton = dialog.findViewById(R.id.btn_starttimer);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        muteoff_hour = tp_clock.getHour();
                        muteoff_min = tp_clock.getMinute();

                        byte[] payload = new byte[2];
                        payload[0] = (byte)muteoff_hour;
                        payload[1] = (byte)muteoff_min;
                        MainActivity.sendAuraPacket(MainActivity.message_ids.MUTE_OFF_TIME, MainActivity.message_type.CHANGE_VALUE, payload);
                        MainActivity.vibrate();
                        tw_muteschedule_off.setText(String.format("Unmute at: %02d:%02d", muteoff_hour, muteoff_min));

                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;
        }
    }

    private boolean mqtt_username_changed = false;
    private void set_mqtt_username_changed(boolean value) {
        mqtt_username_changed = value;
    }
    private boolean get_mqtt_username_changed() {
        return mqtt_username_changed;
    }

    private boolean mqtt_password_changed = false;
    private void set_mqtt_password_changed(boolean value) {
        mqtt_password_changed = value;
    }
    private boolean get_mqtt_password_changed() {
        return mqtt_password_changed;
    }

    private void handle_mqtt_config_window() {
        sw_mqtt.setChecked(!sw_mqtt.isChecked());
        set_mqtt_username_changed(false);
        set_mqtt_password_changed(false);

        final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setContentView(R.layout.fragment_mqtt_configure);
        dialog.setTitle("Configure MQTT broker");

        EditText et_mqtt_broker_address = dialog.findViewById(R.id.et_mqtt_server);
        EditText et_mqtt_broker_port = dialog.findViewById(R.id.et_mqtt_port);
        EditText et_mqtt_username = dialog.findViewById(R.id.et_mqtt_username);
        EditText et_mqtt_password = dialog.findViewById(R.id.et_mqtt_password);

        Button ok_button = dialog.findViewById(R.id.btn_mqtt_apply);
        Button disable_button = dialog.findViewById(R.id.btn_mqtt_disable);

        if(sw_mqtt.isChecked()) {
            et_mqtt_broker_address.setText(cfg_mqtt_broker_address);
            et_mqtt_broker_port.setText(cfg_mqtt_broker_port+"");
            ok_button.setText("Change configuration");
            disable_button.setVisibility(View.VISIBLE);

            et_mqtt_username.setHint("(tap to change)");
            et_mqtt_username.setOnTouchListener((view, motionEvent) -> {
                et_mqtt_username.setHint("");
                set_mqtt_username_changed(true);
                return false;
            });

            et_mqtt_password.setHint("(tap to change)");
            et_mqtt_password.setOnTouchListener((view, motionEvent) -> {
                et_mqtt_password.setHint("");
                set_mqtt_password_changed(true);
                return false;
            });
        } else {
            set_mqtt_username_changed(true);
            set_mqtt_password_changed(true);
            ok_button.setText("Connect");
            disable_button.setVisibility(View.GONE);
        }

        disable_button.setOnClickListener(v -> {
            byte[] payload = new byte[1];
            payload[0] = 0;
            MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_CLIENT_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
            mqtt_status_change_visibility(false);
            sw_mqtt.setChecked(false);
            MainActivity.vibrate();
            dialog.dismiss();
        });

        ok_button.setOnClickListener(v -> {

            String broker_address = et_mqtt_broker_address.getText().toString();
            String broker_username = et_mqtt_username.getText().toString() + '\0';
            String broker_password = et_mqtt_password.getText().toString() + '\0';
            int broker_port = Integer.reverseBytes(Integer.parseInt(et_mqtt_broker_port.getText().toString()));

            cfg_mqtt_broker_address = broker_address;
            cfg_mqtt_broker_port = Integer.reverseBytes(broker_port);

            if(broker_address.matches("") || broker_address.matches("")) {
                Toast.makeText(getContext(), "Server name and port cannot be empty!", Toast.LENGTH_SHORT).show();
                MainActivity.vibrate();
                return;
            }

            if(!Patterns.IP_ADDRESS.matcher(broker_address).matches()) {
                Toast.makeText(getContext(), "Invalid server IP provided!", Toast.LENGTH_SHORT).show();
                MainActivity.vibrate();
                return;
            }

            broker_address += '\0';


            if(Integer.reverseBytes(broker_port) < 0 || Integer.reverseBytes(broker_port) > 65535) {
                Toast.makeText(getContext(), "Invalid port number!", Toast.LENGTH_SHORT).show();
                MainActivity.vibrate();
                return;
            }

            byte[] port_payload = new byte[2];
            Arrays.fill(port_payload, (byte)0);
            port_payload = Arrays.copyOfRange((BigInteger.valueOf(broker_port).toByteArray()), 0, 2);

            try {
                MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_BROKER_ADDRESS, MainActivity.message_type.CHANGE_VALUE, broker_address.getBytes());
                Thread.sleep(5);
                MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_BROKER_PORT, MainActivity.message_type.CHANGE_VALUE, port_payload);
                Thread.sleep(5);

                if(get_mqtt_username_changed()) {
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_BROKER_USERNAME, MainActivity.message_type.CHANGE_VALUE, broker_username.getBytes());
                    Thread.sleep(5);
                }
                if(get_mqtt_password_changed()) {
                    MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_BROKER_PASSWORD, MainActivity.message_type.CHANGE_VALUE, broker_password.getBytes());
                    Thread.sleep(5);
                }
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }

            byte[] payload = new byte[1];
            payload[0] = 1;
            MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_CLIENT_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);

            mqtt_status_change_visibility(true);
            sw_mqtt.setChecked(true);
            MainActivity.vibrate();
            dialog.dismiss();
            MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_CLIENT_STATUS, MainActivity.message_type.REQUEST, null);
        });

        dialog.show();
    }


    private boolean ww_api_key_changed = false;
    private void set_ww_api_key_changed(boolean value) {
        ww_api_key_changed = value;
    }
    private boolean get_ww_api_key_changed() {
        return ww_api_key_changed;
    }

    private void handle_weather_config_window() {
        sw_webweather.setChecked(!sw_webweather.isChecked());
        set_ww_api_key_changed(false);

        final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setContentView(R.layout.fragment_webweather_configure);
        dialog.setTitle("Set up weather service");

        EditText et_ww_api_key = dialog.findViewById(R.id.et_ww_api_key);
        EditText et_ww_location = dialog.findViewById(R.id.et_ww_location);
        Button ok_button = dialog.findViewById(R.id.btn_ww_apply);
        Button disable_button = dialog.findViewById(R.id.btn_ww_disable);

        if(sw_webweather.isChecked()) {
            et_ww_location.setText(cfg_weather_location);
            ok_button.setText("Change configuration");
            disable_button.setVisibility(View.VISIBLE);

            et_ww_api_key.setHint("(tap to change)");
            et_ww_api_key.setOnTouchListener((view, motionEvent) -> {
                et_ww_api_key.setHint("");
                set_ww_api_key_changed(true);
                return false;
            });
        } else {
            set_ww_api_key_changed(true);
            ok_button.setText("Connect");
            disable_button.setVisibility(View.GONE);
        }

        disable_button.setOnClickListener(v -> {
            byte[] payload = new byte[1];
            payload[0] = 0;
            MainActivity.sendAuraPacket(MainActivity.message_ids.WEBWEATHER_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
            weather_status_change_visibility(false);
            sw_webweather.setChecked(false);
            MainActivity.vibrate();
            dialog.dismiss();
        });

        ok_button.setOnClickListener(v -> {

            if(get_ww_api_key_changed()) {
                if(et_ww_api_key.getText().toString().matches("")) {
                    Toast.makeText(getContext(), "API key cannot be empty!", Toast.LENGTH_SHORT).show();
                    MainActivity.vibrate();
                    return;
                }
                String webweather_apikey = et_ww_api_key.getText().toString() + '\0';
                MainActivity.sendAuraPacket(MainActivity.message_ids.WEBWEATHER_API_KEY, MainActivity.message_type.CHANGE_VALUE, webweather_apikey.getBytes());
            }

            if(et_ww_location.getText().toString().matches("")) {
                Toast.makeText(getContext(), "Location cannot be empty!", Toast.LENGTH_SHORT).show();
                MainActivity.vibrate();
                return;
            }
            String webweather_location = et_ww_location.getText().toString() + '\0';
            MainActivity.sendAuraPacket(MainActivity.message_ids.WEBWEATHER_LOCATION, MainActivity.message_type.CHANGE_VALUE, webweather_location.getBytes());
            cfg_weather_location = webweather_location;

            byte[] payload = new byte[1];
            payload[0] = 1;
            MainActivity.sendAuraPacket(MainActivity.message_ids.WEBWEATHER_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);

            weather_status_change_visibility(true);
            sw_webweather.setChecked(true);
            MainActivity.vibrate();
            dialog.dismiss();
            MainActivity.sendAuraPacket(MainActivity.message_ids.WEATHER_CLIENT_STATUS, MainActivity.message_type.REQUEST, null);
        });

        dialog.show();
    }

    private static void set_mute_menu_expanded(boolean is_expanded) {
        if(is_expanded) {
            sw_muteschedule.setVisibility(View.VISIBLE);
            if(sw_muteschedule.isChecked()) {
                tw_muteschedule_on.setVisibility(View.VISIBLE);
                tw_muteschedule_off.setVisibility(View.VISIBLE);
            }
            sw_mutewhenscreenisoff.setVisibility(View.VISIBLE);
            sw_disable_hourlybeep.setVisibility(View.VISIBLE);
        } else {
            sw_muteschedule.setVisibility(View.GONE);
            tw_muteschedule_on.setVisibility(View.GONE);
            tw_muteschedule_off.setVisibility(View.GONE);
            sw_mutewhenscreenisoff.setVisibility(View.GONE);
            sw_disable_hourlybeep.setVisibility(View.GONE);
        }
    }

    private static void set_autodisplayonoff_menu_expanded(boolean is_expanded) {
        if(is_expanded) {
            tw_autodisplayoff.setVisibility(View.VISIBLE);
            tw_autodisplayon.setVisibility(View.VISIBLE);
            sw_autodisplayonoff_onlyonweekdays.setVisibility(View.VISIBLE);
        } else {
            tw_autodisplayoff.setVisibility(View.GONE);
            tw_autodisplayon.setVisibility(View.GONE);
            sw_autodisplayonoff_onlyonweekdays.setVisibility(View.GONE);
        }
    }

    private boolean aio_api_key_changed = false;
    private void set_aio_api_key_changed(boolean value) {
        aio_api_key_changed = value;
    }
    private boolean get_aio_api_key_changed() {
        return aio_api_key_changed;
    }

    private boolean aio_username_changed = false;
    private void set_aio_username_changed(boolean value) {
        aio_username_changed = value;
    }
    private boolean get_aio_username_changed() {
        return aio_username_changed;
    }

    private void handle_aio_config_window() {
        sw_aio.setChecked(!sw_aio.isChecked());
        set_aio_api_key_changed(false);
        set_aio_username_changed(false);

        final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setContentView(R.layout.fragment_adafruitio_configure);
        dialog.setTitle("Connect to Adafruit IO");

        EditText et_aio_username = dialog.findViewById(R.id.et_aio_username);
        EditText et_aio_api_key = dialog.findViewById(R.id.et_aio_api_key);
        Button ok_button = dialog.findViewById(R.id.btn_aio_apply);
        Button disable_button = dialog.findViewById(R.id.btn_aio_disable);

        if(sw_aio.isChecked()) {
            ok_button.setText("Change configuration");
            disable_button.setVisibility(View.VISIBLE);

            et_aio_username.setHint("(tap to change)");
            et_aio_username.setOnTouchListener((view, motionEvent) -> {
                et_aio_username.setHint("");
                set_aio_username_changed(true);
                return false;
            });

            et_aio_api_key.setHint("(tap to change)");
            et_aio_api_key.setOnTouchListener((view, motionEvent) -> {
                et_aio_api_key.setHint("");
                set_aio_api_key_changed(true);
                return false;
            });
        } else {
            set_aio_username_changed(true);
            set_aio_api_key_changed(true);
            ok_button.setText("Connect");
            disable_button.setVisibility(View.GONE);
        }

        disable_button.setOnClickListener(v -> {
            byte[] payload = new byte[1];
            payload[0] = 0;
            MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);
            aio_status_change_visibility(false);
            sw_aio.setChecked(false);
            MainActivity.vibrate();
            dialog.dismiss();
        });

        ok_button.setOnClickListener(v -> {

            if(get_aio_username_changed()) {
                if(et_aio_username.getText().toString().matches("")) {
                    Toast.makeText(getContext(), "Username cannot be empty!", Toast.LENGTH_SHORT).show();
                    MainActivity.vibrate();
                    return;
                }
                String aio_username = et_aio_username.getText().toString() + '\0';
                MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_USERNAME, MainActivity.message_type.CHANGE_VALUE, aio_username.getBytes());
            }

            if(get_aio_api_key_changed()) {
                if(et_aio_api_key.getText().toString().matches("")) {
                    Toast.makeText(getContext(), "API key cannot be empty!", Toast.LENGTH_SHORT).show();
                    MainActivity.vibrate();
                    return;
                }
                String aio_api_key = et_aio_api_key.getText().toString() + '\0';
                MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_API_KEY, MainActivity.message_type.CHANGE_VALUE, aio_api_key.getBytes());
            }

            byte[] payload = new byte[1];
            payload[0] = 1;
            MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_ENABLED, MainActivity.message_type.CHANGE_VALUE, payload);

            aio_status_change_visibility(true);
            sw_aio.setChecked(true);
            MainActivity.vibrate();
            dialog.dismiss();
            MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_CLIENT_STATUS, MainActivity.message_type.REQUEST, null);
        });

        dialog.show();
    }

    static int pt_to_pixel(int sizeInPt) {
        int sizeInDp = (int)(sizeInPt * 2.222);
        int dpAsPixels = (int) (sizeInDp * scale + 0.5f);
        return dpAsPixels;
    }

    private static void mqtt_status_change_visibility(boolean visible) {
        if(visible) {
            tw_mqttstate.setVisibility(View.VISIBLE);
            sw_mqtt.setPadding(sw_mqtt.getPaddingLeft(), sw_mqtt.getPaddingTop(), sw_mqtt.getPaddingRight(), pt_to_pixel(0));
            mqtt_status_update_timer();
        } else {
            tw_mqttstate.setText("Status: disconnected");
            tw_mqttstate.setVisibility(View.GONE);
            sw_mqtt.setPadding(sw_mqtt.getPaddingLeft(), sw_mqtt.getPaddingTop(), sw_mqtt.getPaddingRight(), pt_to_pixel(5));
            if(mqtt_status_update_timer != null) mqtt_status_update_timer.cancel();
        }
    }

    public static void mqtt_status_update_timer() {
        mqtt_status_update_timer = new CountDownTimer(Long.MAX_VALUE, 2000) {

            public void onTick(long millisUntilFinished) {
                MainActivity.sendAuraPacket(MainActivity.message_ids.MQTT_CLIENT_STATUS, MainActivity.message_type.REQUEST, null);
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    private static void aio_status_change_visibility(boolean visible) {
        if(visible) {
            tw_aiostate.setVisibility(View.VISIBLE);
            sw_aio.setPadding(sw_aio.getPaddingLeft(), sw_aio.getPaddingTop(), sw_aio.getPaddingRight(), pt_to_pixel(0));
            aio_status_update_timer();
        } else {
            tw_aiostate.setText("Status: disconnected");
            tw_aiostate.setVisibility(View.GONE);
            sw_aio.setPadding(sw_aio.getPaddingLeft(), sw_aio.getPaddingTop(), sw_aio.getPaddingRight(), pt_to_pixel(5));
            if(aio_status_update_timer != null) aio_status_update_timer.cancel();
        }
    }

    public static void aio_status_update_timer() {
        aio_status_update_timer = new CountDownTimer(Long.MAX_VALUE, 2000) {

            public void onTick(long millisUntilFinished) {
                MainActivity.sendAuraPacket(MainActivity.message_ids.ADAFRUITIO_CLIENT_STATUS, MainActivity.message_type.REQUEST, null);
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    private static void weather_status_change_visibility(boolean visible) {
        if(visible) {
            tw_weatherstate.setVisibility(View.VISIBLE);
            sw_webweather.setPadding(sw_webweather.getPaddingLeft(), sw_webweather.getPaddingTop(), sw_webweather.getPaddingRight(), pt_to_pixel(0));
            weather_status_update_timer();
        } else {
            tw_weatherstate.setText("Status: disconnected");
            tw_weatherstate.setVisibility(View.GONE);
            sw_webweather.setPadding(sw_webweather.getPaddingLeft(), sw_webweather.getPaddingTop(), sw_webweather.getPaddingRight(), pt_to_pixel(5));
            if(weather_status_update_timer != null) weather_status_update_timer.cancel();
        }
    }

    public static void weather_status_update_timer() {
        weather_status_update_timer = new CountDownTimer(Long.MAX_VALUE, 2000) {

            public void onTick(long millisUntilFinished) {
                MainActivity.sendAuraPacket(MainActivity.message_ids.WEATHER_CLIENT_STATUS, MainActivity.message_type.REQUEST, null);
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    DialogInterface.OnClickListener reboot_dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    long timestamp = Long.reverseBytes(FragmentMainNew.current_synced_unix_time);
                    byte[] timestamp_payload = new byte[4];
                    Arrays.fill(timestamp_payload, (byte)0);
                    timestamp_payload = Arrays.copyOfRange((BigInteger.valueOf(timestamp).toByteArray()), 0, 4);
                    MainActivity.sendAuraPacket(MainActivity.message_ids.REBOOT, MainActivity.message_type.REQUEST, timestamp_payload);
                    MainActivity.vibrate();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    DialogInterface.OnClickListener factoryreset_dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    long timestamp = Long.reverseBytes(FragmentMainNew.current_synced_unix_time);
                    byte[] timestamp_payload = new byte[4];
                    Arrays.fill(timestamp_payload, (byte)0);
                    timestamp_payload = Arrays.copyOfRange((BigInteger.valueOf(timestamp).toByteArray()), 0, 4);
                    MainActivity.sendAuraPacket(MainActivity.message_ids.FACTORY_RESET, MainActivity.message_type.REQUEST, timestamp_payload);
                    MainActivity.vibrate();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:

                    break;
            }
        }
    };

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
