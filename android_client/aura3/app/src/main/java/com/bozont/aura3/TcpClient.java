package com.bozont.aura3;

import android.util.Log;

import com.bozont.aura3.ui.main.FragmentMain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

import static com.bozont.aura3.MainActivity.toast_connected;

public class TcpClient {

    public static final String TAG = TcpClient.class.getSimpleName();
    public static String SERVER_IP = "";
    public static int SERVER_PORT = 0;
    // message to send to the server
    private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;

    Socket socket;

    public boolean isConnected = false;

    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TcpClient(OnMessageReceived listener, String server_ip, int server_port) {
        mMessageListener = listener;
        SERVER_IP = server_ip;
        SERVER_PORT = server_port;
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(final String message) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mBufferOut != null) {
                    Log.d(TAG, "Sending: " + message);
                    mBufferOut.println(message);
                    mBufferOut.flush();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message bytes provided by the client
     */
    public void sendMessage(final byte[] message) {

        if(socket == null) return;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Sending: " + message);
                try {
                    OutputStream socketOutputStream = socket.getOutputStream();
                    socketOutputStream.write(message);
                    socketOutputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;
    }

    public void run() {

        mRun = true;

        try {
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
            Log.d("TCP Client", "C: Connecting...");

            socket = new Socket(serverAddr, SERVER_PORT);

            isConnected = true;
            toast_connected.show();
            FragmentMain.displayIpAddress();

            try {

                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                DataInputStream dIn = new DataInputStream(socket.getInputStream());

                int bufcnt = 0;
                byte[] buf = new byte[2048];

                // Listen for incoming data
                while (mRun) {

                    byte current = dIn.readByte();

                    if(current == '\n' && bufcnt > 0 && buf[bufcnt-1] == '\n') {
                        /* We found a correctly terminated message, store the last byte and go ahead */
                        buf[bufcnt] = current;
                    } else {
                        /* We didn't find a message ending, store what we have and continue the loop */
                        buf[bufcnt] = current;
                        bufcnt++;
                        continue;
                    }

                    byte[] buf_out = new byte[bufcnt];
                    for(int i = 0 ; i < bufcnt ; i++) {
                        buf_out[i] = buf[i];
                    }

                    bufcnt = 0;

                    mMessageListener.messageReceived(buf_out);

                }

                Log.d("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

            } catch (Exception e) {
                Log.e("TCP", "S: Error", e);
            } finally {
                // the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
                MainActivity.onServerDisconnected();
                isConnected = false;
            }

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the Activity
    //class at on AsyncTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(byte[] message);
    }

}