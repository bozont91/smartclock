package com.bozont.aura3.ui.main;

import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bozont.aura3.MainActivity;
import com.bozont.aura3.R;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.bozont.aura3.MainActivity.mTcpClient;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentMain extends Fragment implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;
    private static View root;

    private static TextView tw_clock;
    private static TextView tw_date;
    private static TextView tw_temperature;
    private static EditText et_showtext;
    private static SeekBar sb_brightness;

    static boolean temperature_sensor_present = false;

    CountDownTimer countDownTimerClockRefresh;
    CountDownTimer countDownTimerTempRefresh;

    public static FragmentMain newInstance(int index) {
        FragmentMain fragment = new FragmentMain();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_main, container, false);

        Button btn_displayonoff = (Button) root.findViewById(R.id.btn_screenonoff);
        btn_displayonoff.setOnClickListener(this);

        Button btn_showweather = (Button) root.findViewById(R.id.btn_showweather);
        btn_showweather.setOnClickListener(this);

        Button btn_showdate = (Button) root.findViewById(R.id.btn_showdate);
        btn_showdate.setOnClickListener(this);

        Button btn_showtext = (Button) root.findViewById(R.id.btn_showtext);
        btn_showtext.setOnClickListener(this);

        Button btn_starttimer = (Button) root.findViewById(R.id.btn_countdowntimer);
        btn_starttimer.setOnClickListener(this);

        tw_clock = (TextView) root.findViewById(R.id.tw_clock);
        tw_date = (TextView) root.findViewById(R.id.tw_date);
        sb_brightness = (SeekBar) root.findViewById(R.id.seekbar_brightness);
        sb_brightness.setOnSeekBarChangeListener(this);
        et_showtext = (EditText) root.findViewById(R.id.et_showtext);
        tw_temperature = root.findViewById(R.id.tw_temperature);

        final TextView textView = root.findViewById(R.id.section_label);
        textView.setText("Connected to "+mTcpClient.SERVER_IP);

        return root;
    }

    public static void displayIpAddress() {
        /*final TextView textView = root.findViewById(R.id.section_label);
        textView.post(new Runnable() {
            public void run() {
                textView.setText("Connected to "+mTcpClient.SERVER_IP);
            }
        });*/
    }

    public static void doInitialSync() {
        MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICECLOCK, MainActivity.message_type.REQUEST, null);
        if (mTcpClient != null) {
            //mTcpClient.sendMessage("getscreenbrightness");
            //mTcpClient.sendMessage("getdeviceclock");
            //mTcpClient.sendMessage("getsensorpresenttemp");
            //mTcpClient.sendMessage("getdevicetemp");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateClockTimer();
        updateTemperatureTimer();
        Log.d("aura", "Main fragment started");
    }


    @Override
    public void onPause() {
        super.onPause();
        countDownTimerClockRefresh.cancel();
        countDownTimerTempRefresh.cancel();
        Log.d("aura", "Main fragment paused");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_screenonoff:
                MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_ON_OFF, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.btn_showweather:
                MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_WEATHER, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.btn_showdate:
                MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_DATE, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.btn_showtext:
                String text_to_show = et_showtext.getText().toString() + '\0';
                byte[] payload = text_to_show.getBytes();
                MainActivity.sendAuraPacket(MainActivity.message_ids.SHOW_TEXT, MainActivity.message_type.REQUEST, payload);
                MainActivity.vibrate();
                break;
            case R.id.btn_countdowntimer:

                final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
                dialog.setContentView(R.layout.fragment_set_timer);
                dialog.setTitle("Select time...");

                final TimePicker tp_countdowntimer = dialog.findViewById(R.id.tp_timer);
                tp_countdowntimer.setIs24HourView(true);
                tp_countdowntimer.setHour(0);
                tp_countdowntimer.setMinute(0);


                Button dialogButton = (Button) dialog.findViewById(R.id.btn_starttimer);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int countdown_seconds = tp_countdowntimer.getHour()*60+tp_countdowntimer.getMinute();
                        byte[] payload = ByteBuffer.allocate(4).putInt(Integer.reverseBytes(countdown_seconds)).array();
                        MainActivity.sendAuraPacket(MainActivity.message_ids.COUNTDOWNTIMER_START, MainActivity.message_type.REQUEST, payload);
                        MainActivity.vibrate();
                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;
        }
    }

    public void updateClockTimer() {
        countDownTimerClockRefresh = new CountDownTimer(Long.MAX_VALUE, 2000) {

            public void onTick(long millisUntilFinished) {
                MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICECLOCK, MainActivity.message_type.REQUEST, null);
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    public void updateTemperatureTimer() {
        countDownTimerTempRefresh = new CountDownTimer(Long.MAX_VALUE, 3000) {

            public void onTick(long millisUntilFinished) {
                if (mTcpClient != null) {
                    if (!temperature_sensor_present) {
                        MainActivity.sendAuraPacket(MainActivity.message_ids.TEMPSENSOR_PRESENT, MainActivity.message_type.REQUEST, null);
                    } else {
                        MainActivity.sendAuraPacket(MainActivity.message_ids.DEVICETEMP, MainActivity.message_type.REQUEST, null);
                    }
                }
            }

            public void onFinish() {
                start();
            }
        }.start();
    }


    public static void messageReceived(MainActivity.message_ids message_id, byte[] payload) {

        switch(message_id) {
            case DEVICECLOCK:

                long unixSeconds = 0;

                StringBuilder res = new StringBuilder();
                for(int i = 0 ; i < payload.length ; i++) {
                    res.append(String.format("0x%02x ", payload[i]));
                }
                Log.d("aura_com", "Payload time: " + res.toString());
                for (int i = 0; i < payload.length; i++)
                {
                    unixSeconds += ((long) payload[i] & 0xffL) << (8 * i);
                }

                Date date = new java.util.Date(unixSeconds * 1000L);
                // the format of your date
                SimpleDateFormat str_time = new java.text.SimpleDateFormat("HH:mm");
                SimpleDateFormat str_date = new java.text.SimpleDateFormat("yyyy. MM. dd.");
                // give a timezone reference for formatting (see comment at the bottom)
                str_time.setTimeZone(java.util.TimeZone.getTimeZone("GMT+0"));
                String formattedTime = str_time.format(date);
                str_date.setTimeZone(java.util.TimeZone.getTimeZone("GMT+0"));
                String formattedDate = str_date.format(date);

                tw_clock.setText(formattedTime);
                tw_date.setText(formattedDate);
                break;

            case TEMPSENSOR_PRESENT:
                int sensor_present_rcv = payload[0] & 0xff;
                if(sensor_present_rcv == 1) {
                    temperature_sensor_present = true;
                } else {
                    temperature_sensor_present = false;
                }
                if(!temperature_sensor_present) {
                    tw_temperature.setText("");
                }
                break;

            case DEVICETEMP:
                if(!temperature_sensor_present) {
                    return;
                }
                float temperature = ByteBuffer.wrap(payload).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                tw_temperature.setText((int)Math.floor(temperature)+"°");
                break;

            case DISPLAY_BRIGHTNESS:
                int brightness_rcv = payload[0] & 0xff;
                sb_brightness.setProgress(brightness_rcv);
                break;
        }
        /*
        switch(messagetype) {
            case MESSAGE_DEVICECLOCK:
                long unixSeconds = Integer.decode(message);
                Date date = new java.util.Date(unixSeconds*1000L);
                // the format of your date
                SimpleDateFormat str_time = new java.text.SimpleDateFormat("HH:mm");
                SimpleDateFormat str_date = new java.text.SimpleDateFormat("yyyy. MM. dd.");
                // give a timezone reference for formatting (see comment at the bottom)
                str_time.setTimeZone(java.util.TimeZone.getTimeZone("GMT+0"));
                String formattedTime = str_time.format(date);
                str_date.setTimeZone(java.util.TimeZone.getTimeZone("GMT+0"));
                String formattedDate = str_date.format(date);

                tw_clock.setText(formattedTime);
                tw_date.setText(formattedDate);
                break;

            case MESSAGE_SCREENBRIGHTNESS:
                sb_brightness.setProgress(Integer.decode(message));
                break;

            case MESSAGE_DEVICETEMP:
                if(!temperature_sensor_present) {
                    return;
                }
                float temperature = Float.parseFloat(message);
                tw_temperature.setText((int)Math.floor(temperature)+"°");
                break;

            case MESSAGE_TEMPSENSORPRESENT:
                if(message.equals("1")) {
                    temperature_sensor_present = true;
                } else {
                    temperature_sensor_present = false;
                }
                if(!temperature_sensor_present) {
                    tw_temperature.setText("");
                }
                break;
        }

         */


    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        Log.d("aura_dbg", "brightness progressbar: " + sb_brightness.getProgress());
        byte[] payload = new byte[1];
        payload[0] = (byte)sb_brightness.getProgress();
        MainActivity.sendAuraPacket(MainActivity.message_ids.DISPLAY_BRIGHTNESS, MainActivity.message_type.CHANGE_VALUE, payload);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}