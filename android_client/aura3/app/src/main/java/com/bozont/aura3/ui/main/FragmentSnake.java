package com.bozont.aura3.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bozont.aura3.MainActivity;
import com.bozont.aura3.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentSnake extends Fragment implements View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    public static FragmentSnake newInstance(int index) {
        FragmentSnake fragment = new FragmentSnake();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_snake, container, false);
        /*final TextView textView = root.findViewById(R.id.section_label);
        pageViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        Button btn_snakeup = (Button) root.findViewById(R.id.snake_up);
        btn_snakeup.setOnClickListener(this);
        Button btn_snakedown = (Button) root.findViewById(R.id.snake_down);
        btn_snakedown.setOnClickListener(this);
        Button btn_snakeleft = (Button) root.findViewById(R.id.snake_left);
        btn_snakeleft.setOnClickListener(this);
        Button btn_snakeright = (Button) root.findViewById(R.id.snake_right);
        btn_snakeright.setOnClickListener(this);
        Button btn_snakestart = (Button) root.findViewById(R.id.snake_start);
        btn_snakestart.setOnClickListener(this);
        Button btn_tetrisstart = (Button) root.findViewById(R.id.tetris_start);
        btn_tetrisstart.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.snake_up:
                MainActivity.sendAuraPacket(MainActivity.message_ids.GAME_UP, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.snake_down:
                MainActivity.sendAuraPacket(MainActivity.message_ids.GAME_DOWN, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.snake_left:
                MainActivity.sendAuraPacket(MainActivity.message_ids.GAME_LEFT, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.snake_right:
                MainActivity.sendAuraPacket(MainActivity.message_ids.GAME_RIGHT, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.snake_start:
                MainActivity.sendAuraPacket(MainActivity.message_ids.START_SNAKE, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
            case R.id.tetris_start:
                MainActivity.sendAuraPacket(MainActivity.message_ids.START_TETRIS, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                break;
        }
    }
}