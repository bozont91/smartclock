package com.bozont.aura3;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.bozont.aura3.ui.main.FragmentAlarm;
import com.bozont.aura3.ui.main.FragmentDebug;
import com.bozont.aura3.ui.main.FragmentMain;
import com.bozont.aura3.ui.main.FragmentMainNew;
import com.bozont.aura3.ui.main.FragmentSettings;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.os.CountDownTimer;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.bozont.aura3.ui.main.SectionsPagerAdapter;

import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.zip.CRC32;

public class MainActivity extends AppCompatActivity {

    public static TcpClient mTcpClient;
    public static Vibrator v;
    public static String TcpResponse;
    static ConnectTask ct;
    static Toast toast_connected;
    static Toast toast_disconnected;
    ServiceDiscovery sd;
    public static String SERVER_IP = "";
    public static int SERVER_PORT = 0;
    private Dialog connectingDialog;
    CountDownTimer connectionKeepaliveTimer;

    private static byte[] sharedSecret = null;

    public static void vibrate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(50);
        }
    }

    class ServiceDiscoveryThread implements Runnable {
        @Override
        public void run() {

            if(mTcpClient != null && mTcpClient.isConnected == true) return;

            try {
                sd = new ServiceDiscovery(getApplicationContext());
                sd.discoverServices();
                while(sd.serviceFound == false) {
                    Thread.sleep(2000);
                }
                SERVER_IP = String.valueOf(sd.discoveredService.getHost()).substring(1);
                SERVER_PORT = sd.discoveredService.getPort();
                ct = new ConnectTask();
                ct.execute("");
                destroyConnectingDialog();
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayMainUI();
                    }
                });*/
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class GetInitialDataThread implements Runnable {
        @Override
        public void run() {
            try {
                while(mTcpClient == null) Thread.sleep(200);
                FragmentMainNew.doInitialSync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayMainUI();

        toast_connected=Toast.makeText(getApplicationContext(),"Connected to device",Toast.LENGTH_SHORT);
        toast_disconnected=Toast.makeText(getApplicationContext(),"Disconnected from device",Toast.LENGTH_LONG);

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        connectingDialog = new Dialog(this);

        if(mTcpClient == null || mTcpClient.isConnected == false) createConnectingDialog();

        new Thread(new ServiceDiscoveryThread()).start();
        connectionKeepaliveTimer();

    }

    public void connectionKeepaliveTimer() {
        connectionKeepaliveTimer = new CountDownTimer(Long.MAX_VALUE, 50000) {

            public void onTick(long millisUntilFinished) {
                sendAuraPacket(message_ids.KEEPALIVE, MainActivity.message_type.REQUEST, null);
            }

            public void onFinish() {
                start();
            }
        }.start();
    }

    private void displayMainUI() {
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    private void createConnectingDialog() {
        connectingDialog.setContentView(R.layout.fragment_connecting);
        connectingDialog.setTitle("Connecting...");
        connectingDialog.setCanceledOnTouchOutside(false);
        connectingDialog.show();
    }

    private void destroyConnectingDialog() {
        connectingDialog.cancel();
    }

    public static void onServerDisconnected() {
        sharedSecret = null;
        toast_disconnected.show();
        ct.cancel(true);
        ct = new ConnectTask();
        ct.execute("");
    }

    public enum message_type {
        REQUEST(0xe0),
        RESPONSE(0xe1),
        CHANGE_VALUE(0xe2);

        private final int value;

        message_type(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }
    }

    public enum message_ids {
        KEEPALIVE(0),

        START_SNAKE(1),
        START_TETRIS(2),
        GAME_UP(3),
        GAME_DOWN(4),
        GAME_LEFT(5),
        GAME_RIGHT(6),

        DEVICECLOCK(7),
        DEVICETEMP(8),
        TEMPSENSOR_PRESENT(9),
        TEMPSENSOR_CALIBRATION_VALUE(10),
        TEMPERATURE_UNIT(11),

        DISPLAY_ON_OFF(12),
        DISPLAY_BRIGHTNESS(13),

        DISPLAY_AUTO_ONOFF_ENABLED(14),
        DISPLAY_AUTO_ONOFF_OFF_TIME(15),
        DISPLAY_AUTO_ONOFF_ON_TIME(16),
        DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS(17),

        SHOW_WEATHER(18),
        SHOW_DATE(19),
        SHOW_TEXT(20),

        ALARM_TIME(21),
        ALARM_DISABLE(22),
        ALARM_ONLY_ON_WEEKDAYS(23),

        TIMEZONE(24),
        CLOCK_STYLE(25),
        COUNTDOWNTIMER_START(26),
        HOURLY_BEEP_ENABLED(27),
        DISPLAY_INVERT(28),
        FACTORY_RESET(29),
        MUTE_DEVICE(30),
        DEBUGINFO(31),

        MQTT_CLIENT_ENABLED(32),
        MQTT_BROKER_ADDRESS(33),
        MQTT_BROKER_PORT(34),

        ADAFRUITIO_ENABLED(35),
        ADAFRUITIO_USERNAME(36),
        ADAFRUITIO_API_KEY(37),

        WEBWEATHER_ENABLED(38),
        WEBWEATHER_LOCATION(39),
        WEBWEATHER_API_KEY(40),

        FORCE_NTP_SYNC(41),
        SHOW_DETAILED_WEATHER(42),
        SHOW_DETAILED_DATE(43),
        ALARM_SNOOZE_TIME(44),
        ALARM_SOUND(45),
        DST_ENABLED(46),

        MQTT_BROKER_USERNAME(47),
        MQTT_BROKER_PASSWORD(48),

        OTA_ENABLED(49),
        OTA_PASSWORD(50),

        REBOOT(51),

        MQTT_CLIENT_STATUS(52),
        ADAFRUITIO_CLIENT_STATUS(53),
        WEATHER_CLIENT_STATUS(54),

        ENCRYPTION_KEY_EXCHANGE(55),

        MAGIC_NUMBERS_ENABLED(56),
        MUTE_WHEN_SCREEN_IS_OFF(57),
        CLOCK_TYPE(58),
        BUTTON_SHORTPRESS_ACTION(59),
        BUTTON_LONGPRESS_ACTION(60),

        MUTE_SCHEDULE_ENABLED(61),
        MUTE_ON_TIME(62),
        MUTE_OFF_TIME(63);

        private final int value;

        message_ids(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }
    }

    private static byte[] longToByteArray(final long i) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        dos.writeLong(i);
        dos.flush();
        return bos.toByteArray();
    }

    private static void log_byte_array(String message, byte[] data) {
        StringBuilder data_str = new StringBuilder();
        for(int i = 0 ; i < data.length ; i++) {
            data_str.append(String.format("0x%02x ", data[i]));
        }
        Log.d("aura_com", message + data_str.toString());
    }

    public static void sendAuraPacket(message_ids msg_id, message_type msg_type, byte[] payload) {

        int payload_size = 0;
        if(payload != null) {
            payload_size = payload.length;
        }

        byte[] msg_out = new byte[12 + payload_size];

        // Add the header
        // ToDo: use the correct byte order
        msg_out[1] = (byte)0x42;
        msg_out[0] = (byte)0x91;

        // Add the message size
        msg_out[2] = (byte)(12 + payload_size);

        // Add the command ID
        msg_out[3] = (byte)msg_id.getValue();

        // Add the command type
        msg_out[4] = (byte)msg_type.getValue();

        // Add the payload
        msg_out[5] = (byte)payload_size;
        for(int i = 0 ; i < payload_size ; i++) {
            msg_out[6 + i] = payload[i];
        }

        int payload_end_index = 5 + payload_size;

        // Add the CRC
        byte[] crc_data = Arrays.copyOfRange(msg_out, 0, payload_end_index + 1);
        CRC32 crc32 = new CRC32();
        crc32.update(crc_data);
        long calc_crc32 = crc32.getValue();
        try {
            byte[] crc_bytes = longToByteArray(calc_crc32);
            // Endianness is swapped!
            msg_out[payload_end_index + 1] = crc_bytes[7];
            msg_out[payload_end_index + 2] = crc_bytes[6];
            msg_out[payload_end_index + 3] = crc_bytes[5];
            msg_out[payload_end_index + 4] = crc_bytes[4];
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Add a \n\n
        msg_out[payload_end_index + 5] = '\n';
        msg_out[payload_end_index + 6] = '\n';

        StringBuilder res = new StringBuilder();
        for(int i = 0 ; i < msg_out.length ; i++) {
            res.append(String.format("0x%02x ", msg_out[i]));
        }
        Log.d("aura_com", "TX message raw: " + res.toString());

        // Encrypt the message if the shared key is available
        if(sharedSecret != null && msg_id != message_ids.ENCRYPTION_KEY_EXCHANGE) {
            int key_step = 0;
            for (int i = 0; i < payload_end_index + 5; i++) {
                msg_out[i] = (byte) (msg_out[i] ^ sharedSecret[key_step]);
                key_step++;
                if (key_step == sharedSecret.length) key_step = 0;
            }

            log_byte_array("Encrypted message: ", msg_out);
        }

        if (mTcpClient != null) {
            mTcpClient.sendMessage(msg_out);
        }
    }

    public static class ConnectTask extends AsyncTask<String, byte[], TcpClient> {

        @Override
        protected TcpClient doInBackground(String... message) {

            //we create a TCPClient object
            mTcpClient = new TcpClient(new TcpClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(byte[] message) {
                    publishProgress(message);
                }
            }, SERVER_IP, SERVER_PORT);
            mTcpClient.run();

            return null;
        }

        @Override
        protected void onProgressUpdate(byte[]... values) {
            super.onProgressUpdate(values);

            Log.d("aura_com", "Received data: " + values[0]);
            byte[] raw_response = values[0];

            if(raw_response.length < 1) return;

            StringBuilder res = new StringBuilder();
            for(int i = 0 ; i < raw_response.length ; i++) {
                res.append(String.format("0x%02x ", raw_response[i]));
            }
            Log.d("aura_com", "Received data as bytes: " + res.toString());

            // Decrypt message if the shared key is available
            if(sharedSecret != null) {
                int key_step = 0;
                for (int i = 0; i < raw_response.length; i++) {
                    raw_response[i] = (byte) (raw_response[i] ^ sharedSecret[key_step]);
                    key_step++;
                    if (key_step == sharedSecret.length) key_step = 0;
                }

                log_byte_array("Decrypted data as bytes: ", raw_response);
            }

            // Header check
            if(raw_response[0] == (byte)0x91 && raw_response[1] == (byte)0x42) {
                Log.d("aura_com", "Header check succeeded!");
            } else {
                Log.d("aura_com", "Header check FAILED!");
                return;
            }

            // Get the size
            int msg_size = raw_response[2];
            if(msg_size > raw_response.length + 2) {
                Log.d("aura_com", "Size check FAILED!");
                return;
            } else {
                Log.d("aura_com", "Size check ok! Size: " + msg_size + " bytes");
            }

            // CRC check
            int crc_start_offset = msg_size - 6;
            byte[] msg_crc = Arrays.copyOfRange(raw_response, crc_start_offset, crc_start_offset+4);

            long msg_crc32 = 0;
            for (int i = 0; i < msg_crc.length; i++)
            {
                msg_crc32 += ((long) msg_crc[i] & 0xffL) << (8 * i);
            }
            Log.d("aura_com", "Received CRC32: " + msg_crc32);

            byte[] msg_data = Arrays.copyOfRange(raw_response, 0, crc_start_offset);
            CRC32 crc32 = new CRC32();
            crc32.update(msg_data);
            long calc_crc32 = crc32.getValue();

            if(calc_crc32 == msg_crc32) {
                Log.d("aura_com", "CRC check ok!");
            } else {
                Log.d("aura_com", "CRC check FAILED! recv: " + msg_crc32 + "  calc: " + calc_crc32);
                return;
            }

            // Get the message ID and the request type
            int message_id = raw_response[3] & 0xff;
            int message_type = raw_response[4] & 0xff;

            StringBuilder msgidandtype = new StringBuilder();
            msgidandtype.append(String.format("Msg ID: 0x%02x  Msg type: 0x%02x", message_id, message_type));
            Log.d("aura_com", msgidandtype.toString());

            // Get the payload
            byte payload_length = raw_response[5];
            byte[] payload = Arrays.copyOfRange(raw_response, 6, 6 + payload_length);
            Log.d("aura_com", "Payload size: "+(int)payload_length);
            Log.d("aura_com", "Payload: " + payload);

            if(message_ids.values().length <= message_id) {
                Log.e("aura_msg", "Unknown message ID received");
                return;
            }

            switch (message_ids.values()[message_id]) {
                case DEVICECLOCK:
                    Log.d("aura_msg", "DEVICECLOCK response");
                    FragmentMainNew.messageReceived(message_ids.DEVICECLOCK, payload);
                    break;

                case DEVICETEMP:
                    Log.d("aura_msg", "DEVICETEMP response");
                    FragmentMainNew.messageReceived(message_ids.DEVICETEMP, payload);
                    break;

                case TEMPSENSOR_PRESENT:
                    Log.d("aura_msg", "TEMPSENSOR_PRESENT response");
                    FragmentMainNew.messageReceived(message_ids.TEMPSENSOR_PRESENT, payload);
                    break;

                case DISPLAY_BRIGHTNESS:
                    Log.d("aura_msg", "DISPLAY_BRIGHTNESS response");
                    FragmentMainNew.messageReceived(message_ids.DISPLAY_BRIGHTNESS, payload);
                    break;

                case DEBUGINFO:
                    Log.d("aura_msg", "DEBUGINFO response");
                    FragmentDebug.messageReceived(message_ids.DEBUGINFO, payload);
                    break;

                case ALARM_TIME:
                    Log.d("aura_msg", "ALARM_TIME response");
                    FragmentAlarm.messageReceived(message_ids.ALARM_TIME, payload);
                    break;

                case ALARM_ONLY_ON_WEEKDAYS:
                    Log.d("aura_msg", "ALARM_ONLY_ON_WEEKDAYS response");
                    FragmentAlarm.messageReceived(message_ids.ALARM_ONLY_ON_WEEKDAYS, payload);
                    break;

                case CLOCK_STYLE:
                    Log.d("aura_msg", "CLOCK_TYPE response");
                    FragmentSettings.messageReceived(message_ids.CLOCK_STYLE, payload);
                    break;

                case DISPLAY_AUTO_ONOFF_ENABLED:
                    Log.d("aura_msg", "DISPLAY_AUTO_ONOFF_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.DISPLAY_AUTO_ONOFF_ENABLED, payload);
                    break;

                case DISPLAY_AUTO_ONOFF_OFF_TIME:
                    Log.d("aura_msg", "DISPLAY_AUTO_ONOFF_OFF_TIME response");
                    FragmentSettings.messageReceived(message_ids.DISPLAY_AUTO_ONOFF_OFF_TIME, payload);
                    break;

                case DISPLAY_AUTO_ONOFF_ON_TIME:
                    Log.d("aura_msg", "DISPLAY_AUTO_ONOFF_ON_TIME response");
                    FragmentSettings.messageReceived(message_ids.DISPLAY_AUTO_ONOFF_ON_TIME, payload);
                    break;

                case DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS:
                    Log.d("aura_msg", "DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS response");
                    FragmentSettings.messageReceived(message_ids.DISPLAY_AUTO_ONOFF_ONLY_ON_WEEKDAYS, payload);
                    break;

                case MUTE_DEVICE:
                    Log.d("aura_msg", "MUTE_DEVICE response");
                    FragmentSettings.messageReceived(message_ids.MUTE_DEVICE, payload);
                    break;

                case HOURLY_BEEP_ENABLED:
                    Log.d("aura_msg", "HOURLY_BEEP_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.HOURLY_BEEP_ENABLED, payload);
                    break;

                case TIMEZONE:
                    Log.d("aura_msg", "TIMEZONE response");
                    FragmentSettings.messageReceived(message_ids.TIMEZONE, payload);
                    break;

                case MQTT_CLIENT_ENABLED:
                    Log.d("aura_msg", "MQTT_CLIENT_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.MQTT_CLIENT_ENABLED, payload);
                    break;

                case MQTT_BROKER_ADDRESS:
                    Log.d("aura_msg", "MQTT_BROKER_ADDRESS response");
                    FragmentSettings.messageReceived(message_ids.MQTT_BROKER_ADDRESS, payload);
                    break;

                case MQTT_BROKER_PORT:
                    Log.d("aura_msg", "MQTT_BROKER_PORT response");
                    FragmentSettings.messageReceived(message_ids.MQTT_BROKER_PORT, payload);
                    break;

                case ADAFRUITIO_ENABLED:
                    Log.d("aura_msg", "ADAFRUITIO_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.ADAFRUITIO_ENABLED, payload);
                    break;

                case TEMPSENSOR_CALIBRATION_VALUE:
                    Log.d("aura_msg", "TEMPSENSOR_CALIBRATION_VALUE response");
                    FragmentSettings.messageReceived(message_ids.TEMPSENSOR_CALIBRATION_VALUE, payload);
                    break;

                case TEMPERATURE_UNIT:
                    Log.d("aura_msg", "TEMPERATURE_UNIT response");
                    FragmentSettings.messageReceived(message_ids.TEMPERATURE_UNIT, payload);
                    break;

                case WEBWEATHER_ENABLED:
                    Log.d("aura_msg", "WEBWEATHER_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.WEBWEATHER_ENABLED, payload);
                    break;

                case WEBWEATHER_LOCATION:
                    Log.d("aura_msg", "WEBWEATHER_LOCATION response");
                    FragmentSettings.messageReceived(message_ids.WEBWEATHER_LOCATION, payload);
                    break;

                case ALARM_SNOOZE_TIME:
                    Log.d("aura_msg", "ALARM_SNOOZE_TIME response");
                    FragmentSettings.messageReceived(message_ids.ALARM_SNOOZE_TIME, payload);
                    break;

                case ALARM_SOUND:
                    Log.d("aura_msg", "ALARM_SNOOZE_TIME response");
                    FragmentSettings.messageReceived(message_ids.ALARM_SOUND, payload);
                    break;

                case DST_ENABLED:
                    Log.d("aura_msg", "DST_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.DST_ENABLED, payload);
                    break;

                case OTA_ENABLED:
                    Log.d("aura_msg", "OTA_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.OTA_ENABLED, payload);
                    break;

                case MQTT_CLIENT_STATUS:
                    Log.d("aura_msg", "MQTT_CLIENT_STATUS response");
                    FragmentSettings.messageReceived(message_ids.MQTT_CLIENT_STATUS, payload);
                    break;

                case ADAFRUITIO_CLIENT_STATUS:
                    Log.d("aura_msg", "ADAFRUITIO_CLIENT_STATUS response");
                    FragmentSettings.messageReceived(message_ids.ADAFRUITIO_CLIENT_STATUS, payload);
                    break;

                case WEATHER_CLIENT_STATUS:
                    Log.d("aura_msg", "WEATHER_CLIENT_STATUS response");
                    FragmentSettings.messageReceived(message_ids.WEATHER_CLIENT_STATUS, payload);
                    break;

                case ENCRYPTION_KEY_EXCHANGE:
                    Log.d("aura_msg", "ENCRYPTION_KEY_EXCHANGE response");

                    Curve25519 cipher = Curve25519.getInstance(Curve25519.BEST);
                    Curve25519KeyPair keyPair = cipher.generateKeyPair();
                    sendAuraPacket(message_ids.ENCRYPTION_KEY_EXCHANGE, MainActivity.message_type.RESPONSE, keyPair.getPublicKey());
                    byte[] sharedSecret_tmp = cipher.calculateAgreement(payload, keyPair.getPrivateKey());
                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        sharedSecret = digest.digest(sharedSecret_tmp);
                        log_byte_array("SHA256 key: ", sharedSecret);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }

                    new Thread(new GetInitialDataThread()).start();
                    break;

                case MAGIC_NUMBERS_ENABLED:
                    Log.d("aura_msg", "MAGIC_NUMBERS_ENABLED response");
                    FragmentSettings.messageReceived(message_ids.MAGIC_NUMBERS_ENABLED, payload);
                    break;

                case MUTE_WHEN_SCREEN_IS_OFF:
                    Log.d("aura_msg", "MUTE_WHEN_SCREEN_IS_OFF response");
                    FragmentSettings.messageReceived(message_ids.MUTE_WHEN_SCREEN_IS_OFF, payload);
                    break;

                case CLOCK_TYPE:
                    Log.d("aura_msg", "CLOCK_TYPE response");
                    FragmentSettings.messageReceived(message_ids.CLOCK_TYPE, payload);
                    break;

                case BUTTON_SHORTPRESS_ACTION:
                    FragmentSettings.messageReceived(message_ids.BUTTON_SHORTPRESS_ACTION, payload);
                    break;

                case BUTTON_LONGPRESS_ACTION:
                    FragmentSettings.messageReceived(message_ids.BUTTON_LONGPRESS_ACTION, payload);
                    break;

                case MUTE_SCHEDULE_ENABLED:
                    FragmentSettings.messageReceived(message_ids.MUTE_SCHEDULE_ENABLED, payload);
                    break;

                case MUTE_ON_TIME:
                    FragmentSettings.messageReceived(message_ids.MUTE_ON_TIME, payload);
                    break;

                case MUTE_OFF_TIME:
                    FragmentSettings.messageReceived(message_ids.MUTE_OFF_TIME, payload);
                    break;

                default:
                    break;

            }

            Log.d("aura_msg", "---------");

        }
    }
}
