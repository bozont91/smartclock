package com.bozont.aura3.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bozont.aura3.MainActivity;
import com.bozont.aura3.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentDebug extends Fragment implements View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private PageViewModel pageViewModel;
    private static ListView lv_debuginfo;
    static List<String> debuginfoList;
    static ArrayAdapter<String> debugInfoArrayAdapter;


    public static FragmentDebug newInstance(int index) {
        FragmentDebug fragment = new FragmentDebug();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);

        debuginfoList = new ArrayList<String>();
        debugInfoArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, debuginfoList);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_debug, container, false);

        Button btn_refresh = (Button) root.findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(this);

        lv_debuginfo = (ListView) root.findViewById(R.id.lv_debuginfo);
        lv_debuginfo.setAdapter(debugInfoArrayAdapter);

        if (MainActivity.mTcpClient != null) {
            MainActivity.sendAuraPacket(MainActivity.message_ids.DEBUGINFO, MainActivity.message_type.REQUEST, null);
            debugInfoArrayAdapter.clear();
        }

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_refresh:
                MainActivity.sendAuraPacket(MainActivity.message_ids.DEBUGINFO, MainActivity.message_type.REQUEST, null);
                MainActivity.vibrate();
                debugInfoArrayAdapter.clear();
                break;
        }
    }

    public static void messageReceived(MainActivity.message_ids message_id, byte[] payload) {
        try {
            String debug_message = new String(payload, "UTF-8");
            debugInfoArrayAdapter.add(debug_message);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}